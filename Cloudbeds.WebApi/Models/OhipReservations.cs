﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class OhipReservationIdList
    {
        public string id { get; set; }
        public string type { get; set; }

    }
    public class OhipRoomStay
    {
        public string roomId { get; set; }

    }
    public class OhipReservationInfo
    {
        public List<OhipReservationIdList> reservationIdList { get; set; }
        public OhipRoomStay roomStay { get; set; }
    }
    public class OhipReservations
    {
        public List<OhipReservationInfo> reservationInfo { get; set; }
    }
}
