﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class TaxationResponse
    {
        [JsonProperty("TaxEnvironments")]
        public TaxEnvironment[] TaxEnvironments { get; set; }

        [JsonProperty("Taxations")]
        public Taxation[] Taxations { get; set; }

        [JsonProperty("TaxRates")]
        public TaxRate[] TaxRates { get; set; }
    }

    public partial class TaxEnvironment
    {
        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("ValidityStartUtc")]
        public DateTimeOffset? ValidityStartUtc { get; set; }

        [JsonProperty("ValidityEndUtc")]
        public DateTimeOffset? ValidityEndUtc { get; set; }

        [JsonProperty("TaxationCodes")]
        public string[] TaxationCodes { get; set; }

    }
    public partial class TaxRate
    {
        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("TaxationCode")]
        public string TaxationCode { get; set; }

        [JsonProperty("Value")]
        public double Value { get; set; }

        [JsonProperty("ValidityInvervalsUtc")]
        public ValidityInvervalsUtc[] ValidityInvervalsUtc { get; set; }

        [JsonProperty("Strategy")]
        public Strategy Strategy { get; set; }
    }

    public partial class Strategy
    {
        [JsonProperty("Discriminator")]
        public Discriminator Discriminator { get; set; }

        [JsonProperty("Value")]
        public Value Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("Value")]
        public double ValueValue { get; set; }

        [JsonProperty("CurrencyCode", NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyCode? CurrencyCode { get; set; }

        [JsonProperty("BaseTaxationCodes", NullValueHandling = NullValueHandling.Ignore)]
        public string[] BaseTaxationCodes { get; set; }
    }

    public partial class ValidityInvervalsUtc
    {
        [JsonProperty("StartUtc")]
        public DateTimeOffset? StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTimeOffset? EndUtc { get; set; }
    }

    public partial class Taxation
    {
        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("LocalName")]
        public string LocalName { get; set; }
    }

    public enum Discriminator { Dependent, Flat, Relative };

    public enum CurrencyCode { Cad, Usd };
}
