﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class Room
    {
        [JsonProperty("roomTypeName")]
        public string Name { get; set; }

        [JsonProperty("roomTypeDescription")]
        public string Description { get; set; }

        [JsonProperty("roomTypeNameShort")]
        public string NameShort { get; set; }

        [JsonProperty("roomTypeFeatures")]
        public string[] Features { get; set; }

        [JsonProperty("roomTypeID")]
        public int TypeID { get; set; }

        [JsonProperty("roomsAvailable")]
        public int Available { get; set; }

        [JsonProperty("roomRate")]
        public decimal Rate { get; set; }
    }
}
