﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class ReservationData
    {
        [JsonProperty("assigned")]
        public ReservationRoom[] Rooms { get; set; }

        [JsonProperty("unassigned")]
        public ReservationRoom[] UnAssignedRooms { get; set; }
        public string Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
