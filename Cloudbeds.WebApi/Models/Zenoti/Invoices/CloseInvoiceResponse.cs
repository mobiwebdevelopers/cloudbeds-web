﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class CloseInvoiceResponse
    {

        [JsonProperty("is_invoice_closed")]
        public bool IsInvoiceClosed { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }

}
