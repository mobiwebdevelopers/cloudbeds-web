﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ActivityListData
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("Type")]
        public int Type { get; set; }
    }

    public partial class ClassSlot
    {
        [JsonProperty("SessionId")]
        public long SessionId { get; set; }

        [JsonProperty("ClassId")]
        public long ClassId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("TechnicianName")]
        public string TechnicianName { get; set; }

        [JsonProperty("TechnicianId")]
        public Guid TechnicianId { get; set; }

        [JsonProperty("Schedule")]
        public string Schedule { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Day")]
        public int Day { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("DefaultPrice")]
        public decimal DefaultPrice { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("Added")]
        public bool Added { get; set; }

        [JsonProperty("FirstAdditional")]
        public bool FirstAdditional { get; set; }

        [JsonProperty("FullDate")]
        public DateTime FullDate { get; set; }

        [JsonProperty("IsService")]
        public bool IsService { get; set; }

        [JsonProperty("DayPeriod")]
        public int DayPeriod { get; set; }
    }
}
