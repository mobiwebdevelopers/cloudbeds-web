﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ClassRegistrationCancelResponse
    {
        [JsonProperty("error")]
        public Error Error { get; set; }
    }

    public partial class Error
    {
        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}