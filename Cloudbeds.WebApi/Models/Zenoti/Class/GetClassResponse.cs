﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class GetClassResponse
    {
        [JsonProperty("sessions")]
        public List<Session> Sessions { get; set; }

        [JsonProperty("classes")]
        public List<Class> Classes { get; set; }

        [JsonProperty("instructors")]
        public List<WelcomeInstructor> Instructors { get; set; }

        [JsonProperty("categories")]
        public List<Category> Categories { get; set; }

        [JsonProperty("rooms")]
        public List<Room> Rooms { get; set; }

        [JsonProperty("levels")]
        public List<object> Levels { get; set; }

        [JsonProperty("tags")]
        public List<object> Tags { get; set; }

        [JsonProperty("opening_time")]
        public string OpeningTime { get; set; }

        [JsonProperty("page_details")]
        public PageDetails PageDetails { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }

    public partial class Category
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("parent_id")]
        public long? ParentId { get; set; }

        [JsonProperty("count")]
        public object Count { get; set; }
    }
    public partial class ZenotiCategory
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("parent_id")]
        public Guid ParentId { get; set; }

        [JsonProperty("display_order")]
        public int? display_order { get; set; }
    }

    public partial class Class
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("category_id")]
        public long CategoryId { get; set; }

        [JsonProperty("start_date")]
        public object StartDate { get; set; }

        [JsonProperty("end_date")]
        public object EndDate { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("html_description")]
        public object HtmlDescription { get; set; }

        [JsonProperty("catalog_price")]
        public object CatalogPrice { get; set; }

        [JsonProperty("capacity")]
        public object Capacity { get; set; }

        [JsonProperty("allow_web_booking")]
        public bool AllowWebBooking { get; set; }

        [JsonProperty("allow_direct_booking")]
        public bool AllowDirectBooking { get; set; }

        [JsonProperty("allow_cancellation_fee")]
        public bool AllowCancellationFee { get; set; }

        [JsonProperty("allow_no_show_fee")]
        public bool AllowNoShowFee { get; set; }

        [JsonProperty("display_name")]
        public object DisplayName { get; set; }

        [JsonProperty("show_in_catalog")]
        public long ShowInCatalog { get; set; }

        [JsonProperty("sort_order")]
        public long SortOrder { get; set; }

        [JsonProperty("show_price")]
        public bool ShowPrice { get; set; }

        [JsonProperty("allow_wait_list")]
        public object AllowWaitList { get; set; }

        [JsonProperty("wait_list_lock_window")]
        public long WaitListLockWindow { get; set; }

        [JsonProperty("enable_enrol")]
        public bool EnableEnrol { get; set; }

        [JsonProperty("level_id")]
        public object LevelId { get; set; }

        [JsonProperty("registration_notes")]
        public object RegistrationNotes { get; set; }

        [JsonProperty("enable_payments")]
        public bool EnablePayments { get; set; }

        [JsonProperty("image_paths")]
        public object ImagePaths { get; set; }

        [JsonProperty("min_time")]
        public object MinTime { get; set; }

        [JsonProperty("max_time")]
        public object MaxTime { get; set; }

        [JsonProperty("web_capacity")]
        public object WebCapacity { get; set; }

        [JsonProperty("registration_lock_window")]
        public long RegistrationLockWindow { get; set; }

        [JsonProperty("auto_confirm_lock_window")]
        public long AutoConfirmLockWindow { get; set; }

        [JsonProperty("sac")]
        public object Sac { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("enable_virtual_links")]
        public long EnableVirtualLinks { get; set; }

        [JsonProperty("tags")]
        public object Tags { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }
    }

    public partial class WelcomeInstructor
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }
    }

    public partial class PageDetails
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }

    public partial class Session
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("session_guid")]
        public Guid SessionGuid { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("class_id")]
        public long ClassId { get; set; }

        [JsonProperty("start_time")]
        public DateTime StartTime { get; set; }

        [JsonProperty("end_time")]
        public DateTime EndTime { get; set; }

        [JsonProperty("start_time_utc")]
        public DateTime StartTimeUtc { get; set; }

        [JsonProperty("end_time_utc")]
        public DateTime EndTimeUtc { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("instructor_id")]
        public Guid InstructorId { get; set; }

        [JsonProperty("instructors")]
        public List<SessionInstructor> Instructors { get; set; }

        [JsonProperty("assistant_id")]
        public object AssistantId { get; set; }

        [JsonProperty("room_id")]
        public Guid? RoomId { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("recurrence_id")]
        public long RecurrenceId { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        [JsonProperty("center_name")]
        public string CenterName { get; set; }

        [JsonProperty("registration_id")]
        public object RegistrationId { get; set; }

        [JsonProperty("registration_status")]
        public object RegistrationStatus { get; set; }

        [JsonProperty("level_id")]
        public object LevelId { get; set; }

        [JsonProperty("is_free_session")]
        public bool IsFreeSession { get; set; }

        [JsonProperty("can_book")]
        public bool CanBook { get; set; }

        [JsonProperty("can_cancel")]
        public bool CanCancel { get; set; }

        [JsonProperty("booking_stage")]
        public long BookingStage { get; set; }

        [JsonProperty("capacity")]
        public long? Capacity { get; set; }

        [JsonProperty("web_capacity")]
        public object WebCapacity { get; set; }

        [JsonProperty("wait_listed_count")]
        public long WaitListedCount { get; set; }

        [JsonProperty("occupancy")]
        public long Occupancy { get; set; }

        [JsonProperty("web_occupancy")]
        public long WebOccupancy { get; set; }

        [JsonProperty("cancel_stage")]
        public long CancelStage { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("html_description")]
        public object HtmlDescription { get; set; }

        [JsonProperty("available_slots")]
        public long? AvailableSlots { get; set; }

        [JsonProperty("current_waitlist_position")]
        public object CurrentWaitlistPosition { get; set; }

        [JsonProperty("can_book_status_id")]
        public object CanBookStatusId { get; set; }

        [JsonProperty("can_book_status")]
        public object CanBookStatus { get; set; }

        [JsonProperty("cancellation_fee_window")]
        public object CancellationFeeWindow { get; set; }

        [JsonProperty("cancellation_fee")]
        public object CancellationFee { get; set; }

        [JsonProperty("is_instructor_substituted")]
        public bool IsInstructorSubstituted { get; set; }

        [JsonProperty("is_sub_requested")]
        public bool IsSubRequested { get; set; }

        [JsonProperty("student_virtual_link")]
        public string StudentVirtualLink { get; set; }

        [JsonProperty("is_virtual_class")]
        public bool IsVirtualClass { get; set; }

        [JsonProperty("virtual_type")]
        public long VirtualType { get; set; }

        [JsonProperty("show_slots")]
        public bool ShowSlots { get; set; }

        [JsonProperty("guest_pass_id")]
        public object GuestPassId { get; set; }

        [JsonProperty("invoice_id")]
        public object InvoiceId { get; set; }

        [JsonProperty("payment_source")]
        public object PaymentSource { get; set; }
    }

    public partial class SessionInstructor
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("is_primary")]
        public bool IsPrimary { get; set; }
    }
}