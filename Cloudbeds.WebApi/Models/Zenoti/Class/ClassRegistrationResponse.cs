﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ClassRegistrationResponse
    {
        [JsonProperty("registrations")]
        public List<Registration> Registrations { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }

    public partial class Registration
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("invoice_id")]
        public Guid InvoiceId { get; set; }

        [JsonProperty("app_group_id")]
        public Guid AppGroupId { get; set; }

        [JsonProperty("group_invoice_id")]
        public object GroupInvoiceId { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("custom_form_status")]
        public long CustomFormStatus { get; set; }

        [JsonProperty("guest_form_status")]
        public long GuestFormStatus { get; set; }

        [JsonProperty("membership_balance")]
        public object MembershipBalance { get; set; }

        [JsonProperty("guest_registrations")]
        public List<object> GuestRegistrations { get; set; }
    }
}