﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class CreateBookingResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }
}
