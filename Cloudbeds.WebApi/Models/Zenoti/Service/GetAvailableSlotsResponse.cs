﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class GetAvailableSlotsResponse
    {
        [JsonProperty("slots")]
        public List<Slot> Slots { get; set; }

        [JsonProperty("future_days")]
        public List<FutureDay> FutureDays { get; set; }

        [JsonProperty("booking_id")]
        public string BookingId { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("Guest")]
        public string Guest { get; set; }

        [JsonProperty("ServiceId")]
        public Guid ServiceId { get; set; }

        [JsonProperty("Error")]
        public object Error { get; set; }
    }

    public partial class FutureDay
    {
        [JsonProperty("Day")]
        public DateTimeOffset Day { get; set; }

        [JsonProperty("IsAvailable")]
        public bool IsAvailable { get; set; }
    }

    public partial class Slot
    {
        [JsonProperty("Time")]
        public DateTime Time { get; set; }

        [JsonProperty("Warnings")]
        public object Warnings { get; set; }

        [JsonProperty("Priority")]
        public long Priority { get; set; }

        [JsonProperty("Available")]
        public bool Available { get; set; }

        [JsonProperty("SalePrice")]
        public object SalePrice { get; set; }

        [JsonProperty("ShortTime")]
        public string ShortTime { get; set; }

        [JsonProperty("DayPeriod")]
        public int DayPeriod { get; set; }

        [JsonProperty("StartTime")]
        public int StartTime { get; set; }
        [JsonProperty("Hour")]
        public int Hour { get; set; }
    }
}
