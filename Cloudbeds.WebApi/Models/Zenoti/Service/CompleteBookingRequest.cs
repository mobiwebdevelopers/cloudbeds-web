﻿
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class CancelAppointmentResponse
    {
        public string BookingId { get; set; }

        public string SlotTime { get; set; }

    }   
}
