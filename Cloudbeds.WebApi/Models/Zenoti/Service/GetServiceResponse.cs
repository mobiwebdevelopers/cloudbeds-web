﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class GetServiceResponse
    {
        [JsonProperty("services")]
        public List<Service> Services { get; set; }

        [JsonProperty("page_info")]
        public PageInfo PageInfo { get; set; }
    }

    public partial class Service
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("recovery_time")]
        public long RecoveryTime { get; set; }

        [JsonProperty("price_info")]
        public PriceInfo PriceInfo { get; set; }

        [JsonProperty("additional_info")]
        public AdditionalInfo AdditionalInfo { get; set; }

        [JsonProperty("catalog_info")]
        public object CatalogInfo { get; set; }

        [JsonProperty("variants_info")]
        public object VariantsInfo { get; set; }

        [JsonProperty("add_ons_info")]
        public AddOnInfo AddOnsInfo { get; set; }

        [JsonProperty("image_paths")]
        public object ImagePaths { get; set; }

        [JsonProperty("parallel_groups")]
        public object ParallelGroups { get; set; }

        [JsonProperty("parallel_service_groups")]
        public object ParallelServiceGroups { get; set; }

        [JsonProperty("prerequisites_info")]
        public object PrerequisitesInfo { get; set; }

        [JsonProperty("finishing_services_info")]
        public object FinishingServicesInfo { get; set; }

        [JsonProperty("Slots")]
        public List<Slot> Slots { get; set; } = new List<Slot>();

        [JsonProperty("day_period")]
        public int DayPeriod { get; set; }
    }

    public partial class PriceInfo
    {
        [JsonProperty("currency_id")]
        public long CurrencyId { get; set; }

        [JsonProperty("sale_price")]
        public long SalePrice { get; set; }

        [JsonProperty("tax_id")]
        public string TaxId { get; set; }

        [JsonProperty("ssg")]
        public long? Ssg { get; set; }

        [JsonProperty("include_tax")]
        public bool IncludeTax { get; set; }

        [JsonProperty("demand_group_id")]
        public string DemandGroupId { get; set; }
    }

    public partial class AdditionalInfo
    {
        [JsonProperty("html_description")]
        public string HtmlDescription { get; set; }

        [JsonProperty("category")]
        public BussinessUnit Category { get; set; }

        [JsonProperty("sub_category")]
        public BussinessUnit SubCategory { get; set; }

        [JsonProperty("bussiness_unit")]
        public BussinessUnit BussinessUnit { get; set; }

        [JsonProperty("allow_cancellation_fee")]
        public bool AllowCancellationFee { get; set; }
    }

    public partial class BussinessUnit
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
    public partial class AddOnInfo
    {
        [JsonProperty("is_add_on")]
        public bool IsAddOn { get; set; }

        [JsonProperty("has_add_ons")]
        public bool HasAddOns { get; set; }

        [JsonProperty("add_ons_list")]
        public List<Guid> AddOnsList { get; set; }

        [JsonProperty("is_mandatory_add_on")]
        public bool IsMandatoryAddOn { get; set; }
    }

}