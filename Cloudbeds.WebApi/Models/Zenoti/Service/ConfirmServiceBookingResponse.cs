﻿
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ConfirmServiceBookingResponse
    {
        [JsonProperty("is_confirmed")]
        public bool IsConfirmed { get; set; }

        [JsonProperty("invoice")]
        public Invoice Invoice { get; set; }

        [JsonProperty("Error")]
        public object Error { get; set; }
    }

    public partial class GroupData
    {
        [JsonProperty("group_name")]
        public string GroupName { get; set; }

    }
}
