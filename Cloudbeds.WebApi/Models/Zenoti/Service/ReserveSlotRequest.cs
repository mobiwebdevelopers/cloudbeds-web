﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ReserveSlotRequest
    {
        [JsonProperty("slot_time")]
        public string SlotTime { get; set; }
    }
}
