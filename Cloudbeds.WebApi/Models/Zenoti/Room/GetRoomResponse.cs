﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class GetRoomResponse
    {
        [JsonProperty("rooms")]
        public List<Room> Rooms { get; set; }
    }

    public partial class Room
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("room_category_id")]
        public string RoomCategoryId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("capacity")]
        public long Capacity { get; set; }

        [JsonProperty("can_exceed_capacity")]
        public bool CanExceedCapacity { get; set; }

        [JsonProperty("only_one_appointment")]
        public bool OnlyOneAppointment { get; set; }
    }
}