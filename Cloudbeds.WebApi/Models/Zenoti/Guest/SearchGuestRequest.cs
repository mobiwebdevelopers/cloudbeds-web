﻿
namespace Cloudbeds.WebApi.Models.Zenoti
{
    public class SearchGuestRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public bool CreateNew { get; set; }
    }
}
