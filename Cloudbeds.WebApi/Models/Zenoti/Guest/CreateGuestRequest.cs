﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class CreateGuestRequest
    {
        [JsonProperty("personal_info")]
        public PersonalInfo PersonalInfo { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }
    }
}
