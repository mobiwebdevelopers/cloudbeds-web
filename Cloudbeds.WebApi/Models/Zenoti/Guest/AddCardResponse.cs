﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class AddCardResponse
    {
        [JsonProperty("token_id")]
        public string token_id { get; set; }

        [JsonProperty("hosted_payment_uri")]
        public string hosted_payment_uri { get; set; }

    }
}
