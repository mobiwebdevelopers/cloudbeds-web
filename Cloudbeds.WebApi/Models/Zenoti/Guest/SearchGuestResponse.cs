﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class SearchGuestResponse
    {
        [JsonProperty("guests")]
        public List<Guest> Guests { get; set; }

        [JsonProperty("page_Info")]
        public PageInfo PageInfo { get; set; }
    }

    public partial class Guest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        [JsonProperty("preferred_service_id")]
        public object PreferredServiceId { get; set; }

        [JsonProperty("center_name")]
        public string CenterName { get; set; }

        [JsonProperty("created_date")]
        public DateTimeOffset CreatedDate { get; set; }

        [JsonProperty("can_edit_personal_info")]
        public bool CanEditPersonalInfo { get; set; }

        [JsonProperty("is_virtual_guest")]
        public bool IsVirtualGuest { get; set; }

        [JsonProperty("is_guest_custom_form_filled")]
        public bool IsGuestCustomFormFilled { get; set; }

        [JsonProperty("personal_info")]
        public PersonalInfo PersonalInfo { get; set; }

        [JsonProperty("address_info")]
        public object AddressInfo { get; set; }

        [JsonProperty("preferences")]
        public object Preferences { get; set; }

        [JsonProperty("tags")]
        public object Tags { get; set; }

        [JsonProperty("referral")]
        public object Referral { get; set; }

        [JsonProperty("primary_employee")]
        public object PrimaryEmployee { get; set; }
    }
    public class Phone
    {
        [JsonProperty("country_code")]
        public int country_code { get; set; }
        [JsonProperty("phone_code")]
        public int phone_code { get; set; }
        [JsonProperty("number")]
        public string number { get; set; }

    }
    public partial class PersonalInfo
    {
        [JsonProperty("id")]
        public Guid? Id { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("mobile_phone")]
        public Phone MobilePhone { get; set; }

        [JsonProperty("work_phone")]
        public Phone WorkPhone { get; set; }

        [JsonProperty("home_phone")]
        public Phone HomePhone { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("date_of_birth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("is_minor")]
        public bool IsMinor { get; set; }

        [JsonProperty("nationality_id")]
        public long NationalityId { get; set; }

        [JsonProperty("language_id")]
        public long LanguageId { get; set; }

        [JsonProperty("anniversary_date")]
        public string AnniversaryDate { get; set; }

        [JsonProperty("lock_guest_custom_data")]
        public bool LockGuestCustomData { get; set; }

        [JsonProperty("pan")]
        public string Pan { get; set; }

        [JsonProperty("dob_incomplete_year")]
        public string DobIncompleteYear { get; set; }
    }

    public partial class PageInfo
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }
}
