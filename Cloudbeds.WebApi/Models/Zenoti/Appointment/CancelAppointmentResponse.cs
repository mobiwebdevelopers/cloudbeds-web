﻿
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class CancelAppointmentResponse
    {
        public bool Success { get; set; }

        public bool IsChargeApplied { get; set; }

    }   
}
