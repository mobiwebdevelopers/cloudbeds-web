﻿using CloudBaseWeb.Api.Entities;
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class TokenData
    {
        [JsonProperty("credentials")]
        public Zenoti_Token Credentials { get; set; }

        [JsonProperty("login_policy_evaluation")]
        public object LoginPolicyEvaluation { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }

    //public partial class Credentials
    //{
    //    [JsonProperty("access_token")]
    //    public string AccessToken { get; set; }

    //    [JsonProperty("access_token_expiry")]
    //    public DateTime AccessTokenExpiry { get; set; }

    //    [JsonProperty("refresh_token")]
    //    public string RefreshToken { get; set; }

    //    [JsonProperty("refresh_token_expiry")]
    //    public DateTime RefreshTokenExpiry { get; set; }

    //    [JsonProperty("token_type")]
    //    public string TokenType { get; set; }

    //    [JsonProperty("token_id")]
    //    public Guid TokenId { get; set; }

    //    [JsonProperty("app_id")]
    //    public string AppId { get; set; }

    //    [JsonProperty("user_type")]
    //    public string UserType { get; set; }
    //}
}