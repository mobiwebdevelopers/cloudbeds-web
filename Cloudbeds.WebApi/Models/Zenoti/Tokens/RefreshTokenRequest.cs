﻿using CloudBaseWeb.Api.Entities;
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class RefreshTokenRequest
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("grant_type")]
        public string GrantType { get; set; }
    }
}