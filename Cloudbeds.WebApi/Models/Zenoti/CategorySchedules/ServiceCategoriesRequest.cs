﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Cloudbeds.WebApi.Models.Zenoti.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class ServiceCategoriesRequest
    {
        public List<string> GuestIds { get; set; } = new List<string>();
        public List<string> CategoryIds { get; set; } = new List<string>();
    }
}
