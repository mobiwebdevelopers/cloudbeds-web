﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Zenoti
{
    public partial class GetAllAppointmentsResponse
    {
        [JsonProperty("appointments")]
        public List<Appointment> Appointments { get; set; }

        [JsonProperty("page_info")]
        public PageInfo PageInfo { get; set; }
    }

    public partial class Appointment
    {
        [JsonProperty("appointment_group_id")]
        public Guid AppointmentGroupId { get; set; }

        [JsonProperty("no_of_guests")]
        public long NoOfGuests { get; set; }

        [JsonProperty("invoice_id")]
        public Guid InvoiceId { get; set; }

        [JsonProperty("invoice_status")]
        public long InvoiceStatus { get; set; }

        [JsonProperty("is_rebooking")]
        public bool IsRebooking { get; set; }

        [JsonProperty("notes")]
        public object Notes { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        [JsonProperty("appointment_services")]
        public List<AppointmentService> AppointmentServices { get; set; }

        [JsonProperty("appointment_packages")]
        public List<object> AppointmentPackages { get; set; }

        [JsonProperty("price")]
        public Price Price { get; set; }

        [JsonProperty("group_invoice_id")]
        public object GroupInvoiceId { get; set; }

        [JsonProperty("is_feedback_submitted")]
        public bool IsFeedbackSubmitted { get; set; }

        [JsonProperty("check_cancel_and_noshow")]
        public bool CheckCancelAndNoshow { get; set; }
    }

    public partial class AppointmentService
    {
        [JsonProperty("appointment_id")]
        public Guid AppointmentId { get; set; }

        [JsonProperty("invoice_item_id")]
        public Guid InvoiceItemId { get; set; }

        [JsonProperty("cart_item_id")]
        public Guid CartItemId { get; set; }

        [JsonProperty("service")]
        public AppointmentServiceDetail Service { get; set; }

        [JsonProperty("requested_therapist_gender")]
        public long RequestedTherapistGender { get; set; }

        [JsonProperty("start_time")]
        public DateTime StartTime { get; set; }

        [JsonProperty("end_time")]
        public DateTime EndTime { get; set; }

        [JsonProperty("room")]
        public Room Room { get; set; }

        [JsonProperty("equipment")]
        public object Equipment { get; set; }

        [JsonProperty("appointment_status")]
        public long AppointmentStatus { get; set; }

        [JsonProperty("requested_therapist_id")]
        public Guid RequestedTherapistId { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("service_custom_data_indicator")]
        public string ServiceCustomDataIndicator { get; set; }

        [JsonProperty("actual_start_time")]
        public object ActualStartTime { get; set; }

        [JsonProperty("completed_time")]
        public object CompletedTime { get; set; }

        [JsonProperty("progress")]
        public long Progress { get; set; }

        [JsonProperty("parent_appointment_id")]
        public object ParentAppointmentId { get; set; }

        [JsonProperty("service_custom_Data")]
        public object ServiceCustomData { get; set; }

        [JsonProperty("item_actions")]
        public string ItemActions { get; set; }

        [JsonProperty("is_membership_applied")]
        public bool? IsMembershipApplied { get; set; }

        [JsonProperty("is_addon")]
        public bool? IsAddon { get; set; }

        [JsonProperty("addon_appointment_id")]
        public string AddonAppointmentId { get; set; }

        [JsonProperty("has_service_form")]
        public bool? HasServiceForm { get; set; }

        [JsonProperty("has_segments")]
        public bool? HasSegments { get; set; }

        [JsonProperty("segments")]
        public object Segments { get; set; }
    }


    public partial class AppointmentServiceDetail
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("display_name")]
        public object DisplayName { get; set; }

        [JsonProperty("price")]
        public Price Price { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("category_id")]
        public Guid CategoryId { get; set; }

        [JsonProperty("is_addon")]
        public bool IsAddon { get; set; }

        [JsonProperty("has_addons")]
        public object HasAddons { get; set; }

        [JsonProperty("addons")]
        public object Addons { get; set; }

        [JsonProperty("is_variant")]
        public object IsVariant { get; set; }

        [JsonProperty("has_variant")]
        public object HasVariant { get; set; }

        [JsonProperty("parent_service_id")]
        public object ParentServiceId { get; set; }
    }

    public partial class Price
    {
        [JsonProperty("currency_id")]
        public long CurrencyId { get; set; }

        [JsonProperty("sales")]
        public long Sales { get; set; }

        [JsonProperty("tax")]
        public long Tax { get; set; }

        [JsonProperty("final")]
        public long Final { get; set; }
    }
}
