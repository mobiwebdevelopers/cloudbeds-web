﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models.Trybe.Enums
{
    public class Enums
    {
        public enum Services
        {
            Treatments,
            Hotsprings,
            Classes
        }

        public enum Gender
        {
            All = -1,
            Female = 0,
            Male = 1
        }

        public enum ServiceType
        {
            All,
            Service,
            Addon
        }
    }
}
