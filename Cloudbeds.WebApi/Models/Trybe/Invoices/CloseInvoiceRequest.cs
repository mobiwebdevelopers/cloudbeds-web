﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CloseInvoiceRequest
    {
        [JsonProperty("status")]
        public int status { get; set; }

        [JsonProperty("closed_by_id")]
        public Guid closed_by_id { get; set; }

    }
}
