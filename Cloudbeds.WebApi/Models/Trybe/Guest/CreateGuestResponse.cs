﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CreateGuestResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        //[JsonProperty("personal_info")]
        //public PersonalInfo PersonalInfo { get; set; }

        [JsonProperty("address_info")]
        public AddressInfo AddressInfo { get; set; }

        [JsonProperty("preferences")]
        public Preferences Preferences { get; set; }

        [JsonProperty("tags")]
        public object Tags { get; set; }

        [JsonProperty("referral")]
        public object Referral { get; set; }

        [JsonProperty("primary_employee")]
        public object PrimaryEmployee { get; set; }
    }

    public partial class AddressInfo
    {
        [JsonProperty("address_1")]
        public string Address1 { get; set; }

        [JsonProperty("address_2")]
        public string Address2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country_id")]
        public long CountryId { get; set; }

        [JsonProperty("state_id")]
        public long StateId { get; set; }

        [JsonProperty("state_other")]
        public string StateOther { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
    }    

    public partial class MobilePhone
    {
        [JsonProperty("country_code")]
        public long CountryCode { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }
    }

    public partial class Preferences
    {
        [JsonProperty("receive_transactional_email")]
        public bool ReceiveTransactionalEmail { get; set; }

        [JsonProperty("receive_transactional_sms")]
        public bool ReceiveTransactionalSms { get; set; }

        [JsonProperty("receive_marketing_email")]
        public bool ReceiveMarketingEmail { get; set; }

        [JsonProperty("receive_marketing_sms")]
        public bool ReceiveMarketingSms { get; set; }

        [JsonProperty("recieve_lp_stmt")]
        public bool RecieveLpStmt { get; set; }

        [JsonProperty("preferred_therapist")]
        public object PreferredTherapist { get; set; }
    }
}
