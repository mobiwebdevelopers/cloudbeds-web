﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class AddCardRequest
    {
        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        [JsonProperty("source")]
        public string source { get; set; } // = 0
        [JsonProperty("redirect_uri")]
        public string redirect_uri { get; set; }
        [JsonProperty("share_cards_to_web")]
        public bool share_cards_to_web { get; set; }
        [JsonProperty("skip_billingInfo")]
        public bool skip_billingInfo { get; set; }
        [JsonProperty("host")]
        public string host { get; set; }
        [JsonProperty("avs_source")]
        public string avs_source { get; set; } //  = 0

    }
}
