﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class MergeGuestRequest
    {
        [JsonProperty("merge_guest_ids")]
        public List<Guid> merge_guest_ids { get; set; }

        [JsonProperty("merge_type")]
        public int merge_type { get; set; }
    }
}
