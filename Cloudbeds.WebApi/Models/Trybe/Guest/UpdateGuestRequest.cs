﻿using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class UpdateGuestRequest
    {
        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        //[JsonProperty("personal_info")]
        //public UpdatePersonalInfo PersonalInfo { get; set; }
    }

    public partial class UpdatePersonalInfo
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("first_name")]
        public string first_name { get; set; }
        [JsonProperty("last_name")]
        public string last_name { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
        //[JsonProperty("mobile_phone")]
        //public Phone mobile_phone { get; set; }
        //[JsonProperty("home_phone")]
        //public Phone home_phone { get; set; }
        //[JsonProperty("phone")]
        //public string phone { get; set; }
        //[JsonProperty("number")]
        //public string number { get; set; }

    }
}
