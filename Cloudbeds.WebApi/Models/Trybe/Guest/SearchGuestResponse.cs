﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class SearchGuestResponse
    {
        [JsonProperty("data")]
        public List<Guest> Guests { get; set; }

        [JsonProperty("page_Info")]
        public PageInfo PageInfo { get; set; }
    }

    public partial class Guest
    {
        public string id { get; set; }
        public Guid site_id { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String full_name { get; set; }
        public String phone { get; set; }
        public String email { get; set; }
        public String stripe_id { get; set; }

    }

    public partial class PageInfo
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }
}
