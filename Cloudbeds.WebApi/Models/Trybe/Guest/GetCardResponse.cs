﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{

    public class TrybePaymentAccount
    {
        [JsonProperty("payment_account_id")]
        public string payment_account_id { get; set; }

        [JsonProperty("account_id")]
        public string account_id { get; set; }
        [JsonProperty("card_logo")]
        public string card_logo { get; set; }
        [JsonProperty("last_four")]
        public string last_four { get; set; }
        [JsonProperty("expiry_on")]
        public string expiry_on { get; set; }
        [JsonProperty("identifier")]
        public string identifier { get; set; }

    }
    public partial class GetCardResponse
    {
        [JsonProperty("accounts")]
        public List<TrybePaymentAccount> accounts { get; set; }
        //[JsonProperty("account_id")]
        //public string account_id { get; set; }

        //[JsonProperty("card_logo")]
        //public string card_logo { get; set; }
        //[JsonProperty("last_four")]
        //public string last_four { get; set; }
        //[JsonProperty("expiry_on")]
        //public string expiry_on { get; set; }
        [JsonProperty("has_expired_cards")]
        public bool has_expired_cards { get; set; }

    }
}
