﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using static Cloudbeds.WebApi.Models.Trybe.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Trybe.Cart
{
    public partial class Cart
    {
        [JsonProperty("Guest")]
        public Guest Guest { get; set; }

        [JsonProperty("MultiGuest")]
        public List<Guest> MultiGuest { get; set; } = new List<Guest>();

        [JsonProperty("BookingServicesData")]
        public List<BookingServicesData> BookingServicesData { get; set; } = new List<BookingServicesData>();

        [JsonProperty("BookingClassesData")]
        public List<BookingClassesData> BookingClassesData { get; set; } = new List<BookingClassesData>();
    }

    public partial class Guest
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Idx")]
        public int Idx { get; set; }
    }

    public partial class BookingServicesData
    {
        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("BookingId")]
        public string BookingId { get; set; }

        [JsonProperty("FirstAdditional")]
        public bool FirstAdditional { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Schedule")]
        public string Schedule { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("DefaultPrice")]
        public decimal DefaultPrice { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("ServiceTypeId")]
        public int ServiceTypeId { get; set; }

        [JsonProperty("Added")]
        public bool Added { get; set; }

        [JsonProperty("FullDate")]
        public DateTime FullDate { get; set; }

        [JsonProperty("IsService")]
        public bool IsService { get; set; }

        [JsonProperty("IsAddon")]
        public bool IsAddon { get; set; }

        [JsonProperty("IsMultiGuest")]
        public bool IsMultiGuest { get; set; }        

        [JsonProperty("Addons")]
        public List<Guid> Addons { get; set; } = new List<Guid>();

        [JsonProperty("selected")]
        public bool Selected { get; set; }

        [JsonProperty("AddonsDetail")]
        public List<BookingServicesData> AddonsDetail { get; set; } = new List<BookingServicesData>();

        [JsonProperty("SelectedAddons")]
        public List<BookingServicesData> SelectedAddons { get; set; } = new List<BookingServicesData>();

        [JsonProperty("SelectedTherapist")]
        public SelectedTherapist SelectedTherapist { get; set; }

        [JsonProperty("IsUpdated")]
        public bool IsUpdated { get; set; }
    }

    public partial class BookingClassesData
    {
        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("SessionId")]
        public int SessionId { get; set; }

        [JsonProperty("ClassId")]
        public int ClassId { get; set; }

        [JsonProperty("Waitlist")]
        public bool Waitlist { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }

        [JsonProperty("BookingSource")]
        public int BookingSource { get; set; }

        [JsonProperty("Added")]
        public bool Added { get; set; }

        [JsonProperty("IsService")]
        public bool IsService { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }
    }

    public partial class SelectedTherapist
    {
        [JsonProperty("Id")]
        public Guid? Id { get; set; }

        [JsonProperty("Gender")]
        public Gender Gender { get; set; }
    }
}
