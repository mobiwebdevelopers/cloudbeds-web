﻿using Newtonsoft.Json;
using System.Collections.Generic;
using static Cloudbeds.WebApi.Models.Trybe.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class FullSpaService
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public Services Type { get; set; }

        [JsonProperty("services")]
        public List<Service> Services { get; set; }

        [JsonProperty("classes")]
        public List<Class> Classes { get; set; }
    }




}