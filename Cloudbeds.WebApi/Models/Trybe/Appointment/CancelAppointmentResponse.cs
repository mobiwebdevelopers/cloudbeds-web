﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public class Trybe_Appointment
    {
        //public string Status { get; set; }
        public string Stage { get; set; }
    }
    public class Trybe_Session_CheckIn
    {
        public bool is_checked_in { get; set; }
    }
    public class Trybe_Session
    {
        public int num_checked_in { get; set; }
        public int num_no_show { get; set; }
    }
    public partial class CancelAppointmentResponse
    {
        public bool Success { get; set; }

        public bool IsChargeApplied { get; set; }

    }
    public class Trybe_Order
    {
        //public string Status { get; set; }
        public string stage { get; set; }
    }
    public class Trybe_Tip
    {
        public int amount { get; set; }
    }
    public class Trybe_Order_Min
    {
        public string id { get; set; }
        public string status { get; set; }
        public string stage { get; set; }
        public string customer_id { get; set; }
        public bool intake_forms_complete { get; set; }
        public bool intake_form_required { get; set; }
    }
    public class Trybe_Order_List
    {
        public List<Trybe_Order_Min> data { get; set; }
    }
}
