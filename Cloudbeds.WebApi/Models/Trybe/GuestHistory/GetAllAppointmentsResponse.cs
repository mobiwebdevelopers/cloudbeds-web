﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public class Trybe_Guest
    {
        public string id { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public bool intake_form_complete { get; set; }
        public bool is_lead_booker { get; set; }
        public string customer_id { get; set; }
    }
    public class Trybe_Total
    {
        public int subtotal { get; set; }
        public int order_discount { get; set; }
        public int total { get; set; }
    }
    public class Trybe_Amount
    {
        public int amount { get; set; }
        public int percentage { get; set; }
        public int total { get; set; }
    }
    public class Trybe_Processor
    {
        public string card_brand { get; set; }
        public string card_last4 { get; set; }
        public string arn { get; set; }
    }
    public class Trybe_PaymentTotal
    {
        public int paid { get; set; }
        public int pending { get; set; }
        public int chargeable { get; set; }
        public int provided { get; set; }
        public int missing { get; set; }
        public int unpaid { get; set; }
    }
    public class Trybe_Payment
    {
        public string id { get; set; }
        public string order_ref { get; set; }
        public string processor { get; set; }
        public string details_source { get; set; }
        public string capture_method { get; set; }
        public int amount { get; set; }
        public int refundable_amount { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string paid_at { get; set; }
        public Trybe_Processor processor_data { get; set; }

    }
    public class Trybe_Room
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class Trybe_Practitioner
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class Trybe_Offering
    {
        public string id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string product_code { get; set; }
    }
    public class Trybe_PromoCode
    {
        public string id { get; set; }
        public string code { get; set; }
        public string percentage { get; set; }
    }
    public class Trybe_Booking
    {
        public string id { get; set; }
        public string status { get; set; }
        public Trybe_Offering offering { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int duration { get; set; }
        public List<string> practitioner_ids { get; set; }
        public List<Trybe_Practitioner> practitioners { get; set; }
        public string room_id { get; set; }
        public Trybe_Room room { get; set; }
        public string session_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
    public class Trybe_PackageItem
    {
        public string id { get; set; }
        public string choice_id { get; set; }
        public string option_id { get; set; }
        public string offering_type { get; set; }
        public string offering_id { get; set; }
        public string offering_name { get; set; }
        public string time { get; set; }
        public int duration { get; set; }
        public string reserved_until { get; set; }
        public int price_change { get; set; }
        //public string item_configuration { get; set; }

        public string status { get; set; }

        public Trybe_Booking booking_summary { get; set; }
    }
    public partial class GetAllAppointmentsResponse
    {
        public List<Appointment> data { get; set; }

    }

    public partial class Appointment
    {
        public string id { get; set; }
        public string customer_id { get; set; }
        public string status { get; set; }
        public string stage { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public List<Trybe_Guest> guests { get; set; }
        public string item_status { get; set; }
        public List<BookingItem> booking_items { get; set; }
        public List<Trybe_PackageItem> package_items { get; set; }
        public string booking_items_start_date { get; set; }
        public string booking_items_end_date { get; set; }
        public Trybe_Total totals { get; set; }
        public int total_cost { get; set; }
        public int discount_total { get; set; }
        public int total_tax { get; set; }
        public int net_total { get; set; }
        public int tip_amount { get; set; }
        public Trybe_Amount service_charge { get; set; }
        public int submit_payment_amount { get; set; }
        public int submit_auth_amount { get; set; }
        public int outstanding_payment_amount { get; set; }
        public Trybe_PaymentTotal payment_totals { get; set; }
        public int total_paid_or_authed { get; set; }
        public List<Trybe_Payment> payments { get; set; }
        public string applied_promo_code_id { get; set; }
        public Trybe_PromoCode applied_promo_code { get; set; }
        public int applied_promo_code_discount_total { get; set; }
        public bool is_promo_code_discount_applied { get; set; }
        public string promo_code_applied_at { get; set; }
        public bool is_locked { get; set; }
        public string submitted_at { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }



    public partial class BookingItem
    {
        public string id { get; set; }
        public string status { get; set; }
        public string item_type { get; set; }
        public string date { get; set; }
        public string type_id { get; set; }
        public string type_name { get; set; }
        public string type_product_code { get; set; }
        public List<Trybe_Guest> guests { get; set; }
        public int price { get; set; }
        public int discount_amount { get; set; }
        public List<Discount> discounts { get; set; }
        public Trybe_Booking booking_summary { get; set; }
        public int total_cost { get; set; }
        public int net_total { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public Trybe_Guest guest { get; set; }
        public bool is_modifiable { get; set; }
    }

    public partial class Discount
    {
        public string id { get; set; }
        public string discount_type_code { get; set; }
        public string amount_type { get; set; }
        public string discount_amount { get; set; }
        public int calculated_amount { get; set; }
        public string reason_code { get; set; }
        public string coupon_code_id { get; set; }
    }
}
