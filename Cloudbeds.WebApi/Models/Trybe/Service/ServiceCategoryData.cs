﻿using System;
using System.Collections.Generic;
using Cloudbeds.WebApi.Models.Trybe;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class ServiceCategoryData
    {
        [JsonProperty("ServiceCategoryData")]
        public List<CategoryData> CategoryData { get; set; }
    }

    public partial class CategoryData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("display_order")]
        public int? display_order { get; set; }

        public string ImagePaths { get; set; }
        [JsonProperty("services")]
        public List<Service> Services { get; set; } = new List<Service>();
    }



    
}
