﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Cloudbeds.WebApi.Models.Trybe.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CreateBookingRequest
    {
        [JsonProperty("is_only_catalog_employees")]
        public bool IsOnlyCatalogEmployees { get; set; }

        [JsonProperty("is_double_booking_enabled")]
        public bool IsDoubleBookingEnabled { get; set; }

        [JsonProperty("center_id")]
        public Guid CenterId { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("guests")]
        public List<BookingRequestGuest> Guests { get; set; } = new List<BookingRequestGuest>();
    }

    public partial class BookingRequestGuest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("invoice_id")]
        public string InvoiceId { get; set; }

        [JsonProperty("items")]
        public List<BookingRequestItemElement> Items { get; set; } = new List<BookingRequestItemElement>();
    }

    public partial class BookingRequestItemElement
    {
        [JsonProperty("item")]
        public BookingRequestItem Item { get; set; }

        [JsonProperty("therapist")]
        public BookingRequestTherapist Therapist { get; set; }

        [JsonProperty("room")]
        public BookingRequestRoom Room { get; set; }

        [JsonProperty("add_ons")]
        public List<BookingRequestAddOn> AddOns { get; set; }

        [JsonProperty("invoice_item_id")]
        public string InvoiceItemId { get; set; }
    }

    public partial class BookingRequestItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }

    public partial class BookingRequestTherapist
    {
        [JsonProperty("id")]
        public Guid? Id { get; set; }

        [JsonProperty("gender")]
        public Gender Gender { get; set; }
    }

    public partial class BookingRequestRoom
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }

    public partial class BookingRequestAddOn
    {
        [JsonProperty("item")]
        public BookingRequestItem BookingRequestItem { get; set; }
    }
}
