﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class ReserveSlotResponse
    {
        [JsonProperty("is_reserved")]
        public bool IsReserved { get; set; }

        [JsonProperty("reservation_id")]
        public Guid ReservationId { get; set; }

        [JsonProperty("expiry_time")]
        public DateTimeOffset ExpiryTime { get; set; }

        [JsonProperty("blocking_time")]
        public long BlockingTime { get; set; }

        [JsonProperty("invoices")]
        public List<Invoice> Invoices { get; set; }

        [JsonProperty("Error")]
        public object Error { get; set; }
    }

    public partial class Invoice
    {
        [JsonProperty("guest")]
        public ZGuest Guest { get; set; }

        [JsonProperty("invoice_id")]
        public object InvoiceId { get; set; }

        [JsonProperty("items")]
        public List<ItemElement> Items { get; set; }
    }

    public partial class ZGuest
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }
    }

    public partial class ItemElement
    {

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("therapist")]
        public Therapist Therapist { get; set; }

        [JsonProperty("room")]
        public Room Room { get; set; }

        [JsonProperty("equipment")]
        public object Equipment { get; set; }

        [JsonProperty("start_time")]
        public DateTimeOffset StartTime { get; set; }

        [JsonProperty("end_time")]
        public DateTimeOffset EndTime { get; set; }

        [JsonProperty("invoice_item_id")]
        public object InvoiceItemId { get; set; }

        [JsonProperty("appointment_id")]
        public object AppointmentId { get; set; }

        [JsonProperty("join_link")]
        public object JoinLink { get; set; }

        [JsonProperty("addons")]
        public object Addons { get; set; }

        [JsonProperty("package_services")]
        public object PackageServices { get; set; }
    }

    public partial class ItemItem
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("item_type")]
        public long ItemType { get; set; }

        [JsonProperty("item_display_name")]
        public string ItemDisplayName { get; set; }
    }

    public partial class Therapist
    {
        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("nick_name")]
        public string NickName { get; set; }

        [JsonProperty("therapist_request_type")]
        public object TherapistRequestType { get; set; }
    }
}
