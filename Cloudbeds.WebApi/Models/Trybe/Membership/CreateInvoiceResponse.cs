﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CreateInvoiceResponse
    {
        [JsonProperty("invoice_id")]
        public Guid InvoiceId { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        public string Error { get; set; }
    }

}
