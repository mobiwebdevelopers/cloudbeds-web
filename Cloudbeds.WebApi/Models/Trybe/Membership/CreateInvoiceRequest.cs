﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CreateInvoiceRequest
    {
        [JsonProperty("center_id")]
        public string CenterId { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("membership_ids")]
        public string MembershipIds { get; set; }


    }
}
