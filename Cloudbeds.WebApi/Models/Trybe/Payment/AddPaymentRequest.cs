﻿
using Newtonsoft.Json;
using System;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class AddPaymentRequest
    {
        public int amount { get; set; } 
        public string capture_method { get; set; } 
        public string customer_id { get; set; } 
        public string details_source { get; set; } 
        public string payment_method_id { get; set; } 
        public string processor { get; set; } 

    }
}
