﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class GetTherapistPricingResponse
    {
        [JsonProperty("therapists")]
        public List<Therapist> Therapists { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }
    }

    public partial class Therapist
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("image_paths")]
        public object ImagePaths { get; set; }

        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("service_time")]
        public long ServiceTime { get; set; }

        [JsonProperty("is_catalog_enabled")]
        public bool IsCatalogEnabled { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }
    }

}