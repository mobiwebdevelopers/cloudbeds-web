﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class GetTherapistResponse
    {
        [JsonProperty("therapists")]
        public List<Therapist> Therapists { get; set; }
    }

    public partial class Therapist
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("gender")]
        public int Gender { get; set; }

        [JsonProperty("personal_info")]
        public TherapistPersonalInfo PersonalInfo { get; set; }

        [JsonProperty("job_info")]
        public JobInfo JobInfo { get; set; }

        [JsonProperty("catalog_info")]
        public CatalogInfo CatalogInfo { get; set; }

        [JsonProperty("show")]
        public bool Show { get; set; }
    }

    public partial class CatalogInfo
    {
        [JsonProperty("is_catalog_enabled")]
        public bool IsCatalogEnabled { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }
    }

    public partial class JobInfo
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class TherapistPersonalInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("nick_name")]
        public string NickName { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }
    }

}