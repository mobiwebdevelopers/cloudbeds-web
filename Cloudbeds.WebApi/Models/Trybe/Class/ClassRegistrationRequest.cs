﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class ClassRegistrationRequest
    {
        [JsonProperty("guest_id")]
        public string GuestId { get; set; }

        [JsonProperty("session_id")]
        public long SessionId { get; set; }

        [JsonProperty("center_id")]
        public string CenterId { get; set; }

        [JsonProperty("waitlist")]
        public bool Waitlist { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("booking_source")]
        public long BookingSource { get; set; }
    }       
}