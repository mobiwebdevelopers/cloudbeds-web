﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Cloudbeds.WebApi.Models.Trybe.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class ServiceCategoriesRequest
    {
        public List<string> GuestIds { get; set; } = new List<string>();
        public List<string> CategoryIds { get; set; } = new List<string>();
    }
}
