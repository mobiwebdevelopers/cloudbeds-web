﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static Cloudbeds.WebApi.Models.Trybe.Enums.Enums;

namespace Cloudbeds.WebApi.Models.Trybe
{
    public partial class CategorySchedulesRequest
    {
        public List<string> GuestIds { get; set; }
        public List<Guid> ServiceIds { get; set; }
        public List<Guid> AddonIds { get; set; }
        public string TherapistId { get; set; }
        public int DayPeriod { get; set; }
        public Gender? Gender { get; set; }
        public DateTime Date { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceItemId { get; set; }
    }
}
