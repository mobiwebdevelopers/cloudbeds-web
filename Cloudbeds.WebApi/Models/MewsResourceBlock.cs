﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsResourceBlock : MewsToken
    {
        [JsonProperty("ResourceBlockIds")]
        public List<string> ResourceBlockIds { get; set; }
    }

}
