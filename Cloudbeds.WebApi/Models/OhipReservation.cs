﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class OhipReservation
    {
        public int reservationId { get; set; }
        public DateTime arrivalDate { get; set; }
        public DateTime departureDate { get; set; }
        public int days { get; set; }
        public bool showCoupons { get; set; }
        public string errorMessage { get; set; }
    }
}
