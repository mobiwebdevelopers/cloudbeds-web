﻿namespace Cloudbeds.WebApi.Models
{
    public class SaveInvitationCodeRequest
    {
        public string ReservationId { get; set; }
        public string RoomId { get; set; }
    }
}
