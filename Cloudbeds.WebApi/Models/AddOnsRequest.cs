﻿using System;

namespace Cloudbeds.WebApi.Models
{
    public class AddOnsRequest
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
