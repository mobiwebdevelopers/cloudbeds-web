﻿namespace Cloudbeds.WebApi.Models
{
    public class ReservationRoom
    {
        public int RoomTypeID { get; set; }

        public int Quantity { get; set; }

        public string roomName { get; set; }

        public string roomID { get; set; }
    }
}
