﻿using System;

namespace Cloudbeds.WebApi.Models
{
    public class ReservationRequest
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string GuestFirstName { get; set; }

        public string GuestLastName { get; set; }

        public string GuestCountry { get; set; }

        public string GuestZip { get; set; }

        public string GuestEmail { get; set; }

        public ReservationRoom[] Rooms { get; set; }

        public ReservationRoom[] Adults { get; set; }

        public ReservationRoom[] Children { get; set; }

        public string PaymentMethod { get; set; }
    }
}
