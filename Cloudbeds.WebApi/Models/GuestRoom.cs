﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class GuestRoom
    {
        public string RoomId { get; set; }
        public string RoomName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string RoomNumber { get; set; }
        public string RoomTypeName { get; set; }
        public double Rate { get; set; }
        public string CurrencyCode { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
