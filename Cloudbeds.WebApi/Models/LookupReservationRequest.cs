﻿using System;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class LookupReservationRequest
    {
        public string ReservationID { get; set; }
        public bool Balance { get; set; }
        public string RoomID { get; set; }
        public Reservation ReservationData { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Profile1 { get; set; }
        public string Profile2 { get; set; }
        public string CardNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool Checkout { get; set; }
    }

    public class LookupReservationByRangeRequest
    {
        public DateTime CheckInFrom { get; set; }
        public DateTime CheckInTo { get; set; }
        //public DateTime? CheckOutFrom { get; set; }
        //public DateTime? CheckOutTo { get; set; }
        public string Status { get; set; }
        public bool IncludeGuestsDetails { get; set; }
    }

    public partial class LookupReservationResponse
    {
        public bool Success { get; set; }
        public List<LookupReservationData> Data { get; set; }
        public long? Count { get; set; }
        public long? Total { get; set; }
    }

    public partial class LookupReservationData
    {
        public long PropertyId { get; set; }
        public string ReservationId { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string Status { get; set; }
        public long GuestId { get; set; }
        public string GuestName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long Adults { get; set; }
        public long Children { get; set; }
        public long Balance { get; set; }
        public string SourceName { get; set; }
        public string ThirdPartyIdentifier { get; set; }
        public Dictionary<string, CloudbedsGuest> GuestList { get; set; }
    } 

    public partial class CloudbedsGuest
    {
        public string GuestName { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }
        public string GuestGender { get; set; }
        public string GuestEmail { get; set; }
        public string GuestPhone { get; set; }
        public string GuestCellPhone { get; set; }
        public string GuestAddress { get; set; }
        public string GuestAddress2 { get; set; }
        public string GuestCity { get; set; }
        public string GuestState { get; set; }
        public string GuestCountry { get; set; }
        public string GuestZip { get; set; }
        public string GuestBirthdate { get; set; }
        public string GuestDocumentType { get; set; }
        public string GuestDocumentNumber { get; set; }
        public string GuestDocumentIssueDate { get; set; }
        public string GuestDocumentIssuingCountry { get; set; }
        public string GuestDocumentExpirationDate { get; set; }
        public string TaxId { get; set; }
        public string CompanyTaxId { get; set; }
        public string CompanyName { get; set; }
        public string SubReservationId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool AssignedRoom { get; set; }
        public bool IsAnonymized { get; set; }
        public CloudbedsRoom[] Rooms { get; set; }
        public string RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeName { get; set; }
        public bool IsMainGuest { get; set; }
    }

    public partial class CloudbedsRoom
    {
        public string RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomStatus { get; set; }
        public string SubReservationId { get; set; }
    }
}
