﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsPriceList
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Day")]
        public string Day { get; set; }

        [JsonProperty("Price")]
        public long Price { get; set; }
    }
}
