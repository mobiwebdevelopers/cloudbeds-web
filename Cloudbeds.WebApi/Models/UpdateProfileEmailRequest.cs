﻿namespace Cloudbeds.WebApi.Models
{
    public class UpdateProfileEmailRequest
    {
        public string ProfileID { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }

    }
}
