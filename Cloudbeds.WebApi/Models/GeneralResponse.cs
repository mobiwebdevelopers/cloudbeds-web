﻿using Newtonsoft.Json;

namespace CloudBaseWeb.Api.Models
{
    public class GeneralResponse
    {
        [JsonProperty("data")]
        public string Data { get; set; }
    }
}
