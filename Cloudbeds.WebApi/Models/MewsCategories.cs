﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsCategories : MewsToken
    {
        [JsonProperty("Extent")]
        public MewsResources Extent { get; set; }

        [JsonProperty("ResourceIds")]
        public string[] ResourceIds { get; set; }

    }
}
