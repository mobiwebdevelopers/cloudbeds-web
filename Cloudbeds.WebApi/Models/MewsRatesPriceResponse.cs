﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class RatePrice
    {
        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("BasePrices")]
        public decimal[] BasePrices { get; set; }


        [JsonProperty("BaseAmountPrices")]
        public MewsPrice[] BaseAmountPrices { get; set; }

        [JsonProperty("CategoryAdjustments")]
        public CategoryAdjustment[] CategoryAdjustments { get; set; }

        [JsonProperty("CategoryPrices")]
        public CategoryPrice[] CategoryPrices { get; set; }

        [JsonProperty("DatesUtc")]
        public DateTimeOffset[] DatesUtc { get; set; }
    }

    public partial class CategoryAdjustment
    {
        [JsonProperty("AbsoluteValue")]
        public long AbsoluteValue { get; set; }

        [JsonProperty("CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty("ParentCategoryId")]
        public Guid? ParentCategoryId { get; set; }

        [JsonProperty("RelativeValue")]
        public double RelativeValue { get; set; }
    }

    public partial class CategoryPrice
    {
        [JsonProperty("CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty("Prices")]
        public decimal[] Prices { get; set; }
    }
    public partial class MewsPrice
    {
        [JsonProperty("Tax")]
        public decimal Tax { get; set; }

        [JsonProperty("NetValue")]
        public decimal NetValue { get; set; }

        [JsonProperty("GrossValue")]
        public decimal GrossValue { get; set; }
    }
    public partial class MewsTimeUnit
    {
        [JsonProperty("Index")]
        public int Index { get; set; }

        [JsonProperty("Amount")]
        public MewsPrice Amount { get; set; }

    }
}
