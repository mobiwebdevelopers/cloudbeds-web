﻿namespace Cloudbeds.WebApi.Models
{
    public class PrepareKeyRequest
    {
        public string ReservationId { get; set; }

        public string EndpointId { get; set; }
        public string RoomId { get; set; }
        public string SerialNumber { get; set; }
        public bool Override { get; set; }
        public int Encoder { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
    }
}
