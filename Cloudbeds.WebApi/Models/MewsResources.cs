﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsResources
    {
        [JsonProperty("Resources")]
        public bool Resources { get; set; } = false;

        [JsonProperty("ResourceCategories")]
        public bool ResourceCategories { get; set; } = false;

        [JsonProperty("ResourceCategoryAssignments")]
        public bool ResourceCategoryAssignments { get; set; } = false;

        [JsonProperty("ResourceCategoryImageAssignments")]
        public bool ResourceCategoryImageAssignments { get; set; } = false;

        [JsonProperty("ResourceFeatures")]
        public bool ResourceFeatures { get; set; } = false;
        [JsonProperty("ResourceFeatureAssignments")]
        public bool ResourceFeatureAssignments { get; set; } = false;
        [JsonProperty("Inactive")]
        public bool Inactive { get; set; } = false;

        public DateTime StartUtc { get; set; }

        public DateTime EndUtc { get; set; }
        public String PromoCode { get; set; }
        public String[] ResourceIds { get; set; }
    }
}
