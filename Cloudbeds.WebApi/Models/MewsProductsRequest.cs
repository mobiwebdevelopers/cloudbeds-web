﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsProductsRequest : MewsToken
    {
        [JsonProperty("ServiceIds")]
        public string[] ServiceIds { get; set; }
    }
}
