﻿using System;

namespace Cloudbeds.WebApi.Models
{
    public class RoomsRequest
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Rooms { get; set; }

        public int Adults { get; set; }

        public int Children { get; set; }
    }
}
