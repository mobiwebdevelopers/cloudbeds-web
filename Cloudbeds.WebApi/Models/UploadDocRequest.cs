﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class UploadDocRequest
    {
        public string GuestID { get; set; }
        public string Image { get; set; }      

    }
}
