﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class Reservation
    {
        public string ConfirmationNumber { get; set; }
        public string ResvNameId { get; set; }
        public string Status { get; set; }
        public int DayCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildrenCount { get; set; }
        public string GuestName { get; set; }

        public string ServiceId { get; set; }
        public DateTime CheckInDate { get; set; }

        public DateTime CheckOutDate { get; set; }
        public List<Guest> Guests { get; set; }
        public List<GuestRoom> Rooms { get; set; }
        public decimal BaseRate { get; set; }
        public decimal TaxesAndFees { get; set; }
        public decimal Balance { get; set; }
        public decimal Total { get; set; }
        public string CurrencyCode { get; set; }
        public int BillingCount { get; set; }
        public List<Guest> BillingWindows { get; set; }
        public ValidationStatus Validation { get; set; }
    }
}
