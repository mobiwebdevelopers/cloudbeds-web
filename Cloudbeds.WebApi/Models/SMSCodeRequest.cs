﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class SMSCodeRequest
    {
        public string Phone { get; set; }
        public string Code { get; set; }
    }

    public class SMSRequest
    {
        public string Body { get; set; }
        public string Number { get; set; }
        public string DeviceKey { get; set; }
    }

    public class SMSResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Number { get; set; }
        public string Body { get; set; }
    }

    public class EmailCodeRequest
    {
        public string GuestEmail { get; set; }
        public string Code { get; set; }
    }
}
