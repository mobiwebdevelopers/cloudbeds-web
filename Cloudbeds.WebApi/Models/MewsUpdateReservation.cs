﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsReservationUpdate
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }

        [JsonProperty("StartUtc")]
        public string StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public string EndUtc { get; set; }
    }
    public class MewsUpdateReservation : MewsToken
    {
        [JsonProperty("ReservationUpdates")]
        public List<MewsReservationUpdate> ReservationUpdates { get; set; }

        [JsonProperty("Reason")]
        public string Reason { get; set; }

    }

}
