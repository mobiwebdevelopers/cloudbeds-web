﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsLimitation
    {
        [JsonProperty("Cursor")]
        public string Cursor { get; set; }
        [JsonProperty("Count")]
        public int Count { get; set; }

    }
    public class MewsToken
    {
        [JsonProperty("ClientToken")]
        public string ClientToken { get; set; }

        [JsonProperty("AccessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("Client")]
        public string Client { get; set; }
        [JsonProperty("Limitation")]
        public MewsLimitation Limitation { get; set; }
    }
}
