﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class GuestPhotoRequest
    {
        public string GuestID { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
    }
}
