﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class CartItem
    {
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string Sku { get; set; }
        public string ItemCode { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public bool StockInventory { get; set; }
        public int ItemQuantity { get; set; }
        public float TotalTaxes { get; set; }
        public float TotalFees { get; set; }
        public float PriceWithouFeesAndTaxes { get; set; }
        public float GrandTotal { get; set; }
    }
}
