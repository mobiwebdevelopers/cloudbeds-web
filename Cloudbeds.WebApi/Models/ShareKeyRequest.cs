﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class ShareKeyRequest
    {
        public string Email { get; set; }
        public string ReservationId { get; set; }
        public string Name { get; set; }
    }
}
