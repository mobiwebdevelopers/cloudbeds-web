﻿namespace Cloudbeds.WebApi.Models
{
    public class ReservationPaymentRequest : LookupReservationRequest
    {
        public string Type { get; set; }

        public decimal Amount { get; set; }

        public string CardType { get; set; }
    }
}
