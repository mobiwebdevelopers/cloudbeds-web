﻿using System;

namespace Cloudbeds.WebApi.Models
{
    public class RoomFeesRequest
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Adults { get; set; }

        public int Children { get; set; }

        public Room[] Rooms { get; set; }
    }
}
