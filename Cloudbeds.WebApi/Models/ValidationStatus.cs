﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class ValidationStatus
    {
        public string Status { get; set; }
        public string Message { get; set; }

    }
}
