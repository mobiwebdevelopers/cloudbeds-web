﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class ProductsResponse
    {
        [JsonProperty("data")]
        public CartItem[] Data { get; set; }
    }
}
