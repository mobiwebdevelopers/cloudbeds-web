﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsImage : MewsToken
    {
        public Image[] Images { get; set; }
    }

    public partial class Image
    {
        [JsonProperty("ImageId")]
        public string ImageId { get; set; }
        [JsonProperty("Width")]
        public int Width { get; set; }
        [JsonProperty("Height")]
        public int Height { get; set; }

        [JsonProperty("ResizeMode")]
        public string ResizeMode { get; set; }
    }
}
