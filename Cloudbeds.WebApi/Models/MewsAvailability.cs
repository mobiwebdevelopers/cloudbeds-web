﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsAvailability : MewsToken
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }
        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }
        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }
        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }
    }

    public partial class UpdateAvailabilityRequest : MewsToken
    {
        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("AvailabilityUpdates")]
        public AvailabilityUpdate[] AvailabilityUpdates { get; set; }
    }

    public partial class AvailabilityUpdate
    {
        [JsonProperty("StartUtc")]
        public string StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public string EndUtc { get; set; }

        [JsonProperty("ResourceCategoryId")]
        public string ResourceCategoryId { get; set; }

        [JsonProperty("UnitCountAdjustment")]
        public UnitCountAdjustment UnitCountAdjustment { get; set; }
    }

    public partial class UnitCountAdjustment
    {
        [JsonProperty("Value")]
        public long Value { get; set; }
    }

}
