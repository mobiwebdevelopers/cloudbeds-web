﻿namespace Cloudbeds.WebApi.Models
{
    public class GetReservationsRequest
    {
        public string roomName { get; set; }

        public string Status { get; set; }
        public string RoomID { get; set; }
    }
}
