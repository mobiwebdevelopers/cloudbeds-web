﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsPayment : MewsToken
    {
        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("Amount")]
        public AmountDetails Amount { get; set; }

        [JsonProperty("ExternalIdentifier")]
        public string ExternalIdentifier { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("AccountingCategoryId")]
        public string AccountingCategoryId { get; set; }

    }

    public class AmountDetails {

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("GrossValue")]
        public float GrossValue { get; set; }
    }
    public class MewsGetPaymentRequest : MewsToken
    {
        [JsonProperty("ReservationIds")]
        public List<string> ReservationIds { get; set; }


    }

}
