﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsCustomerUpdate : MewsToken
    {
        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Sex")]
        public string Sex { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("NationalityCode")]
        public string NationalityCode { get; set; }
    }

}
