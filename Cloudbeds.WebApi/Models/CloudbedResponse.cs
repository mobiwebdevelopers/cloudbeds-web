﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class CloudbedResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
    }
}
