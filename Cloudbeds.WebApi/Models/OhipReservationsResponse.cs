﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class OhipReservationsResponse
    {
        public OhipReservations reservations { get; set; }
    }
}
