﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class RoomsResponse
    {
        [JsonProperty("data")]
        public RoomData[] Data { get; set; }

        [JsonProperty("roomCount")]
        public int Count { get; set; }
    }
}
