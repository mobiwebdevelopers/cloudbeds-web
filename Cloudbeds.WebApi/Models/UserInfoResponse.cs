﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class UserInfoResponse
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
