﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsPricing : MewsToken
    {
        [JsonProperty("RateId")]
        public string RateId { get; set; }
        //[JsonProperty("ServiceId")]
        //public string ServiceId { get; set; }
        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }
        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }
    }

    public partial class PriceUpdateRequest : MewsToken
    {
        [JsonProperty("RateId")]
        public string RateId { get; set; }

        [JsonProperty("PriceUpdates")]
        public PriceUpdate[] PriceUpdates { get; set; }
    }

    public class PriceUpdate
    {
        [JsonProperty("FirstTimeUnitStartUtc")]
        public DateTimeOffset FirstTimeUnitStartUtc { get; set; }

        [JsonProperty("LastTimeUnitStartUtc")]
        public DateTimeOffset LastTimeUnitStartUtc { get; set; }

        [JsonProperty("Value")]
        public long Value { get; set; }

        [JsonProperty("CategoryId")]
        public Guid? CategoryId { get; set; }
    }
}
