﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class Customer
    {
        public long Id { get; set; }
        //public string MobileNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public UserAccount UserAccount { get; set; }

    }

}
