﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class AppointmentRequest
    {
        [JsonProperty("appointmentId")]
        public long AppointmentId { get; set; }

        [JsonProperty("customerId")]
        public long CustomerId { get; set; }

        [JsonProperty("locationId")]
        public long LocationId { get; set; }

        [JsonProperty("schedulerDate")]
        public DateTimeOffset SchedulerDate { get; set; }

        [JsonProperty("appointmentStatus")]
        public long AppointmentStatus { get; set; }

        [JsonProperty("guestTypeId")]
        public long GuestTypeId { get; set; }

        [JsonProperty("customerNotes")]
        public string CustomerNotes { get; set; }

        [JsonProperty("internalNotes")]
        public string InternalNotes { get; set; }

        [JsonProperty("appointmentFlags")]
        public string AppointmentFlags { get; set; }

        [JsonProperty("lines")]
        public AppointmentLine[] Lines { get; set; }

        [JsonProperty("notification")]
        public Notification Notification { get; set; }

        [JsonProperty("CreditCard")]
        public CreditCardInfo CreditCard { get; set; }

        [JsonProperty("bookingSource")]
        public long BookingSource { get; set; }

        [JsonProperty("saveCreditCard")]
        public bool SaveCreditCard { get; set; }

        [JsonProperty("couponNumber")]
        public string CouponNumber { get; set; }

        [JsonProperty("reminderStat")]
        public string ReminderStat { get; set; }

        [JsonProperty("tenderTypeId")]
        public long TenderTypeId { get; set; }

        [JsonProperty("pmsGuestInfo")]
        public PmsGuestInfo PmsGuestInfo { get; set; }

        [JsonProperty("sendGuaranteeLink")]
        public bool SendGuaranteeLink { get; set; }

        [JsonProperty("appointmentType")]
        public string AppointmentType { get; set; }

        [JsonProperty("guestInfo")]
        public GuestInfo GuestInfo { get; set; }

        [JsonProperty("confirmationId")]
        public long ConfirmationId { get; set; }

        [JsonProperty("guestReservationNum")]
        public string GuestReservationNum { get; set; }

        [JsonProperty("appointmentReferralId")]
        public long AppointmentReferralId { get; set; }

        [JsonProperty("activityNotes")]
        public object ActivityNotes { get; set; }

        [JsonProperty("isPreview")]
        public bool IsPreview { get; set; }

        [JsonProperty("applyDeposit")]
        public bool ApplyDeposit { get; set; }
    }

    public partial class GuestInfo
    {
        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("sendConfirmationEmail")]
        public bool SendConfirmationEmail { get; set; }
    }

    public partial class AppointmentLine
    {
        [JsonProperty("lineId")]
        public long LineId { get; set; }

        [JsonProperty("RequestTypeId")]
        public long RequestTypeId { get; set; }

        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("ServiceId")]
        public long ServiceId { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("prepareTime")]
        public long PrepareTime { get; set; }

        [JsonProperty("startTime")]
        public long StartTime { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("processTime")]
        public long ProcessTime { get; set; }

        [JsonProperty("cleanUpTime")]
        public long CleanUpTime { get; set; }

        [JsonProperty("equipmentId")]
        public long EquipmentId { get; set; }

        [JsonProperty("facilityId")]
        public long FacilityId { get; set; }

        [JsonProperty("isFacilityLocked")]
        public bool IsFacilityLocked { get; set; }

        [JsonProperty("addOnServiceId")]
        public object AddOnServiceId { get; set; }

        [JsonProperty("packageTemplateLineId")]
        public long PackageTemplateLineId { get; set; }

        [JsonProperty("overrideFacility")]
        public bool OverrideFacility { get; set; }
    }

    public partial class Notification
    {
        [JsonProperty("smsNotification")]
        public bool SmsNotification { get; set; }

        [JsonProperty("smsNotificationSet")]
        public bool SmsNotificationSet { get; set; }

        [JsonProperty("mobilePhone")]
        public string MobilePhone { get; set; }

        [JsonProperty("emailNotification")]
        public bool EmailNotification { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
    }

    public partial class PmsGuestInfo
    {
        [JsonProperty("reservationCode")]
        public string ReservationCode { get; set; }

        [JsonProperty("customerId")]
        public long CustomerId { get; set; }

        [JsonProperty("guestLastName")]
        public string GuestLastName { get; set; }
    }

    public partial class CreditCardInfo
    {
        [JsonProperty("CustomerId")]
        public int CustomerId { get; set; } = 0;

        [JsonProperty("LocationId")]
        public int LocationId { get; set; } = 6156344;

        [JsonProperty("CardType")]
        public string CardType { get; set; } = "Visa";

        [JsonProperty("CardFormat")]
        public string CardFormat { get; set; } = "1";

        [JsonProperty("CardNumber")]
        public string CardNumber { get; set; } = "4055011111111111";

        [JsonProperty("ExpireYear")]
        public string ExpireYear { get; set; } = "23";

        [JsonProperty("ExpireMonth")]
        public string ExpireMonth { get; set; } = "01";

        [JsonProperty("LastFourDigits")]
        public string LastFourDigits { get; set; } = "1111";
    }
}
