﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class UserAccount
    {
        public long UserId { get; set; }
        public bool IsActive { get; set; }
        public string LoginId { get; set; }
        public string MachineId { get; set; }
        public string Password { get; set; }
        public long LocationId { get; set; }
        public int AuthenticationType { get; set; }
        public int NumDaysTillTokenExp { get; set; }

    }

}
