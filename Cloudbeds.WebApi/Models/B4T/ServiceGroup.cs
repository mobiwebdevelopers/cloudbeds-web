﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class ServiceGroup
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ClassId")]
        public long ClassId { get; set; }

        [JsonProperty("Images")]
        public Image[] Images { get; set; }

        [JsonProperty("ColorCode")]
        public string ColorCode { get; set; }

        [JsonProperty("IconCssClass")]
        public string IconCssClass { get; set; }

        [JsonProperty("Rank")]
        public long Rank { get; set; }

        [JsonProperty("TotalServiceCount")]
        public long TotalServiceCount { get; set; }

        [JsonProperty("AddonServiceCount")]
        public long AddonServiceCount { get; set; }

        [JsonProperty("MemberOnlyServiceCount")]
        public long MemberOnlyServiceCount { get; set; }

        [JsonProperty("PackageServiceCount")]
        public long PackageServiceCount { get; set; }
    }

    public partial class Image
    {
        [JsonProperty("Path")]
        public Uri Path { get; set; }

        [JsonProperty("SortOrder")]
        public long SortOrder { get; set; }

        [JsonProperty("Caption")]
        public string Caption { get; set; }

        [JsonProperty("ImageType")]
        public long ImageType { get; set; }

        [JsonProperty("DeviceType")]
        public long DeviceType { get; set; }
    }
}
