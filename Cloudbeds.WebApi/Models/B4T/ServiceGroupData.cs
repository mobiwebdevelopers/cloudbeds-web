﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class ServiceGroupDataList
    {
        [JsonProperty("ServiceGroupData")]
        public List<ServiceGroupData> ServiceGroupData { get; set; }
    }

    public partial class ServiceGroupData
    {
        [JsonProperty("GroupId")]
        public long GroupId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("CategoryType")]
        public int CategoryType { get; set; }

        [JsonProperty("ServiceTypes")]
        public List<ServiceTypeData> ServiceTypes { get; set; }
    }

    public partial class ServiceTypeData
    {
        [JsonProperty("ServiceTypeId")]
        public long ServiceTypeId { get; set; }

        [JsonProperty("TypeName")]
        public string TypeName { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("ExtendedDescription")]
        public string ExtendedDescription { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("Schedules")]
        public List<ServiceTypeScheduleData> Schedules { get; set; }
    }

    public class ServiceTypeScheduleData
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("ServiceId")]
        public long ServiceId { get; set; } = 0;

        [JsonProperty("FacilityId")]
        public int FacilityId { get; set; }

        [JsonProperty("RequestTypeId")]
        public int RequestTypeId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("TechnicianName")]
        public string TechnicianName { get; set; }

        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("Schedule")]
        public string Schedule { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Day")]
        public int Day { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("StartTime")]
        public string StartTime { get; set; }

        [JsonProperty("DefaultPrice")]
        public decimal DefaultPrice { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("Added")]
        public bool Added { get; set; }

        [JsonProperty("FirstAdditional")]
        public bool FirstAdditional { get; set; }

        [JsonProperty("FullDate")]
        public DateTimeOffset FullDate { get; set; }
    }
}
