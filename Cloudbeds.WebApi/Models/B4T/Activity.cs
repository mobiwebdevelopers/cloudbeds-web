﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class Activity
    {
        [JsonProperty("AppointmentId")]
        public long AppointmentId { get; set; }

        [JsonProperty("Date")]
        public DateTime Date { get; set; }

        [JsonProperty("ServiceTypeId")]
        public long ServiceTypeId { get; set; }

        [JsonProperty("ServiceName")]
        public string ServiceName { get; set; }

        [JsonProperty("TechnicianName")]
        public string TechnicianName { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("FacilityName")]
        public string FacilityName { get; set; }

        [JsonProperty("Capacity")]
        public long Capacity { get; set; }

        [JsonProperty("TotalParticipant")]
        public long TotalParticipant { get; set; }

        [JsonProperty("Restrictions")]
        public object Restrictions { get; set; }

        [JsonProperty("ServiceGroupId")]
        public long ServiceGroupId { get; set; }

        [JsonProperty("ServiceGroupName")]
        public string ServiceGroupName { get; set; }

        [JsonProperty("ServiceColorCode")]
        public string ServiceColorCode { get; set; }

        [JsonProperty("LocationDefaultPrice")]
        public decimal LocationDefaultPrice { get; set; }

        [JsonProperty("ActivityImagePath")]
        public string ActivityImagePath { get; set; }

        [JsonProperty("LocationId")]
        public long LocationId { get; set; }

        [JsonProperty("LocationName")]
        public string LocationName { get; set; }

        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("ExtendedDescription")]
        public string ExtendedDescription { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Instructions")]
        public object Instructions { get; set; }

        [JsonProperty("Brand")]
        public Brand Brand { get; set; }

        [JsonProperty("StartTime")]
        public long StartTime { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }
    }

    public partial class Brand
    {
        [JsonProperty("BrandId")]
        public long BrandId { get; set; }

        [JsonProperty("BrandName")]
        public string BrandName { get; set; }
    }
}
