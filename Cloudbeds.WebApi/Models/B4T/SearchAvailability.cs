﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class SearchAvailabilityRequest
    {
        [JsonProperty("AppointmentId")]
        public long AppointmentId { get; set; } = 0;

        [JsonProperty("IsOnline")]
        public bool IsOnline { get; set; } = true;

        [JsonProperty("locationId")]
        public long LocationId { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("StartTime")]
        public long StartTime { get; set; } = 0;

        [JsonProperty("EndTime")]
        public long EndTime { get; set; } = -1;

        [JsonProperty("MaxResult")]
        public long MaxResult { get; set; } = -1;

        [JsonProperty("AllowSkillLevel")]
        public bool AllowSkillLevel { get; set; }

        [JsonProperty("CustomerId")]
        public long CustomerId { get; set; } = 0;

        [JsonProperty("SortOption")]
        public string SortOption { get; set; } = "p";

        [JsonProperty("Source")]
        public long Source { get; set; } = 4;

        [JsonProperty("AppointmentFlags")]
        public string AppointmentFlags { get; set; }

        [JsonProperty("SuggestAlternateTime")]
        public bool SuggestAlternateTime { get; set; }

        [JsonProperty("SearchNextAvailableDate")]
        public bool SearchNextAvailableDate { get; set; }

        [JsonProperty("FlexibleTimeSelected")]
        public bool FlexibleTimeSelected { get; set; }

        [JsonProperty("Lines")]
        public Line[] Lines { get; set; }
    }

    public partial class Line
    {
        [JsonProperty("RequestTypeId")]
        public long RequestTypeId { get; set; }

        [JsonProperty("StartTime")]
        public long StartTime { get; set; } = -1;

        [JsonProperty("Gap")]
        public long Gap { get; set; } = 0;

        [JsonProperty("ServiceId")]
        public long ServiceId { get; set; }

        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("SkillLevelId")]
        public long SkillLevelId { get; set; } = -1;

        [JsonProperty("PackageLineId")]
        public long PackageLineId { get; set; } = -1;

        [JsonProperty("AddonServiceId")]
        public object AddonServiceId { get; set; } = null;

        [JsonProperty("Duration")]
        public long Duration { get; set; } = 0;

        [JsonProperty("IgnoreFacility")]
        public bool IgnoreFacility { get; set; }

        [JsonProperty("IgnoreEquipment")]
        public bool IgnoreEquipment { get; set; }

        [JsonProperty("FacilityId")]
        public long FacilityId { get; set; } = 0;
    }



    public partial class SearchAvailabilityResponse
    {
        [JsonProperty("StartTime")]
        public long StartTime { get; set; }

        [JsonProperty("AvailableDate")]
        public DateTimeOffset AvailableDate { get; set; }

        [JsonProperty("Results")]
        public Result[] Results { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("TechnicianName")]
        public string TechnicianName { get; set; }

        [JsonProperty("StartTime")]
        public long StartTime { get; set; }

        [JsonProperty("Price")]
        public long Price { get; set; }

        [JsonProperty("Duration")]
        public int Duration { get; set; }

        [JsonProperty("Cid")]
        public long Cid { get; set; }

        [JsonProperty("Technician")]
        public Technician Technician { get; set; }

        [JsonProperty("PreTime")]
        public long PreTime { get; set; }

        [JsonProperty("AfterTime")]
        public long AfterTime { get; set; }

        [JsonProperty("ProcessTime")]
        public long ProcessTime { get; set; }

        [JsonProperty("ServiceId")]
        public long ServiceId { get; set; }

        [JsonProperty("FacilityId")]
        public long FacilityId { get; set; }

        [JsonProperty("EquipmentId")]
        public long EquipmentId { get; set; }

        [JsonProperty("AddOns")]
        public object AddOns { get; set; }
    }   
}
