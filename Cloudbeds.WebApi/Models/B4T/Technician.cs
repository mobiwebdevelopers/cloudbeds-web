﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class Technician
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Active")]
        public bool Active { get; set; }

        [JsonProperty("FullNameDisplayed")]
        public string FullNameDisplayed { get; set; }

        [JsonProperty("FirstNameDisplayed")]
        public string FirstNameDisplayed { get; set; }

        [JsonProperty("Gender")]
        public string Gender { get; set; }

        [JsonProperty("Price")]
        public long Price { get; set; }

        [JsonProperty("OnlineEnabled")]
        public bool OnlineEnabled { get; set; }

        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("LocationId")]
        public long LocationId { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("ShortCode")]
        public string ShortCode { get; set; }

        [JsonProperty("SkillLevelId")]
        public int SkillLevelId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }

    }

    public class TechnicianList
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Id")]
        public long Id { get; set; }
    }

    public class TechnicianSchedule
    {
        [JsonProperty("TechnicianId")]
        public long TechnicianId { get; set; }

        [JsonProperty("FullNameDisplayed")]
        public string FullNameDisplayed { get; set; }

        [JsonProperty("FirstNameDisplayed")]
        public string FirstNameDisplayed { get; set; }

        [JsonProperty("Gender")]
        public string Gender { get; set; }

        [JsonProperty("OnlineEnabled")]
        public bool OnlineEnabled { get; set; }

        [JsonProperty("Date")]
        public DateTime Date { get; set; }

        [JsonProperty("StartTime")]
        public int StartTime { get; set; }

        [JsonProperty("EndTime")]
        public int EndTime { get; set; }

        [JsonProperty("Price")]
        public string Price { get; set; }
    }

}
