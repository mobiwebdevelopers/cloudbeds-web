﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class UserResponse
    {
        public long UserId { get; set; }
        public int ReturnCodeValue { get; set; }
        public string Token { get; set; }

    }

}
