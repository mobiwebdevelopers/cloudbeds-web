﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models.B4T
{
    public partial class ServiceActivity
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("GroupId")]
        public long GroupId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Price")]
        public long Price { get; set; }

        [JsonProperty("OriginalPrice")]
        public long OriginalPrice { get; set; }

        [JsonProperty("Duration")]
        public long Duration { get; set; }

        [JsonProperty("PreDuration")]
        public long PreDuration { get; set; }

        [JsonProperty("AfterDuration")]
        public long AfterDuration { get; set; }

        [JsonProperty("InitialDuration")]
        public long InitialDuration { get; set; }

        [JsonProperty("ProcessTime")]
        public long ProcessTime { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("ExtendedDescription")]
        public string ExtendedDescription { get; set; }

        [JsonProperty("WarningNotes")]
        public string WarningNotes { get; set; }

        [JsonProperty("ExtendedWarningNotes")]
        public string ExtendedWarningNotes { get; set; }

        [JsonProperty("ExtendedSuggestion")]
        public string ExtendedSuggestion { get; set; }

        [JsonProperty("ShowFootNote")]
        public bool ShowFootNote { get; set; }

        [JsonProperty("CallToBook")]
        public bool CallToBook { get; set; }

        [JsonProperty("NoRequestTypeOnline")]
        public bool NoRequestTypeOnline { get; set; }

        [JsonProperty("OneAddOn")]
        public bool OneAddOn { get; set; }

        [JsonProperty("ComplimentaryServiceId")]
        public long ComplimentaryServiceId { get; set; }

        [JsonProperty("ContentType")]
        public long ContentType { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("MaxServiceCount")]
        public long MaxServiceCount { get; set; }

        [JsonProperty("ImagesCollection")]
        public object ImagesCollection { get; set; }

        [JsonProperty("ServiceGroup")]
        public ServiceGroup ServiceGroup { get; set; }

        [JsonProperty("IsEquipmentRequire")]
        public bool IsEquipmentRequire { get; set; }

        [JsonProperty("IsFacilityRequire")]
        public bool IsFacilityRequire { get; set; }

        [JsonProperty("IsActivityService")]
        public bool IsActivityService { get; set; }

        [JsonProperty("HasPriceLevel")]
        public bool HasPriceLevel { get; set; }

        [JsonProperty("PriceLevels")]
        public object PriceLevels { get; set; }

        [JsonProperty("ServiceImagePath")]
        public Uri ServiceImagePath { get; set; }

        [JsonProperty("MemberOnly")]
        public bool MemberOnly { get; set; }

        [JsonProperty("ProductId")]
        public long ProductId { get; set; }

        [JsonProperty("ActivityNoteId")]
        public long ActivityNoteId { get; set; }

        [JsonProperty("FacilityTypes")]
        public object FacilityTypes { get; set; }

        [JsonProperty("EquipmentTypes")]
        public object EquipmentTypes { get; set; }
    }
}
