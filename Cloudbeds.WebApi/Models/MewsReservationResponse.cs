﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsReservationResponse : MewsToken
    {
        [JsonProperty("Reservations")]
        public List<ResDetails> Reservations { get; set; }
    }


}
