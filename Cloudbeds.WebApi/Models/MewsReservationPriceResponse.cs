﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class PriceAmount
    {
        [JsonProperty("Currency")]
        public string Currency { get; set; }
        [JsonProperty("NetValue")]
        public decimal NetValue { get; set; }
        [JsonProperty("GrossValue")]
        public decimal GrossValue { get; set; }
        [JsonProperty("Tax")]
        public decimal Tax { get; set; }
    }
    public class ReservationPrice
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }
        [JsonProperty("OrderItems")]
        public List<PriceItem> OrderItems { get; set; }
        [JsonProperty("Items")]
        public List<PriceItem> Items { get; set; }
    }
    public class PriceItem
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("OrderId")]
        public string OrderId { get; set; }

        [JsonProperty("Name")]
        public string? Name { get; set; }

        [JsonProperty("Amount")]
        public PriceAmount Amount { get; set; }

        [JsonProperty("ConsumptionUTC")]
        public DateTime? ConsumptionUTC { get; set; }
    }
    public class MewsOIData_Product
    {
        [JsonProperty("ProductId")]
        public string ProductId { get; set; }
    }
    public class MewsOIData
    {
        [JsonProperty("Product")]
        public MewsOIData_Product Product { get; set; }
    }
    public class OrderItem
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("RevenueType")]
        public string RevenueType { get; set; }

        [JsonProperty("AccountingState")]
        public string AccountingState { get; set; }

        [JsonProperty("UnitCount")]
        public int UnitCount { get; set; }
        public int Count { get; set; }

        [JsonProperty("Amount")]
        public PriceAmount Amount { get; set; }

        [JsonProperty("ConsumptionUTC")]
        public DateTime? ConsumptionUTC { get; set; }

        [JsonProperty("Data")]
        public MewsOIData Data { get; set; }
    }
    public class MewsReservationPriceResponse : MewsToken
    {
        [JsonProperty("Reservations")]
        public List<ReservationPrice> Reservations { get; set; }
    }
    public class MewsOrderItemsPriceResponse : MewsToken
    {
        [JsonProperty("OrderItems")]
        public List<OrderItem> OrderItems { get; set; }
    }


}
