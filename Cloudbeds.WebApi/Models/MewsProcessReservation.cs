﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsProcessReservation : MewsStartReservation
    {
        [JsonProperty("CloseBills")]
        public bool CloseBills { get; set; }

        [JsonProperty("AllowOpenBalance")]
        public bool AllowOpenBalance { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

}
