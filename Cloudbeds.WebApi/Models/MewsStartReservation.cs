﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsStartReservation : MewsToken
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }
    }

}
