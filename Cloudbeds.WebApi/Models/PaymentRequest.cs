﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class PaymentRequest
    {
        public string paymentType { get; set; }
        public float amount { get; set; }
    }
}
