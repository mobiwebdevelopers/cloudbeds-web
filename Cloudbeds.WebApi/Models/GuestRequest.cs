﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class GuestRequest
    {
        public string guestID { get; set; }
        public string reservationID { get; set; }
        public string guestFirstName { get; set; }
        public string guestLastName { get; set; }
        public string guestEmail { get; set; }
        public string guestPhone { get; set; }
        public string guestCountry { get; set; }
    }
}
