﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class ResourceResponse
    {
        [JsonProperty("Resources")]
        public Resource[] Resources { get; set; }

        [JsonProperty("ResourceCategories")]
        public ResourceCategory[] ResourceCategories { get; set; }

        [JsonProperty("ResourceCategoryAssignments")]
        public ResourceCategoryAssignment[] ResourceCategoryAssignments { get; set; }

        [JsonProperty("ResourceCategoryImageAssignments")]
        public ResourceCategoryImageAssignments[] ResourceCategoryImageAssignments { get; set; }

        [JsonProperty("ResourceFeatures")]
        public object[] ResourceFeatures { get; set; }

        [JsonProperty("ResourceFeatureAssignments")]
        public object[] ResourceFeatureAssignments { get; set; }
    }

    public partial class ResourceCategory
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("ServiceId")]
        public Guid ServiceId { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Names")]
        public Descriptions Names { get; set; }

        [JsonProperty("ShortNames")]
        public Descriptions ShortNames { get; set; }

        [JsonProperty("Descriptions")]
        public Descriptions Descriptions { get; set; }

        [JsonProperty("Ordering")]
        public long Ordering { get; set; }

        [JsonProperty("Capacity")]
        public long Capacity { get; set; }

        [JsonProperty("ExtraCapacity")]
        public long ExtraCapacity { get; set; }
    }

    public partial class Descriptions
    {
        [JsonProperty("en-GB")]
        public string EnGb { get; set; }

        [JsonProperty("en-US")]
        public string EnUs { get; set; }
    }

    public partial class ResourceCategoryAssignment
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("ResourceId", NullValueHandling = NullValueHandling.Ignore)]
        public Guid ResourceId { get; set; }

        [JsonProperty("CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty("CreatedUtc")]
        public DateTimeOffset CreatedUtc { get; set; }

        [JsonProperty("UpdatedUtc")]
        public DateTimeOffset UpdatedUtc { get; set; }

        [JsonProperty("ImageId", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? ImageId { get; set; }
    }

    public partial class Resource
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("ParentResourceId")]
        public Guid? ParentResourceId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("Descriptions")]
        public object Descriptions { get; set; }

        [JsonProperty("CreatedUtc")]
        public DateTimeOffset CreatedUtc { get; set; }

        [JsonProperty("UpdatedUtc")]
        public DateTimeOffset UpdatedUtc { get; set; }

        [JsonProperty("Data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("Discriminator")]
        public string Discriminator { get; set; }

        [JsonProperty("Value")]
        public Value Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("FloorNumber")]
        public string FloorNumber { get; set; }

        [JsonProperty("LocationNotes")]
        public string LocationNotes { get; set; }
    }


    public class RoomDTO
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("ImageId")]
        public Guid? ImageId { get; set; }

        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("CategoryId")]
        public string CategoryId { get; set; }

        [JsonProperty("ResourceId")]
        public string ResourceId { get; set; }

        [JsonProperty("RateId")]
        public string RateId { get; set; }

        [JsonProperty("BasePrices")]
        public decimal[] BasePrices { get; set; }

        [JsonProperty("Dates")]
        public DateTimeOffset[] Dates { get; set; }

        [JsonProperty("Capacity")]
        public long Capacity { get; set; }

        [JsonProperty("RoomData")]
        public string RoomData { get; set; }

        [JsonProperty("MenuData")]
        public string MenuData { get; set; }

        [JsonProperty("Sqft")]
        public long Sqft { get; set; }

        [JsonProperty("Images")]
        public List<string> Images { get; set; }

        [JsonProperty("Prices")]
        public List<RatePrice> Prices { get; set; }

        public List<RateGroup> RateGroups { get; set; }
        public List<Rate> Rates { get; set; }
    }

    public class GuestData
    {
        [JsonProperty("UserData")]
        public string UserData { get; set; }

        [JsonProperty("RoomData")]
        public string RoomData { get; set; }

        [JsonProperty("MenuData")]
        public string MenuData { get; set; }
    }

    public class ResourceCategoryImageAssignments
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty("ImageId")]
        public Guid ImageId { get; set; }

        [JsonProperty("CreatedUtc")]
        public DateTime CreatedUtc { get; set; }

        [JsonProperty("UpdatedUyc")]
        public DateTime UpdatedUyc { get; set; }

    }

    public class ImageUrlsData
    {
        [JsonProperty("ImageUrls")]
        public List<ImageUrls> ImageUrls;
    }

    public class ImageUrls
    {
        [JsonProperty("ImageId")]
        public string ImageId { get; set; }

        [JsonProperty("Url")]
        public string Url { get; set; }
    }
}
