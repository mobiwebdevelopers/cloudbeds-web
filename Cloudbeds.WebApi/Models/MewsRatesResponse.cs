﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public partial class RateResponse
    {
        [JsonProperty("Rates")]
        public Rate[] Rates { get; set; }

        [JsonProperty("RateGroups")]
        public RateGroup[] RateGroups { get; set; }
    }

    public partial class RateGroup
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("ServiceId")]
        public Guid ServiceId { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ExternalIdentifier")]
        public string ExternalIdentifier { get; set; }
    }

    public partial class Rate
    {
        [JsonProperty("BaseRateId")]
        public object BaseRateId { get; set; }
        public List<RatePrice> Prices { get; set; }
        public Decimal Price { get; set; }
        public Guid RateId { get; set; }

        [JsonProperty("GroupId")]
        public Guid GroupId { get; set; }

        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("ServiceId")]
        public Guid ServiceId { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("IsEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("IsPublic")]
        public bool IsPublic { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ShortName")]
        public string ShortName { get; set; }

        [JsonProperty("ExternalNames")]
        public ExternalNames ExternalNames { get; set; }
        
        [JsonProperty("Description")]
        public ExternalNames Description { get; set; }
    }

    public partial class ExternalNames
    {
        [JsonProperty("en-US")]
        public string EnUs { get; set; }
    }
}
