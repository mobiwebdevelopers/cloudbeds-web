﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class RoomFeesResponse
    {
        [JsonProperty("data")]
        public RoomFee[] Data { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }
    }
}
