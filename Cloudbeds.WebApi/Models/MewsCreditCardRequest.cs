﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace Cloudbeds.WebApi.Models
{
    public class MewsGetCreditCardRequest : MewsToken
    {
        [JsonProperty("CustomerIds")]
        public string[] CustomerIds { get; set; }
    }
    public class MewsCreditCardRequest : MewsToken
    {
        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("CreditCardData")]
        public CreditCardData CreditCardData { get; set; }
    }

    public class MewsCreditCardResponse
    {
        [JsonProperty("CreditCards")]
        public List<CreditCard> CreditCards { get; set; }

    }
    public class CreditCard
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }
        [JsonProperty("Expiration")]
        public string Expiration { get; set; }
        [JsonProperty("Format")]
        public string Format { get; set; }
        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }
        [JsonProperty("ObjuscatedNumber")]
        public string ObjuscatedNumber { get; set; }
        [JsonProperty("State")]
        public string State { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }

    }

    public class CreditCardData
    {
        [JsonProperty("StorageData")]
        public string StorageData { get; set; }

        //[JsonProperty("ObfuscatedNumber")]
        //public string ObfuscatedNumber { get; set; }

        [JsonProperty("Expiration")]
        public string Expiration { get; set; }
    }
}
