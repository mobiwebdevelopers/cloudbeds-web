﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class ShoppingCartRequest
    {
        public int ReservationId { get; set; }
    }
}
