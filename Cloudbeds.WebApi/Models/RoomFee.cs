﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class RoomFee
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("taxID")]
        public string TypeId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("amountType")]
        public string AmountType { get; set; }

        [JsonProperty("availableFor")]
        public string[] AvailableFor { get; set; }
    }
}
