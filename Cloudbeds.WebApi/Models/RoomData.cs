﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class RoomData
    {
        [JsonProperty("propertyRooms")]
        public Room[] Rooms { get; set; }
    }
}
