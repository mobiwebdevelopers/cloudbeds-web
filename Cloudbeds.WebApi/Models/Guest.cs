﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class Guest
    {
        public string ProfileId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public Address HomeAddress { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    }
}
