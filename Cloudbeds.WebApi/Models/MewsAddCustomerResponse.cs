﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class MewsAddCustomerResponse
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("Number")]
        public long Number { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Sex")]
        public string Sex { get; set; }

        [JsonProperty("Gender")]
        public string Gender { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("SecondLastName")]
        public object SecondLastName { get; set; }

        [JsonProperty("NationalityCode")]
        public string NationalityCode { get; set; }

        [JsonProperty("LanguageCode")]
        public string LanguageCode { get; set; }

        [JsonProperty("BirthDate")]
        public object BirthDate { get; set; }

        [JsonProperty("BirthPlace")]
        public object BirthPlace { get; set; }

        [JsonProperty("CitizenNumber")]
        public object CitizenNumber { get; set; }

        [JsonProperty("MotherName")]
        public object MotherName { get; set; }

        [JsonProperty("FatherName")]
        public object FatherName { get; set; }

        [JsonProperty("Occupation")]
        public object Occupation { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("TaxIdentificationNumber")]
        public object TaxIdentificationNumber { get; set; }

        [JsonProperty("LoyaltyCode")]
        public object LoyaltyCode { get; set; }

        [JsonProperty("AccountingCode")]
        public object AccountingCode { get; set; }

        [JsonProperty("BillingCode")]
        public object BillingCode { get; set; }

        [JsonProperty("Notes")]
        public object Notes { get; set; }

        [JsonProperty("CarRegistrationNumber")]
        public object CarRegistrationNumber { get; set; }

        [JsonProperty("CreatedUtc")]
        public DateTimeOffset CreatedUtc { get; set; }

        [JsonProperty("UpdatedUtc")]
        public DateTimeOffset UpdatedUtc { get; set; }

        [JsonProperty("Passport")]
        public object Passport { get; set; }

        [JsonProperty("IdentityCard")]
        public object IdentityCard { get; set; }

        [JsonProperty("Visa")]
        public object Visa { get; set; }

        [JsonProperty("DriversLicense")]
        public object DriversLicense { get; set; }

        [JsonProperty("Address")]
        public object Address { get; set; }

        [JsonProperty("Classifications")]
        public object[] Classifications { get; set; }

        [JsonProperty("Options")]
        public string[] Options { get; set; }

        [JsonProperty("CategoryId")]
        public object CategoryId { get; set; }

        [JsonProperty("BirthDateUtc")]
        public object BirthDateUtc { get; set; }

        [JsonProperty("ItalianDestinationCode")]
        public object ItalianDestinationCode { get; set; }

        [JsonProperty("ItalianFiscalCode")]
        public object ItalianFiscalCode { get; set; }

        [JsonProperty("CompanyId")]
        public object CompanyId { get; set; }

        [JsonProperty("MergeTargetId")]
        public object MergeTargetId { get; set; }

        [JsonProperty("ActivityState")]
        public string ActivityState { get; set; }
    }
    public partial class MewsAddCompanionResponse
    {
        [JsonProperty("CompanionshipId")]
        public string CompanionshipId { get; set; }

    }
}
