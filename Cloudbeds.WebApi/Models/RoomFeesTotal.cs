﻿namespace Cloudbeds.WebApi.Models
{
    public class RoomFeesTotal
    {
        public decimal Fees { get; set; }

        public decimal Total { get; set; }
    }
}
