﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsReservation : MewsToken
    {
        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("GroupId")]
        public string GroupId { get; set; }

        [JsonProperty("GroupName")]
        public string GroupName { get; set; }

        [JsonProperty("SendConfirmationEmail")]
        public bool SendConfirmationEmail { get; set; }

        [JsonProperty("Reservations")]
        public List<ResDetails> Reservations { get; set; }
    }
    public class MewsPriceReservation : MewsToken
    {
        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("Reservations")]
        public List<ResDetailsPrice> Reservations { get; set; }
    }
    public class MewsPerson
    {
        [JsonProperty("AgeCategoryId")]
        public string AgeCategoryId { get; set; }

        [JsonProperty("Count")]
        public int Count { get; set; }
    }
    public class ResDetails {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("AccountId")]
        public string AccountId { get; set; }
        [JsonProperty("AccountType")]
        public string AccountType { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("AdultCount")]
        public int AdultCount { get; set; }

        [JsonProperty("ChildCount")]
        public int ChildCount { get; set; }

        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("BookerId")]
        public string BookerId { get; set; }

        [JsonProperty("RequestedCategoryId")]
        public string RequestedCategoryId { get; set; }

        [JsonProperty("RequestedResourceCategoryId")]
        public string RequestedResourceCategoryId { get; set; }
        [JsonProperty("AssignedResourceId")]
        public string AssignedResourceId { get; set; }

        [JsonProperty("RateId")]
        public string RateId { get; set; }

        [JsonProperty("CreditCardId")]
        public string CreditCardId { get; set; }

        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }

        [JsonProperty("Number")]
        public string ReservationNumber { get; set; }

        [JsonProperty("NetAmount")]
        public decimal NetAmount { get; set; }
        //[JsonIgnore]
        [JsonProperty("GrossAmount")]
        public decimal GrossAmount { get; set; }
        [JsonProperty("Tax")]
        public decimal Tax { get; set; }
        //[JsonIgnore]
        [JsonProperty("TotalNights")]
        public int TotalNights { get; set; }

        [JsonProperty("OrderItems")]
        public List<OrderItem> OrderItems { get; set; }

        [JsonProperty("Rooms")]
        public List<RoomDTO> Rooms { get; set; }

        [JsonProperty("Items")]
        public List<PriceItem> Items { get; set; }
        [JsonProperty("ProductOrders")]
        public List<Product> ProductOrders { get; set; }
        [JsonProperty("VoucherCode")]
        public string VoucherCode { get; set; }

        [JsonProperty("Ready")]
        public bool Ready { get; set; }

        [JsonProperty("PersonCounts")]
        public List<MewsPerson> PersonCounts { get; set; }
        [JsonProperty("AssignedResourceLocked")]
        public bool AssignedResourceLocked { get; set; }

    }
    public class ResDetailsPrice
    {
        [JsonProperty("Identifier")]
        public string Identifier { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("PersonCounts")]
        public List<MewsPerson> PersonCounts { get; set; }

        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("BookerId")]
        public string BookerId { get; set; }

        [JsonProperty("RequestedCategoryId")]
        public string RequestedCategoryId { get; set; }

        [JsonProperty("RateId")]
        public string RateId { get; set; }

        [JsonProperty("VoucherCode")]
        public string VoucherCode { get; set; }

        [JsonProperty("CreditCardId")]
        public string CreditCardId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }

        [JsonProperty("TimeUnitAmount")]
        public MewsPrice TimeUnitAmount { get; set; }
        //[JsonIgnore]
        [JsonProperty("TimeUnitPrices")]
        public MewsTimeUnit TimeUnitPrices { get; set; }

        [JsonProperty("ProductOrders")]
        public List<Product> ProductOrders { get; set; }

    }

    public class ConfirmReservationRequest : MewsToken
    {
        [JsonProperty("ReservationIds")]
        public string[] ReservationIds { get; set; }
    }
    public class ReservationAddResult_Reservation
    {
        public ResDetails Reservation { get; set; }

    }
    public class ReservationAddResult
    {
        public List<ReservationAddResult_Reservation> Reservations { get; set; }
    }
}
