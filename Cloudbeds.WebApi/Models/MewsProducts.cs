﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class MewsName
    {
        [JsonProperty("en-US")]
        public string Name { get; set; }

    }
    public class MewsProduct
    {
        public string Id { get; set; }
        public string ServiceId { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string PostingMode { get; set; }
        public MewsPrice Price { get; set; }
        public string ExternalIdentifier { get; set; }
    }
    public class MewsProducts
    {

        public List<MewsProduct> Products { get; set; }
        public List<MewsProduct> CustomerProducts { get; set; }
    }


}
