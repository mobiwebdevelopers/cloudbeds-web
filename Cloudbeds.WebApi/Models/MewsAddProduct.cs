﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class MewsAddProduct
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("Products")]
        public Product2[] Products { get; set; }
    }

    public partial class Product
    {
        [JsonProperty("ProductId")]
        public string ProductId { get; set; }

        [JsonProperty("Count")]
        public long Count { get; set; }

        [JsonProperty("UnitAmount")]
        public decimal UnitAmount { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }
    }
    public partial class Product2
    {
        [JsonProperty("ProductId")]
        public string ProductId { get; set; }

        [JsonProperty("Count")]
        public long Count { get; set; }

        [JsonProperty("UnitAmount")]
        public UnitAmount UnitAmount { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }
    }


    public partial class MewsAddProductRequest: MewsToken
    {
        [JsonProperty("ReservationId")]
        public string ReservationId { get; set; }

        [JsonProperty("ProductId")]
        public string ProductId { get; set; }

        [JsonProperty("Count")]
        public long Count { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("UnitAmount")]
        public UnitAmount UnitAmount { get; set; }
    }

    public partial class UnitAmount
    {
        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("GrossValue")]
        public decimal GrossValue { get; set; }

        [JsonProperty("TaxCodes")]
        public string[] TaxCodes { get; set; }
    }

}
