﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsFileRequest : MewsToken
    {
        [JsonProperty("CustomerId")]
        public Guid CustomerId { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Data")]
        public string Data { get; set; }
    }

}
