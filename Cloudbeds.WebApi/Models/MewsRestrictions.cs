﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace Cloudbeds.WebApi.Models
{
    public class MewsRestrictionsRequest : MewsToken
    {
        [JsonProperty("ServiceIds")]
        public List<string> ServiceIds { get; set; }

    }

    public partial class Restriction
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("ServiceId")]
        public string ServiceId { get; set; }

        [JsonProperty("Identifier")]
        public long Identifier { get; set; }

        [JsonProperty("ExternalIdentifier")]
        public long ExternalIdentifier { get; set; }

        [JsonProperty("Conditions")]
        public Conditions Conditions { get; set; }

        [JsonProperty("Exceptions")]
        public Exceptions Exceptions { get; set; }
    }

    public partial class Conditions
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("BaseRateId")]
        public string BaseRateId { get; set; }

        [JsonProperty("ResourceCategoryId")]
        public string ResourceCategoryId { get; set; }

        [JsonProperty("Days")]
        public string[] Days { get; set; }
    }

    public partial class Exceptions
    {
        [JsonProperty("MinLength")]
        public string MinLength { get; set; }

        [JsonProperty("MaxLength")]
        public string MaxLength { get; set; }
    }
}
