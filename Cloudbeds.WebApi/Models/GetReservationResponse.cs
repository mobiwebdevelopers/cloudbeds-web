﻿namespace Cloudbeds.WebApi.Models
{
    public class GetReservationResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public ReservationData Data { get; set; }
    }
}
