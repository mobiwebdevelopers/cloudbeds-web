﻿using System;

namespace Cloudbeds.WebApi.Models
{
    public class ItemRequest
    {
        public string reservationID { get; set; }
        public int itemID { get; set; }
        public int itemQuantity { get; set; }
        public bool itemPaid { get; set; }
        public PaymentRequest[] payments { get; set; }


    }
}
