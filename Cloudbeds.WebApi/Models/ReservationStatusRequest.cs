﻿namespace Cloudbeds.WebApi.Models
{
    public class ReservationStatusRequest : LookupReservationRequest
    {
        public string Status { get; set; }
    }
}
