﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsRates : MewsToken
    {
        [JsonProperty("ServiceIds")]
        public string[] ServiceIds { get; set; }

        [JsonProperty("Extent")]
        public MewsExtentRates Extent { get; set; }

    }

    public class MewsExtentRates {

        [JsonProperty("Rates")]
        public bool Rates { get; set; }

        [JsonProperty("RateGroups")]
        public bool RateGroups { get; set; }

        [JsonProperty("RateRestrictions")]
        public bool RateRestrictions { get; set; } = false;

    }
}
