﻿namespace Cloudbeds.WebApi.Models
{
    public class VoidReservationPaymentRequest : LookupReservationRequest
    {
        public int PaymentID { get; set; }
    }
}
