﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsCancelReservation : MewsToken
    {
        [JsonProperty("ReservationIds")]
        public List<string> ReservationIds { get; set; }

        [JsonProperty("PostCancellationFee")]
        public bool PostCancellationFee { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

}
