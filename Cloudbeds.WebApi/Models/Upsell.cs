﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cloudbeds.WebApi.Models
{
    public class Upsell
    {
        public string ReservationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ServiceId { get; set; }
        public string CategoryId { get; set; }
        public string ResourceId { get; set; }
        public string RateId { get; set; }
        public double Price { get; set; }
        public double BasePrice { get; set; }
        public double TaxesAndFees { get; set; }
        public double Total { get; set; }
        public bool Daily { get; set; }
        public bool isProduct { get; set; }
    }
}
