﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class MewsGetCustomerResponse
    {
        [JsonProperty("Customers")]
        public List<MewsGetCustomer> Customers { get; set; }

        [JsonProperty("Documents")]
        public List<MewsCustomerDocument> Documents { get; set; }
    }

    public partial class MewsGetCustomer : MewsAddCustomerResponse
    {

    }

    public partial class MewsCustomerAddress
    {
        [JsonProperty("Line1")]
        public string Line1 { get; set; }

        [JsonProperty("Line2")]
        public object Line2 { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("PostalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("Latitude")]
        public object Latitude { get; set; }

        [JsonProperty("Longitude")]
        public object Longitude { get; set; }
    }

    public partial class MewsCustomerDocument
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("CustomerId")]
        public Guid CustomerId { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Number")]
        public long Number { get; set; }

        [JsonProperty("Expiration")]
        public DateTimeOffset Expiration { get; set; }

        [JsonProperty("Issuance")]
        public DateTimeOffset Issuance { get; set; }

        [JsonProperty("IssuingCountryCode")]
        public string IssuingCountryCode { get; set; }
    }
}
