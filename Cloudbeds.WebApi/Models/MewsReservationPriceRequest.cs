﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class ResPriceDetails
    {
        [JsonProperty("Identifier")]
        public string Identifier { get; set; }

        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

        [JsonProperty("RateId")]
        public string RateId { get; set; }
        [JsonProperty("RequestedCategoryId")]
        public string RequestedCategoryId { get; set; }

    }

    public class MewsReservationPriceRequest : MewsToken
    {
        [JsonProperty("AccountingStates")]
        public string ServiceId { get; set; }

        [JsonProperty("ServiceOrderIds")]
        public List<string> ServiceOrderIds { get; set; }
        //[JsonProperty("OrderItemIds")]
        //public List<string> OrderItemIds { get; set; }

    }


}
