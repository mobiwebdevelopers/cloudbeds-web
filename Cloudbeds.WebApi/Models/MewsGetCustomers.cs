﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsGetCustomers : MewsToken
    {
        [JsonProperty("Emails")]
        public string[] Emails { get; set; }

        [JsonProperty("CustomerIds")]
        public string[] CustomerIds { get; set; }

        [JsonProperty("FirstNames")]
        public string[] FirstNames { get; set; }

        [JsonProperty("LastNames")]
        public string[] LastNames { get; set; }

        [JsonProperty("Phones")]
        public string[] Phones { get; set; }

        [JsonProperty("ActivityStates")]
        public string[] ActivityStates { get; set; }
    }

}
