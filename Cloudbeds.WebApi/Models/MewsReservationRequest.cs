﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsTimeInterval
    {
        [JsonProperty("StartUtc")]
        public DateTime StartUtc { get; set; }

        [JsonProperty("EndUtc")]
        public DateTime EndUtc { get; set; }

    }
    public class MewsReservationRequest : MewsToken
    {
        [JsonProperty("ScheduledStartUtc")]
        public MewsTimeInterval ScheduledStartUtc { get; set; }

        [JsonProperty("CollidingUtc")]
        public MewsTimeInterval CollidingUtc { get; set; }


        [JsonProperty("AccountIds")]
        public string[] CustomerIds { get; set; }
        [JsonProperty("States")]
        public string[] States { get; set; }
        [JsonProperty("ReservationIds")]
        public string[] ReservationIds { get; set; }

    }
    public class MewsReservationRequest_Lookup : MewsToken
    {
        [JsonProperty("ReservationIds")]
        public string[] ReservationIds { get; set; }

    }


}
