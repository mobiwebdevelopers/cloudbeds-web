﻿namespace Cloudbeds.WebApi.Models
{
    public class AppStateRequestUpdate
    {
        public int PropertyID { get; set; }
        public string App_state { get; set; }
    }
}
