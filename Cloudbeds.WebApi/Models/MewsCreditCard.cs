﻿using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public class MewsCreditCard : MewsToken
    {
        [JsonProperty("TransactionId")]
        public string TransactionId { get; set; }

        [JsonProperty("CustomerId")]
        public string CustomerId { get; set; }

        [JsonProperty("Expiration")]
        public string Expiration { get; set; }
    }
}
