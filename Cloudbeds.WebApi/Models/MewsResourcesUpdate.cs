﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace Cloudbeds.WebApi.Models
{
    public class MewsState
    {
        public string Value { get; set; }

    }
    public class MewsResource
    {
        public string ResourceId { get; set; }
        public MewsState State { get; set; }

    }
    public class MewsResourcesUpdate : MewsToken
    {
        [JsonProperty("ResourceUpdates")]
        public List<MewsResource> ResourceUpdates { get; set; }

    }
}
