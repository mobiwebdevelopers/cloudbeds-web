﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cloudbeds.WebApi.Models
{
    public class PaymentIntentRequest
    {
        public string reservationID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public long amount { get; set; }
    }
}
