﻿using System;
using Newtonsoft.Json;

namespace Cloudbeds.WebApi.Models
{
    public partial class MewsAvailabilityResponse
    {
        [JsonProperty("DatesUtc")]
        public DateTime[] DatesUtc { get; set; }

        [JsonProperty("CategoryAvailabilities")]
        public CategoryAvailability[] CategoryAvailabilities { get; set; }
    }

    public partial class CategoryAvailability
    {
        [JsonProperty("CategoryId")]
        public Guid CategoryId { get; set; }

        [JsonProperty("Availabilities")]
        public long[] Availabilities { get; set; }

        [JsonProperty("Adjustments")]
        public long[] Adjustments { get; set; }
    }
}
