using System;
using AssaAbloy.ServiceProvider;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cloudbeds.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Filters.Add<UserContextFilter>();
            })
                .AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddCors(options =>
            {
                options.AddPolicy("All", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });

            services.AddDbContext<MobileKeyDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("MobileKey"));
                options.UseLazyLoadingProxies();
            });

            services.AddScoped<IAssaAbloyService>(s => new AssaAbloyService(
                Configuration["AssaAbloy:BaseApiUrl"].ToString(),
                Configuration["AssaAbloy:TenantUserName"].ToString(),
                Configuration["AssaAbloy:TenantPassword"].ToString()));

            services.AddScoped<IVisionlineService>(s => new VisionlineService(
                Configuration["Visionline:BaseUrl"].ToString(),
                Configuration["Visionline:Username"].ToString(),
                Configuration["Visionline:Password"].ToString()));

            services.AddScoped<IMobileKeyService, MobileKeyService>();

            var cloudbedsClientSettings = new CloudbedsClient.CloudbedsClientSettings
            {
                ApiBaseUrl = new Uri(Configuration["CloudbedsApi:BaseUrl"]),
                ClientId = Configuration["CloudbedsApi:UserId"],
                ClientSecret = Configuration["CloudbedsApi:Secret"],
                RedirectUri = Configuration["CloudbedsApi:RedirectUrl"],
                SettingsId = Convert.ToInt32(Configuration["CloudbedsApi:SettingsId"])
            };

            services.AddSingleton(cloudbedsClientSettings);
            services.AddTransient<ICloudbedsClient, CloudbedsClient>();

            services.AddScoped<UserContext>();
            services.AddMemoryCache();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();

            app.UseCors("All");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers(); //Routes for my API controllers
                endpoints.MapControllerRoute("Default", "{controller= Home}/{action=Index}/{id?}");
            });

        }
    }
}
