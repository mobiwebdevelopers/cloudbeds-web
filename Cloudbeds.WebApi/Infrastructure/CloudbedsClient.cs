﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CloudBaseWeb.Api.Entities;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cloudbeds.WebApi.Infrastructure
{
    public class CloudbedsClient : ICloudbedsClient
    {
        public class CloudbedsClientSettings
        {
            public Uri ApiBaseUrl { get; set; }

            public string ClientId { get; set; }

            public string ClientSecret { get; set; }

            public string RedirectUri { get; set; }
            public int SettingsId { get; set; }
        }

        private readonly MobileKeyDbContext _dbContext;
        private readonly CloudbedsClientSettings _cloudbedsClientSettings;

        public CloudbedsClient(MobileKeyDbContext dbContext, CloudbedsClientSettings cloudbedsClientSettings)
        {
            _dbContext = dbContext;
            _cloudbedsClientSettings = cloudbedsClientSettings;
        }

        public async Task<JObject> GetReservations(GetReservationsRequest request)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", await GetAccessToken());
                httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;

                var query = GetQueryStringFromRequest(request);
                using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservations{query}"))
                {
                    using (var response = await httpClient
                        .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        return JObject.Parse(await response.Content.ReadAsStringAsync());
                    }
                }
            }
        }

        public async Task<GetReservationResponse> GetReservation(string reservationId)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", await GetAccessToken());
                httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;

                using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation?reservationID={reservationId}"))
                {
                    using (var response = await httpClient
                        .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        return await response.Content.ReadAsAsync<GetReservationResponse>();
                    }
                }
            }
        }

        public async Task<string> GetAccessToken()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            
            var settings = await _dbContext.Set<CloudbedsSettings>()
                //.Where(c => c.IsActive)
                .Where(c => c.Id == _cloudbedsClientSettings.SettingsId)
                .FirstOrDefaultAsync();

            if (settings == null)
            {
                throw new Exception("There is no cloudbeds settings in the data base.");
            }

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", settings.AccessToken);
                    httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/userinfo"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            if (!response.IsSuccessStatusCode)
                            {
                                settings.AccessToken = await RefreshAccessToken();
                            }
                        }
                    }
                }

                return settings.AccessToken;
            }
            catch(Exception e)
            {
                return await RefreshAccessToken();
            }
        }

        public async Task<string> RefreshAccessToken()
        {
            var settings = await _dbContext.Set<CloudbedsSettings>()
                //.Where(c => c.IsActive)
                .Where(c => c.Id == _cloudbedsClientSettings.SettingsId)
                .FirstOrDefaultAsync();

            if (settings == null)
            {
                throw new Exception("There is no cloudbeds settings in the data base.");
            }
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization =
                //    new AuthenticationHeaderValue("Bearer", await GetAccessToken());

                httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;
                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/access_token"))
                {
                    var requestContent = new FormUrlEncodedContent(new[]
                    {
                            new KeyValuePair<string, string>("grant_type", "refresh_token"),
                            new KeyValuePair<string, string>("client_id", _cloudbedsClientSettings.ClientId),
                            new KeyValuePair<string, string>("client_secret", _cloudbedsClientSettings.ClientSecret),
                            //new KeyValuePair<string, string>("redirect_uri", _cloudbedsClientSettings.RedirectUri),
                            new KeyValuePair<string, string>("refresh_token", settings.RefreshToken)
                        });

                    httpRequest.Content = requestContent;
                    using (var response = await httpClient
                        .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        var responseString = await response.Content.ReadAsStringAsync();
                        var accessTokenResponse = JsonConvert.DeserializeObject<AccessTokenResponse>(responseString);

                        settings.RefreshToken = accessTokenResponse.RefreshToken;
                        settings.AccessToken = accessTokenResponse.AccessToken;
                        settings.UpdatedAt = DateTime.UtcNow;

                        _dbContext.Update(settings);
                        await _dbContext.SaveChangesAsync();

                        return accessTokenResponse.AccessToken;
                    }
                }
            }
        }
        public async Task<string> CheckAccessToken()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;
                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/access_token_check"))
                {
                    using (var response = await httpClient
                        .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        var responseString = await response.Content.ReadAsStringAsync();

                        return responseString;
                    }
                }
            }
        }

        public async void SetAppStatus(string status)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;
                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postAppState"))
                {
                    var requestContent = new FormUrlEncodedContent(new[]
                    {
                            new KeyValuePair<string, string>("propertyID", "201938"),
                            new KeyValuePair<string, string>("app_state", status),
                        });

                    httpRequest.Content = requestContent;
                    using (var response = await httpClient
                        .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                        .ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        var responseString = await response.Content.ReadAsStringAsync();
                        var accessTokenResponse = JsonConvert.DeserializeObject<AccessTokenResponse>(responseString);

                    }
                }
            }

        }
        public async Task<string> GetAccessToken(string authCode)
        {
            CloudbedsSettings settings = null;
            try
            {
                settings = await _dbContext.Set<CloudbedsSettings>()
                    //.Where(c => c.IsActive || c.Id == 2)
                    .Where(c => c.Id == _cloudbedsClientSettings.SettingsId)
                    .FirstOrDefaultAsync();

                if (settings == null)
                {
                    settings = new CloudbedsSettings
                    {
                        AccessToken = string.Empty,
                        RefreshToken = string.Empty,
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow,
                        IsActive = true
                    };
                    SetAppStatus("enabled");
                    _dbContext.Add(settings);
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch(Exception e)
            {
                return "ERROR: " + e.Message;
            }
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = _cloudbedsClientSettings.ApiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/access_token"))
                    {
                        var requestContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("grant_type", "authorization_code"),
                            new KeyValuePair<string, string>("client_id", _cloudbedsClientSettings.ClientId),
                            new KeyValuePair<string, string>("client_secret", _cloudbedsClientSettings.ClientSecret),
                            new KeyValuePair<string, string>("redirect_uri", _cloudbedsClientSettings.RedirectUri),
                            new KeyValuePair<string, string>("code", authCode)
                        });

                        httpRequest.Content = requestContent;
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            var responseString = await response.Content.ReadAsStringAsync();
                            var accessTokenResponse = JsonConvert.DeserializeObject<AccessTokenResponse>(responseString);

                            settings.RefreshToken = accessTokenResponse.RefreshToken;
                            settings.AccessToken = accessTokenResponse.AccessToken;
                            settings.UpdatedAt = DateTime.UtcNow;

                            _dbContext.Update(settings);
                            await _dbContext.SaveChangesAsync();

                            return settings.AccessToken;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                return "ERROR 2: " + ex.Message;
            }
        }

        public string GetQueryStringFromRequest<T>(T request)
        {
            string queryString = null;
            var properties = typeof(T).GetProperties().Where(p => p.Name != "AccessToken");
            foreach (var property in properties)
            {
                queryString = queryString == null ? "?" : $"{queryString}&";

                var value = property.GetValue(request);
                if (property.PropertyType.Name == "DateTime")
                {
                    value = ((DateTime)property.GetValue(request)).ToString("yyyy-MM-dd");
                }

                queryString = $"{queryString}{property.Name[0].ToString().ToLower()}{property.Name.Substring(1)}={value}";
            }

            return queryString;
        }
    }
}
