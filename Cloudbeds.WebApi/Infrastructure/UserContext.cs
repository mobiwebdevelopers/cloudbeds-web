﻿namespace Cloudbeds.WebApi.Infrastructure
{
    public class UserContext
    {
        public UserProfile Profile { get; set; }
    }
}
