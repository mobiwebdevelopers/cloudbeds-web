﻿using System.Threading.Tasks;
using Cloudbeds.WebApi.Models;
using Newtonsoft.Json.Linq;

namespace Cloudbeds.WebApi.Infrastructure
{
    public interface ICloudbedsClient
    {
        Task<JObject> GetReservations(GetReservationsRequest request);

        Task<GetReservationResponse> GetReservation(string reservationId);

        Task<string> GetAccessToken();

        Task<string> GetAccessToken(string authCode);

        Task<string> RefreshAccessToken();
        Task<string> CheckAccessToken();

        string GetQueryStringFromRequest<T>(T request);
    }
}
