﻿namespace Cloudbeds.WebApi.Infrastructure
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public int DomainId { get; set; }
        public string Role { get; set; }
        public string Duty { get; set; }
    }
}
