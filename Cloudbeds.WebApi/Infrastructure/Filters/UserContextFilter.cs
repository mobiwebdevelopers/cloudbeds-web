﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace Cloudbeds.WebApi.Infrastructure.Filters
{
    public class UserContextFilter : IAsyncAuthorizationFilter
    {
        private readonly UserContext _userContext;
        private readonly Uri _userApiUrl;
        private readonly IConfiguration _configuration;

        public UserContextFilter(UserContext userContext, IConfiguration configuration)
        {
            _userContext = userContext;
            _configuration = configuration;
            _userApiUrl = new Uri(_configuration["UserApi:BaseUrl"]);
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Request.Path.HasValue && context.HttpContext.Request.Path.Value == "/api/auth/callback")
            {
                return;
            }

            var token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrWhiteSpace(token))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            token = token.Split(" ")[1];
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    client.BaseAddress = _userApiUrl;
                    var response = await client.GetAsync("api/v1/token-information");
                    _userContext.Profile = await response.Content.ReadAsAsync<UserProfile>();

                    if (_userContext?.Profile?.UserId >= 0)
                    {
                        return;
                    }
                }
            }
            catch { }

            context.Result = new UnauthorizedResult();
        }
    }
}
