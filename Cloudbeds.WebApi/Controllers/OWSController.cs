﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OWSReservationService;
using OWSResvAdvancedService;
using OWSNameService;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [Route("api/ows")]
    [AllowAnonymous]
    public class OWSController : ControllerBase
    {
        private readonly UserContext _userContext;

        private readonly IConfiguration _configuration;
        private readonly ICloudbedsClient _cloudbedsClient;
        private readonly IMobileKeyService _mobileKeyService;
        private readonly Uri _apiBaseUrl;
        private const string Percentage = "percentage";
        private const string Rate = "rate";
        private ReservationServiceSoapClient owsClient;
        private ResvAdvancedServiceSoapClient owsAdvClient;
        private NameServiceSoapClient owsNameClient;

        private OWSReservationService.HotelReference hotel = new OWSReservationService.HotelReference();
        private FetchBookingRequest bookingRequest = new FetchBookingRequest();
        private GetReservationStatusRequest reservationRequest = new GetReservationStatusRequest();
        private CheckInRequest checkinRequest = new CheckInRequest();
        private CheckOutRequest checkoutRequest = new CheckOutRequest();
        private OWSReservationService.OGHeader header = new OWSReservationService.OGHeader();
        private OWSResvAdvancedService.OGHeader headerAdv = new OWSResvAdvancedService.OGHeader();
        private OWSResvAdvancedService.HotelReference hotelAdv = new OWSResvAdvancedService.HotelReference();
        private OWSNameService.OGHeader headerName = new OWSNameService.OGHeader();
        private bool _isCloud = false;
        public OWSController(IConfiguration configuration, ICloudbedsClient cloudbedsClient, IMobileKeyService mobileKeyService, UserContext userContext)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            _configuration = configuration;
            _cloudbedsClient = cloudbedsClient;
            _mobileKeyService = mobileKeyService;
            _apiBaseUrl = new Uri(configuration["CloudbedsApi:BaseUrl"]);
            _userContext = userContext;

            OWSReservationService.OGHeaderAuthentication auth = new OWSReservationService.OGHeaderAuthentication();
            OWSResvAdvancedService.OGHeaderAuthentication authAdv = new OWSResvAdvancedService.OGHeaderAuthentication();
            OWSNameService.OGHeaderAuthentication authName = new OWSNameService.OGHeaderAuthentication();

            hotel.hotelCode = "WWIS"; // GERO
            hotel.chainCode = "CHA";

            bookingRequest.HotelReference = hotel;
            reservationRequest.HotelReference = hotel;
            hotelAdv.chainCode = hotel.chainCode;
            hotelAdv.hotelCode = hotel.hotelCode;
            //hotelAdv.Value = hotel.Value;

            OWSReservationService.OGHeaderAuthenticationUserCredentials cred = new OWSReservationService.OGHeaderAuthenticationUserCredentials();
            cred.UserName = "KIOSK";
            cred.UserPassword = "$$$KIOSK$$";
            cred.Domain = "WWIS"; // GERO
            auth.UserCredentials = cred;

            OWSResvAdvancedService.OGHeaderAuthenticationUserCredentials credAdv = new OWSResvAdvancedService.OGHeaderAuthenticationUserCredentials();
            credAdv.UserName = "KIOSK";
            credAdv.UserPassword = "$$$KIOSK$$";
            credAdv.Domain = "WWIS"; // GERO
            authAdv.UserCredentials = credAdv;

            OWSNameService.OGHeaderAuthenticationUserCredentials credName = new OWSNameService.OGHeaderAuthenticationUserCredentials();
            credName.UserName = "KIOSK";
            credName.UserPassword = "$$$KIOSK$$";
            credName.Domain = "WWIS"; // GERO
            authName.UserCredentials = credName;

            header.Authentication = auth;
            header.timeStamp = DateTime.Now;
            header.transactionID = "000000";

            headerAdv.Authentication = authAdv;
            headerAdv.timeStamp = DateTime.Now;
            headerAdv.transactionID = "000000";

            headerName.Authentication = authName;
            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = "000000";

            OWSReservationService.EndPoint origin = new OWSReservationService.EndPoint();
            origin.entityID = "KIOSK";
            origin.systemType = "KIOSK";
            OWSReservationService.EndPoint destination = new OWSReservationService.EndPoint();
            destination.entityID = "TI";
            destination.systemType = "PMS";

            header.Origin = origin;
            header.Destination = destination;

            OWSResvAdvancedService.EndPoint originAdv = new OWSResvAdvancedService.EndPoint();
            originAdv.entityID = "KIOSK";
            originAdv.systemType = "KIOSK";

            OWSResvAdvancedService.EndPoint destinationAdv = new OWSResvAdvancedService.EndPoint();
            destinationAdv.entityID = "TI";
            destinationAdv.systemType = "PMS";

            headerAdv.Origin = originAdv;
            headerAdv.Destination = destinationAdv;

            OWSNameService.EndPoint originName = new OWSNameService.EndPoint();
            originName.entityID = "KIOSK";
            originName.systemType = "KIOSK";
            OWSNameService.EndPoint destinationName = new OWSNameService.EndPoint();
            destinationName.entityID = "TI";
            destinationName.systemType = "PMS";

            headerName.Origin = originName;
            headerName.Destination = destinationName;

            OWSReservationService.ReservationServiceSoapClient.EndpointConfiguration endpoint = new OWSReservationService.ReservationServiceSoapClient.EndpointConfiguration();
            OWSResvAdvancedService.ResvAdvancedServiceSoapClient.EndpointConfiguration endpointAdv = new OWSResvAdvancedService.ResvAdvancedServiceSoapClient.EndpointConfiguration();
            OWSNameService.NameServiceSoapClient.EndpointConfiguration endpointName = new OWSNameService.NameServiceSoapClient.EndpointConfiguration();

            // OnPremise
            //EndpointAddress epaReservation = new EndpointAddress("https://kiosk.westwindinn.com:4443/OWS_WS_51/Reservation.asmx");
            //EndpointAddress epaAdvReservation = new EndpointAddress("https://kiosk.westwindinn.com:4443/OWS_WS_51/AdvReservation.asmx");
            //EndpointAddress epaName = new EndpointAddress("https://kiosk.westwindinn.com:4443/OWS_WS_51/Name.asmx");
            //owsClient = new OWSReservationService.ReservationServiceSoapClient(endpoint,epaReservation);
            //owsAdvClient = new OWSResvAdvancedService.ResvAdvancedServiceSoapClient(endpointAdv, epaAdvReservation);
            //owsNameClient = new OWSNameService.NameServiceSoapClient(endpointName, epaName);
            owsClient = new OWSReservationService.ReservationServiceSoapClient(endpoint);
            owsAdvClient = new OWSResvAdvancedService.ResvAdvancedServiceSoapClient(endpointAdv);
            owsNameClient = new OWSNameService.NameServiceSoapClient(endpointName);

            // Cloud
            //System.ServiceModel.BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
            ////System.ServiceModel.WSHttpBinding binding = new WSHttpBinding(SecurityMode.Transport);

            //EndpointAddress address = new EndpointAddress("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/Reservation");
            //EndpointAddress addressAdv = new EndpointAddress("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/ResvAdvanced");
            //EndpointAddress addressName = new EndpointAddress("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/Name");
            //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;

            //owsClient = new OWSReservationService.ReservationServiceSoapClient(binding, address);
            //owsAdvClient = new OWSResvAdvancedService.ResvAdvancedServiceSoapClient(binding, addressAdv);
            //owsNameClient = new OWSNameService.NameServiceSoapClient(binding, addressName);


        }
        private String GetNextUniqueId(String CodePrefix)
        {
            String id = CodePrefix;
            Guid newGuid = Guid.NewGuid();
            return id + ":" + newGuid;
        }

        [HttpPost]
        [Route("lookup")]
        public async Task<Reservation> GetReservation(LookupReservationRequest request)
        {
            try
            {
                var reservationId = new OWSReservationService.UniqueID();
                reservationId.type = OWSReservationService.UniqueIDType.INTERNAL;
                reservationId.Value = request.ReservationID; // "23900";
                var balance = false;
                if (request.Balance)
                    balance = request.Balance;
                var result = FindReservation(reservationId, balance, request.FirstName, request.LastName);
                if (result == null)
                {
                    reservationId.type = OWSReservationService.UniqueIDType.EXTERNAL;
                    reservationId.Value = request.ReservationID; // "23900";
                    result = FindReservation(reservationId, request.Balance);
                }
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("validation")]
        public async Task<Reservation> GetValidation(LookupReservationRequest request)
        {
            try
            {
                String status = "";
                Reservation result = null;
                if (request.ReservationID.Length != 5)
                {
                    var reservationId = new OWSReservationService.UniqueID();
                    reservationId.type = OWSReservationService.UniqueIDType.INTERNAL;
                    reservationId.Value = request.ReservationID;
                    var balance = false;
                    result = FindReservation(reservationId, balance);
                    if (result == null)
                    {
                        reservationId.type = OWSReservationService.UniqueIDType.EXTERNAL;
                        reservationId.Value = request.ReservationID;
                        result = FindReservation(reservationId, balance);
                    }
                }
                else
                {
                    // hardcoded for testing
                    switch (request.ReservationID)
                    {
                        case "11111":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date;
                            result.CheckOutDate = result.CheckInDate.AddDays(1);
                            break;
                        case "22222":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date;
                            result.CheckOutDate = result.CheckInDate.AddDays(2);
                            break;
                        case "33333":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date;
                            result.CheckOutDate = result.CheckInDate.AddDays(3);
                            break;
                        case "00000":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            status = "Invalid Confirmation Number";
                            break;
                        case "44444":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date.AddDays(-5);
                            result.CheckOutDate = result.CheckInDate.AddDays(3);
                            status = "Your reservation has already passed.";
                            break;
                        case "55555":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date.AddDays(5);
                            result.CheckOutDate = result.CheckInDate.AddDays(3);
                            status = "It is not time for your stay yet.  Please check back later.";
                            break;
                        case "66666":
                            result = new Reservation();
                            result.ConfirmationNumber = request.ReservationID;
                            result.CheckInDate = DateTime.Now.Date;
                            result.CheckOutDate = result.CheckInDate.AddDays(3);
                            status = "This coupon as already been redeemed.";
                            break;

                    }
                }

                if (result != null)
                {
                    result.Validation = new ValidationStatus();
                    if (status.Length > 0)
                    {
                        result.Validation.Status = "ERROR";
                        result.Validation.Message = status;
                    }
                    else
                    {
                        var dayCount = (result.CheckOutDate - result.CheckInDate).TotalDays;
                        if (dayCount > 2)
                        {
                            result.Validation.Status = "SUCCESS";
                        }
                        else
                        {
                            result.Validation.Status = "ERROR";
                            result.Validation.Message = "Your reservation needs to be longer than 2 nights.";
                        }
                    }
                }
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private Reservation FindReservation(OWSReservationService.UniqueID key, Boolean balance = true, String firstName = "", String lastName = "")
        {
            Reservation reservationResult = new Reservation();

            try
            {
                OWSReservationService.FutureBookingSummaryRequest bookingRequest = new FutureBookingSummaryRequest();
                OWSReservationService.FetchBookingFilters filter = new FetchBookingFilters();
                if (key.type == OWSReservationService.UniqueIDType.INTERNAL)
                {
                    filter.ConfirmationNumber = key;
                }
                else if (key.type == OWSReservationService.UniqueIDType.EXTERNAL)
                    filter.ResvNameId = key;

                bookingRequest.AdditionalFilters = filter;
                // add wsse - start
                var resultBooking = new OWSReservationService.FutureBookingSummaryResponse1();
                resultBooking = (OWSReservationService.FutureBookingSummaryResponse1) ExecuteReservationRequest(bookingRequest, "FutureBookingSummary");

                if (resultBooking.FutureBookingSummaryResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                {
                    reservationResult.Rooms = new List<GuestRoom>();
                    if (resultBooking.FutureBookingSummaryResponse.HotelReservations.Length > 0)
                    {
                        int index = 0;
                        Guest guest = new Guest();
                        if (!string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(firstName))
                        {
                            for (int i = 0; i < resultBooking.FutureBookingSummaryResponse.HotelReservations.Length; i++)
                            {
                                for (int j = 0; j < resultBooking.FutureBookingSummaryResponse.HotelReservations[i].ResGuests.Length; j++)
                                {
                                    OWSReservationService.Customer customer = (OWSReservationService.Customer)resultBooking.FutureBookingSummaryResponse.HotelReservations[i].ResGuests[j].Profiles[0].Item;
                                    if (customer.PersonName.firstName.ToUpper() == firstName.ToUpper() && customer.PersonName.lastName.ToUpper() == lastName.ToUpper())
                                    {
                                        index = i;
                                        guest.Gender = customer.gender.ToString();
                                        guest.FirstName = customer.PersonName.firstName;
                                        guest.LastName = customer.PersonName.lastName;
                                        if (customer.PersonName.nameTitle != null)
                                            guest.Title = customer.PersonName.nameTitle[0];
                                        break;
                                    }

                                }
                            }
                        }
                        else
                        {
                            OWSReservationService.Customer customer = (OWSReservationService.Customer)resultBooking.FutureBookingSummaryResponse.HotelReservations[index].ResGuests[0].Profiles[0].Item;
                            guest.Gender = customer.gender.ToString();
                            guest.FirstName = customer.PersonName.firstName;
                            guest.LastName = customer.PersonName.lastName;

                        }
                        for (int i = 0; i < resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList.Length; i++)
                        {
                            if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].source == null)
                                reservationResult.ConfirmationNumber = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].Value;
                            else if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].source == "RESVID")
                                reservationResult.ResvNameId = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].Value;
                        }
                        reservationResult.Status = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].computedReservationStatus.ToString();
                        reservationResult.AdultCount = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].GuestCounts.GuestCount[0].count;
                        reservationResult.ChildrenCount = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].GuestCounts.GuestCount[1].count;

                        reservationResult.Guests = new List<Guest>();
                        reservationResult.Guests.Add(guest);

                        reservationResult.CheckInDate = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].TimeSpan.StartDate;
                        reservationResult.CheckOutDate = Convert.ToDateTime(resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].TimeSpan.Item);
                        //if (resultBooking.FutureBookingSummaryResponse.HotelReservations[0].RoomStays[0].ExpectedCharges != null)
                        //    reservationResult.Balance = resultBooking.FutureBookingSummaryResponse.HotelReservations[0].RoomStays[0].ExpectedCharges.;
                        reservationResult.Total = Convert.ToDecimal(resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].Total.Value);
                        reservationResult.CurrencyCode = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].Total.currencyCode;

                        GuestRoom room = new Models.GuestRoom();
                        OWSReservationService.RoomType roomType = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0];
                        OWSReservationService.Paragraph t = roomType.RoomTypeDescription;
                        room.RoomName = ((OWSReservationService.Text)t.Items[0]).Value;
                        room.Description = ((OWSReservationService.Text)t.Items[0]).Value;
                        room.RoomTypeName = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].roomTypeCode;
                        if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].RoomNumber != null)
                            room.RoomNumber = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].RoomNumber[0].ToString();

                        room.Rate = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomRates[0].Rates[0].Base.Value;
                        room.Adults = reservationResult.AdultCount;
                        room.Children = reservationResult.ChildrenCount;
                        room.StartDate = reservationResult.CheckInDate;
                        room.EndDate = reservationResult.CheckOutDate;
                        room.CurrencyCode = reservationResult.CurrencyCode;
                        reservationResult.Rooms.Add(room);
                    }

                }
                else
                {
                    return null;
                }
                if (balance)
                {
                    OWSResvAdvancedService.InvoiceRequest invoiceRequest = new InvoiceRequest();
                    var resv = new OWSResvAdvancedService.UniqueID();
                    resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
                    resv.source = "RESV_NAME_ID";
                    resv.Value = reservationResult.ResvNameId.ToString();
                    ReservationRequestBase resvReq = new ReservationRequestBase();
                    resvReq.HotelReference = hotelAdv;
                    resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
                    resvReq.ReservationID[0] = resv;

                    invoiceRequest.ReservationRequest = new ReservationRequestBase();
                    invoiceRequest.ReservationRequest.ReservationID = resvReq.ReservationID;
                    invoiceRequest.ReservationRequest.HotelReference = hotelAdv;

                    try
                    {
                        var result = (OWSResvAdvancedService.InvoiceResponse1) ExecuteResvAdvancedRequest(invoiceRequest, "Invoice");
                        if (result != null && result.InvoiceResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        {
                            if (result.InvoiceResponse.Invoice != null)
                            {
                                reservationResult.BillingWindows = new List<Guest>();
                                reservationResult.Balance = 0;
                                reservationResult.BillingCount = result.InvoiceResponse.Invoice.Length;
                                for (int i = 0; i < result.InvoiceResponse.Invoice.Length; i++)
                                {
                                    reservationResult.Balance = reservationResult.Balance + Convert.ToDecimal(result.InvoiceResponse.Invoice[i].CurrentBalance.Value);
                                    if (result.InvoiceResponse.Invoice[i].ProfileIDs != null && result.InvoiceResponse.Invoice[i].ProfileIDs.Length > 0)
                                    {
                                        Guest guest = new Guest();
                                        guest.ProfileId = result.InvoiceResponse.Invoice[i].ProfileIDs[0].Value;
                                        reservationResult.BillingWindows.Add(guest);
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    //using (var scope2 = new OperationContextScope(owsAdvClient.InnerChannel))
                    //{
                    //    headerAdv.timeStamp = DateTime.Now;
                    //    headerAdv.transactionID = GetNextUniqueId("SANI");
                    //    headerAdv.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                    //    headerAdv.Authentication.UserCredentials.UserPassword = null;
                    //    headerAdv.Authentication.UserCredentials.Domain = "OWS";
                    //    MessageHeaders messageHeadersElement2 = OperationContext.Current.OutgoingMessageHeaders;
                    //    messageHeadersElement2.Add(new SoapSecurityHeader(headerAdv.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));
                    //    try
                    //    {

                    //        var result = owsAdvClient.InvoiceAsync(headerAdv, invoiceRequest).GetAwaiter().GetResult();
                    //        if (result.InvoiceResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                    //        {
                    //            if (result.InvoiceResponse.Invoice != null)
                    //            {
                    //                reservationResult.BillingWindows = new List<Guest>();
                    //                reservationResult.Balance = 0;
                    //                reservationResult.BillingCount = result.InvoiceResponse.Invoice.Length;
                    //                for (int i = 0; i < result.InvoiceResponse.Invoice.Length; i++)
                    //                {
                    //                    reservationResult.Balance = reservationResult.Balance + result.InvoiceResponse.Invoice[i].CurrentBalance.Value;
                    //                    if (result.InvoiceResponse.Invoice[i].ProfileIDs != null && result.InvoiceResponse.Invoice[i].ProfileIDs.Length > 0)
                    //                    {
                    //                        Guest guest = new Guest();
                    //                        guest.ProfileId = result.InvoiceResponse.Invoice[i].ProfileIDs[0].Value;
                    //                        reservationResult.BillingWindows.Add(guest);
                    //                    }
                    //                }
                    //            }
                    //        }

                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }
                    //}

                }

                //using (var scope = new OperationContextScope(owsClient.InnerChannel))
                //{
                //    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                //    messageHeadersElement.Add(new SoapSecurityHeader(header.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                //    try
                //    {
                //        header.timeStamp = DateTime.Now;
                //        header.transactionID = GetNextUniqueId("SANI");
                //        header.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                //        header.Authentication.UserCredentials.UserPassword = null;
                //        header.Authentication.UserCredentials.Domain = "OWS";
                //        resultBooking = owsClient.FutureBookingSummaryAsync(header, bookingRequest).GetAwaiter().GetResult();

                //    }
                //    catch (Exception e)
                //    {

                //    }
                //}

                // add wsse - end


                return reservationResult;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        //private async Task<Reservation> GetInvoice(string resvNameId)
        //{
        //    Reservation reservationResult = new Reservation();
        //    headerAdv.timeStamp = DateTime.Now;
        //    headerAdv.transactionID = GetNextUniqueId("SANI");
        //    try
        //    {
        //        var resv = new OWSResvAdvancedService.UniqueID();
        //        resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
        //        resv.source = "RESV_NAME_ID";
        //        resv.Value = resvNameId;
        //        ReservationRequestBase resvReq = new ReservationRequestBase();
        //        resvReq.HotelReference = hotelAdv;
        //        resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
        //        resvReq.ReservationID[0] = resv;
        //        OWSResvAdvancedService.InvoiceRequest invoiceRequest = new InvoiceRequest();

        //        invoiceRequest.ReservationRequest = new ReservationRequestBase();
        //        invoiceRequest.ReservationRequest.ReservationID = resvReq.ReservationID;
        //        invoiceRequest.ReservationRequest.HotelReference = hotelAdv;
        //        try
        //        {

        //            var result = await owsAdvClient.InvoiceAsync(headerAdv, invoiceRequest);
        //            if (result.InvoiceResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //            {
        //                reservationResult.Balance = result.InvoiceResponse.ExpectedCharges.CurrentBalance.Value;
        //                reservationResult.Total = result.InvoiceResponse.ExpectedCharges.TotalCharges.Value;
        //                reservationResult.CurrencyCode = result.InvoiceResponse.ExpectedCharges.CurrentBalance.currencyCode;
        //                return reservationResult;
        //            }
        //            else
        //                return null;

        //        }
        //        catch (Exception ex)
        //        {
        //            return null;

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }

        //}
        [HttpGet]
        [Route("reservation")]
        public async Task<Reservation> GetReservationData(LookupReservationRequest request)
        {
            try
            {
                var reservationId = new OWSReservationService.UniqueID();
                reservationId.type = OWSReservationService.UniqueIDType.INTERNAL;
                reservationId.Value = request.ReservationID; // "23900";
                var result = FindReservation(reservationId);
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        //[HttpPost]
        //[Route("checkin")]
        //public async Task<IActionResult> CheckIn(LookupReservationRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ReservationID))
        //    {
        //        return BadRequest("ReservationId parameter should not be null or empty.");
        //    }

        //    var reservation = await GetReservationData(request);

        //    if (reservation == null)
        //    {
        //        return StatusCode(StatusCodes.Status404NotFound, "error retrrieving reservation");
        //    }
        //    switch (reservation.Status.ToUpper())
        //    {
        //        case "NOSHOW":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Reservation is pending confirmation.");
        //        case "INHOUSE":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked in.");
        //        case "CANCELED":
        //            return StatusCode(StatusCodes.Status403Forbidden, "This reservation was canceled.");
        //        case "CHECKEDOUT":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked out");
        //        case "RESERVED":
        //            break;
        //        default:
        //            break;
        //    }

        //    if (reservation.CheckOutDate < DateTime.Now.Date)
        //    {
        //        //return StatusCode(StatusCodes.Status403Forbidden, "Reservation End date is expired.");
        //    }
        //    if (reservation.CheckInDate > DateTime.Now.Date)
        //    {
        //        return StatusCode(StatusCodes.Status403Forbidden, "Reservation Checkin date is not here yet.");
        //    }
        //    if (reservation.Rooms == null || !reservation.Rooms.Any())
        //    {
        //        return StatusCode(StatusCodes.Status403Forbidden, "ReservationId does not contains rooms.");
        //    }

        //    try
        //    {
        //        if (String.IsNullOrEmpty(reservation.Rooms[0].RoomNumber))
        //        {
        //            OWSReservationService.AssignRoomRequest myRequest = new OWSReservationService.AssignRoomRequest();
        //            myRequest.HotelReference = hotel;
        //            var resv1 = new OWSReservationService.UniqueID();
        //            resv1.type = OWSReservationService.UniqueIDType.EXTERNAL;
        //            resv1.source = "RESV_NAME_ID";
        //            resv1.Value = reservation.ResvNameId.ToString();
        //            myRequest.ResvNameId = resv1;
        //            myRequest.StationID = "KIOSK";
        //            try
        //            {
        //                header.timeStamp = DateTime.Now;
        //                header.transactionID = GetNextUniqueId("SANI");
        //                var result = await owsClient.AssignRoomAsync(header, myRequest);
        //                if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                    reservation.Rooms[0].RoomNumber = result.AssignRoomResponse.RoomNoAssigned;
        //            }
        //            catch (Exception ex)
        //            {
        //            }



        //        }

        //        var resv = new OWSResvAdvancedService.UniqueID();
        //        resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
        //        resv.source = "RESV_NAME_ID";
        //        resv.Value = reservation.ResvNameId.ToString();
        //        ReservationRequestBase resvReq = new ReservationRequestBase();
        //        resvReq.HotelReference = hotelAdv;
        //        resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
        //        resvReq.ReservationID[0] = resv;
        //        checkinRequest.ReservationRequest = resvReq;
        //        try
        //        {
        //            headerAdv.timeStamp = DateTime.Now;
        //            headerAdv.transactionID = GetNextUniqueId("SANI");

        //            var result = await owsAdvClient.CheckInAsync(headerAdv, checkinRequest);
        //            if (result.CheckInResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok(result);
        //            else
        //                return BadRequest(result.CheckInResponse.Result.Text[0].Value);

        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);

        //        }
        //        //var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationID);

        //        //return Ok(result);
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}
        [HttpPost]
        [Route("checkinOnly")]
        public async Task<IActionResult> CheckInOnly(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            var resv = new OWSResvAdvancedService.UniqueID();
            resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
            resv.source = "RESV_NAME_ID";
            resv.Value = request.ReservationID.ToString();
            ReservationRequestBase resvReq = new ReservationRequestBase();
            resvReq.HotelReference = hotelAdv;
            resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
            resvReq.ReservationID[0] = resv;
            checkinRequest.ReservationRequest = resvReq;

            if (String.IsNullOrEmpty(request.RoomID) || (request.RoomID == "Auto-assign"))
            {
                OWSReservationService.AssignRoomRequest myRequest = new OWSReservationService.AssignRoomRequest();
                myRequest.HotelReference = hotel;
                var resv1 = new OWSReservationService.UniqueID();
                resv1.type = OWSReservationService.UniqueIDType.EXTERNAL;
                resv1.source = "RESV_NAME_ID";
                resv1.Value = request.ReservationID.ToString();
                myRequest.ResvNameId = resv1;
                myRequest.StationID = "KIOSK";
                try
                {
                    var result = (OWSReservationService.AssignRoomResponse1) ExecuteReservationRequest(myRequest, "AssignRoom");
                    if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        request.RoomID = result.AssignRoomResponse.RoomNoAssigned;
                }
                catch (Exception ex)
                {
                }



            }

            try
            {
                var result = (OWSResvAdvancedService.CheckInResponse1) ExecuteResvAdvancedRequest(checkinRequest, "CheckIn");
                if (result.CheckInResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                    return Ok(result);
                else
                {
                    if (result.CheckInResponse.Result.Text != null)
                        return BadRequest(result.CheckInResponse.Result.Text[0].Value);
                    else
                        return BadRequest("Error Check In");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }


            //try
            //{
            //    headerAdv.timeStamp = DateTime.Now;
            //    headerAdv.transactionID = GetNextUniqueId("SANI");
            //    using (var scope = new OperationContextScope(owsAdvClient.InnerChannel))
            //    {
            //        MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
            //        messageHeadersElement.Add(new SoapSecurityHeader(headerAdv.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

            //        try
            //        {
            //            var resv = new OWSResvAdvancedService.UniqueID();
            //            resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
            //            resv.source = "RESV_NAME_ID";
            //            resv.Value = request.ReservationID.ToString();
            //            ReservationRequestBase resvReq = new ReservationRequestBase();
            //            resvReq.HotelReference = hotelAdv;
            //            resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
            //            resvReq.ReservationID[0] = resv;
            //            checkinRequest.ReservationRequest = resvReq;

            //            if (String.IsNullOrEmpty(request.RoomID) || (request.RoomID == "Auto-assign"))
            //            {
            //                OWSReservationService.AssignRoomRequest myRequest = new OWSReservationService.AssignRoomRequest();
            //                myRequest.HotelReference = hotel;
            //                var resv1 = new OWSReservationService.UniqueID();
            //                resv1.type = OWSReservationService.UniqueIDType.EXTERNAL;
            //                resv1.source = "RESV_NAME_ID";
            //                resv1.Value = request.ReservationID.ToString();
            //                myRequest.ResvNameId = resv1;
            //                myRequest.StationID = "KIOSK";
            //                try
            //                {
            //                    header.timeStamp = DateTime.Now;
            //                    header.transactionID = GetNextUniqueId("SANI");
            //                    header.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
            //                    header.Authentication.UserCredentials.UserPassword = null;
            //                    header.Authentication.UserCredentials.Domain = "OWS";
            //                    var result = owsClient.AssignRoomAsync(header, myRequest).GetAwaiter().GetResult(); ;
            //                    if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
            //                        request.RoomID = result.AssignRoomResponse.RoomNoAssigned;
            //                }
            //                catch (Exception ex)
            //                {
            //                }



            //            }

            //            try
            //            {
            //                headerAdv.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
            //                headerAdv.Authentication.UserCredentials.UserPassword = null;
            //                headerAdv.Authentication.UserCredentials.Domain = "OWS";

            //                var result = owsAdvClient.CheckInAsync(headerAdv, checkinRequest).GetAwaiter().GetResult(); 
            //                if (result.CheckInResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
            //                    return Ok(result);
            //                else
            //                {
            //                    if (result.CheckInResponse.Result.Text != null)
            //                        return BadRequest(result.CheckInResponse.Result.Text[0].Value);
            //                    else
            //                        return BadRequest("Error Check In");
            //                }

            //            }
            //            catch (Exception ex)
            //            {
            //                return BadRequest(ex.Message);

            //            }

            //        }
            //        catch (Exception e)
            //        {

            //        }
            //    }


            //    return Ok();
            //    //var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationID);

            //    //return Ok(result);
            //}
            //catch (ArgumentException ae)
            //{
            //    return BadRequest(ae.Message);
            //}
            //catch (Exception e)
            //{
            //    return StatusCode(StatusCodes.Status500InternalServerError, e);
            //}
        }

        [HttpPost]
        [Route("checkout")]
        public async Task<IActionResult> CheckOut(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            try
            {


                var resv = new OWSResvAdvancedService.UniqueID();
                resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = request.ReservationID.ToString();
                ReservationRequestBase resvReq = new ReservationRequestBase();
                resvReq.HotelReference = hotelAdv;
                resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
                resvReq.ReservationID[0] = resv;
                Guest guest1 = null;
                Guest guest2 = null;
                if (request.Profile1 != null && request.Profile2 != null)
                {
                    if (request.Profile1 != request.Profile2)
                    {
                        guest1 = await GetGuestProfile(request.Profile1);
                        guest2 = await GetGuestProfile(request.Profile2);
                        if (guest1.Email != null && guest2.Email != null && guest1.Email != guest2.Email)
                        {
                            OWSNameService.OGHeader headerName = new OWSNameService.OGHeader();
                            if (guest2.ProfileId != request.Profile2)
                            {
                                OWSNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
                                reqUpdateEmail.NameEmail = new OWSNameService.NameEmail();
                                reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(guest2.ProfileId);
                                reqUpdateEmail.NameEmail.operaIdSpecified = true;
                                reqUpdateEmail.NameEmail.Value = guest1.Email;
                                reqUpdateEmail.NameEmail.primary = true;

                                try
                                {
                                    var result = (OWSNameService.UpdateEmailResponse1) ExecuteNameRequest(reqUpdateEmail, "UpdateEmail");
                                    if (result.UpdateEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result.UpdateEmailResponse.Result.Text[0].Value);
                                }
                                catch (Exception e)
                                {

                                }

                            }
                            else
                            {
                                OWSNameService.InsertEmailRequest reqInsertEmail = new InsertEmailRequest();
                                var resvInsert = new OWSNameService.UniqueID();
                                resvInsert.type = OWSNameService.UniqueIDType.INTERNAL;
                                resvInsert.Value = guest2.ProfileId;
                                reqInsertEmail.NameID = resvInsert;

                                reqInsertEmail.NameEmail = new OWSNameService.NameEmail();
                                reqInsertEmail.NameEmail.operaIdSpecified = false;
                                reqInsertEmail.NameEmail.Value = guest1.Email;
                                reqInsertEmail.NameEmail.primary = true;

                                try
                                {
                                    var result2 = (OWSNameService.InsertEmailResponse1) ExecuteNameRequest(reqInsertEmail, "InsertEmail");
                                    if (result2.InsertEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result2.InsertEmailResponse.Result.Text[0].Value);
                                    else
                                        guest2.ProfileId = result2.InsertEmailResponse.Result.IDs[0].operaId.ToString();
                                }
                                catch (Exception e)
                                {

                                }

                            }
                        }
                        else
                        {

                        }
                    }
                }

                checkoutRequest.ReservationRequest = resvReq;
                checkoutRequest.overrideEmailPrivacy = true;
                checkoutRequest.EmailFolio = true;
                checkoutRequest.EmailFolioSpecified = true;
                checkoutRequest.overrideEmailPrivacySpecified = true;

                try
                {
                    var result = (OWSResvAdvancedService.CheckOutResponse1) ExecuteResvAdvancedRequest(checkoutRequest, "CheckOut");
                    if (result.CheckOutResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                    {
                        if (guest1 != null && guest2 != null && guest1.Email != guest2.Email)
                        {
                            OWSNameService.OGHeader headerName = new OWSNameService.OGHeader();
                            if (guest2.ProfileId != request.Profile2)
                            {
                                OWSNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
                                reqUpdateEmail.NameEmail = new OWSNameService.NameEmail();
                                reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(guest2.ProfileId);
                                if (guest2.Email.Length > 0)
                                    reqUpdateEmail.NameEmail.Value = guest2.Email;
                                else
                                    reqUpdateEmail.NameEmail.Value = " ";

                                reqUpdateEmail.NameEmail.operaIdSpecified = true;
                                reqUpdateEmail.NameEmail.primary = true;

                                try
                                {
                                    var result2 = (OWSNameService.UpdateEmailResponse1)ExecuteNameRequest(reqUpdateEmail, "UpdateEmail");
                                    if (result2.UpdateEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result2.UpdateEmailResponse.Result.Text[0].Value);
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }

                        return Ok("Success");
                    }
                    else
                        return BadRequest(result.CheckOutResponse.Result.Text[0].Value);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        //[HttpPost]
        //[Route("update-email")]
        //public async Task<IActionResult> UpdateEmail(UpdateProfileEmailRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ProfileID))
        //    {
        //        return BadRequest("ProfileId parameter should not be null or empty.");
        //    }

        //    try
        //    {
        //        OWSNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
        //        reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(request.ProfileID);
        //        reqUpdateEmail.NameEmail.Value = request.Email;
        //        OWSNameService.OGHeader headerName = new OWSNameService.OGHeader();

        //        headerName.timeStamp = DateTime.Now;
        //        headerName.transactionID = GetNextUniqueId("SANI");

        //        try
        //        {
        //            var result = await owsNameClient.UpdateEmailAsync(headerName, reqUpdateEmail);
        //            if (result.UpdateEmailResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok("Success");
        //            else
        //                return BadRequest(result.UpdateEmailResponse.Result.Text[0].Value);
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}
        private async Task<Guest> GetGuestProfile(string profileId)
        {
            OWSNameService.FetchEmailListRequest reqEmail = new FetchEmailListRequest();
            OWSNameService.UniqueID id = new OWSNameService.UniqueID();
            id.type = OWSNameService.UniqueIDType.INTERNAL;
            id.Value = profileId;
            reqEmail.NameID = id;

            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = GetNextUniqueId("SANI");
            Guest guest = new Guest();
            try
            {
                var result = (OWSNameService.FetchEmailListResponse1) ExecuteNameRequest(reqEmail, "FetchEmail");
                if (result.FetchEmailListResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                {
                    if (result.FetchEmailListResponse.NameEmailList != null)
                    {
                        guest.ProfileId = result.FetchEmailListResponse.NameEmailList[0].operaId.ToString();
                        guest.Email = result.FetchEmailListResponse.NameEmailList[0].Value;
                    }
                    else
                    {
                        guest.ProfileId = profileId;
                        guest.Email = "";
                    }
                }
                return guest;
            }
            catch (Exception ex)
            {
                return guest;
            }
        }
        //[HttpPost]
        //[Route("profile")]
        //public async Task<IActionResult> GetProfile(UpdateProfileEmailRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ProfileID))
        //    {
        //        return BadRequest("ProfileId parameter should not be null or empty.");
        //    }

        //    try
        //    {
        //        OWSNameService.FetchEmailListRequest reqEmail = new FetchEmailListRequest();
        //        OWSNameService.UniqueID id = new OWSNameService.UniqueID();
        //        id.type = OWSNameService.UniqueIDType.INTERNAL;
        //        id.Value = request.ProfileID;
        //        reqEmail.NameID = id;

        //        headerName.timeStamp = DateTime.Now;
        //        headerName.transactionID = GetNextUniqueId("SANI");

        //        try
        //        {
        //            var result = await owsNameClient.FetchEmailListAsync(headerName, reqEmail);
        //            if (result.FetchEmailListResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok(result);
        //            else
        //                return BadRequest(result.FetchEmailListResponse.Result.Text[0].Value);
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}

        [HttpPost]
        [Route("guest-messages")]
        public async Task<IActionResult> GuestMessages(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            try
            {
                var resv = new OWSResvAdvancedService.UniqueID();
                resv.type = OWSResvAdvancedService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = request.ReservationID.ToString();
                ReservationRequestBase resvReq = new ReservationRequestBase();
                resvReq.HotelReference = hotelAdv;
                resvReq.ReservationID = new OWSResvAdvancedService.UniqueID[1];
                resvReq.ReservationID[0] = resv;
                checkinRequest.ReservationRequest = resvReq;
                try
                {
                    OWSResvAdvancedService.GuestMessagesRequest guestRequest = new GuestMessagesRequest();

                    guestRequest.ReservationRequest = new ReservationRequestBase();
                    guestRequest.ReservationRequest.HotelReference = hotelAdv;
                    //var Ids = new OWSResvAdvancedService.UniqueID[1];
                    //Ids[0] = resv;
                    guestRequest.ReservationRequest = resvReq;
                    //guestRequest.ReservationRequest.ReservationID = Ids;


                    var result = (OWSResvAdvancedService.GuestMessagesResponse1) ExecuteResvAdvancedRequest(guestRequest, "GuestMessages");
                    if (result.GuestMessagesResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        return Ok(result);
                    else
                        return BadRequest(result.GuestMessagesResponse.Result.Text[0].Value);

                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);

                }
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        //[HttpGet]
        //[Route("ReservationList")]
        //public async Task<JObject> GetReservationList()
        //{
        //    try
        //    {
        //        headerAdv.timeStamp = DateTime.Now;
        //        try
        //        {
        //            OWSResvAdvancedService.FetchQueueReservationsRequest request = new FetchQueueReservationsRequest();
        //            request.HotelReference = hotelAdv;
        //            headerAdv.transactionID = "1b6db1d6-2d57-4856-b7c0-b7e87411209a";
        //            var resultStatus = await owsAdvClient.FetchQueueReservationsAsync(headerAdv, request);
        //            if (resultStatus.FetchQueueReservationsResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //            {

        //                return JObject.FromObject(resultStatus);
        //            }
        //            else
        //                return null;

        //        }
        //        catch (Exception e)
        //        {
        //            return null;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        [HttpPost]
        [Route("assign-room")]
        public async Task<JObject> AssignRoom(LookupReservationRequest request)
        {
            try
            {
                var reservation = await GetReservationData(request);
                OWSReservationService.AssignRoomRequest myRequest = new OWSReservationService.AssignRoomRequest();
                myRequest.HotelReference = hotel;
                var resv = new OWSReservationService.UniqueID();
                resv.type = OWSReservationService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = reservation.ResvNameId.ToString();
                myRequest.ResvNameId = resv;
                myRequest.StationID = "KIOSK";
                try
                {
                    var result = (OWSReservationService.AssignRoomResponse1) ExecuteReservationRequest(myRequest, "AssignRoom");
                    if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        return JObject.FromObject(result);
                    else
                        return JObject.Parse("Error Checking out...");
                }
                catch (Exception ex)
                {
                    return JObject.Parse(ex.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private FormUrlEncodedContent GetFormUrlContentFromRequest<T>(T request)
        {
            var keyValuePairArrays = new List<KeyValuePair<string, string>>();
            var properties = typeof(T).GetProperties().Where(p => p.Name != "AccessToken");
            foreach (var property in properties)
            {
                var value = property.GetValue(request);
                switch (property.PropertyType.Name)
                {
                    case "DateTime":
                        value = ((DateTime)property.GetValue(request)).ToString("yyyy-MM-dd");
                        break;

                    case "ReservationRoom[]":
                        var rooms = (ReservationRoom[])property.GetValue(request);
                        for (var i = 0; i < rooms.Length; i++)
                        {
                            var roomProperties = typeof(ReservationRoom).GetProperties();
                            foreach (var roomProperty in roomProperties)
                            {
                                var roomPropertyValue = roomProperty.GetValue(rooms[i]);
                                keyValuePairArrays.Add(new KeyValuePair<string, string>(
                                    $"{CamelCase(property.Name)}[{i}][{CamelCase(roomProperty.Name)}]", roomPropertyValue.ToString()));
                            }
                        }

                        continue;
                }

                keyValuePairArrays.Add(new KeyValuePair<string, string>($"{CamelCase(property.Name)}", value.ToString()));
            }

            return new FormUrlEncodedContent(keyValuePairArrays.ToArray());
        }

        private string CamelCase(string name)
        {
            return $"{name[0].ToString().ToLower()}{name.Substring(1)}";
        }

        private object ExecuteReservationRequest(object request, string action)
        {
            header.timeStamp = DateTime.Now;
            header.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {
                try
                {
                    switch (action.ToLower())
                    {
                        case "futurebookingsummary":
                            return owsClient.FutureBookingSummaryAsync(header, (OWSReservationService.FutureBookingSummaryRequest)request).GetAwaiter().GetResult();
                        case "assignroom":
                            return owsClient.AssignRoomAsync(header, (OWSReservationService.AssignRoomRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                using (var scope = new OperationContextScope(owsClient.InnerChannel))
                {
                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(header.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    try
                    {
                        header.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        header.Authentication.UserCredentials.UserPassword = null;
                        header.Authentication.UserCredentials.Domain = "OWS";
                        switch (action.ToLower())
                        {
                            case "futurebookingsummary":
                                return owsClient.FutureBookingSummaryAsync(header, (OWSReservationService.FutureBookingSummaryRequest)request).GetAwaiter().GetResult();
                            case "assignroom":
                                return owsClient.AssignRoomAsync(header, (OWSReservationService.AssignRoomRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }
        private object ExecuteResvAdvancedRequest(object request, string action)
        {
            headerAdv.timeStamp = DateTime.Now;
            headerAdv.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {
                try
                {
                    switch (action.ToLower())
                    {
                        case "invoice": // token error
                            return owsAdvClient.InvoiceAsync(headerAdv, (OWSResvAdvancedService.InvoiceRequest)request).GetAwaiter().GetResult();
                        case "checkin": // ok
                            return owsAdvClient.CheckInAsync(headerAdv, (OWSResvAdvancedService.CheckInRequest)request).GetAwaiter().GetResult();
                        case "checkout":
                            return owsAdvClient.CheckOutAsync(headerAdv, (OWSResvAdvancedService.CheckOutRequest)request).GetAwaiter().GetResult();
                        case "guestmessages":
                            return owsAdvClient.GuestMessagesAsync(headerAdv, (OWSResvAdvancedService.GuestMessagesRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                using (var scope = new OperationContextScope(owsAdvClient.InnerChannel))
                {
                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(headerAdv.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    try
                    {
                        //headerAdv.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        //headerAdv.Authentication.UserCredentials.UserPassword = null;
                        //headerAdv.Authentication.UserCredentials.Domain = "OWS";
                        switch (action.ToLower())
                        {
                            case "invoice": // token error
                                return owsAdvClient.InvoiceAsync(headerAdv, (OWSResvAdvancedService.InvoiceRequest)request).GetAwaiter().GetResult();
                            case "checkin": // ok
                                return owsAdvClient.CheckInAsync(headerAdv, (OWSResvAdvancedService.CheckInRequest)request).GetAwaiter().GetResult();
                            case "checkout":
                                return owsAdvClient.CheckOutAsync(headerAdv, (OWSResvAdvancedService.CheckOutRequest)request).GetAwaiter().GetResult();
                            case "guestmessages":
                                return owsAdvClient.GuestMessagesAsync(headerAdv, (OWSResvAdvancedService.GuestMessagesRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }
        private object ExecuteNameRequest(object request, string action)
        {
            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {

                try
                {
                    switch (action.ToLower())
                    {
                        case "fetchaddress":
                            return owsNameClient.FetchAddressListAsync(headerName, (OWSNameService.FetchAddressListRequest)request).GetAwaiter().GetResult();
                        case "updateemail":
                            return owsNameClient.UpdateEmailAsync(headerName, (OWSNameService.UpdateEmailRequest)request).GetAwaiter().GetResult();
                        case "insertemail":
                            return owsNameClient.InsertEmailAsync(headerName, (OWSNameService.InsertEmailRequest)request).GetAwaiter().GetResult();
                        case "fetchemail":
                            return owsNameClient.FetchEmailListAsync(headerName, (OWSNameService.FetchEmailListRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                using (var scope = new OperationContextScope(owsNameClient.InnerChannel))
                {
                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(headerName.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    try
                    {
                        //headerName.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        //headerName.Authentication.UserCredentials.UserPassword = null;
                        //headerName.Authentication.UserCredentials.Domain = "OWS";
                        switch (action.ToLower())
                        {
                            case "fetchaddress":
                                return owsNameClient.FetchAddressListAsync(headerName, (OWSNameService.FetchAddressListRequest)request).GetAwaiter().GetResult();
                            case "updateemail":
                                return owsNameClient.UpdateEmailAsync(headerName, (OWSNameService.UpdateEmailRequest)request).GetAwaiter().GetResult();
                            case "insertemail":
                                return owsNameClient.InsertEmailAsync(headerName, (OWSNameService.InsertEmailRequest)request).GetAwaiter().GetResult();
                            case "fetchemail":
                                return owsNameClient.FetchEmailListAsync(headerName, (OWSNameService.FetchEmailListRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }
        //public class SoapSecurityHeader : MessageHeader
        //{
        //    private readonly string _password, _username, _expiredDate, _createdDate;

        //    public SoapSecurityHeader(string id, string username, string password, string expired, string created)
        //    {
        //        _password = password;
        //        _username = username;
        //        _expiredDate = expired;
        //        _createdDate = created;
        //        this.Id = id;
        //    }

        //    public string Id { get; set; }

        //    public override string Name
        //    {
        //        get { return "Security"; }
        //    }

        //    public override string Namespace
        //    {
        //        get { return "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"; }
        //    }

        //    protected override void OnWriteStartHeader(System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
        //    {
        //        writer.WriteStartElement("wsse", Name, Namespace);
        //        writer.WriteXmlnsAttribute("wsse", Namespace);
        //        writer.WriteXmlnsAttribute("wsu", Namespace);
        //    }


        //    protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        //    {
        //        writer.WriteStartElement("wsse", "UsernameToken", Namespace);
        //        //writer.WriteAttributeString("wsu:Id", "UsernameToken-15");
        //        writer.WriteAttributeString("wsu:Id", this.Id);
        //        writer.WriteAttributeString("xmlns:wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

        //        writer.WriteStartElement("wsse", "Username", Namespace);
        //        writer.WriteValue(_username);
        //        writer.WriteEndElement();

        //        writer.WriteStartElement("wsse", "Password", Namespace);
        //        writer.WriteAttributeString("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest");
        //        writer.WriteValue(_password);
        //        writer.WriteEndElement();
        //        writer.WriteEndElement();

        //        writer.WriteStartElement("wsu", "Timestamp", "TS-FC74A697E2DA91DA43161349474033561");

        //        writer.WriteStartElement("wsu:Created");
        //        writer.WriteValue(_createdDate);
        //        writer.WriteEndElement();

        //        writer.WriteStartElement("wsse", "Expires", Namespace);
        //        writer.WriteValue(_expiredDate);
        //        writer.WriteEndElement();

        //        writer.WriteEndElement();
        //    }
        //}

        ///// <summary>
        ///// Represents a message inspector object that can be added to the <c>MessageInspectors</c> collection to view or modify messages.
        ///// </summary>
        //public class ClientMessageInspector : IClientMessageInspector
        //{
        //    /// <summary>
        //    /// Enables inspection or modification of a message before a request message is sent to a service.
        //    /// </summary>
        //    /// <param name="request">The message to be sent to the service.</param>
        //    /// <param name="channel">The WCF client object channel.</param>
        //    /// <returns>
        //    /// The object that is returned as the <paramref name="correlationState " /> argument of
        //    /// the <see cref="M:System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        //    /// This is null if no correlation state is used.The best practice is to make this a <see cref="T:System.Guid" /> to ensure that no two
        //    /// <paramref name="correlationState" /> objects are the same.
        //    /// </returns>
        //    public object BeforeSendRequest(ref Message request, IClientChannel channel)
        //    {
        //        ///enter your username and password here.
        //        SoapSecurityHeader header = new SoapSecurityHeader("UsernameToken-FC74A697E2DA91DA43161349474033562", "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", "nonce-test", DateTime.UtcNow.ToString());
        //        request.Headers.Add(header);
        //        return header.Name;
        //    }

        //    /// <summary>
        //    /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        //    /// </summary>
        //    /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        //    /// <param name="correlationState">Correlation state data.</param>
        //    public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        //    {

        //    }
        //}

        ///// <summary>
        ///// Represents a run-time behavior extension for a client endpoint.
        ///// </summary>
        //public class CustomEndpointBehavior : IEndpointBehavior
        //{
        //    /// <summary>
        //    /// Implements a modification or extension of the client across an endpoint.
        //    /// </summary>
        //    /// <param name="endpoint">The endpoint that is to be customized.</param>
        //    /// <param name="clientRuntime">The client runtime to be customized.</param>
        //    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        //    {
        //        clientRuntime.ClientMessageInspectors.Add(new ClientMessageInspector());
        //    }

        //    /// <summary>
        //    /// Implement to pass data at runtime to bindings to support custom behavior.
        //    /// </summary>
        //    /// <param name="endpoint">The endpoint to modify.</param>
        //    /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
        //    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        //    {
        //        // Nothing special here
        //    }

        //    /// <summary>
        //    /// Implements a modification or extension of the service across an endpoint.
        //    /// </summary>
        //    /// <param name="endpoint">The endpoint that exposes the contract.</param>
        //    /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
        //    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        //    {
        //        // Nothing special here
        //    }

        //    /// <summary>
        //    /// Implement to confirm that the endpoint meets some intended criteria.
        //    /// </summary>
        //    /// <param name="endpoint">The endpoint to validate.</param>
        //    public void Validate(ServiceEndpoint endpoint)
        //    {
        //        // Nothing special here
        //    }
        //}


    }



}