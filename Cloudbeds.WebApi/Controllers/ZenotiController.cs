﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Models.Zenoti;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;
using Cloudbeds.Data;
using System.Text;
using Cloudbeds.WebApi.Models.Zenoti.Enums;
using System.Globalization;
using CloudBaseWeb.Api.Entities;
using Microsoft.EntityFrameworkCore;
using Cloudbeds.WebApi.Models.Zenoti.Cart;
using static Cloudbeds.WebApi.Models.Zenoti.Enums.Enums;
using Microsoft.Extensions.Caching.Memory;

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/zenoti")]
    [ApiController]
    [AllowAnonymous]
    [Authorize]
    public class ZenotiController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly Uri _apiBaseUrl;
        private readonly string _centerId;
        private readonly string _apiKey;
        private readonly string _host;
        private readonly MobileKeyDbContext _context;
        private IMemoryCache _cache;

        public ZenotiController(IConfiguration configuration, MobileKeyDbContext context, IMemoryCache cache)
        {
            _configuration = configuration;
            _apiBaseUrl = new Uri(configuration["Zenoti:BaseUrl"]);
            _centerId = configuration["Zenoti:CenterId"];
            _apiKey = configuration["Zenoti:ApiKey"];
            _host = configuration["Zenoti:Host"];
            _context = context;
            _cache = cache;
        }
      
        [HttpGet]
        [Route("getChooseServiceList")]
        public async Task<List<ChooseService>> GetChooseServiceList()
        {
            try
            {
                var data = new List<ChooseService>();

                data.Add(new ChooseService
                {
                    Name = "Spa Treatments",
                    ImageUrl = "/assets/img/spa-menu/treatments.png",
                    Type = Enums.Services.Treatments
                });

                data.Add(new ChooseService
                {
                    Name = "Thermal Springs",
                    ImageUrl = "/assets/img/spa-menu/Hotsprings.jpg",
                    Type = Services.Hotsprings
                });

                var categories = await _context.Set<Zenoti_Category>().ToListAsync();

                //foreach (var category in categories)
                //{
                //    data.Add(new ChooseService
                //    {
                //        Name = "CLASSES",//category.Name.ToUpper(),
                //        ImageUrl = category.Image,
                //        Type = Services.Classes
                //    });
                //}

                data.Add(new ChooseService
                {
                    Name = "Classes & Experiences",
                    ImageUrl = categories.FirstOrDefault().Image,
                    Type = Services.Classes
                });

                return data;
             
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getActivityListMenuItems")]
        public async Task<List<ActivityListData>> getActivityListMenuItems(string start, string end, string category)
        {
            try
            {
                var data = new List<ActivityListData>();

                var classResponse = await GetClasses(start, end);

                if (classResponse == null || classResponse.Categories ==  null)
                    return data;

                var categoryData = classResponse.Categories.FirstOrDefault(x => x.Name == category && x.ParentId == null);

                data.Add(new ActivityListData
                {
                    Id = categoryData != null ? categoryData.Id : 0,
                    Name =  "All Classes"
                });

                var parentCategory = classResponse.Categories.FirstOrDefault(x => /*x.Name == category &&*/ x.ParentId == null);

                if (parentCategory != null)
                {
                    var categories = classResponse.Categories.Where(x => x.ParentId == parentCategory.Id).ToList();

                    foreach (var c in categories)
                    {
                        if (!data.Any(x => x.Name == c.Name))
                        {
                            data.Add(new ActivityListData
                            {
                                Id = c.Id,
                                Name = c.Name
                            });
                        }
                    }
                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getActivityListClass")]
        public async Task<List<ClassSlot>> GetActivityListClass(long categoryId, bool parent, string start, string end)
        {
            try
            {
                var data = new List<ClassSlot>();
                var sessions = new List<Session>();
                var classesIds = new List<long>();
                var categoriesIds = new List<long>();

                var classResponse = await GetClasses(start, end);

                if (classResponse == null || classResponse.Categories == null)
                    return data;

                if (categoryId == 0)
                    sessions = classResponse.Sessions;
                else
                {
                    if (parent)
                    {
                        categoriesIds = classResponse.Categories.Where(x => x.ParentId == categoryId).Select(x => x.Id).ToList();
                        classesIds = classResponse.Classes.Where(x => categoriesIds.Contains(x.CategoryId)).Select(x => x.Id).ToList();
                    }
                    else                    
                        classesIds = classResponse.Classes.Where(x => x.CategoryId == categoryId).Select(x => x.Id).ToList();                    

                    sessions = classResponse.Sessions.Where(x => classesIds.Contains(x.ClassId)).OrderBy(x => x.StartTime).ToList();

                }

                foreach (var session in sessions)
                {
                    if (!data.Any(x => x.SessionId == session.Id))
                    {
                        var hour = session.StartTime.Hour.ToString().Length == 1 ? "0" + session.StartTime.Hour : session.StartTime.Hour.ToString();
                        var minute = session.StartTime.Minute.ToString().Length == 1 ? "0" + session.StartTime.Minute : session.StartTime.Minute.ToString();

                        CultureInfo ci = new CultureInfo("en-US");

                        var newDate = new DateTime(session.StartTime.Year, session.StartTime.Month, session.StartTime.Day);

                        var currentInstructor = classResponse.Instructors.FirstOrDefault(x => x.Id == session.InstructorId);

                        data.Add(new ClassSlot
                        {
                            SessionId = session.Id,
                            ClassId = session.ClassId,
                            Name = session.Name,
                            TechnicianId = session.InstructorId,
                            TechnicianName = currentInstructor.Name,
                            Description = session.Description,
                            FullDate = session.StartTime,
                            Date = newDate.ToString("MMMM dd, yyyy", ci),
                            Day = session.StartTime.Day,
                            Duration = Convert.ToInt32((session.EndTime - session.StartTime).TotalMinutes),
                            DefaultPrice = session.Price,
                            Price = session.Price,
                            Schedule = (session.StartTime.Hour > 12 ? "0" + (session.StartTime.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (session.StartTime.Hour >= 12 ? " PM" : " AM"),
                            DayPeriod = ((session.StartTime.Hour >= 4) && (session.StartTime.Hour <= 12)) ? 1 : ((session.StartTime.Hour >= 11) && (session.StartTime.Hour <= 17)) ? 2 : 3

                    });
                    }
                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getTechnicians/{date}")]
        public async Task<List<Therapist>> GetTechnicians(string date)
        {
            try
            {
                List<Therapist> cacheEntry = new List<Therapist>();
                //if (!_cache.TryGetValue("technicians", out cacheEntry))
                //{
                var technicianList = await GetTherapists(date);

                for(var i = 0; i < technicianList.Therapists.Count; i++)
                {
                    technicianList.Therapists[i].Show = true;
                }

                var info1 = new TherapistPersonalInfo();
                info1.Name = "Female";
                info1.NickName = "Instructor";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info1,
                        Id = "0",
                        Code = "Female",
                        Gender = 2
                    });

                var info2 = new TherapistPersonalInfo();
                info2.Name = "Male";
                info2.NickName = "Instructor";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info2,
                        Id = "1",
                        Code = "Male",
                        Gender = 1
                    });


                var info3 = new TherapistPersonalInfo();
                info3.Name = "All Instructors";
                info3.NickName = "Instructors";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info3,
                        Id = "-1",
                        Code = "All",
                        Gender = 0
                    });

                cacheEntry = technicianList.Therapists;

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(3));

                _cache.Set("technicians", cacheEntry, cacheEntryOptions);
                //}

                return cacheEntry;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getTechniciansPricing/{serviceId}")]
        public async Task<List<Therapist>> GetTechniciansPricing(string serviceId)
        {
            try
            {
                List<Therapist> cacheEntry = new List<Therapist>();
                var technicianList = await GetTherapistsHotspringsPricing(serviceId);

                for (var i = 0; i < technicianList.Therapists.Count; i++)
                {
                    technicianList.Therapists[i].Show = true;
                }

                var info1 = new TherapistPersonalInfo();
                info1.NickName = "Instructor";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info1,
                        Id = "0",
                        Code = "Female",
                        Gender = 0,
                        DisplayName = "Female"
                    });

                var info2 = new TherapistPersonalInfo();
                info2.NickName = "Instructor";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info2,
                        Id = "1",
                        Code = "Male",
                        Gender = 1,
                        DisplayName = "Male"
                    });


                var info3 = new TherapistPersonalInfo();
                info3.NickName = "Instructors";

                technicianList.Therapists.Insert(0,
                    new Therapist
                    {
                        PersonalInfo = info3,
                        Id = "-1",
                        Code = "All",
                        Gender = 0,
                        DisplayName = "All Instructors"
                    });

                return technicianList.Therapists;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getServiceCategories")]
        public async Task<List<CategoryData>> GetServiceCategories([FromBody] ServiceCategoriesRequest request)
        {
            try
            {
                var data = new ServiceCategoryData();
                data.CategoryData = new List<CategoryData>();                

                var servicesResponse = await GetServices(ServiceType.Service);

                if (servicesResponse.Services == null)
                    return data.CategoryData;

                var categoryList = servicesResponse.Services.Where(x => x.AddOnsInfo.IsAddOn == false && !request.CategoryIds.Contains(x.AdditionalInfo.Category.Id.ToString())).GroupBy(x => new { x.AdditionalInfo.Category.Id, x.AdditionalInfo.Category.Name }).Distinct().ToList();
                var imageList = servicesResponse.Services.Where(x => x.AddOnsInfo.IsAddOn == false && x.ImagePaths != null).GroupBy(x => new { x.AdditionalInfo.Category.Id, x.ImagePaths }).Distinct().ToList();

                foreach (var category in categoryList)
                {
                    var categoryData = new CategoryData();

                    categoryData.Id = category.Key.Id;
                    categoryData.Name = category.Key.Name;

                    for(int i=0; i < imageList.Count; i++)
                    {
                        if (imageList[i].Key.Id == category.Key.Id)
                        {
                            categoryData.ImagePaths = imageList[i].Key.ImagePaths.ToString();
                            break;
                        }
                    }
                    categoryData.Services.AddRange(category.Distinct());

                    var zenotiCategoryData = await GetCategory(categoryData.Id.ToString());
                    categoryData.display_order = zenotiCategoryData.display_order;
                    data.CategoryData.Add(categoryData);                    
                    
                }

                return data.CategoryData.OrderBy(c => c.display_order).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getCategorySchedules")]
        public async Task<List<GetAvailableSlotsResponse>> GetCategorySchedules([FromBody] CategorySchedulesRequest data)
        {
            try
            {
                var request = new CreateBookingRequest();
                var slotsResponseList = new List<GetAvailableSlotsResponse>();

                request.CenterId = new Guid(_centerId);
                request.IsDoubleBookingEnabled = true;
                request.Date = data.Date.ToString("yyyy-MM-dd");

                foreach (var id in data.ServiceIds)
                {

                    var itemElement = new BookingRequestItemElement();
                    var item = new BookingRequestItem();
                    var therapist = new BookingRequestTherapist();
                    var addons = new List<BookingRequestAddOn>();

                    item.Id = id;
                    itemElement.Item = item;

                    if (!string.IsNullOrEmpty(data.TherapistId) && data.TherapistId != "-1")
                        therapist.Id = new Guid(data.TherapistId);

                    if (data.Gender != null)
                        therapist.Gender = data.Gender.Value;

                    itemElement.Therapist = therapist;

                    foreach (var guestId in data.GuestIds)
                    {
                        var guest = new BookingRequestGuest();
                        guest.Id = guestId;
                        guest.Items.Add(itemElement);
                        request.Guests.Add(guest);
                    }
                    if (data.AddonIds != null)
                    {
                        foreach (var addonId in data.AddonIds)
                        {
                            var addonItem = new BookingRequestAddOn();
                            addonItem.BookingRequestItem = new BookingRequestItem();
                            addonItem.BookingRequestItem.Id = addonId;

                            addons.Add(addonItem);
                        }
                    }

                    itemElement.AddOns = addons;

                    var serviceBookingResponse = await CreateServiceBooking(request);

                    if (serviceBookingResponse.Error != null)
                        return null;

                    var bookingId = serviceBookingResponse.Id.ToString();

                    var slotsResponse = await GetAvailableSlots(bookingId);

                    if (slotsResponse != null && slotsResponse.Slots != null)
                    {
                        for (var i = 0; i < slotsResponse.Slots.Count(); i++)
                        {
                            var slot = slotsResponse.Slots[i];
                            var hour = slot.Time.Hour.ToString().Length == 1 ? "0" + slot.Time.Hour : slot.Time.Hour.ToString();
                            var minute = slot.Time.Minute.ToString().Length == 1 ? "0" + slot.Time.Minute : slot.Time.Minute.ToString();
                            slotsResponse.Slots[i].ShortTime = (slot.Time.Hour > 12 ? "0" + (slot.Time.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (slot.Time.Hour >= 12 ? " PM" : " AM");
                            slotsResponse.Slots[i].DayPeriod = ((slot.Time.Hour >= 4) && (slot.Time.Hour <= 12)) ? 1 : ((slot.Time.Hour >= 11) && (slot.Time.Hour <= 17)) ? 2 : 3;
                            slotsResponse.Slots[i].StartTime = int.Parse(hour + "" + minute);
                            slotsResponse.Slots[i].Hour = slot.Time.Hour;
                        }

                        slotsResponse.BookingId = bookingId;
                        slotsResponse.Date = request.Date;
                        //slotsResponse.Guest = guest.Id;
                        slotsResponse.ServiceId = id;

                        slotsResponseList.Add(slotsResponse);
                    }

                }


                return slotsResponseList;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("getAddonsTimes")]
        public async Task<List<Cart>> GetAddonsTimes([FromBody] List<Cart> data)
        {
            var servicesResponse = await GetServices(ServiceType.Addon);

            foreach (var item in data)
            {
                foreach(var service in item.BookingServicesData)
                {
                    var request = new CategorySchedulesRequest();
                    request.Date = DateTime.Parse(service.Date);
                    request.GuestIds = new List<string>();// item.Guest.Select(x => x.Id).ToList();
                    request.ServiceIds = new List<Guid>();
                    request.ServiceIds.AddRange(service.Addons.Take(2));

                    var addons = await GetCategorySchedules(request);

                    service.AddonsDetail = new List<BookingServicesData>();

                    foreach(var addon in addons)
                    {
                        var currentService = servicesResponse.Services.FirstOrDefault(x => x.Id == addon.ServiceId);
                        var endTime = DateTime.Parse(service.Date).AddMinutes(service.Duration);
                        var slotTime = addon.Slots.FirstOrDefault(x => x.Time >= endTime);

                        if (slotTime != null)
                        {
                            service.AddonsDetail.Add(
                                new BookingServicesData()
                                {
                                    Date = slotTime.Time.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"),
                                    BookingId = addon.BookingId,
                                    Name = currentService.Name,
                                    Duration = currentService.Duration,
                                    Id = currentService.Id.ToString(),
                                    IsService = true,
                                    IsAddon = true,
                                    Price = currentService.PriceInfo.SalePrice,
                                    Schedule = slotTime.ShortTime
                                });
                        }
                    }

                }

            }

            return data;
        }

        [HttpGet]
        [Route("getFullSpaList/{start}/{end}")]
        public async Task<List<FullSpaService>> GetFullSpaList(string start, string end)
        {
            try
            {
                var data = new List<FullSpaService>();

                var servicesResponse = await GetServices(ServiceType.Service);
                var classesResponse = await GetClasses(start, end);

                var services = servicesResponse.Services.Where(x => x.AddOnsInfo.IsAddOn == false && x.AdditionalInfo.Category.Name.ToLower() != "thermal springs").ToList();
                var hotsprings = servicesResponse.Services.Where(x => x.AddOnsInfo.IsAddOn == false && x.AdditionalInfo.Category.Name.ToLower() == "thermal springs").ToList();

                data.Add(new FullSpaService()
                {
                    Name = "Spa Treatments",
                    Type = Services.Treatments,
                    ImageUrl = "/assets/img/spa-menu/treatments.png"
                });

                if (servicesResponse != null)
                {
                    data[0].Services = services;
                }

                data.Add(new FullSpaService()
                {
                    Name = "Thermal Springs",
                    Type = Services.Hotsprings,
                    ImageUrl = "/assets/img/spa-menu/Hotsprings.jpg"
                });

                if (servicesResponse != null)
                {
                    data[1].Services = hotsprings;
                }

                var categories = await _context.Set<Zenoti_Category>().ToListAsync();

                data.Add(new FullSpaService()
                {
                    Name = "Classes & Experiences",
                    Type = Services.Classes,
                    ImageUrl = categories.FirstOrDefault().Image
                    //ImageUrl = "/assets/img/spa-menu/Hotsprings.jpg"
                });

                data[2].Classes = classesResponse.Classes;


                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getServices/{type}")]
        public async Task<GetServiceResponse> GetServices(ServiceType type)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    //RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/services?expand=additional_info&expand=add_ons_info&expand=image_paths&only_add_ons=false&catalog_enabled=true&page=1&size=50"),
                    RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/services?expand=additional_info&expand=image_paths&expand=add_ons_info&page=1&size=50"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetServiceResponse>(jsonContent);

                    if (type == ServiceType.All)
                        return result;

                    else if (type == ServiceType.Service)
                    {
                        result.Services = result.Services.Where(x => !x.AddOnsInfo.IsAddOn).ToList();
                        return result;                         
                    }
                    else
                    {
                        result.Services = result.Services.Where(x => x.AddOnsInfo.IsAddOn).ToList();
                        return result;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpGet]
        [Route("getService/{id}")]
        public async Task<Service> GetService(string id)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/services/{id}?expand=additional_info&expand=add_ons_info&expand=image_paths&only_add_ons=false&catalog_enabled=true"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Service>(jsonContent);

                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpGet]
        [Route("getClasses")]
        public async Task<GetClassResponse> GetClasses(string start, string end)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}classes/sessions?center_id={_centerId}&start_date={start}&end_date={end}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetClassResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getTherapists")]
        public async Task<GetTherapistResponse> GetTherapists(string date)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/therapists?date={date}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetTherapistResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getTherapistsHotspringsPricing")]
        public async Task<GetTherapistPricingResponse> GetTherapistsHotspringsPricing(string serviceId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/services/{serviceId}/therapists"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetTherapistPricingResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getRooms")]
        public async Task<GetRoomResponse> GetRooms()
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}/centers/{_centerId}/rooms"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetRoomResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("createServiceBooking")]
        public async Task<CreateBookingResponse> CreateServiceBooking(CreateBookingRequest data)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                // override employee -> customer (atran@trueomni.com)
                data.IsDoubleBookingEnabled = true;
                
                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}bookings"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CreateBookingResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getAvailableSlots/{bookingId}")]
        public async Task<GetAvailableSlotsResponse> GetAvailableSlots(string bookingId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}bookings/{bookingId}/slots"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetAvailableSlotsResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("reserveSlot/{bookingId}")]
        public async Task<ReserveSlotResponse> ReserveSlot(string bookingId, ReserveSlotRequest data)
        {

            var result = new ReserveSlotResponse();

            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}bookings/{bookingId}/slots/reserve"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ReserveSlotResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Error = "error when trying to reserve current slot time";
                return result;
            }
        }

        [HttpPost]
        [Route("confirmServiceBooking/{bookingId}")]
        public async Task<ConfirmServiceBookingResponse> ConfirmServiceBooking(string bookingId, bool multiple = false)
        {
            var result = new ConfirmServiceBookingResponse();

            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                var objAsJson = "";

                if (multiple)
                {
                    var data = new GroupData();
                    data.GroupName = "Group Booking: " + bookingId;
                    objAsJson = JsonConvert.SerializeObject(data);
                }

                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}bookings/{bookingId}/slots/confirm"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ConfirmServiceBookingResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Error = "Error trying to confirm current booking";
                return result;
            }
        }

        [HttpPost]
        [Route("checkinAppointment/{appointmentId}")]
        public async Task<ConfirmServiceBookingResponse> CheckinAppointment(string appointmentId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = new Uri($"{_apiBaseUrl}appointments/{appointmentId}/checkin"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<ConfirmServiceBookingResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("completeBooking")]
        public async Task<bool> CompleteBooking([FromBody] List<Cart> data)
        {
            try
            {                
                var guests = data.Select(x => x.Guest).ToList();

                var multiGuest = data.SelectMany(x => x.MultiGuest).ToList();

                //One Cart Item (Single guest)
                if (guests.Count() == 1 && multiGuest.Count == 0)
                {
                    var guestList = guests.Select(x => x.Id).ToList();

                    var cart = data.FirstOrDefault();
                    var bookingResponse = new ConfirmServiceBookingResponse();
                    var startTime = new DateTime();
                    var endTime = new DateTime();
                    var duration = 0;
                    var currentService = 1;

                    foreach (var service in cart.BookingServicesData)
                    {
                        // check and swap out bookingId for existing Guest
                        var slot = new ReserveSlotRequest();
                        slot.SlotTime = service.Date;

                        if (currentService == 1)
                        {
                            if (service.AddonsDetail.Count == 0 && !service.IsUpdated && !string.IsNullOrEmpty(service.BookingId))
                                bookingResponse = await ProcessService(service, slot);
                            else                            
                                bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                        }
                        else
                        {
                            bookingResponse = new ConfirmServiceBookingResponse();
                            if (endTime == DateTime.Parse(service.Date) || service.AddonsDetail.Count != 0 || service.IsUpdated || string.IsNullOrEmpty(service.BookingId))
                                bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                            else
                                bookingResponse = await ProcessService(service, slot);
                        }

                        startTime = DateTime.Parse(service.Date);
                        endTime = DateTime.Parse(service.Date).AddMinutes(service.Duration);
                        duration = service.Duration;

                        currentService++;
                    }

                    //Process Classes

                    //1 - get user token
                    //var userToken = await GetUserToken();

                    /*
                    var today = DateTime.Now;

                    if (userToken.AccessTokenExpiry < today)
                    {
                        var request = new RefreshTokenRequest();
                        request.GrantType = "refresh_token";
                        request.RefreshToken = userToken.RefreshToken;

                        var refreshTokenResponse = await RefreshToken("marc.c", request);

                        if (refreshTokenResponse != null)
                            userToken = await GetUserToken();
                        else //generate new token
                        {

                            var generateNewTokenResponse = await GenerateToken("marc.c");

                            if (generateNewTokenResponse != null)
                                userToken = await GetUserToken();
                            else
                            {
                                //TODO
                            }

                        }
                    }
                    */

                    //2 - register
                    foreach (var c in cart.BookingClassesData)
                    {
                        var classRequest = new ClassRegistrationRequest();
                        classRequest.CenterId = _centerId;
                        classRequest.SessionId = c.SessionId;
                        classRequest.BookingSource = 0;
                        classRequest.Waitlist = true;
                        classRequest.GuestId = guestList.FirstOrDefault();

                        var classRegistration = await ClassRegistration(classRequest, c.ClassId);
                    }

                }
                else if (guests.Count() > 1 && multiGuest.Count == 0) //multiple separated guests
                {
                    //TODO: Pending same room by each service
                    //IE: if guest one has service 1 at 08:00 AM
                    //AND guest 2 has same service and schedule
                    //we need to try to assign same room
                    foreach ( var guest in guests)
                    {
                        var cart = data.First(x => x.Guest.Id == guest.Id);
                        var bookingResponse = new ConfirmServiceBookingResponse();
                        var startTime = new DateTime();
                        var endTime = new DateTime();
                        var duration = 0;
                        var currentService = 1;

                        foreach (var service in cart.BookingServicesData)
                        {
                            var slot = new ReserveSlotRequest();
                            slot.SlotTime = service.Date;
                            var guestList = new List<string>
                            {
                                guest.Id
                            };

                            if (currentService == 1)
                            {
                                if (service.AddonsDetail.Count == 0 && !service.IsUpdated)
                                    bookingResponse = await ProcessService(service, slot);
                                else
                                    bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                            }
                            else
                            {
                                if (endTime == DateTime.Parse(service.Date) || service.IsUpdated)
                                    bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                                else
                                    bookingResponse = await ProcessService(service, slot);
                            }

                            startTime = DateTime.Parse(service.Date);
                            endTime = DateTime.Parse(service.Date).AddMinutes(service.Duration);
                            duration = service.Duration;

                            currentService++;
                        }

                        //Process Classes

                        //1 - get user token
                        //var userToken = await GetUserToken();

                        /*
                        var today = DateTime.Now;

                        if (userToken.AccessTokenExpiry < today)
                        {
                            var request = new RefreshTokenRequest();
                            request.GrantType = "refresh_token";
                            request.RefreshToken = userToken.RefreshToken;

                            var refreshTokenResponse = await RefreshToken("marc.c", request);

                            if (refreshTokenResponse != null)
                                userToken = await GetUserToken();
                            else //generate new token
                            {

                                var generateNewTokenResponse = await GenerateToken("marc.c");

                                if (generateNewTokenResponse != null)
                                    userToken = await GetUserToken();
                                else
                                {
                                    //TODO
                                }

                            }
                        }

                        */

                        //2 - register
                        foreach (var c in cart.BookingClassesData)
                        {
                            var classRequest = new ClassRegistrationRequest();
                            classRequest.CenterId = _centerId;
                            classRequest.SessionId = c.SessionId;
                            classRequest.BookingSource = 0;
                            classRequest.GuestId = guest.Id;

                            var classRegistration = await ClassRegistration(classRequest, c.ClassId);
                        }
                    }
                }
                else //Couples, ALL Guests
                {
                    var guestList = multiGuest.Select(x => x.Id).ToList();

                    var cart = data.FirstOrDefault(x => x.Guest.Id.Length == 1);
                    var bookingResponse = new ConfirmServiceBookingResponse();
                    var startTime = new DateTime();
                    var endTime = new DateTime();
                    var duration = 0;
                    var currentService = 1;

                    foreach (var service in cart.BookingServicesData)
                    {
                        var slot = new ReserveSlotRequest();
                        slot.SlotTime = service.Date;

                        if (currentService == 1)
                        {
                            if (service.AddonsDetail.Count == 0 && !service.IsUpdated)
                                bookingResponse = await ProcessService(service, slot, true);
                            else
                                bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                        }
                        else
                        {
                            if (endTime == DateTime.Parse(service.Date) || service.IsUpdated)
                                bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                            else
                                bookingResponse = await ProcessService(service, slot, true);
                        }

                        startTime = DateTime.Parse(service.Date);
                        endTime = DateTime.Parse(service.Date).AddMinutes(service.Duration);
                        duration = service.Duration;

                        currentService++;
                    }

                    //Process Classes

                    //1 - get user token
                    //var userToken = await GetUserToken();

                    /*
                    var today = DateTime.Now;

                    if (userToken.AccessTokenExpiry < today)
                    {
                        var request = new RefreshTokenRequest();
                        request.GrantType = "refresh_token";
                        request.RefreshToken = userToken.RefreshToken;

                        var refreshTokenResponse = await RefreshToken("marc.c", request);

                        if (refreshTokenResponse != null)
                            userToken = await GetUserToken();
                        else //generate new token
                        {

                            var generateNewTokenResponse = await GenerateToken("marc.c");

                            if (generateNewTokenResponse != null)
                                userToken = await GetUserToken();
                            else
                            {
                                //TODO
                            }

                        }
                    }
                    */

                    //2 - register all guests
                    foreach (var guest in guestList)
                    {
                        foreach (var c in cart.BookingClassesData)
                        {
                            var classRequest = new ClassRegistrationRequest();
                            classRequest.CenterId = _centerId;
                            classRequest.SessionId = c.SessionId;
                            classRequest.BookingSource = 0;
                            classRequest.GuestId = guest;

                            var classRegistration = await ClassRegistration(classRequest, c.ClassId);
                        }
                    }

                    var singleGuestBookings = data.Where(x => x.Guest.Id.Length != 1).ToList();

                    foreach (var currentCart in singleGuestBookings)
                    {
                        cart = data.First(x => x.Guest.Id == currentCart.Guest.Id);
                        bookingResponse = new ConfirmServiceBookingResponse();
                        startTime = new DateTime();
                        endTime = new DateTime();
                        duration = 0;
                        currentService = 1;

                        foreach (var service in cart.BookingServicesData)
                        {
                            var slot = new ReserveSlotRequest();
                            slot.SlotTime = service.Date;
                            guestList = new List<string>
                            {
                                cart.Guest.Id
                            };

                            if (currentService == 1)
                            {
                                if (service.AddonsDetail.Count == 0 && !service.IsUpdated)
                                    bookingResponse = await ProcessService(service, slot);
                                else
                                    bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                            }
                            else
                            {
                                if (endTime == DateTime.Parse(service.Date) || service.IsUpdated)
                                    bookingResponse = await GenerateNewReservation(bookingResponse, guestList, slot, service);
                                else
                                    bookingResponse = await ProcessService(service, slot);
                            }

                            startTime = DateTime.Parse(service.Date);
                            endTime = DateTime.Parse(service.Date).AddMinutes(service.Duration);
                            duration = service.Duration;

                            currentService++;
                        }

                        //Process Classes

                        //1 - get user token
                        //userToken = await GetUserToken();

                        //2 - register
                        foreach (var c in cart.BookingClassesData)
                        {
                            var classRequest = new ClassRegistrationRequest();
                            classRequest.CenterId = _centerId;
                            classRequest.SessionId = c.SessionId;
                            classRequest.BookingSource = 0;
                            classRequest.GuestId = cart.Guest.Id;

                            var classRegistration = await ClassRegistration(classRequest, c.ClassId);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private async Task<ConfirmServiceBookingResponse> GenerateNewReservation(ConfirmServiceBookingResponse bookingResponse, List<string> guestIds, ReserveSlotRequest slot, BookingServicesData service)
        {
            var request = new CreateBookingRequest();

            request.CenterId = new Guid(_centerId);
            request.Date = service.Date;

            var itemElement = new BookingRequestItemElement();
            var item = new BookingRequestItem();
            var therapist = new BookingRequestTherapist();
            var requestRoom = new BookingRequestRoom();
            var addons = new List<BookingRequestAddOn>();

            item.Id = new Guid(service.Id);
            itemElement.Item = item;

            if (bookingResponse.Invoice != null && bookingResponse.Error == null)
            {
                requestRoom.Id = bookingResponse.Invoice.Items.FirstOrDefault().Room.Id;
                itemElement.Room = requestRoom;
            }

            therapist.Id = service.SelectedTherapist.Id;
            therapist.Gender = service.SelectedTherapist.Gender;

            itemElement.Therapist = therapist;

            var addonIds = service.AddonsDetail.Select(x => x.Id).ToList();
            addonIds.AddRange(service.SelectedAddons.Where(x => x.Selected).Select(x => x.Id).ToList());

            foreach (var guestId in guestIds)
            {
                var guest = new BookingRequestGuest();
                guest.Id = guestId;
                guest.Items.Add(itemElement);
                request.Guests.Add(guest);
            }

            foreach (var addonId in addonIds)
            {
                var addonItem = new BookingRequestAddOn();
                addonItem.BookingRequestItem = new BookingRequestItem();
                addonItem.BookingRequestItem.Id = new Guid(addonId);

                addons.Add(addonItem);
            }

            itemElement.AddOns = addons;

            var serviceBookingResponse = await CreateServiceBooking(request);

            if (serviceBookingResponse.Error == null)
            {
                var nextSlot = new ReserveSlotRequest();
                slot.SlotTime = service.Date;
                service.BookingId = serviceBookingResponse.Id.ToString();

                bookingResponse = await ProcessService(service, slot);
            }

            return bookingResponse;
        }

        private async Task<ConfirmServiceBookingResponse> ProcessService(BookingServicesData service, ReserveSlotRequest slot, bool multiple = false)
        {
            var result = new ConfirmServiceBookingResponse();

            try
            {
                var reserveSlotResponse = await ReserveSlot(service.BookingId, slot);

                if (reserveSlotResponse.Error != null)
                {
                    result.Error = "Error on current Reserve slot process";
                    return result;
                }

                result = await ConfirmServiceBooking(service.BookingId, multiple);

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Error = "Error on current Confirm Booking process";
                return result;
            }

        }

        [HttpPost]
        [Route("generateToken")]
        public async Task<TokenData> GenerateToken(string userName)
        {
            try
            {
                var loginUser = await GetLocalUser(userName);

                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization") || loginUser == null)
                {
                    return null;
                }

                var client = new HttpClient();

                var objAsJson = JsonConvert.SerializeObject(loginUser);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}tokens"),
                    Content = content,
                    Headers =
                    {
                        { "accept", "application/json" },
                        { "Authorization", $" {headers["Authorization"]}" }
                    }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<TokenData>(jsonContent);

                    //store token on db
                    if (result.Error == null)
                        await SaveUserToken(loginUser.Id, result.Credentials);

                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        [HttpPost]
        [Route("refreshToken")]
        public async Task<TokenData> RefreshToken(string userName, RefreshTokenRequest data)
        {
            try
            {
                var loginUser = await GetLocalUser(userName);


                var client = new HttpClient();

                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, null, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}tokens#"),
                    Content = content
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<TokenData>(jsonContent);

                    //store token on db
                    if (result.Error == null)
                        await SaveUserToken(loginUser.Id, result.Credentials);

                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        [HttpGet]
        [Route("getLocalUser")]
        public async Task<Zenoti_LoginUser> GetLocalUser(string userName)
        {
            try
            {
                var user = await _context.Set<Zenoti_LoginUser>()
                                  .Where(x => x.UserName == userName).FirstOrDefaultAsync();

                return user;
            
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getUserToken")]
        public async Task<Zenoti_Token> GetUserToken(int userId = 1)
        {
            try
            {
                var user = await _context.Set<Zenoti_LoginUser>()
                                    .Where(x => x.Id == userId).FirstOrDefaultAsync();

                if (user == null)
                    return null;

                var token = await _context.Set<Zenoti_Token>()
                                  .Where(x => x.UserId == user.Id && x.Active).FirstOrDefaultAsync();

                return token;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("saveUserToken")]
        public async Task<bool> SaveUserToken(int userId, Zenoti_Token tokenData)
        {
            try
            {
                var user = await _context.Set<Zenoti_LoginUser>()
                                    .Where(x => x.Id == userId).FirstOrDefaultAsync();

                if (user == null)
                    return false;

                var savedToken = await _context.Set<Zenoti_Token>()
                                            .Where(x => x.Id == userId).FirstOrDefaultAsync();

                savedToken.Active = true;
                savedToken.AccessToken = tokenData.AccessToken;
                savedToken.AccessTokenExpiry = tokenData.AccessTokenExpiry;
                savedToken.RefreshToken = tokenData.RefreshToken;
                savedToken.RefreshTokenExpiry = tokenData.RefreshTokenExpiry;

                _context.Update(savedToken);
                var result = await _context.SaveChangesAsync();

                return result != -1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("classRegistration/{classId}")]
        public async Task<ClassRegistrationResponse> ClassRegistration([FromBody] ClassRegistrationRequest data, int classId)
        {
            var result = new ClassRegistrationResponse();

            try
            {
                var client = new HttpClient();

                data.CenterId = _centerId;

                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}classes/{classId}/registrations"),
                    Content = content,
                    Headers =
                    {
                        { "accept", "application/json" },
                        { "Authorization", $"apikey {_apiKey}" }
                    }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ClassRegistrationResponse>(jsonContent);
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Error = "Error on Class Registration";
                return result;
            }
        }

        [HttpPut]
        [Route("classRegistrationCancel/{classId}/{registrationId}")]
        public async Task<ClassRegistrationCancelResponse> ClassRegistration(int classId, int registrationId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = new Uri($"{_apiBaseUrl}classes/{classId}/registrations/{registrationId}/cancel"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<ClassRegistrationCancelResponse>(jsonContent);
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //Guest section
        #region

        [HttpGet]
        [Route("getGuest/{guestNumber}")]
        public async Task<List<Zenoti_Guest>> GetGuest(short guestNumber)
        {
            try
            {                
                var guestList = await _context.Set<Zenoti_Guest>()
                                    .Where(x => x.Status == GuestStatus.Available).Take(guestNumber).ToListAsync();

                if (guestList.Count != guestNumber)
                {
                    var newGuests = 1;

                    do
                    {
                        var request = new CreateGuestRequest();
                        request.PersonalInfo = new PersonalInfo();
                        request.PersonalInfo.FirstName = $"Guest {newGuests}";
                        request.PersonalInfo.LastName = $"Guest {newGuests} LastName";
                        request.PersonalInfo.Gender = 1;

                        var createGuestResponse = await CreateGuest(request);

                        if (createGuestResponse != null)
                        {
                            //save on db

                            var newGuest = new Zenoti_Guest();
                            newGuest.GuestId = createGuestResponse.Id.ToString();
                            newGuest.IsActive = true;
                            newGuest.Status = GuestStatus.Available;
                            newGuest.Name = createGuestResponse.PersonalInfo.FirstName;


                            await _context.AddAsync(newGuest);
                            await _context.SaveChangesAsync();

                            newGuests++;
                        }


                    } while (newGuests <= guestNumber);

                    guestList.AddRange(await _context.Set<Zenoti_Guest>()
                                .Where(x => x.Status == GuestStatus.Available).Skip(guestList.Count()).Take(guestNumber - guestList.Count).ToListAsync());                    
                }

                //disable guests
                for (var i = 0; i < guestList.Count(); i++)
                {
                    guestList[i].Status = GuestStatus.NotAvailable;
                    _context.Update(guestList[i]);
                    await _context.SaveChangesAsync();
                }

                return guestList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("searchGuest")]
        public async Task<Models.Zenoti.Guest> SearchGuest(SearchGuestRequest data)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                var searchParam = "";
                if (!string.IsNullOrEmpty(data.Email))
                {
                    searchParam = searchParam + $"&email={data.Email}";
                }
                if (!string.IsNullOrEmpty(data.Phone))
                {
                    searchParam = searchParam + $"&phone={data.Phone}";
                }
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/search?center_id={_centerId}{searchParam}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    try
                    {
                        var result = JsonConvert.DeserializeObject<SearchGuestResponse>(jsonContent);

                        if (result != null && result.Guests.Count > 0)
                        {
                            return result.Guests.FirstOrDefault();
                        }
                        else if (data.CreateNew)
                        {
                            CreateGuestRequest newGuestRequest = new CreateGuestRequest();
                            newGuestRequest.CenterId = new Guid(_centerId);
                            newGuestRequest.PersonalInfo = new PersonalInfo();
                            newGuestRequest.PersonalInfo.Email = data.Email;
                            newGuestRequest.PersonalInfo.FirstName = data.FirstName;
                            newGuestRequest.PersonalInfo.LastName = data.LastName;
                            var newGuestResponse = CreateGuest(newGuestRequest);
                            Models.Zenoti.Guest newGuest = new Models.Zenoti.Guest();
                            newGuest.Id = newGuestResponse.Result.Id.ToString();
                            newGuest.PersonalInfo = newGuestResponse.Result.PersonalInfo;
                            return newGuest;
                        }
                    }
                    catch (Exception ee)
                    {

                    }

                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("searchGuests")]
        public async Task<List<Models.Zenoti.Guest>> SearchGuests(SearchGuestRequest data)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                var searchParam = "";
                if (!string.IsNullOrEmpty(data.Email))
                {
                    searchParam = searchParam + $"&email={data.Email}";
                }
                if (!string.IsNullOrEmpty(data.Phone))
                {
                    searchParam = searchParam + $"&phone={data.Phone}";
                }
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/search?center_id={_centerId}{searchParam}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    try
                    {
                        var result = JsonConvert.DeserializeObject<SearchGuestResponse>(jsonContent);

                        if (result != null && result.Guests.Count > 0)
                        {
                            return result.Guests;
                        }
                    }
                    catch (Exception ee)
                    {

                    }

                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("createGuest")]
        public async Task<CreateGuestResponse> CreateGuest(CreateGuestRequest data)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                data.CenterId = new Guid(_centerId);

                var client = new HttpClient();

                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}guests"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CreateGuestResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpGet]
        [Route("retrieveGuest")]
        public async Task<Models.Zenoti.Guest> RetrieveGuest(string guestId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Models.Zenoti.Guest>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPut]
        [Route("updateGuests")]
        public async Task<List<CreateGuestResponse>> UpdateGuests(List<UpdateGuestRequest> data)
        {
            try
            {
                

                List<CreateGuestResponse> guests = new List<CreateGuestResponse>();

                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                foreach (var e in data)
                {
                    e.CenterId = new Guid(_centerId);
                    Models.Zenoti.Guest existingGuest = SearchGuest(new SearchGuestRequest { Email = e.PersonalInfo.email }).Result;
                    if (existingGuest != null)
                    {
                        CreateGuestResponse eGuest = new CreateGuestResponse();
                        eGuest.Id = new Guid(existingGuest.Id);
                        eGuest.PersonalInfo = existingGuest.PersonalInfo;
                        guests.Add(eGuest);

                    }
                    else
                    {
                        var objAsJson = JsonConvert.SerializeObject(e);
                        var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                        var request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Put,
                            RequestUri = new Uri($"{_apiBaseUrl}guests/{e.PersonalInfo.Id}"),
                            Content = content,
                            Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                        };

                        using (var response = await client.SendAsync(request))
                        {
                            response.EnsureSuccessStatusCode();
                            var jsonContent = await response.Content.ReadAsStringAsync();
                            var result = JsonConvert.DeserializeObject<CreateGuestResponse>(jsonContent);

                            if (result.Id != null)
                            {
                                guests.Add(result);
                            }
                        }
                    }
                }

                return guests;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPut]
        [Route("updateGuest")]
        public async Task<CreateGuestResponse> UpdateGuest(UpdateGuestRequest data)
        {
            try
            {
                CreateGuestResponse guest = new CreateGuestResponse();

                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                data.CenterId = new Guid(_centerId);
                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{data.PersonalInfo.Id}"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CreateGuestResponse>(jsonContent);

                    if (result.Id != null)
                        guest = result;
                }

                return guest;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        [Route("updateGuestInfo")]
        public async Task<CreateGuestResponse> UpdateGuestInfo(SearchGuestRequest data)
        {
            try
            {
                Models.Zenoti.Guest existingGuest = SearchGuest(new SearchGuestRequest { Email = data.Email }).Result;
                if (existingGuest != null)
                {
                    Models.Zenoti.Guest guest = RetrieveGuest(existingGuest.Id).Result;
                    if (guest != null)
                    {
                        guest.PersonalInfo.FirstName = !string.IsNullOrEmpty(data.FirstName) ? data.FirstName : guest.PersonalInfo.FirstName;
                        guest.PersonalInfo.LastName = !string.IsNullOrEmpty(data.LastName) ? data.LastName : guest.PersonalInfo.LastName;
                        guest.PersonalInfo.Id = new Guid(existingGuest.Id);
                        UpdateGuestRequest updateGuestReq = new UpdateGuestRequest();
                        updateGuestReq.PersonalInfo = new UpdatePersonalInfo();
                        updateGuestReq.PersonalInfo.email = guest.PersonalInfo.Email;
                        updateGuestReq.PersonalInfo.first_name = guest.PersonalInfo.FirstName;
                        if (!string.IsNullOrEmpty(data.Phone) && data.Phone.Substring(0, 1) == "1")
                        {
                            updateGuestReq.PersonalInfo.mobile_phone = new Phone();
                            updateGuestReq.PersonalInfo.mobile_phone = guest.PersonalInfo.MobilePhone;
                            updateGuestReq.PersonalInfo.home_phone = new Phone();
                            updateGuestReq.PersonalInfo.home_phone.number = data.Phone.Substring(1);
                            updateGuestReq.PersonalInfo.home_phone.phone_code = 0;
                            updateGuestReq.PersonalInfo.home_phone.country_code = 225;
                        }
                        updateGuestReq.PersonalInfo.Id = new Guid(existingGuest.Id);
                        updateGuestReq.PersonalInfo.last_name = guest.PersonalInfo.LastName;
                        updateGuestReq.PersonalInfo.UserName = guest.PersonalInfo.UserName;
                        updateGuestReq.CenterId = new Guid(_centerId);
                        var result = UpdateGuest(updateGuestReq).Result;
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion

        [HttpGet]
        [Route("getAllAppointments/{guestId}")]
        public async Task<List<Appointment>> GetAllAppointments(string guestId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}/appointments"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetAllAppointmentsResponse>(jsonContent);
                    return result.Appointments;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("getActiveAppointments/{guestId}/{days?}")]
        public async Task<List<Appointment>> GetActiveAppointments(string guestId, int days=1)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                var startDate = DateTime.Now.ToString("yyyy-MM-dd");
                var endDate = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}/appointments?start_date={startDate}&end_date={endDate}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetAllAppointmentsResponse>(jsonContent);
                    return result.Appointments;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPut]
        [Route("cancelAppointment/{invoiceId}")]
        public async Task<CancelAppointmentResponse> CancelAppointment(string invoiceId)
        {
            try
            {
                List<CreateGuestResponse> guests = new List<CreateGuestResponse>();

                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var content = new StringContent("{\r\n  \"comments\": \"cancelled by guest\"\r\n}", null, "application/json");
                var request = new HttpRequestMessage
                {


                    Method = HttpMethod.Put,
                    RequestUri = new Uri($"{_apiBaseUrl}invoices/{invoiceId}/cancel"),
                    Content = content,
                    Headers =
                    {
                        { "accept", "application/json" },
                        { "Authorization", $" {headers["Authorization"]}" }
                    }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CancelAppointmentResponse>(jsonContent);
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("getNewSchedules")]
        public async Task<GetAvailableSlotsResponse> GetNewSchedules([FromBody] CategorySchedulesRequest data)
        {
            try
            {
                var request = new CreateBookingRequest();
                var slotsResponse = new GetAvailableSlotsResponse();

                request.CenterId = new Guid(_centerId);
                request.Date = data.Date.ToString("yyyy-MM-dd");

                foreach (var id in data.ServiceIds)
                {
                    var itemElement = new BookingRequestItemElement();
                    var item = new BookingRequestItem();
                    var therapist = new BookingRequestTherapist();
                    var addons = new List<BookingRequestAddOn>();

                    item.Id = id;
                    itemElement.Item = item;

                    if (!string.IsNullOrEmpty(data.TherapistId) && data.TherapistId != "-1")
                        therapist.Id = new Guid(data.TherapistId);

                    if (data.Gender != null)
                        therapist.Gender = data.Gender.Value;

                    itemElement.Therapist = therapist;
                    itemElement.InvoiceItemId = data.InvoiceItemId;

                    foreach (var guestId in data.GuestIds)
                    {
                        var guest = new BookingRequestGuest();
                        guest.Id = guestId;
                        guest.InvoiceId = data.InvoiceId;
                        guest.Items.Add(itemElement);
                        request.Guests.Add(guest);
                    }

                    foreach (var addonId in data.AddonIds)
                    {
                        var addonItem = new BookingRequestAddOn();
                        addonItem.BookingRequestItem = new BookingRequestItem();
                        addonItem.BookingRequestItem.Id = addonId;

                        addons.Add(addonItem);
                    }

                    itemElement.AddOns = addons;

                    var serviceBookingResponse = await CreateServiceBooking(request);

                    if (serviceBookingResponse.Error != null)
                        return null;

                    var bookingId = serviceBookingResponse.Id.ToString();

                    slotsResponse = await GetAvailableSlots(bookingId);

                    for (var i = 0; i < slotsResponse.Slots.Count(); i++)
                    {
                        var slot = slotsResponse.Slots[i];
                        var hour = slot.Time.Hour.ToString().Length == 1 ? "0" + slot.Time.Hour : slot.Time.Hour.ToString();
                        var minute = slot.Time.Minute.ToString().Length == 1 ? "0" + slot.Time.Minute : slot.Time.Minute.ToString();
                        slotsResponse.Slots[i].ShortTime = (slot.Time.Hour > 12 ? "0" + (slot.Time.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (slot.Time.Hour >= 12 ? " PM" : " AM");
                        slotsResponse.Slots[i].DayPeriod = ((slot.Time.Hour >= 4) && (slot.Time.Hour <= 12)) ? 1 : ((slot.Time.Hour >= 11) && (slot.Time.Hour <= 17)) ? 2 : 3;
                        slotsResponse.Slots[i].StartTime = int.Parse(hour + "" + minute);
                        slotsResponse.Slots[i].Hour = slot.Time.Hour;
                    }

                    slotsResponse.BookingId = bookingId;
                    slotsResponse.Date = request.Date;
                    slotsResponse.ServiceId = id;

                }

                return slotsResponse;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        //[HttpPost]
        //[Route("addMembership/{userId}")]
        //public async Task<CreateInvoiceResponse> AddMembership(string userId, string membershipId)
        //{
        //    var result = new CreateInvoiceResponse();

        //    try
        //    {
        //        var headers = Request.Headers;

        //        if (!headers.ContainsKey("Authorization"))
        //        {
        //            return null;
        //        }

        //        var client = new HttpClient();
        //        var objAsJson = "";

        //        var data = new CreateInvoiceRequest();
        //        data.CenterId = _centerId;
        //        data.UserId = userId;
        //        data.MembershipIds = membershipId;
        //        objAsJson = JsonConvert.SerializeObject(data);

        //        var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

        //        var request = new HttpRequestMessage
        //        {
        //            Method = HttpMethod.Post,
        //            RequestUri = new Uri($"{_apiBaseUrl}invoices/memberships"),
        //            Content = content,
        //            Headers =
        //                {
        //                    { "accept", "application/json" },
        //                    { "Authorization", $" {headers["Authorization"]}" }
        //                }
        //        };

        //        using (var response = await client.SendAsync(request))
        //        {
        //            response.EnsureSuccessStatusCode();
        //            var jsonContent = await response.Content.ReadAsStringAsync();
        //            result = JsonConvert.DeserializeObject<CreateInvoiceResponse>(jsonContent);
        //            return result;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        result.Error = "Error creating membership invoice";
        //        return result;
        //    }
        //}
        [HttpPost]
        [Route("closeMembershipInvoice/{invoiceId}")]
        public async Task<CloseInvoiceResponse> CloseMembershipInvoice(string invoiceId)
        {
            var result = new CloseInvoiceResponse();

            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                var objAsJson = "";

                var data = new CloseInvoiceRequest();
                data.status = 0;
                data.closed_by_id = new Guid("50445307-0adc-4a20-bce5-a7838f1f4cde");
                objAsJson = JsonConvert.SerializeObject(data);

                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}invoices/{invoiceId}/close"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<CloseInvoiceResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Error = "Error creating membership invoice";
                return result;
            }
        }
        [HttpGet]
        [Route("getCategory/{id}")]
        public async Task<ZenotiCategory> GetCategory(string id)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}centers/{_centerId}/categories/{id}?type=1"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<ZenotiCategory>(jsonContent);

                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPut]
        [Route("mergeAccounts/{id}/{mergedId}")]
        public async Task<bool> MergeAccounts(string id, Guid mergedId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return false;
                }

                var userToken = await GetUserToken(2);

                var client = new HttpClient();
                var objAsJson = "";

                var data = new MergeGuestRequest();
                data.merge_type = 1;
                data.merge_guest_ids = new List<Guid>();
                data.merge_guest_ids.Add(mergedId);
                objAsJson = JsonConvert.SerializeObject(data);

                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{id}/merge"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();

                    return true;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("getPaymentUrl/{guestId}")]
        public async Task<AddCardResponse> GetPaymentUrl(string guestId, [FromBody] AddCardRequest data)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                data.CenterId = new Guid(_centerId);
                data.avs_source = "1";
                data.host = _host;
                data.share_cards_to_web = false;
                data.skip_billingInfo = false;
                data.source = "1";

                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}/accounts"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<AddCardResponse>(jsonContent);
                    return result;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpGet]
        [Route("getGuestPayment/{guestId}")]
        public async Task<GetCardResponse> GetGuestPayment(string guestId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}/accounts?center_id={_centerId}"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<GetCardResponse>(jsonContent);
                    return result;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpGet]
        [Route("getMemberships/{guestId}")]
        public async Task<JObject> GetMemberships(string guestId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{_apiBaseUrl}guests/{guestId}/memberships?center_id={ _centerId }"),
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);
                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("addMembership/{guestId}/{membershipId}")]
        public async Task<CreateInvoiceResponse> AddMembership(string guestId, string membershipId)
        {
            try
            {
                var headers = Request.Headers;

                if (!headers.ContainsKey("Authorization"))
                {
                    return null;
                }

                var client = new HttpClient();
                CreateInvoiceRequest data = new CreateInvoiceRequest();
                data.CenterId = _centerId;
                data.UserId = guestId;
                data.MembershipIds = membershipId;
                var objAsJson = JsonConvert.SerializeObject(data);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{_apiBaseUrl}invoices/memberships"),
                    Content = content,
                    Headers =
                        {
                            { "accept", "application/json" },
                            { "Authorization", $" {headers["Authorization"]}" }
                        }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CreateInvoiceResponse>(jsonContent);
                    if (result.Success)
                    {
                        // complete invoice
                        CloseMembershipInvoice(result.InvoiceId.ToString());
                    }
                    return result;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}
