﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using PMS;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [EnableCors("All")]
    [Route("api/salto")]
    public class SaltoController : ControllerBase
    {
        private readonly UserContext _userContext;


        public SaltoController(UserContext userContext)
        {
            _userContext = userContext;
        }

        [HttpPost]
        [Route("create-card")]
        public async Task<IActionResult> CreateCard(PrepareKeyRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }
            try
            {
                using (var client = new Client("192.168.0.240", 8999))
                {
                    var writeData = new WriteData
                    {
                        EncoderNumber = request.Encoder > 0 ? request.Encoder : 1,
                        Room1 = request.RoomId,
                        InitialDateTime = request.StartDate,
                        FinalDateTime = request.EndDate
                    };

                    var uid = client.Write(writeData);
                    Console.WriteLine($"Written on {uid} tag");

                    Console.WriteLine("Read ... ");

                    var readData = new ReadData
                    {
                        EncoderNumber = 1,
                        ExpellingType = EjectionType.E
                    };

                    var read = client.Read(readData);
                    Console.WriteLine($"Room {read.Room1} {read.Uid}");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            return Ok();
        }

    }
}
