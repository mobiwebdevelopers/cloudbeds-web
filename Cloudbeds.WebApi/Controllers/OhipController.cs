﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using CloudBaseWeb.Api.Entities;
using Cloudbeds.Data;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/ohip")]
    [ApiController]
    [EnableCors("All")]
    [AllowAnonymous]
    public class OhipController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly Uri _apiBaseUrl;
        private readonly MobileKeyDbContext _dbContext;
        private string globalAppKey;

        public OhipController(IConfiguration configuration, MobileKeyDbContext dbContext)
        {
            _configuration = configuration;
            _apiBaseUrl = new Uri(configuration["OHIP:BaseUrl"]);
            _dbContext = dbContext;
    }

        // GET: api/<OhipController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet]
        [Route("test")]
        public async Task<JObject> Test(string hotel, int resID)
        {
            return null;
        }

        // GET api/<OhipController>/5
        //[HttpGet("reservation/{id}")]
        [HttpGet]
        [Route("reservation")]
        public async Task<OhipReservation> Get(string hotel, string resID)
        {
            var nameSearch = resID.IndexOf("|") > -1;
            if (nameSearch)
            {
                return await GetFromNameRoom(hotel, resID);
            }
            var _resId = Convert.ToInt32(resID);
            try
            {
                // added for temporary app testing
                var resTemp = new OhipReservation();

                resTemp.reservationId = _resId;
                switch(_resId)
                {
                    case 16999:
                        resTemp.days = 10;
                        resTemp.arrivalDate = DateTime.Now.Date;
                        resTemp.departureDate = resTemp.arrivalDate.AddDays(resTemp.days);
                        break;
                    case 16903:
                    case 16909:
                        resTemp.days = 1;
                        resTemp.arrivalDate = DateTime.Now.Date;
                        resTemp.departureDate = resTemp.arrivalDate.AddDays(resTemp.days);
                        break;
                    case 17151:
                        resTemp.days = 2;
                        resTemp.arrivalDate = DateTime.Now.Date;
                        resTemp.departureDate = resTemp.arrivalDate.AddDays(resTemp.days);
                        break;
                    default:
                        resTemp = null;
                        //Random random = new Random();
                        //resTemp.days = random.Next(1, 10);
                        //resTemp.arrivalDate = DateTime.Now.Date;
                        //resTemp.departureDate = resTemp.arrivalDate.AddDays(resTemp.days);
                        break;

                }
                if (resTemp != null)
                {
                    resTemp.showCoupons = false;

                    if (resTemp.days >= 2)
                    {
                        resTemp.showCoupons = true;
                    }

                    return resTemp;
                }

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                // production OHIP integration
                OhipToken token = await GetToken(hotel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    client.DefaultRequestHeaders.Authorization =new AuthenticationHeaderValue("Bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Add("x-hotelid", hotel);
                    client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);


                    HttpResponseMessage tokenResponse = await client.GetAsync("rsv/v1/hotels/"+ hotel + "/reservations/" + _resId.ToString());// + "?fetchInstructions=Reservation"
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    if (jsonContent.Length == 0)
                    {
                        HttpResponseMessage tokenResponse2 = await client.GetAsync("rsv/v1/hotels/" + hotel + "/reservations?confirmationNumberList=" + _resId.ToString());
                        var jsonContent2 = await tokenResponse2.Content.ReadAsStringAsync();
                        try
                        {
                            OhipReservationsResponse data2 = JsonConvert.DeserializeObject<OhipReservationsResponse>(jsonContent2);
                            if (data2.reservations.reservationInfo != null)
                            {
                                for (var i = 0; i < data2.reservations.reservationInfo.Count(); i++)
                                {
                                    for (int j = 0; j < data2.reservations.reservationInfo[i].reservationIdList.Count(); j++)
                                    {
                                        if (data2.reservations.reservationInfo[i].reservationIdList[j].type == "Confirmation" && data2.reservations.reservationInfo[i].reservationIdList[j].id == _resId.ToString())
                                        {
                                            if (j == 0)
                                            {
                                                _resId = Convert.ToInt32(data2.reservations.reservationInfo[i].reservationIdList[1].id);
                                            }
                                            else
                                            {
                                                _resId = Convert.ToInt32(data2.reservations.reservationInfo[i].reservationIdList[0].id);
                                            }
                                            tokenResponse = await client.GetAsync("rsv/v1/hotels/" + hotel + "/reservations/" + _resId.ToString());
                                            jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                                            break;
                                        }
                                    }
                                    if (jsonContent.Length > 0)
                                        break;
                                }
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    if (jsonContent.Length > 0)
                    {
                        dynamic data = JsonConvert.DeserializeObject(jsonContent);
                        //Console.WriteLine(data.reservations);
                        dynamic arreglo = JObject.Parse(jsonContent);
                        try
                        {
                            string startDate = arreglo.reservations.reservation[0].roomStay.arrivalDate;
                            string endDate = arreglo.reservations.reservation[0].roomStay.departureDate;

                            var res = new OhipReservation();
                            res.reservationId = _resId;
                            res.arrivalDate = Convert.ToDateTime(startDate);
                            res.departureDate = Convert.ToDateTime(endDate);
                            res.days = (res.departureDate - res.arrivalDate).Days;
                            res.showCoupons = false;

                            if (res.days >= 2)
                            {
                                res.showCoupons = true;
                            }
                            return res;
                        }
                        catch (Exception e)
                        {
                            var res = new OhipReservation();
                            res.reservationId = _resId;
                            res.showCoupons = false;
                            res.errorMessage = e.Message;

                            return res;
                        }
                    }
                    else
                    {
                        var res = new OhipReservation();
                        res.reservationId = _resId;
                        res.showCoupons = false;
                        res.errorMessage = "Reservation not found";
                        return res;
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        public async Task<OhipReservation> GetFromNameRoom(string hotel, string key)
        {
            try
            {
                // added for temporary app testing
                var resTemp = new OhipReservation();
                if (key=="999|omni")
                {
                    // debug
                    var res = new OhipReservation();
                    res.reservationId = 999;
                    res.arrivalDate = DateTime.Today;
                    res.departureDate = DateTime.Today.AddDays(3);
                    res.days = (res.departureDate - res.arrivalDate).Days;
                    res.showCoupons = false;

                    if (res.days >= 2)
                    {
                        res.showCoupons = true;
                    }
                    return res;

                }
                else
                {
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    // production OHIP integration
                    OhipToken token = await GetToken(hotel);
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = _apiBaseUrl;
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
                        client.DefaultRequestHeaders.Add("x-hotelid", hotel);
                        client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);

                        var resID = 0;
                        var jsonContent = "";
                        var nameSearch = key.IndexOf("|") > -1;
                        if (nameSearch)
                        {
                            var keys = key.Split('|'); // <room>|<lastname>
                            HttpResponseMessage tokenResponse = await client.GetAsync("rsv/v1/hotels/" + hotel + "/reservations?surname=" + keys[1]);
                            jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                            try
                            {
                                OhipReservationsResponse data = JsonConvert.DeserializeObject<OhipReservationsResponse>(jsonContent);
                                jsonContent = "";
                                if (data.reservations.reservationInfo != null)
                                {
                                    for (var i = 0; i < data.reservations.reservationInfo.Count(); i++)
                                    {
                                        if (data.reservations.reservationInfo[i].roomStay.roomId == keys[0])
                                        {
                                            for (int j = 0; j < data.reservations.reservationInfo[i].reservationIdList.Count(); j++)
                                            {
                                                if (data.reservations.reservationInfo[i].reservationIdList[j].type == "Confirmation")
                                                {
                                                    if (j == 0)
                                                    {
                                                        resID = Convert.ToInt32(data.reservations.reservationInfo[i].reservationIdList[1].id);
                                                    }
                                                    else
                                                    {
                                                        resID = Convert.ToInt32(data.reservations.reservationInfo[i].reservationIdList[0].id);
                                                    }
                                                    tokenResponse = await client.GetAsync("rsv/v1/hotels/" + hotel + "/reservations/" + resID.ToString());
                                                    jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                                                    break;
                                                }
                                            }
                                        }
                                        if (jsonContent.Length > 0)
                                            break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {

                            }
                        }
                        if (jsonContent.Length > 0)
                        {
                            dynamic data = JsonConvert.DeserializeObject(jsonContent);
                            //Console.WriteLine(data.reservations);
                            dynamic arreglo = JObject.Parse(jsonContent);
                            try
                            {
                                string startDate = arreglo.reservations.reservation[0].roomStay.arrivalDate;
                                string endDate = arreglo.reservations.reservation[0].roomStay.departureDate;

                                var res = new OhipReservation();
                                res.reservationId = resID;
                                res.arrivalDate = Convert.ToDateTime(startDate);
                                res.departureDate = Convert.ToDateTime(endDate);
                                res.days = (res.departureDate - res.arrivalDate).Days;
                                res.showCoupons = false;

                                if (res.days >= 2)
                                {
                                    res.showCoupons = true;
                                }
                                return res;
                            }
                            catch (Exception e)
                            {
                                var res = new OhipReservation();
                                res.reservationId = resID;
                                res.showCoupons = false;
                                res.errorMessage = e.Message;

                                return res;
                            }
                        }
                        else
                        {
                            var res = new OhipReservation();
                            res.reservationId = resID;
                            res.showCoupons = false;
                            res.errorMessage = "Reservation not found";
                            return res;
                        }

                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("resdata")]
        public async Task<JObject> ResData(string hotel, int resID)
        {
            try
            {
                OhipToken token = await GetToken(hotel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Add("x-hotelid", hotel);
                    client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);

                    HttpResponseMessage tokenResponse = await client.GetAsync("rsv/v1/hotels/" + hotel + "/reservations/" + resID.ToString() + "?fetchInstructions=Reservation");
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic data = JsonConvert.DeserializeObject(jsonContent);
                    Console.WriteLine(data.reservations);
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("checkin/{hotel}/{resID}")]
        public async Task<JObject> Checkin(string hotel, int resID)
        {
            try
            {
                OhipToken token = await GetToken(hotel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Add("x-hotelid", hotel);
                    client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);

                    string str = "{\"reservation\": { \"ignoreWarnings\": true, \"stopCheckin\": false,\"printRegistration\": false},\"fetchReservationInstruction\": [\"ReservationDetail\"],\"includeNotifications\": true}";
                    dynamic json = JsonConvert.DeserializeObject(str);

                    var objAsJson = JsonConvert.SerializeObject(json);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("fof/v1/hotels/" + hotel + "/reservations/" + resID.ToString() + "/checkIns", content);
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic data = JsonConvert.DeserializeObject(jsonContent);
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("checkout/{hotel}/{resID}")]
        public async Task<JObject> Checkout(string hotel, int resID)
        {
            try
            {
                OhipToken token = await GetToken(hotel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
                    client.DefaultRequestHeaders.Add("x-hotelid", hotel);
                    client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);

                    string str = "{\"reservation\":{\"reservationIdList\": {\"id\": "+ resID + ",\"type\": \"Reservation\"},\"stopCheckout\": false,\"cashierId\": 4,\"hotelId\": \""+ hotel + "\",\"eventType\": \"CheckOut\",\"autoCheckout\": false,\"checkoutInstr\": {\"roomStatus\": \"Dirty\",\"ignoreWarnings\": true}},\"verificationOnly\": false}";
                    dynamic json = JsonConvert.DeserializeObject(str);

                    var objAsJson = JsonConvert.SerializeObject(json);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("csh/v0/hotels/" + hotel + "/reservations/" + resID.ToString() + "/checkOuts", content);
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic data = JsonConvert.DeserializeObject(jsonContent);
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        private async Task<OhipToken> GetToken(string code)
        {
            try
            {
                var credentials = _dbContext.Set<OhipCredentials>()
                .Where(c => c.Code == code).FirstOrDefault();

                string username = credentials.UserName;
                string password = credentials.Password;
                globalAppKey = credentials.AppKey;
                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                var form = new Dictionary<string, string>
                    {
                        {"grant_type", "password"},
                        {"username", credentials.AuthUser},
                        {"password", credentials.AuthPwd}
                    };

                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    client.DefaultRequestHeaders.Add("Authorization", "Basic " + svcCredentials);
                    client.DefaultRequestHeaders.Add("x-app-key", globalAppKey);
                    
                    HttpResponseMessage tokenResponse = await client.PostAsync("oauth/v1/tokens", new FormUrlEncodedContent(form));
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    OhipToken tok = JsonConvert.DeserializeObject<OhipToken>(jsonContent);
                    return tok;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }

    }
}
