﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net.Mail;
using Regula.DocumentReader.WebClient.Api;
using Regula.DocumentReader.WebClient.Model;
using Regula.DocumentReader.WebClient.Model.Ext;
using Regula.DocumentReader.WebClient.Model.Ext.Autheticity;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/regula")]
    [ApiController]
    [EnableCors("All")]
    [AllowAnonymous]
    public class RegulaController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private string _API_BASE_PATH = "https://api.regulaforensics.com";
        private string _LICENSE_FILE_NAME = "regula.license";
        public RegulaController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [Route("processDocument")]
        public async Task<JToken> ProcessDocument([FromBody] GuestPhotoRequest data)
        {
			try
			{
				var apiBaseUrl = Environment.GetEnvironmentVariable(_API_BASE_PATH) ?? "https://api.regulaforensics.com";

				var licenseFromFile = System.IO.File.Exists(_LICENSE_FILE_NAME)
					? System.IO.File.ReadAllBytes(_LICENSE_FILE_NAME)
					: null;

				byte[] imageData = Convert.FromBase64String(data.Image.Substring(data.Image.IndexOf(",") + 1));
				//var imageData = System.IO.File.ReadAllBytes("Test1.jpg");

				var requestParams = new RecognitionParams()
					.WithScenario(Scenario.FULL_AUTH)
					.WithResultTypeOutput(new List<int>
					{
					// actual results
					Result.STATUS, Result.AUTHENTICITY, Result.TEXT, Result.IMAGES,
					Result.DOCUMENT_TYPE, Result.DOCUMENT_TYPE_CANDIDATES, Result.DOCUMENT_POSITION,
					//// legacy results
					//Result.MRZ_TEXT, Result.VISUAL_TEXT, Result.BARCODE_TEXT, Result.RFID_TEXT,
					//Result.VISUAL_GRAPHICS, Result.BARCODE_GRAPHICS, Result.RFID_GRAPHICS,
					//Result.LEXICAL_ANALYSIS, Result.IMAGE_QUALITY
					})
					.WithLog(false)
					.WithProcessAuth(AuthenticityResultType.EXTENDED_MRZ_CHECK | AuthenticityResultType.EXTENDED_OCR_CHECK);

				//var request = new RecognitionRequest(requestParams, new List<ProcessRequestImage>
				//{
				//	new ProcessRequestImage(new ImageDataExt(imageData), Light.WHITE)
				//});
				var request = new RecognitionRequest(requestParams, new ProcessRequestImage(new ImageDataExt(imageData), Light.WHITE));

				//var api = licenseFromEnv != null
				//	? new DocumentReaderApi(apiBaseUrl).WithLicense(licenseFromEnv)
				//	: new DocumentReaderApi(apiBaseUrl).WithLicense(licenseFromFile);
				var api = new DocumentReaderApi(apiBaseUrl).WithLicense(licenseFromFile);
				var response = api.Process(request);

				// var authHeaders = new Dictionary<string, string>()
				// {
				// 	{ "Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes("USER:PASSWORD"))}" }
				// };
				// var response = api.Process(request, headers: authHeaders);

				Console.WriteLine(response.Log());

				// overall status results 
				var status = response.Status();
				var docOverallStatus = status.OverallStatus == CheckResult.OK ? "valid" : "not valid";
				var docOpticalTextStatus = status.DetailsOptical.Text == CheckResult.OK ? "valid" : "not valid";

				// text results
				var docNumberField = response.Text().GetField(TextFieldType.DOCUMENT_NUMBER);
				var docNumberVisual = docNumberField.GetValue(Source.VISUAL);
				var docNumberMrz = docNumberField.GetValue(Source.MRZ);
				var docNumberVisualValidity = docNumberField.SourceValidity(Source.VISUAL);
				var docNumberMrzValidity = docNumberField.SourceValidity(Source.MRZ);
				var docNumberMrzVisualMatching = docNumberField.CrossSourceComparison(Source.MRZ, Source.VISUAL);

				var docType = response.DocumentType();


				var docAuthenticity = response.Authenticity();
				var docIRB900 = docAuthenticity.IrB900Checks();
				var docIRB900Blank = docIRB900?.ChecksByElement(SecurityFeatureType.BLANK);

				var docImagePattern = docAuthenticity.ImagePatternChecks();
				var docImagePatternBlank = docImagePattern?.ChecksByElement(SecurityFeatureType.BLANK);

				var docImageQuality = response.ImageQualityChecks();

				var info = api.Ping();
				// var info = api.Ping(headers: authHeaders);

				Console.WriteLine("-----------------------------------------------------------------");
				Console.WriteLine($"                API Version: {info.Version}");
				Console.WriteLine("-----------------------------------------------------------------");
				Console.WriteLine($"           Document Overall Status: {docOverallStatus}");
				Console.WriteLine($"      Document Optical Text Status: {docOpticalTextStatus}");
				Console.WriteLine($"            Document Number Visual: {docNumberVisual}");
				Console.WriteLine($"            Document Number MRZ: {docNumberMrz}", docNumberMrz);
				Console.WriteLine($"            Document Name: {docType.DocumentName}");
				Console.WriteLine($"Validity Of Document Number Visual: {docNumberVisualValidity}");
				Console.WriteLine($"   Validity Of Document Number MRZ: {docNumberMrzValidity}");
				Console.WriteLine($"      MRZ-Visual values comparison: {docNumberMrzVisualMatching}");
				Console.WriteLine("-----------------------------------------------------------------");

				// images results     
				var documentImage = response.Images().GetField(GraphicFieldType.DOCUMENT_FRONT).GetValue();
				var portraitField = response.Images().GetField(GraphicFieldType.PORTRAIT);
				var portraitFromVisual = portraitField.GetValue(Source.VISUAL);

				System.IO.File.WriteAllBytes("document-image.jpg", documentImage);
				System.IO.File.WriteAllBytes("portrait.jpg", portraitFromVisual);
				return null;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			return null;
		}


		private string phoneString(string input)
        {
            string result = "";
            if (!string.IsNullOrEmpty(input))
            {
                result = input.Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
            }
            return result;
        }

    }
}
