﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;
using System.IO;
using Stripe;

namespace Cloudbeds.WebApi.Controllers
{
    public class AssignRoomRequest
    {
        public string ReservationID { get; set; }
        public string NewRoomID { get; set; }
        public int RoomTypeID { get; set; }
    }
    public class UnassignedRooms
    {
        public string RoomID { get; set; }
        public string RoomName { get; set; }
        public string RoomDescription { get; set; }
        public int MaxGuests { get; set; }
        public int RoomTypeID { get; set; }
        public bool IsPrivate { get; set; }
        public bool RoomBlocked { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeNameShort { get; set; }
    }
    public class UnassignedRoomData
    {
        public string PropertyID { get; set; }
        public List<UnassignedRooms> Rooms { get; set; }
    }
    public class UnAssignedRoomResult
    {
        public bool Success { get; set; }
        public List<UnassignedRoomData> Data { get; set; }
    }
    public class PaymentRequest
    {
        public string ReservationID { get; set; }
        public float Amount { get; set; }
        public string Type { get; set; }
        public string CardType { get; set; }
        public string Description { get; set; }
    }

    [ApiController]
    [Route("api/cloudbeds")]
    [EnableCors("All")]
    [AllowAnonymous]
    public class CloudbedsController : ControllerBase
    {
        private readonly UserContext _userContext;

        private readonly IConfiguration _configuration;
        private readonly ICloudbedsClient _cloudbedsClient;
        private readonly IMobileKeyService _mobileKeyService;
        private readonly Uri _apiBaseUrl;
        private const string Percentage = "percentage";
        private const string Rate = "rate";

        public CloudbedsController(IConfiguration configuration, ICloudbedsClient cloudbedsClient, IMobileKeyService mobileKeyService, UserContext userContext)
        {
            _configuration = configuration;
            _cloudbedsClient = cloudbedsClient;
            _mobileKeyService = mobileKeyService;
            _apiBaseUrl = new Uri(configuration["CloudbedsApi:BaseUrl"]); 
             _userContext = userContext;
        }

        [HttpGet]
        [Route("userInfo")]
        public async Task<UserInfoResponse> GetUserInfo()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/userinfo"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<UserInfoResponse>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("createPayment")]
        public async Task<JObject> PaymentIntent(PaymentIntentRequest request)
        {
            try
            {
                StripeConfiguration.ApiKey = _configuration["Stripe:apiTestKey"];
                string paymentDesc = _configuration["Stripe:description"];

                //Get All the Customers
                /*var service2 = new CustomerService();
                var customers = service2.List();*/

                //Create New Customer
                var cusOptions = new CustomerCreateOptions
                {
                    Name = request.name,
                    Email = request.email,
                };
                var cusService = new CustomerService();
                var customer = cusService.Create(cusOptions);

                //Create New PaymentIntent related to that customer
                var payOptions = new PaymentIntentCreateOptions
                {
                    Amount = request.amount,
                    Currency = "mxn",
                    Customer = customer.Id,
                    Description = paymentDesc + request.reservationID
                };

                var payService = new PaymentIntentService();
                var paymentIntent = payService.Create(payOptions);

                //Return paymentIntent Data
                return paymentIntent.RawJObject;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("registerItem")]
        public async Task<JObject> UploadDocument(ItemRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postItem"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("addGuest")]
        public async Task<JObject> AddGuest(GuestRequest request)
        {
            try
            {
                //Set default
                if (request.guestCountry == null || request.guestCountry.Length == 0)
                    request.guestCountry = "MX";

                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    request.guestID = "";
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postGuest"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);

                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPut]
        [Route("updateGuest")]
        public async Task<JObject> UpdateGuest(GuestRequest request)
        {
            try
            {
                //Set default
                if (request.guestCountry.Length == 0)
                    request.guestCountry = "MX";

                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    request.reservationID = "";
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Put, "api/v1.1/putGuest"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);

                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost] 
        [Route("uploadDoc/{guestID}")]
        public async Task<JObject> UploadDocument(string guestID)
        {
            try
            {
                               
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    
                    //var httpRequest2 = HttpContext.Request;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postGuestDocument"))
                    {
                        IFormFile file = Request.Form.Files.GetFile("file");

                        if (file == null) {
                            return JObject.Parse("{success : false,  message: 'file not found'}");
                        }

                        byte[] data;
                        using (var br = new BinaryReader(file.OpenReadStream()))
                            data = br.ReadBytes((int)file.OpenReadStream().Length);
                        ByteArrayContent bytes = new ByteArrayContent(data);

                        MultipartFormDataContent form = new MultipartFormDataContent();
                        form.Add(new StringContent(guestID), "guestID");
                        form.Add(bytes, "file", file.FileName);

                        httpRequest.Content = form;

                        //httpRequest.Content = GetFormUrlContentFromRequest(HttpContext.Request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("uploadImage")]
        public async Task<JObject> UploadImage([FromBody] GuestPhotoRequest request)
        {
            try
            {
                string guestID = request.GuestID;
                string image = request.Image;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    //var httpRequest2 = HttpContext.Request;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postGuestDocument"))
                    {
                        byte[] data = Convert.FromBase64String(image.Substring(image.IndexOf(",") + 1));
                        ByteArrayContent bytes = new ByteArrayContent(data, 0, data.Length);

                        MultipartFormDataContent form = new MultipartFormDataContent();
                        string filename = "photo_" + guestID + "_" + Guid.NewGuid().ToString();

                        form.Add(new StringContent(guestID), "guestID");
                        form.Add(bytes, "file", filename + ".jpg");

                        httpRequest.Content = form;
                        try
                        {
                            //httpRequest.Content = GetFormUrlContentFromRequest(HttpContext.Request);
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                var result = await response.Content.ReadAsStringAsync();
                                return JObject.Parse(result);
                            }
                        }
                        catch (Exception e)
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }        

        [HttpPost]
        [Route("paymentMethods")]
        public async Task<JObject> GetPaymentMethods()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getPaymentMethods"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("payments/{resID}")]
        public async Task<JObject> getPayments(string resID)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    //var query = _cloudbedsClient.GetQueryStringFromRequest(resID);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getPayments?reservationID=" + resID))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<JObject>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("transactions/{resID}")]
        public async Task<JObject> getTransactions(string resID)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    //var query = _cloudbedsClient.GetQueryStringFromRequest(resID);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getTransactions?reservationID=" + resID))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<JObject>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("rooms")]
        public async Task<RoomsResponse> GetAvailableRooms(RoomsRequest request)
        {
            if (request == null || request.Rooms < 1 || request.Adults < 1 || request.EndDate.Date <= request.StartDate.Date)
            {
                BadRequest();
            }

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getAvailableRoomTypes{query}"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<RoomsResponse>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("rates")]
        public async Task<RoomFeesTotal> GetRateTotals(RoomFeesRequest request)
        {
            if (request == null || request.Adults < 1 || request.EndDate.Date <= request.StartDate.Date)
            {
                BadRequest();
            }

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getTaxesAndFees"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            var feeResponse = await response.Content.ReadAsAsync<RoomFeesResponse>();
                            var roomFeesTotal = new RoomFeesTotal();
                            if (feeResponse != null && request.Rooms != null)
                            {
                                foreach (var room in request.Rooms)
                                {
                                    var fees = 0m;
                                    foreach (var fee in feeResponse.Data)
                                    {
                                        if (!decimal.TryParse(fee.Amount, out var amount))
                                        {
                                            continue;
                                        }

                                        if (fee.AvailableFor == null)
                                        {
                                            continue;
                                        }

                                        if (!fee.AvailableFor.Contains(Rate))
                                        {
                                            continue;
                                        }

                                        switch (fee.AmountType)
                                        {
                                            case Percentage:
                                                fees += room.Rate * amount / 100;
                                                break;
                                        }
                                    }

                                    roomFeesTotal.Fees += fees;
                                    roomFeesTotal.Total += room.Rate + fees;
                                }
                            }

                            return roomFeesTotal;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("reservation")]
        public async Task<JObject> SaveReservation(ReservationRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postReservation"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("lookup")]
        public async Task<JObject> GetReservation(LookupReservationRequest request)
        {
            JObject reservation = null;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                //await _cloudbedsClient.RefreshAccessToken();
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    if (!string.IsNullOrEmpty(request.CardNumber) || !string.IsNullOrEmpty(request.Email) || !string.IsNullOrEmpty(request.Phone) || request.ReservationID.Length == 16 || request.ReservationID.Length != 13)
                    {
                        string sourceId = "";
                        if (request.ReservationID.Length == 16)
                        {
                            request.CardNumber = request.ReservationID.Substring(request.ReservationID.Length - 4, 4);
                        }
                        else if (!string.IsNullOrEmpty(request.CardNumber))
                        {
                            request.CardNumber = request.CardNumber.Substring(request.CardNumber.Length - 4, 4);
                        }
                        else
                        {
                            sourceId = request.ReservationID;
                        }

                        request.ReservationID = "";


                        var requestByRange = new LookupReservationByRangeRequest();

                        if (request.Checkout != null && request.Checkout)
                        {
                            // checkout
                            requestByRange.Status = "checked_in";
                            //requestByRange.CheckOutFrom = DateTime.Now.Date.AddDays(-2);
                            //requestByRange.CheckOutTo = DateTime.Now.Date.AddDays(1);
                            //requestByRange.CheckInFrom = null;
                            //requestByRange.CheckInTo = null;
                        }
                        else
                        {
                            // checkin
                            requestByRange.Status = "confirmed,checked_in,not_confirmed";
                            requestByRange.CheckInFrom = DateTime.Now.Date.AddDays(-7);
                            requestByRange.CheckInTo = DateTime.Now.Date.AddDays(1);
                        }
                        //requestByRange.CheckInFrom = DateTime.Now.Date.AddDays(-7);
                        //requestByRange.CheckInTo = DateTime.Now.Date.AddDays(1);
                        requestByRange.IncludeGuestsDetails = true;

                        var query = _cloudbedsClient.GetQueryStringFromRequest(requestByRange);
                        query = query.Replace("0001-01-01", "");
                        using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservations{query}"))
                        {
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                string result = await response.Content.ReadAsStringAsync();

                                var resultObject = JsonConvert.DeserializeObject<LookupReservationResponse>(result);

                                var reservationsFound = resultObject.Data.Where(r => r.Status == "confirmed" || r.Status == "checked_in" || r.Status == "not_confirmed").ToList();
                                foreach (var item in reservationsFound)
                                {
                                    bool match = false;
                                    if (sourceId.Length > 0)
                                    {
                                        if (item.ThirdPartyIdentifier != null && item.ThirdPartyIdentifier == sourceId.ToUpper() )
                                        {
                                            match = true;
                                            LookupReservationRequest request3 = new LookupReservationRequest();
                                            request3.ReservationID = item.ReservationId;
                                            var query3 = _cloudbedsClient.GetQueryStringFromRequest(request3);
                                            using (var httpRequest3 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation{query3}"))
                                            {
                                                using (var response3 = await httpClient
                                                    .SendAsync(httpRequest3, HttpCompletionOption.ResponseHeadersRead)
                                                    .ConfigureAwait(false))
                                                {
                                                    response3.EnsureSuccessStatusCode();
                                                    string result3 = await response3.Content.ReadAsStringAsync();

                                                    //Creating an array from GuestList intead on JObjects
                                                    JArray arr3 = new JArray();
                                                    dynamic arreglo3 = JObject.Parse(result3);
                                                    reservation = arreglo3;
                                                }
                                            }
                                            break;

                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(request.CardNumber))
                                    {
                                        LookupReservationRequest request2 = new LookupReservationRequest();
                                        using (var httpRequest2 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation?reservationID={item.ReservationId}"))
                                        {
                                            using (var response2 = await httpClient
                                                .SendAsync(httpRequest2, HttpCompletionOption.ResponseHeadersRead)
                                                .ConfigureAwait(false))
                                            {
                                                response2.EnsureSuccessStatusCode();
                                                string result2 = await response2.Content.ReadAsStringAsync();

                                                //Creating an array from GuestList intead on JObjects
                                                JArray arr2 = new JArray();
                                                dynamic arreglo2 = JObject.Parse(result2);
                                                // credit card lookup
                                                foreach (var item2 in arreglo2.data.cardsOnFile)
                                                {
                                                    if (item2.cardNumber == request.CardNumber)
                                                    {
                                                        match = true;
                                                        break;
                                                    }
                                                }
                                                if (match)
                                                {
                                                    reservation = arreglo2;
                                                    break;
                                                }

                                            }
                                        }
                                    }

                                    else if (!string.IsNullOrEmpty(request.Phone) || !string.IsNullOrEmpty(request.Email))
                                    {
                                        Boolean guestFound = false;
                                        foreach (var guest in item.GuestList)
                                        {
                                            if (!string.IsNullOrEmpty(request.Email))
                                            {
                                                if (guest.Value.GuestEmail != null && guest.Value.GuestEmail.ToLower() == request.Email.ToLower())
                                                {
                                                    guestFound = true;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                var searchPhone = phoneString(request.Phone);
                                                var test = phoneString(guest.Value.GuestPhone);
                                                if (test.Length > 9 && test.Length < 11)
                                                {
                                                    // no country code
                                                    searchPhone = searchPhone.Substring(searchPhone.Length - test.Length);

                                                }
                                                if (guest.Value.GuestPhone != null && test.Contains(searchPhone))
                                                {
                                                    guestFound = true;
                                                    break;
                                                }

                                                //if (guest.Value.GuestPhone != null && phoneString(guest.Value.GuestPhone) == request.Phone)
                                                //{
                                                //    guestFound = true;
                                                //    break;
                                                //}
                                                test = phoneString(guest.Value.GuestCellPhone);
                                                if (test.Length > 9 && test.Length < 11)
                                                {
                                                    // no country code
                                                    if (searchPhone.Length > test.Length)
                                                        searchPhone = searchPhone.Substring(searchPhone.Length - test.Length);

                                                }
                                                if (guest.Value.GuestCellPhone != null && test == searchPhone)
                                                {
                                                    guestFound = true;
                                                    break;
                                                }
                                                //if (guest.Value.GuestCellPhone != null && phoneString(guest.Value.GuestCellPhone) == request.Phone)
                                                //{
                                                //    guestFound = true;
                                                //    break;
                                                //}
                                            }
                                        }
                                        if (guestFound)
                                        {
                                            LookupReservationRequest request4 = new LookupReservationRequest();
                                            using (var httpRequest4 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation?reservationID={item.ReservationId}"))
                                            {
                                                using (var response4 = await httpClient
                                                    .SendAsync(httpRequest4, HttpCompletionOption.ResponseHeadersRead)
                                                    .ConfigureAwait(false))
                                                {
                                                    response4.EnsureSuccessStatusCode();
                                                    string result4 = await response4.Content.ReadAsStringAsync();
                                                    dynamic arreglo4 = JObject.Parse(result4);
                                                    reservation = arreglo4;
                                                }
                                            }
                                            break;
                                        }
                                    }

                                }

                                return reservation;
                            }
                        }

                    }
                    else
                    {
                        //var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                        var query = "?reservationID=" + request.ReservationID;
                        //query = query.Replace("&checkInFrom=0001-01-01&checkInTo=0001-01-01", "");
                        using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation{query}"))
                        {
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                string result = await response.Content.ReadAsStringAsync();

                                //Creating an array from GuestList intead on JObjects
                                JArray arr = new JArray();
                                dynamic arreglo = JObject.Parse(result);
                                //if (arreglo.data.status == "checked_out")
                                //{
                                //    return null;
                                //}
                                //foreach (var item in arreglo.data.guestList)
                                //{
                                    
                                //}
                                JObject res = arreglo;
                                return res;
                            }
                        }


                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }
        [HttpPost]
        [Route("lookup2")]
        public async Task<List<JObject>> GetReservations(LookupReservationRequest request)
        {
            JObject reservation = null;
            List<JObject> reservations = new List<JObject>();
            var testId = "";
            if (request.ReservationID == "1111222233331")
            {
                testId = request.ReservationID;
                request.ReservationID = "28256177185";
                var query = "?reservationID=" + request.ReservationID;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                using (var httpClient = new HttpClient())
                {
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation{query}"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            string result = await response.Content.ReadAsStringAsync();

                            //Creating an array from GuestList intead on JObjects
                            JArray arr = new JArray();
                            dynamic arreglo = JObject.Parse(result);
                            JObject res = arreglo;
                            reservations.Add(res);
                            return reservations;
                        }
                    }
                }

            }
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                //await _cloudbedsClient.RefreshAccessToken();
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    if (!string.IsNullOrEmpty(request.CardNumber) || !string.IsNullOrEmpty(request.Email) || !string.IsNullOrEmpty(request.Phone) || request.ReservationID.Length == 16 || request.ReservationID.Length != 13)
                    {
                        string sourceId = "";
                        if (request.ReservationID.Length == 16)
                        {
                            request.CardNumber = request.ReservationID.Substring(request.ReservationID.Length - 4, 4);
                        }
                        else if (!string.IsNullOrEmpty(request.CardNumber))
                        {
                            request.CardNumber = request.CardNumber.Substring(request.CardNumber.Length - 4, 4);
                        }
                        else
                        {
                            sourceId = request.ReservationID;
                        }

                        request.ReservationID = "";


                        var requestByRange = new LookupReservationByRangeRequest();

                        if (request.Checkout != null && request.Checkout)
                        {
                            // checkout
                            requestByRange.Status = "checked_in";
                            //requestByRange.CheckOutFrom = DateTime.Now.Date.AddDays(-2);
                            //requestByRange.CheckOutTo = DateTime.Now.Date.AddDays(1);
                            //requestByRange.CheckInFrom = null;
                            //requestByRange.CheckInTo = null;
                        }
                        else
                        {
                            // checkin
                            requestByRange.Status = "confirmed,checked_in,not_confirmed";
                            requestByRange.CheckInFrom = DateTime.Now.Date.AddDays(-7);
                            requestByRange.CheckInTo = DateTime.Now.Date.AddDays(1);
                        }
                        //requestByRange.CheckInFrom = DateTime.Now.Date.AddDays(-7);
                        //requestByRange.CheckInTo = DateTime.Now.Date.AddDays(1);
                        requestByRange.IncludeGuestsDetails = true;

                        var query = _cloudbedsClient.GetQueryStringFromRequest(requestByRange);
                        query = query.Replace("0001-01-01", "");
                        using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservations{query}"))
                        {
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                string result = await response.Content.ReadAsStringAsync();

                                var resultObject = JsonConvert.DeserializeObject<LookupReservationResponse>(result);

                                var reservationsFound = resultObject.Data.Where(r => r.Status == "confirmed" || r.Status == "checked_in" || r.Status == "not_confirmed").ToList();
                                foreach (var item in reservationsFound)
                                {
                                    bool match = false;
                                    if (sourceId.Length > 0)
                                    {
                                        if (item.ThirdPartyIdentifier != null && item.ThirdPartyIdentifier == sourceId.ToUpper())
                                        {
                                            match = true;
                                            LookupReservationRequest request3 = new LookupReservationRequest();
                                            request3.ReservationID = item.ReservationId;
                                            var query3 = _cloudbedsClient.GetQueryStringFromRequest(request3);
                                            using (var httpRequest3 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation{query3}"))
                                            {
                                                using (var response3 = await httpClient
                                                    .SendAsync(httpRequest3, HttpCompletionOption.ResponseHeadersRead)
                                                    .ConfigureAwait(false))
                                                {
                                                    response3.EnsureSuccessStatusCode();
                                                    string result3 = await response3.Content.ReadAsStringAsync();

                                                    //Creating an array from GuestList intead on JObjects
                                                    JArray arr3 = new JArray();
                                                    dynamic arreglo3 = JObject.Parse(result3);
                                                    reservation = arreglo3;
                                                }
                                            }
                                            break;

                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(request.CardNumber))
                                    {
                                        LookupReservationRequest request2 = new LookupReservationRequest();
                                        using (var httpRequest2 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation?reservationID={item.ReservationId}"))
                                        {
                                            using (var response2 = await httpClient
                                                .SendAsync(httpRequest2, HttpCompletionOption.ResponseHeadersRead)
                                                .ConfigureAwait(false))
                                            {
                                                response2.EnsureSuccessStatusCode();
                                                string result2 = await response2.Content.ReadAsStringAsync();

                                                //Creating an array from GuestList intead on JObjects
                                                JArray arr2 = new JArray();
                                                dynamic arreglo2 = JObject.Parse(result2);
                                                // credit card lookup
                                                foreach (var item2 in arreglo2.data.cardsOnFile)
                                                {
                                                    if (item2.cardNumber == request.CardNumber)
                                                    {
                                                        match = true;
                                                        break;
                                                    }
                                                }
                                                if (match)
                                                {
                                                    reservation = arreglo2;
                                                    reservations.Add(reservation);
                                                    //break;
                                                }

                                            }
                                        }
                                    }

                                    else if (!string.IsNullOrEmpty(request.Phone) || !string.IsNullOrEmpty(request.Email))
                                    {
                                        Boolean guestFound = false;
                                        foreach (var guest in item.GuestList)
                                        {
                                            if (!string.IsNullOrEmpty(request.Email))
                                            {
                                                if (guest.Value.GuestEmail != null && guest.Value.GuestEmail.ToLower() == request.Email.ToLower())
                                                {
                                                    guestFound = true;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                var searchPhone = phoneString(request.Phone);
                                                var test = phoneString(guest.Value.GuestPhone);
                                                if (test.Length > 9 && test.Length < 11)
                                                {
                                                    // no country code
                                                    searchPhone = searchPhone.Substring(searchPhone.Length - test.Length);

                                                }
                                                if (guest.Value.GuestPhone != null && test.Contains(searchPhone))
                                                {
                                                    guestFound = true;
                                                    break;
                                                }

                                                //if (guest.Value.GuestPhone != null && phoneString(guest.Value.GuestPhone) == request.Phone)
                                                //{
                                                //    guestFound = true;
                                                //    break;
                                                //}
                                                test = phoneString(guest.Value.GuestCellPhone);
                                                if (test.Length > 9 && test.Length < 11)
                                                {
                                                    // no country code
                                                    if (searchPhone.Length > test.Length)
                                                        searchPhone = searchPhone.Substring(searchPhone.Length - test.Length);

                                                }
                                                if (guest.Value.GuestCellPhone != null && test.Contains(searchPhone))
                                                {
                                                    guestFound = true;
                                                    break;
                                                }
                                                //if (guest.Value.GuestCellPhone != null && phoneString(guest.Value.GuestCellPhone) == request.Phone)
                                                //{
                                                //    guestFound = true;
                                                //    break;
                                                //}
                                            }
                                        }
                                        if (guestFound)
                                        {
                                            LookupReservationRequest request4 = new LookupReservationRequest();
                                            using (var httpRequest4 = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation?reservationID={item.ReservationId}"))
                                            {
                                                using (var response4 = await httpClient
                                                    .SendAsync(httpRequest4, HttpCompletionOption.ResponseHeadersRead)
                                                    .ConfigureAwait(false))
                                                {
                                                    response4.EnsureSuccessStatusCode();
                                                    string result4 = await response4.Content.ReadAsStringAsync();
                                                    dynamic arreglo4 = JObject.Parse(result4);
                                                    reservation = arreglo4;
                                                    reservations.Add(reservation);
                                                }
                                            }
                                            //break;
                                        }
                                    }

                                }

                                return reservations;
                            }
                        }

                    }
                    else
                    {
                        //var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                        var query = "?reservationID=" + request.ReservationID;
                        //query = query.Replace("&checkInFrom=0001-01-01&checkInTo=0001-01-01", "");
                        using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getReservation{query}"))
                        {
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                string result = await response.Content.ReadAsStringAsync();

                                //Creating an array from GuestList intead on JObjects
                                JArray arr = new JArray();
                                dynamic arreglo = JObject.Parse(result);
                                //if (arreglo.data.status == "checked_out")
                                //{
                                //    return null;
                                //}
                                //foreach (var item in arreglo.data.guestList)
                                //{

                                //}
                                JObject res = arreglo;
                                reservations.Add(res);
                                return reservations;
                            }
                        }


                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }
        private string phoneString(string input)
        {
            string result = "";
            if (!string.IsNullOrEmpty(input))
            {
                result = input.Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
            }
            return result;
        }
        [HttpPost]
        [Route("guest")]
        public async Task<JObject> GetGuest(LookupReservationRequest request)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getGuest{query}"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            string result = await response.Content.ReadAsStringAsync();
                            return JObject.Parse(result);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("status")]
        public async Task<JObject> SaveReservationStatus(ReservationStatusRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Put, "api/v1.1/putReservation"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("payment")]
        public async Task<JObject> SavePayment(ReservationPaymentRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postPayment"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("voidPayment")]
        public async Task<JObject> SavePayment(VoidReservationPaymentRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postVoidPayment"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("reservations")]
        public async Task<JObject> GetReservations(GetReservationsRequest request)
        {
            try
            {
                return await _cloudbedsClient.GetReservations(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("checkin")]
        public async Task<IActionResult> CheckIn(SaveInvitationCodeRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
            if (reservation == null || reservation.Data == null || !reservation.Success)
            {
                return StatusCode(StatusCodes.Status404NotFound, reservation.Message);
            }

            if (reservation.Data.EndDate.Date < DateTime.Now.Date)
            {
                return StatusCode(StatusCodes.Status403Forbidden, "Reservation End date is expired.");
            }
            if (reservation.Data.StartDate.Date > DateTime.Now.Date)
            {
                return StatusCode(StatusCodes.Status403Forbidden, "Reservation Checkin date is not here yet.");
            }
            //if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
            //{
            //    return StatusCode(StatusCodes.Status403Forbidden, "ReservationId does not contains rooms.");
            //}
            if (reservation.Data.Rooms.Length == 1)
            {
                switch (reservation.Data.Status)
                {
                    case "not_confirmed":
                        return StatusCode(StatusCodes.Status403Forbidden, "Reservation is pending confirmation.");
                    case "checked_in":
                        return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked in.");
                    case "canceled":
                        return StatusCode(StatusCodes.Status403Forbidden, "This reservation was canceled.");
                    case "checked_out":
                        return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked out");
                    default:
                        break;
                }
            }
            if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
            {
                // assign room
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getRoomsUnassigned"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            var data = await response.Content.ReadAsStringAsync();
                            try
                            {
                                UnAssignedRoomResult rooms = JsonConvert.DeserializeObject<UnAssignedRoomResult>(data.ToString());
                                var room = rooms.Data[0].Rooms.Where(x => x.RoomTypeID == reservation.Data.UnAssignedRooms[0].RoomTypeID);
                                AssignRoomRequest roomReq = new AssignRoomRequest();
                                roomReq.ReservationID = request.ReservationId;
                                roomReq.NewRoomID = rooms.Data[0].Rooms[0].RoomID;
                                roomReq.RoomTypeID = reservation.Data.UnAssignedRooms[0].RoomTypeID;
                                using (var httpClient2 = new HttpClient())
                                {
                                    httpClient2.DefaultRequestHeaders.Authorization =
                                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                                    httpClient2.BaseAddress = _apiBaseUrl;
                                    using (var httpRequest2 = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postRoomAssign"))
                                    {
                                        httpRequest2.Content = GetFormUrlContentFromRequest(roomReq);
                                        using (var response2 = await httpClient2
                                            .SendAsync(httpRequest2, HttpCompletionOption.ResponseHeadersRead)
                                            .ConfigureAwait(false))
                                        {
                                            response2.EnsureSuccessStatusCode();
                                            var result2 = await response2.Content.ReadAsStringAsync();
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
            }
            var roomId = reservation.Data.Rooms[0].roomID;
            if (!string.IsNullOrEmpty(request.RoomId))
                roomId = request.RoomId;

            try
            {
                //var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationId);
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, $"api/v1.1/postRoomCheckIn"))
                    {
                        var requestBody = new Dictionary<string, string> { { "reservationID", request.ReservationId }, { "roomID", roomId } }; 
                        httpRequest.Content = new FormUrlEncodedContent(requestBody);

                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            string result2 = await response.Content.ReadAsStringAsync();
                            CloudbedResponse resp = JsonConvert.DeserializeObject<CloudbedResponse>(result2);
                            Console.WriteLine(result2);
                            if (!resp.success)
                            {
                                return BadRequest(resp.message);
                            }
                            else
                            {

                            }
                            //return Ok(result2);
                        }
                    }

                    //using (var httpRequest2 = new HttpRequestMessage(HttpMethod.Put, $"api/v1.1/putReservation"))
                    //{
                    //    var requestBody2 = new Dictionary<string, string> { { "reservationID", request.ReservationId }, { "status", "checked_in" } };
                    //    httpRequest2.Content = new FormUrlEncodedContent(requestBody2);

                    //    using (var response2 = await httpClient
                    //        .SendAsync(httpRequest2, HttpCompletionOption.ResponseHeadersRead)
                    //        .ConfigureAwait(false))
                    //    {
                    //        response2.EnsureSuccessStatusCode();
                    //        string result3 = await response2.Content.ReadAsStringAsync();
                    //        CloudbedResponse resp2 = JsonConvert.DeserializeObject<CloudbedResponse>(result3);
                    //        Console.WriteLine(result3);
                    //        return Ok(result3);
                    //    }
                    //}

                }
                return Ok(null);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("checkout")]
        public async Task<IActionResult> CheckOut(LookupReservationRequest request)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var reservation = await _cloudbedsClient.GetReservation(request.ReservationID);
                if (reservation == null || reservation.Data == null || !reservation.Success)
                {
                    return StatusCode(StatusCodes.Status404NotFound, reservation.Message);
                }
                if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
                {
                    return StatusCode(StatusCodes.Status403Forbidden, "ReservationId does not contains rooms.");
                }

                switch (reservation.Data.Status)
                {
                    case "not_confirmed":
                        return StatusCode(StatusCodes.Status403Forbidden, "Reservation is pending confirmation.");
                    case "canceled":
                        return StatusCode(StatusCodes.Status403Forbidden, "This reservation was canceled.");
                    case "checked_out":
                        return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked out");
                    default:
                        if (reservation.Data.Status != "checked_in")
                            return StatusCode(StatusCodes.Status403Forbidden, "Guest have not checked in yet.");
                        break;
                }
                var roomId = reservation.Data.Rooms[0].roomID;
                if (!string.IsNullOrEmpty(request.RoomID))
                    roomId = request.RoomID;

                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    string result = "";
                    for (int i=0; i < reservation.Data.Rooms.Length; i++)
                    {
                        using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, $"api/v1.1/postRoomCheckOut"))
                        {
                            //var requestBody = new Dictionary<string, string> { { "reservationID", request.ReservationID }, { "roomID", request.RoomID } };
                            var requestBody = new Dictionary<string, string> { { "reservationID", request.ReservationID }, { "roomID", roomId } };
                            httpRequest.Content = new FormUrlEncodedContent(requestBody);
                            using (var response = await httpClient
                                .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                                .ConfigureAwait(false))
                            {
                                response.EnsureSuccessStatusCode();
                                result = await response.Content.ReadAsStringAsync();
                            }
                        }
                    }
                    return Ok(result);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getHotels")]
        public async Task<JObject> GetHotels()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getHotels"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpGet]
        [Route("getAppState")]
        public async Task<JObject> GetAppState()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    AppStateRequest req = new AppStateRequest();
                    req.PropertyID = 201938; // 164567 (HN)
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getAppState"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(req);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        [HttpPost]
        [Route("postAppState")]
        public async Task<JObject> PostAppState(AppStateRequestUpdate request)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    AppStateRequestUpdate req = new AppStateRequestUpdate();
                    if (request == null)
                    {
                        req.PropertyID = 201938; // 164567 (HN)
                        req.App_state = "disabled"; // 164567 (HN)
                    }
                    else
                    {
                        req.PropertyID = request.PropertyID;
                        req.App_state = request.App_state;
                    }
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postAppState"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(req);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            var result = await response.Content.ReadAsStringAsync();
                            return JObject.Parse(result);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getRoomsUnassigned")]
        public async Task<JObject> GetRoomsUnassigned()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getRoomsUnassigned"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("postRoomAssign")]
        public async Task<JObject> PostRoomAssign(AssignRoomRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postRoomAssign"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("postPayment")]
        public async Task<JObject> PostPayment(PaymentRequest request)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postPayment"))
                    {
                        httpRequest.Content = GetFormUrlContentFromRequest(request);
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return JObject.Parse(await response.Content.ReadAsStringAsync());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private FormUrlEncodedContent GetFormUrlContentFromRequest<T>(T request)
        {
            var keyValuePairArrays = new List<KeyValuePair<string, string>>();
            var properties = typeof(T).GetProperties().Where(p => p.Name != "AccessToken");
            foreach (var property in properties)
            {
                var value = property.GetValue(request);
                switch (property.PropertyType.Name)
                {
                    case "DateTime":
                        value = ((DateTime)property.GetValue(request)).ToString("yyyy-MM-dd");
                        break;

                    case "ReservationRoom[]":
                        var rooms = (ReservationRoom[])property.GetValue(request);
                        for (var i = 0; i < rooms.Length; i++)
                        {
                            var roomProperties = typeof(ReservationRoom).GetProperties();
                            foreach (var roomProperty in roomProperties)
                            {
                                var roomPropertyValue = roomProperty.GetValue(rooms[i]);
                                keyValuePairArrays.Add(new KeyValuePair<string, string>(
                                    $"{CamelCase(property.Name)}[{i}][{CamelCase(roomProperty.Name)}]", roomPropertyValue.ToString()));
                            }
                        }
                        continue;

                    case "PaymentRequest[]":
                        var payments = (PaymentRequest[])property.GetValue(request);
                        for (var i = 0; i < payments.Length; i++)
                        {
                            var paymentProperties = typeof(PaymentRequest).GetProperties();
                            foreach (var paymenyProperty in paymentProperties)
                            {
                                var paymentPropertyValue = paymenyProperty.GetValue(payments[i]);
                                keyValuePairArrays.Add(new KeyValuePair<string, string>(
                                    $"{CamelCase(property.Name)}[{i}][{CamelCase(paymenyProperty.Name)}]", paymentPropertyValue.ToString()));
                            }
                        }
                        continue;
                }

                keyValuePairArrays.Add(new KeyValuePair<string, string>($"{CamelCase(property.Name)}", value.ToString()));
            }

            return new FormUrlEncodedContent(keyValuePairArrays.ToArray());
        }


        private string CamelCase(string name)
        {
            return $"{name[0].ToString().ToLower()}{name.Substring(1)}";
        }
    }
}