﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CloudBaseWeb.Api.Entities;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Net.Mail;
using System.Net;
using Microsoft.AspNetCore.Cors;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [Route("api/cart")]
    [EnableCors("All")]
    [AllowAnonymous]
    public class ShoppingCartController : ControllerBase
    {
        private readonly UserContext _userContext;
        private readonly IConfiguration _configuration;
        private readonly ICloudbedsClient _cloudbedsClient;
        private readonly MobileKeyDbContext _dbContext;
        private readonly Uri _apiBaseUrl;
        private const string Percentage = "percentage";
        private const string Rate = "rate";

        public ShoppingCartController(IConfiguration configuration, ICloudbedsClient cloudbedsClient, MobileKeyDbContext dbContext, UserContext userContext)
        {
            _configuration = configuration;
            _cloudbedsClient = cloudbedsClient;
            _apiBaseUrl = new Uri(configuration["CloudbedsApi:BaseUrl"]);
            _userContext = userContext;
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("products")]
        public async Task<ProductsResponse> GetProducts()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, "api/v1.1/getItems"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<ProductsResponse>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("addOns/{startDate}/{endDate}")]
        public async Task<JObject> GetAddOns(DateTime startDate, DateTime endDate)
        {
            try
            {
                var request = new AddOnsRequest();
                request.startDate = startDate;
                request.endDate = endDate;
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getRatePlans{query}"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<JObject>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("products/details")]
        public async Task<ProductsResponse> GetProductDetails(ProductRequest request)
        {
            if (request == null || request.ItemId < 1)
            {
                BadRequest();
            }

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
                    httpClient.BaseAddress = _apiBaseUrl;

                    var query = _cloudbedsClient.GetQueryStringFromRequest(request);
                    using (var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"api/v1.1/getItem{query}"))
                    {
                        using (var response = await httpClient
                            .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
                            .ConfigureAwait(false))
                        {
                            response.EnsureSuccessStatusCode();
                            return await response.Content.ReadAsAsync<ProductsResponse>();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //Save email and reservationID of a sharedKey
        [HttpPost]
        [Route("shareKey")]
        public async Task<JObject> CreateShared(ShareKeyRequest request)
        {
            try
            {
                //Check if email has already being used
                var key = await _dbContext.Set<SharedKey>()
               .Where(c => c.Email == request.Email)
               .Where(c => c.Claimed == false).FirstOrDefaultAsync();
               //.SingleOrDefaultAsync();

                if (key != null) {
                    return JObject.Parse("{success : false,  message: 'A key has already been shared for this email'}");
                }

                var newSharedKey = new SharedKey
                {
                    Email = request.Email,
                    ReservationId = request.ReservationId,
                    Claimed = false,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    IsActive = true
                };

                _dbContext.Add(newSharedKey);
                await _dbContext.SaveChangesAsync();

                //Send Email
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("monitor@trueomni.com");
                message.To.Add(new MailAddress(request.Email));
                message.Subject = "Hotel Next - Shared Key";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = "Greetings, <br/> <br/>" + request.Name + ", has Shared a Key from Hotel Next with you <br/>" ;
                message.Body += "To be able to access to the Key please make sure to download and login to the application: <br/><br/>";
                message.Body += "<a href='https://apps.apple.com/us/app/hotel-next/id1541673098'>iOS application</a> <br/><br/>";
                message.Body += "<a href='https://play.google.com/store/apps/details?id=com.omniex.hotelnext'>Android application</a>";
                message.Body += "<br/><br/><br/>We hope to see you soon!<br/><br/>";
                message.Body += "<img src = 'https://www.hotelnext.com/wp-content/uploads/2019/09/logo-png.png' alt=''>";

                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("monitor@trueomni.com", "viyyukgjgvrhoikc");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);

            }
            catch (Exception ex)
            {
                return JObject.Parse("{success : false,  message: 'An error has ocurred'}");
            }

            return JObject.Parse("{success : true,  message: 'Your key has been shared'}");
        }

        //Get the data if a key was shared to that email
        [HttpPost]
        [Route("getShared")]
        public async Task<SharedKey> GetShared(ShareKeyRequest request)
        {
            var share = new SharedKey();
            try
            {
                 share = await _dbContext.Set<SharedKey>()
                    .Where(c => c.Email == request.Email)
                    .Where(c => c.Claimed == false)
                    .SingleOrDefaultAsync();

                if (share == null) {
                    share = new SharedKey();
                }
            
                return share;
            }
            catch (Exception ex)
            {
                return share;
            }
        }

        // Update to Clamed = true if the sharedKey was claimed
        [HttpPost]
        [Route("updateShared")]
        public async Task<SharedKey> UpdateShared(ShareKeyRequest request)
        {
            var share = new SharedKey();
            try
            {
                share = await _dbContext.Set<SharedKey>()
                   .Where(c => c.Email == request.Email)
                   .Where(c => c.Claimed == false)
                   .SingleOrDefaultAsync();

                if (share != null) {
                    share.Claimed = true;
                    share.UpdatedAt = DateTime.Now;
                    _dbContext.Update(share);
                    await _dbContext.SaveChangesAsync();
                }

                return share;
            }
            catch (Exception ex)
            {
                return share;
            }
        }



        [HttpPost]
        [Route("createcart")]
        public async Task<Guid?> CreateCart(ShoppingCartRequest request)
        {
            var cartGUID = Guid.NewGuid();

            try
            {
                var newShoppingCart = new ShoppingCart
                {
                    GUID = cartGUID,
                    ReservationId = request.ReservationId,
                    ExpirationDate = DateTime.Now.AddHours(4),
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    IsActive = true
                };

                _dbContext.Add(newShoppingCart);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return null;
            }

            return cartGUID;
        }

        [HttpPost]
        [Route("addcartitem")]
        public async Task<bool> AddCartItem(CartItemRequest request)
        {
            try
            {
                var cart = _dbContext.Set<ShoppingCart>()
                                    .Where(c => c.GUID == request.CartGUID)
                                    .SingleOrDefaultAsync();

                if (cart != null)
                {
                    var newCartItem = new CartItems
                    {
                        ShoppingCartId = cart.Id,
                        ItemId = request.ItemId,
                        Quantity = request.Quantity,
                        Price = request.Price,
                        SubTotal = request.Quantity * request.Price,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        IsActive = true
                    };

                    _dbContext.Add(newCartItem);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        [HttpGet]
        [Route("cartdetails")]
        public async Task<IEnumerable<CartItems>> GetCartItems(CartItemRequest request)
        {
            var cartItems = new List<CartItems>();

            try
            {
                var cart = _dbContext.Set<ShoppingCart>()
                                    .Where(c => c.GUID == request.CartGUID)
                                    .SingleOrDefaultAsync();

                if (cart != null)
                {
                    cartItems = await _dbContext.Set<CartItems>()
                                            .Where(c => c.Id == cart.Id)
                                            .ToListAsync();
                }

                return cartItems;
            }
            catch (Exception ex)
            {
                return cartItems;
            }
        }
    }
}
