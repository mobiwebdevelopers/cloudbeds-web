﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [EnableCors("All")]
    [Route("api/mobileKey")]
    public class MobileKeyController : ControllerBase
    {
        private readonly UserContext _userContext;

        private readonly IMobileKeyService _mobileKeyService;
        private readonly ICloudbedsClient _cloudbedsClient;

        public MobileKeyController(UserContext userContext, IMobileKeyService mobileKeyService, ICloudbedsClient cloudbedsClient)
        {
            _mobileKeyService = mobileKeyService;
            _cloudbedsClient = cloudbedsClient;
            _userContext = userContext;
        }

        [HttpGet]
        [Route("codes")]
        public async Task<IActionResult> GetUserInvitationCodes()
        {
            return Ok(await _mobileKeyService.GetUserEndpoints(_userContext.Profile.UserId));
        }

        [HttpPost]
        [Route("codes")]
        public async Task<IActionResult> SaveUserInvitationCode(SaveInvitationCodeRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
            if (reservation == null || reservation.Data == null || !reservation.Success)
            {
                return BadRequest(reservation.Message);
            }

            if (reservation.Data.EndDate.Date < DateTime.Now.Date)
            {
                return BadRequest("Reservation End date is expired.");
            }

            try
            {
                var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationId);
                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        //[HttpPost]
        //[Route("checkin")]
        //public async Task<IActionResult> CheckIn(SaveInvitationCodeRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ReservationId))
        //    {
        //        return BadRequest("ReservationId parameter should not be null or empty.");
        //    }

        //    var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
        //    if (reservation == null || reservation.Data == null || !reservation.Success)
        //    {
        //        return BadRequest(reservation.Message);
        //    }

        //    if (reservation.Data.EndDate.Date < DateTime.Now.Date)
        //    {
        //        return BadRequest("Reservation End date is expired.");
        //    }
        //    if (reservation.Data.StartDate.Date > DateTime.Now.Date)
        //    {
        //        return BadRequest("Reservation Checkin date is not here yet.");
        //    }
        //    switch (reservation.Data.Status)
        //    {
        //        case "not_confirmed":
        //            return BadRequest("Reservation is pending confirmation.");
        //        case "checked_in":
        //            return BadRequest("Guest already checked in.");
        //        case "canceled":
        //            return BadRequest("This reservation was canceled.");
        //        case "checked_out":
        //            return BadRequest("Guest already checked out");
        //    }

        //    try
        //    {
        //        var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationId);
        //        return Ok(result);
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}



        [HttpPost]
        [Route("prepare")]
        public async Task<IActionResult> PrepareKey(PrepareKeyRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            if (string.IsNullOrWhiteSpace(request.EndpointId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
            if (reservation == null || reservation.Data == null || !reservation.Success)
            {
                return BadRequest(reservation.Message);
            }

            if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
            {
                return BadRequest("ReservationId does not contains rooms.");
            }

            //if (reservation.Data.Status != "checked_in")
            //{
            //    return BadRequest("Reservation Status is not checked in.");
            //}

            try
            {

                var result = await _mobileKeyService.IssueMobileKey(
                    _userContext.Profile.UserId,
                    request.ReservationId,
                    request.EndpointId,
                    reservation.Data.EndDate,
                    reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray());

                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }
        [HttpPost]
        [Route("create-card")]
        public async Task<IActionResult> CreateCard(PrepareKeyRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            if (request.ReservationId.Length == 3)
            {
                // Simulation

                try
                {
                    string[] door = new string[1];
                    door[0] = request.ReservationId;
                    var result = await _mobileKeyService.IssuePhysicalKey(
                        request.ReservationId,
                        "",
                        DateTime.Now.Date.AddDays(3), door
                        );

                    return Ok(result);
                }
                catch (ArgumentException ae)
                {
                    return BadRequest(ae.Message);
                }
                catch (Exception e)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, e);
                }
            }
            else
            {
                var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
                if (reservation == null || reservation.Data == null || !reservation.Success)
                {
                    return BadRequest(reservation.Message);
                }

                if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
                {
                    return BadRequest("ReservationId does not contains rooms.");
                }

                //if (reservation.Data.Status != "checked_in")
                //{
                //    return BadRequest("Reservation Status is not checked in.");
                //}

                try
                {
                    // 4817692
                    if (request.Override)
                    {
                        var result = await _mobileKeyService.CancelPhysicalKey(
                            request.ReservationId,
                            "",
                            reservation.Data.StartDate,
                            reservation.Data.EndDate.AddHours(12),
                            string.IsNullOrEmpty(request.RoomId) ? reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray() : reservation.Data.Rooms.Where(x => x.roomID == request.RoomId).ToList().Select(r => r.roomName).ToArray());

                        return Ok(result);
                    }
                    else
                    {
                        var result = await _mobileKeyService.IssuePhysicalKey(
                            request.ReservationId,
                            "",
                            reservation.Data.EndDate.AddHours(12),
                            string.IsNullOrEmpty(request.RoomId) ? reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray() : reservation.Data.Rooms.Where(x => x.roomID == request.RoomId).ToList().Select(r => r.roomName).ToArray());

                        return Ok(result);
                    }
                }
                catch (ArgumentException ae)
                {
                    return BadRequest(ae.Message);
                }
                catch (Exception e)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, e);
                }
            }
        }
        [HttpPost]
        [Route("create-key-image")]
        public async Task<IActionResult> CreateCard2(PrepareKeyRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationId))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }
            if (string.IsNullOrWhiteSpace(request.SerialNumber))
            {
                return BadRequest("SerialNumber parameter should not be null or empty.");
            }

            if (request.ReservationId.Length == 3)
            {
                // Simulation

                try
                {
                    string[] door = new string[1];
                    door[0] = request.ReservationId;
                    var result = await _mobileKeyService.IssueKeyImage(
                        request.ReservationId,
                        //string.IsNullOrEmpty(request.SerialNumber) ? "04C42B82DE6A80" : request.SerialNumber, // 7 bytes
                        request.SerialNumber.Split(','), // 7 bytes
                        DateTime.Now.Date.AddDays(3), door, request.Override
                        );

                    return Ok(result);
                }
                catch (ArgumentException ae)
                {
                    return BadRequest(ae.Message);
                }
                catch (Exception e)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, e);
                }
            }
            else
            {
                var reservation = await _cloudbedsClient.GetReservation(request.ReservationId);
                if (reservation == null || reservation.Data == null || !reservation.Success)
                {
                    return BadRequest(reservation.Message);
                }

                if (reservation.Data.Rooms == null || !reservation.Data.Rooms.Any())
                {
                    return BadRequest("ReservationId does not contains rooms.");
                }

                //if (reservation.Data.Status != "checked_in")
                //{
                //    return BadRequest("Reservation Status is not checked in.");
                //}

                try
                {
                    // 4817692
                    //if (request.Override)
                    //{
                    //    var result = await _mobileKeyService.CancelPhysicalKey(
                    //        request.ReservationId,
                    //        request.SerialNumber,
                    //        reservation.Data.StartDate,
                    //        reservation.Data.EndDate.AddHours(12),
                    //        string.IsNullOrEmpty(request.RoomId) ? reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray() : reservation.Data.Rooms.Where(x => x.roomID == request.RoomId).ToList().Select(r => r.roomName).ToArray());

                    //    return Ok(result);
                    //}
                    //else
                    //{
                    var result = await _mobileKeyService.IssueKeyImage(
                        request.ReservationId,
                        request.SerialNumber.Split(','),
                        reservation.Data.EndDate.AddHours(12),
                        string.IsNullOrEmpty(request.RoomId) ? reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray() : reservation.Data.Rooms.Where(x => x.roomID == request.RoomId).ToList().Select(r => r.roomName).ToArray());

                    return Ok(result);
                    //}
                }
                catch (ArgumentException ae)
                {
                    return BadRequest(ae.Message);
                }
                catch (Exception e)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, e);
                }
            }
        }
        //[HttpPost]
        //[Route("cards")]
        //public async Task<IActionResult> GetCards(GetCardsRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.CardId))
        //    {
        //        return BadRequest("CardId parameter should not be null or empty.");
        //    }

        //    try
        //    {

        //        var result = await _mobileKeyService.IssueMobileKey(
        //            _userContext.Profile.UserId,
        //            request.ReservationId,
        //            request.EndpointId,
        //            reservation.Data.EndDate,
        //            reservation.Data.Rooms.ToList().Select(r => r.roomName).ToArray());

        //        return Ok(result);
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}
        [HttpPost]
        [Route("card-info")]
        public async Task<IActionResult> GetCardInfo()
        {
            try
            {

                var result = await _mobileKeyService.GetCardInfo();

                return Ok(result);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }


    }
}
