﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;
using System.IO;
using Stripe;

namespace Cloudbeds.WebApi.Controllers
{

    [ApiController]
    [Route("api/salesforce")]
    [EnableCors("All")]
    [AllowAnonymous]
    public class SalesforceController : ControllerBase
    {
        private readonly UserContext _userContext;

        private readonly IConfiguration _configuration;
        private readonly ICloudbedsClient _cloudbedsClient;
        private readonly IMobileKeyService _mobileKeyService;
        private readonly Uri _apiBaseUrl;
        private const string Percentage = "percentage";
        private const string Rate = "rate";

        public SalesforceController(IConfiguration configuration, ICloudbedsClient cloudbedsClient, IMobileKeyService mobileKeyService, UserContext userContext)
        {
            //_configuration = configuration;
            //_cloudbedsClient = cloudbedsClient;
            //_mobileKeyService = mobileKeyService;
            //_apiBaseUrl = new Uri(configuration["CloudbedsApi:BaseUrl"]); 
            // _userContext = userContext;
        }

        [Route("callback")]
        public async Task<string> SalesforceAuthCallBack([FromQuery] string code)
        {
            return "code:" + code;
        }


    }
}