﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using Cloudbeds.Data;
using CloudBaseWeb.Api.Entities;
using Cloudbeds.WebApi.Models.B4T;
using System.Globalization;

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/b4t")]
    [ApiController]
    [AllowAnonymous]
    public class B4TController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly Uri _apiBaseUrl;
        private string _code = "CASCADA";
        private string _shopifyToken = "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";
        private string _shopifyUrl = "https://trueomni-dev.myshopify.com";

        public B4TController(IConfiguration configuration, MobileKeyDbContext dbContext)
        {
            _configuration = configuration;
            _apiBaseUrl = new Uri(configuration["MEWS:BaseUrl"]);
        }

        //[Route("addCustomer/{locationId}/{email}/{firstName}/{lastName}")]
        [HttpGet]
        [Route("addCustomer")]
        public async Task<UserResponse> AddCustomer(int locationId, string email="", string firstName="", string lastName="")
        //public async Task<UserResponse> AddCustomer(int locationId, string email, string firstName, string lastName)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Customer req = new Customer();
                    req.Email = email;
                    req.FirstName = firstName;
                    req.LastName = lastName;
                    req.Id = 0;
                    req.UserAccount = new UserAccount();
                    req.UserAccount.AuthenticationType = 1;
                    req.UserAccount.IsActive = true;
                    req.UserAccount.LoginId = email;
                    req.UserAccount.LocationId = locationId;
                    req.UserAccount.MachineId = "CASCADA";
                    req.UserAccount.NumDaysTillTokenExp = 7;
                    req.UserAccount.Password = "Cascada";
                    req.UserAccount.UserId = 0;

                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");
                    req.UserAccount.LocationId = locationId;
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                    HttpResponseMessage tokenResponse = await client.PostAsync($"Book4TimeAPI2/api/Customer/RegisterGuest", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<UserResponse>(jsonContent);
                    AddUpdateShopifyCustomer(email, firstName, lastName, data.UserId);
                    return data;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getServiceGroups/{locationId}")]
        public async Task<List<ServiceGroup>> GetServiceGroups(int locationId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Service/GetServiceGroups?locationId={locationId}&onlineEnable=true&excludeActivities=true");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<ServiceGroup>>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getServiceTypes/{locationId}")]
        public async Task<List<ServiceType>> GetServiceTypes(int locationId, long serviceGroupId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Service/GetServiceTypes?locationId={locationId}&service_group_id={serviceGroupId}&onlineBooking=true");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<ServiceType>>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getServiceType/{locationId}/{serviceTypeId}")]
        public async Task<ServiceType> GetServiceType(int locationId, long serviceTypeId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Service/GetServiceType?locationId={locationId}&service_type_id={serviceTypeId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ServiceType>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getServiceAllTechniciansByLocation/{locationId}/{serviceTypeId}/{date}")]
        public async Task<List<Technician>> GetServiceAllTechniciansByLocation(int locationId, long serviceTypeId, string date)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Technician/GetAllTechniciansByLocation?date={date}&serviceTypeId={serviceTypeId}&locationId={locationId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Technician>>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("GetTechnicianByServiceDate/{locationId}/{serviceTypeId}/{date}")]
        public async Task<List<Technician>> GetTechnicianByServiceDate(int locationId, long serviceTypeId, string date)
        {
            try
            {
                var technicianList = new List<Technician>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Technician/GetTechnicianByServiceDate?date={date}&serviceId={serviceTypeId}&locationId={locationId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var response = JsonConvert.DeserializeObject<List<Technician>>(jsonContent);

                    foreach (var data in response)
                    {
                        if (!technicianList.Any(x => x.TechnicianId == data.TechnicianId))
                        {
                            technicianList.Add(data);
                        }
                    }

                    return technicianList;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getActivityServices/{locationId}/{technicianId}")]
        public async Task<List<ServiceActivity>> GetActivityServices(int locationId, long technicianId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Service/GetActivityServices?locationId={locationId}&technicianId={technicianId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();

                    if (tokenResponse.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<List<ServiceActivity>>(jsonContent);
                    else 
                        return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getTechnicianSchedulesByDateRange")]
        public async Task<List<TechnicianSchedule>> GetTechnicianSchedulesByDateRange(long technicianId, string start, string end)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Technician/GetTechnicianSchedulesByDateRange?startDate={start}&endDate={end}&technicianId={technicianId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();

                    if (tokenResponse.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<List<TechnicianSchedule>>(jsonContent);
                    else
                        return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getTechnicianSchedulesByServiceDate")]
        public async Task<List<TechnicianSchedule>> GetTechnicianSchedulesByServiceDate(string date, long serviceId, long locationId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/api/Technician/getTechnicianSchedulesByServiceDate?date={date}&serviceTypeId={serviceId}&locationId={locationId}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();

                    if (tokenResponse.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<List<TechnicianSchedule>>(jsonContent);
                    else
                        return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getActivityList")]
        public async Task<List<Activity>> GetActivityList(int locationId, string start, string end)
        {
            try
            {
                var activityList = new List<Activity>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/Api/Appointment/GetActivityList/{locationId}/{start}/{end}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    activityList = JsonConvert.DeserializeObject<List<Activity>>(jsonContent);

                    return activityList;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getActivityListByGroup")]
        public async Task<List<ActivityListData>> GetActivityListByGroup(int locationId, long serviceGroupId, string start, string end)
        {
            try
            {
                var data = new List<ActivityListData>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/Api/Appointment/GetActivityList/{locationId}/{serviceGroupId}/{start}/{end}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var activityList = JsonConvert.DeserializeObject<List<Activity>>(jsonContent);

                    if (activityList == null)
                        return data;

                    var firsActivity = activityList.FirstOrDefault();

                    if (firsActivity != null)
                    {
                        data.Add(new ActivityListData
                        {
                            Id = 0,
                            Name = firsActivity.ServiceGroupName + ": All Instructors",
                        });
                    }

                    foreach (var activity in activityList)
                    {
                        if (!data.Any(x => x.Id == activity.ServiceTypeId))
                        {
                            data.Add(new ActivityListData
                            {
                                Id = activity.TechnicianId,
                                Name = activity.TechnicianName
                            });
                        }
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getChooseServiceList")]
        public async Task<List<ActivityListData>> GetChooseServiceList(int locationId, string date)
        {
            try
            {
                var data = new List<ActivityListData>();


                var activityList = await GetActivityList(locationId, date, date);
                var serviceGroups = await GetServiceGroups(locationId);

                var firstService = serviceGroups.FirstOrDefault(x => x.Images.Length > 0);

                if (firstService != null)
                {
                    data.Add(new ActivityListData
                    {
                        Id = firstService.Id,
                        Name = "TREATMENTS",
                        ImageUrl = firstService.Images.FirstOrDefault().Path.ToString(),
                        Type = 1
                    });
                }

                if (activityList != null)
                {
                    foreach (var activity in activityList)
                    {
                        if (!data.Any(x => x.Id == activity.ServiceGroupId))
                        {
                            var serviceGroup = serviceGroups.FirstOrDefault(x => x.Id == activity.ServiceGroupId);

                            data.Add(new ActivityListData
                            {
                                Id = activity.ServiceGroupId,
                                Name = activity.ServiceGroupName,
                                ImageUrl = serviceGroup != null && serviceGroup.Images.Length > 0 ? serviceGroup.Images.FirstOrDefault().Path.ToString() : "",
                                Type = 2
                            });
                        }
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getActivityListMenuItems")]
        public async Task<List<ActivityListData>> getActivityListMenuItems(int locationId, long serviceGroupId, string start, string end)
        {
            try
            {
                var data = new List<ActivityListData>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/Api/Appointment/GetActivityList/{locationId}/{serviceGroupId}/{start}/{end}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var activityList = JsonConvert.DeserializeObject<List<Activity>>(jsonContent);

                    if (activityList == null)
                        return data;

                    var firsActivity = activityList.FirstOrDefault();

                    if (firsActivity != null)
                    {
                        data.Add(new ActivityListData
                        {
                            Id = 0,
                            Name = firsActivity.ServiceGroupName + ": All Classes",
                        });
                    }

                    foreach (var activity in activityList)
                    {
                        if (!data.Any(x => x.Id == activity.ServiceTypeId))
                        {
                            data.Add(new ActivityListData
                            {
                                Id = activity.ServiceTypeId,
                                Name = activity.ServiceName
                            });
                        }
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getActivityListClass")]
        public async Task<List<ActivityListClass>> GetActivityListClass(int locationId, long serviceGroupId, long serviceTypeId, string start, string end)
        {
            try
            {
                var data = new List<ActivityListClass>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/Api/Appointment/GetActivityList/{locationId}/{serviceGroupId}/{start}/{end}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var activityList = JsonConvert.DeserializeObject<List<Activity>>(jsonContent);

                    if (activityList == null)
                        return data;

                    if (serviceTypeId != 0)
                        activityList = activityList.Where(x => x.ServiceTypeId == serviceTypeId).ToList();

                    foreach (var activity in activityList)
                    {
                        if (!data.Any(x => x.Id == activity.ServiceTypeId))
                        {
                            var hour = activity.Date.Hour.ToString().Length == 1 ? "0" + activity.Date.Hour : activity.Date.Hour.ToString();
                            var minute = activity.Date.Minute.ToString().Length == 1 ? "0" + activity.Date.Minute : activity.Date.Minute.ToString();

                            CultureInfo ci = new CultureInfo("en-US");

                            var newDate = new DateTime(activity.Date.Year, activity.Date.Month, activity.Date.Day);
                            data.Add(new ActivityListClass
                            {
                                Id = activity.AppointmentId,
                                Name = activity.ServiceName,
                                TechnicianId = activity.TechnicianId,
                                TechnicianName = activity.TechnicianName,
                                Description = activity.Description,
                                FullDate = activity.Date,
                                Date = newDate.ToString("MMMM dd, yyyy", ci),
                                Day = activity.Date.Day,
                                Duration = activity.Duration,
                                DefaultPrice = activity.LocationDefaultPrice,
                                Price = activity.Price,
                                Schedule = (activity.Date.Hour > 12 ?  "0" + (activity.Date.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (activity.Date.Hour >= 12 ? " PM" : " AM")
                            });
                        }
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getAdditionalActivityListClass")]
        public async Task<List<ActivityListClass>> GetAdditionalActivityListClass(int locationId, long appointmentId, string start, string end)
        {
            try
            {
                var data = new List<ActivityListClass>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    HttpResponseMessage tokenResponse = await client.GetAsync($"Book4TimeAPI2/Api/Appointment/GetActivityList/{locationId}/0/{start}/{end}");

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var activityList = JsonConvert.DeserializeObject<List<Activity>>(jsonContent);

                    if (activityList == null)
                        return data;

                    var currentActivity = activityList.FirstOrDefault(x => x.AppointmentId == appointmentId);
                    var currentActivityTime = currentActivity.Date.AddMinutes(currentActivity.Duration);

                    var activityListFiltered = activityList.Where(x => x.Date != currentActivityTime && currentActivity.ServiceTypeId != x.ServiceTypeId).Take(2).ToList();
                    //var activityListFiltered = activityList.Where(x => x.Date != currentActivityTime).Take(2).ToList();

                    var count = 0;
                    foreach (var activity in activityListFiltered)
                    {
                        if (!data.Any(x => x.Id == activity.ServiceTypeId))
                        {
                            //var hour = activity.Date.Hour.ToString().Length == 1 ? "0" + activity.Date.Hour : activity.Date.Hour.ToString();
                            //var minute = activity.Date.Minute.ToString().Length == 1 ? "0" + activity.Date.Minute : activity.Date.Minute.ToString();

                            CultureInfo ci = new CultureInfo("en-US");

                            var newDate = new DateTime(activity.Date.Year, activity.Date.Month, activity.Date.Day);
                            data.Add(new ActivityListClass
                            {
                                Id = activity.AppointmentId,
                                Name = activity.ServiceName,
                                TechnicianId = activity.TechnicianId,
                                TechnicianName = activity.TechnicianName,
                                Description = activity.Description,
                                FullDate = activity.Date,
                                Date = newDate.ToString("MMMM dd, yyyy", ci),
                                Day = activity.Date.Day,
                                Duration = activity.Duration,
                                DefaultPrice = activity.LocationDefaultPrice,
                                Price = activity.Price, 
                                FirstAdditional = count == 0,
                                Schedule = activity.Date.ToString("hh:mm tt")//(activity.Date.Hour > 12 ? "0" + (activity.Date.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (activity.Date.Hour >= 12 ? " PM" : " AM")
                            });
                        }

                        count++;
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        [Route("getFullByDay")]
        public async Task GetFullByDay(int locationId, string date)
        {
            try
            {
                locationId = 6156344;
                var serviceTypes = new List<ServiceType>();
                var technicians = new List<Technician>();
                var technicians2 = new List<Technician>();

                var activityServices = new List<ServiceActivity>();


                var services = await GetServiceGroups(locationId);
                var activities = await GetActivityList(locationId, date, date);
                foreach (var service in services)
                {
                    var response = await GetServiceTypes(locationId, service.Id);
                    if (response != null)
                        serviceTypes.AddRange(response);
                }

                foreach (var type in serviceTypes)
                {
                    var response = await GetServiceAllTechniciansByLocation(locationId, type.Id, date);
                    if (response != null)
                        technicians.AddRange(response);
                }

                //demo
                foreach (var service in services)
                {
                    var response = await GetTechnicianByServiceDate(locationId, service.Id, date);
                    if (response != null)
                        technicians2.AddRange(response);
                }

                //activities
                //foreach (var activity in activities)
                //{
                //    var response = await GetTechnicianSchedulesByServiceDate(date, activity.ServiceTypeId, locationId);
                //    if (response != null)
                //        technicians3.AddRange(response);
                //}

                foreach (var technician in technicians2)
                {
                    var response = await GetActivityServices(locationId, technician.TechnicianId);
                    if (response != null)
                        activityServices.AddRange(response);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getTechnicians")]
        public async Task<List<TechnicianList>> GetTechnicians(int locationId, string date)
        {
            try
            {
                locationId = 6156344;
                var technicians = new List<Technician>();
                var technicianList = new List<TechnicianList>();

                var services = await GetServiceGroups(locationId);

                foreach (var service in services)
                {
                    var response = await GetTechnicianByServiceDate(locationId, service.Id, date);
                    if (response != null)
                        technicians.AddRange(response);
                }

                technicianList.Add(
                    new TechnicianList
                    {
                        Name = "Any Therapist",
                        Id = 0
                    });
                technicianList.Add(
                    new TechnicianList
                    {
                        Name = "Female Therapist",
                        Id = 1
                    });
                technicianList.Add(
                    new TechnicianList
                    {
                        Name = "Male Therapist",
                        Id = 2
                    });


                foreach(var technician in technicians)
                {
                    if (!technicianList.Any(x => x.Id == technician.Id))
                        technicianList.Add(new TechnicianList { Id = technician.Id, Name = technician.FullNameDisplayed });
                }

                return technicianList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getCategorySchedulesByTechnician")]
        public async Task<ServiceGroupDataList> GetCategorySchedulesByTechnician(int locationId, string date, long id)
        {
            try
            {
                locationId = 6156344;
                var technicians = new List<Technician>();

                var data = new ServiceGroupDataList();
                data.ServiceGroupData = new List<ServiceGroupData>();

                var services = await GetServiceGroups(locationId);
                var count = 0;

                for (var i = 0; i < services.Count; i++)
                {
                    data.ServiceGroupData.Add(
                         new ServiceGroupData
                         {
                             GroupId = services[i].Id,
                             Name = services[i].Name,
                             ImageUrl = services[i].Images.Length > 0 ? services[i].Images.FirstOrDefault().Path.ToString() : ""
                         });

                    data.ServiceGroupData[i].ServiceTypes = new List<ServiceTypeData>();

                    var types = await GetServiceTypes(locationId, services[i].Id);

                    if (types == null)
                        continue;
                    int counter = 0;
                    foreach (var type in types)
                    {
                        counter++;
                        if (counter > 5)
                            break;

                        var technicianList = await GetTechnicianByServiceDate(locationId, type.Id, date);

                        var technicianListFiltered = id == 0 ? technicianList.ToList()
                                            : id == 1 ? technicianList.Where(x => x.Gender == "F").ToList()
                                            : id == 2 ? technicianList.Where(x => x.Gender == "M").ToList()
                                            : technicianList.Where(x => x.Id == id).ToList();

                        if (technicianListFiltered.Count() > 0)
                        {
                            var serviceType = new ServiceTypeData
                            {
                                ServiceTypeId = type.Id,
                                Description = type.Name,
                                Price = type.Price
                            };

                            serviceType.Schedules = new List<ServiceTypeScheduleData>();

                            foreach (var technician in technicianListFiltered)
                            {
                                //search schedues
                                var request = new SearchAvailabilityRequest
                                {
                                    LocationId = locationId,
                                    Date = date                                    
                                };

                                request.Lines = new Line[1];
                                request.Lines[0] = new Line
                                {
                                    RequestTypeId = 601,
                                    ServiceId = type.Id,
                                    TechnicianId = technician.Id
                                };

                                var schedules = await SearchAvailability(request, locationId);


                                if (schedules != null && schedules.Count() > 0)
                                {
                                    foreach (var schedule in schedules)
                                    {
                                        foreach (var result in schedule.Results)
                                        {
                                            var startTime = schedule.AvailableDate.AddMinutes(result.StartTime);

                                            var hour = startTime.Hour.ToString().Length == 1 ? "0" + startTime.Hour : startTime.Hour.ToString();
                                            var minute = startTime.Minute.ToString().Length == 1 ? "0" + startTime.Minute : startTime.Minute.ToString();
                                            var currentSchedule = (startTime.Hour > 12 ? "0" + (startTime.Hour - 12).ToString() + ": " + minute : hour + ": " + minute) + (startTime.Hour >= 12 ? " PM" : " AM");
                                            CultureInfo ci = new CultureInfo("en-US");

                                            if (serviceType.Schedules.Any(x => x.Name == type.Name && x.Schedule == currentSchedule))
                                                continue;

                                            serviceType.Schedules.Add(
                                                            new ServiceTypeScheduleData
                                                            {
                                                                Id = count,
                                                                Name = type.Name,
                                                                TechnicianId = technician.Id,
                                                                TechnicianName = technician.FullNameDisplayed,
                                                                Date = startTime.ToString("MMMM dd, yyyy", ci),
                                                                Day = startTime.Day,
                                                                Duration = result.Duration,
                                                                DefaultPrice = result.Price,
                                                                Price = result.Price,
                                                                Schedule = currentSchedule,
                                                                FullDate = startTime
                                                            }
                                            );

                                            count++;

                                        }

                                        serviceType.Schedules = serviceType.Schedules.OrderBy(x => x.FullDate).ToList();
                                    }                                    
                                }
                            }

                            data.ServiceGroupData[i].ServiceTypes.Add(serviceType);
                        }

                    }              
                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getServiceCategories")]
        public async Task<ServiceGroupDataList> GetServiceCategories(int locationId)
        {
            try
            {
                if (locationId == 0)
                    locationId = 6156344;
                var technicians = new List<Technician>();

                var data = new ServiceGroupDataList();
                data.ServiceGroupData = new List<ServiceGroupData>();

                var services = await GetServiceGroups(locationId);

                for (var i = 0; i < services.Count; i++)
                {
                    data.ServiceGroupData.Add(
                         new ServiceGroupData
                         {
                             GroupId = services[i].Id,
                             Name = services[i].Name,
                             ImageUrl = services[i].Images.Length > 0 ? services[i].Images.FirstOrDefault().Path.ToString() : ""
                         });

                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getCategorySchedules")]
        public async Task<ServiceGroupDataList> GetCategorySchedules(int locationId, string date, long id, int serviceId, int timePeriod = 0)
        {
            try
            {
                if (locationId == 0)
                    locationId = 6156344;
                var technicians = new List<Technician>();

                var data = new ServiceGroupDataList();
                data.ServiceGroupData = new List<ServiceGroupData>();

                var services = await GetServiceGroups(locationId);
                var count = 0;

                for (var i = 0; i < services.Count; i++)
                {
                    if (services[i].Id == serviceId)
                    {
                        data.ServiceGroupData.Add(
                             new ServiceGroupData
                             {
                                 GroupId = services[i].Id,
                                 Name = services[i].Name,
                                 ImageUrl = services[i].Images.Length > 0 ? services[i].Images.FirstOrDefault().Path.ToString() : ""
                             });

                        data.ServiceGroupData[0].ServiceTypes = new List<ServiceTypeData>();

                        var types = await GetServiceTypes(locationId, services[i].Id);

                        if (types == null)
                            continue;
                        int counter = 0;
                        foreach (var type in types)
                        {
                            counter++;
                            if (counter > 5)
                                break;

                            //var technicianList = await GetTechnicianByServiceDate(locationId, type.Id, date);

                            //var technicianListFiltered = id == 0 ? technicianList.ToList()
                            //                    : id == 1 ? technicianList.Where(x => x.Gender == "F").ToList()
                            //                    : id == 2 ? technicianList.Where(x => x.Gender == "M").ToList()
                            //                    : technicianList.Where(x => x.Id == id).ToList();

                            //if (technicianListFiltered.Count() > 0)
                            //{
                                var serviceType = new ServiceTypeData
                                {
                                    ServiceTypeId = type.Id,
                                    Description = type.Name,
                                    Price = type.Price
                                };

                                serviceType.Schedules = new List<ServiceTypeScheduleData>();

                                //foreach (var technician in technicianListFiltered)
                                //{
                                    //search schedues
                                    var request = new SearchAvailabilityRequest
                                    {
                                        LocationId = locationId,
                                        Date = date
                                    };

                                    request.Lines = new Line[1];
                                    var temp = new Line();
                                    temp.TechnicianId = 0;
                                    temp.RequestTypeId = 601;
                                    temp.ServiceId = Convert.ToInt64(type.Id);

                                    request.Lines[0] = temp;
                                    switch (timePeriod)
                                    {
                                        case 1: // morning 5am-12pm
                                            request.StartTime = 300;
                                            request.EndTime = 720;
                                            break;
                                        case 2: // afternoon 12pm-5pm
                                            request.StartTime = 720;
                                            request.EndTime = 1020;
                                            break;
                                        case 3: // evening 5pm-9pm
                                            request.StartTime = 1020;
                                            request.EndTime = 1260;
                                            break;
                                    }
                                    try
                                    {
                                        var schedules = await SearchAvailability(request, locationId);


                                        if (schedules != null && schedules.Count() > 0)
                                        {
                                            foreach (var schedule in schedules)
                                            {
                                                foreach (var result in schedule.Results)
                                                {
                                                    var startTime = schedule.AvailableDate.AddMinutes(result.StartTime);

                                                    var hour = startTime.Hour.ToString().Length == 1 ? "0" + startTime.Hour : startTime.Hour.ToString();
                                                    var minute = startTime.Minute.ToString().Length == 1 ? "0" + startTime.Minute : startTime.Minute.ToString();
                                                    var currentSchedule = (startTime.Hour > 12 ? "0" + (startTime.Hour - 12).ToString() + ":" + minute : hour + ":" + minute) + (startTime.Hour >= 12 ? " PM" : " AM");
                                                    CultureInfo ci = new CultureInfo("en-US");

                                                    if (serviceType.Schedules.Any(x => x.Name == type.Name && x.Schedule == currentSchedule))
                                                        continue;

                                                    serviceType.Schedules.Add(
                                                                    //new ServiceTypeScheduleData
                                                                    //{
                                                                    //    Id = count,
                                                                    //    Name = type.Name,
                                                                    //    TechnicianId = technician.Id,
                                                                    //    TechnicianName = technician.FullNameDisplayed,
                                                                    //    Date = startTime.ToString("MMMM dd, yyyy", ci),
                                                                    //    Day = startTime.Day,
                                                                    //    Duration = result.Duration,
                                                                    //    StartTime = result.StartTime.ToString(),
                                                                    //    ServiceId = result.ServiceId,
                                                                    //    FacilityId = 6177222,
                                                                    //    DefaultPrice = result.Price,
                                                                    //    RequestTypeId = 601,
                                                                    //    Price = result.Price,
                                                                    //    Schedule = currentSchedule,
                                                                    //    FullDate = startTime
                                                                    //}
                                                                    new ServiceTypeScheduleData
                                                                    {
                                                                        Id = count,
                                                                        Name = type.Name,
                                                                        TechnicianId = result.TechnicianId,
                                                                        TechnicianName = result.TechnicianId.ToString(), //technician.FullNameDisplayed,
                                                                        Date = startTime.ToString("MMMM dd, yyyy", ci),
                                                                        Day = startTime.Day,
                                                                        Duration = result.Duration,
                                                                        DefaultPrice = result.Price,
                                                                        Price = result.Price,
                                                                        Schedule = currentSchedule,
                                                                        FullDate = startTime,
                                                                        StartTime = result.StartTime.ToString(),
                                                                        ServiceId = result.ServiceId,
                                                                        FacilityId = 6177222,
                                                                        RequestTypeId = 601
                                                                    }

                                                    );

                                                    count++;

                                                }

                                                serviceType.Schedules = serviceType.Schedules.OrderBy(x => x.FullDate).ToList();
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                //    break;
                                //}
                                if (serviceType.Schedules.Count > 0)
                                    data.ServiceGroupData[0].ServiceTypes.Add(serviceType);
                            //}

                        }
                    }
                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getFullSpaList")]
        public async Task<ServiceGroupDataList> GetFullSpaList(int locationId, string start, string end)
        {
            try
            {
                locationId = 6156344;
                var data = new ServiceGroupDataList();
                data.ServiceGroupData = new List<ServiceGroupData>();

                var serviceTypes = new List<ServiceType>();
                var activityServices = new List<ServiceActivity>();

                var services = await GetServiceGroups(locationId);
                var activities = await GetActivityList(locationId, start, end);
                foreach (var service in services)
                {
                    var response = await GetServiceTypes(locationId, service.Id);
                    if (response != null)
                        serviceTypes.AddRange(response);
                }

                var firstService = services.FirstOrDefault();

                if (firstService != null)
                {
                    data.ServiceGroupData.Add(
                                new ServiceGroupData
                                {
                                    GroupId = firstService.Id,
                                    Name = "TREATMENTS",
                                    ImageUrl = firstService.Images.Length > 0 ? firstService.Images.FirstOrDefault().Path.ToString() : "",
                                    CategoryType = 1
                                });

                    data.ServiceGroupData[0].ServiceTypes = new List<ServiceTypeData>();

                    foreach (var type in serviceTypes)
                    {

                        data.ServiceGroupData[0].ServiceTypes.Add(
                                new ServiceTypeData
                                {
                                    ServiceTypeId = type.Id,
                                    TypeName = type.Name,
                                    Description = type.Description,
                                    ExtendedDescription = type.ExtendedDescription,
                                    Price = type.Price,
                                    Duration = type.Duration
                                });
                    }
                }

                foreach (var activity in activities)
                {
                    if (!data.ServiceGroupData.Any(x => x.GroupId == activity.ServiceGroupId))
                    {
                        data.ServiceGroupData.Add(
                                        new ServiceGroupData
                                        {
                                            GroupId = activity.ServiceGroupId,
                                            Name = activity.ServiceGroupName,
                                            ImageUrl = activity.ActivityImagePath,
                                            CategoryType = 2
                                        });
                    }
                }

                foreach (var service in data.ServiceGroupData)
                {
                    if (service.Name == "TREATMENTS")
                        continue;

                    service.ServiceTypes = new List<ServiceTypeData>();

                    var groupActivities = activities.Where(x => x.ServiceGroupId == service.GroupId);

                    foreach (var activity in groupActivities)
                    {
                        if (!service.ServiceTypes.Any(x => x.ServiceTypeId == activity.ServiceTypeId && x.TypeName == activity.ServiceName && x.Price == activity.Price))
                        {
                            service.ServiceTypes.Add(new ServiceTypeData
                            {
                                ServiceTypeId = activity.ServiceTypeId,
                                TypeName = activity.ServiceName,
                                Description = activity.Description,
                                ExtendedDescription = activity.ExtendedDescription,
                                Price = activity.Price,
                                Duration = activity.Duration
                            });
                        }
                    }
                }

                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("searchAvailability/{locationId}")]
        public async Task<List<SearchAvailabilityResponse>> SearchAvailability(SearchAvailabilityRequest req, int locationId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                    HttpResponseMessage tokenResponse = await client.PostAsync($"Book4TimeAPI2/api/Appointment/SearchAvailability?match_result=true", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<List<SearchAvailabilityResponse>>(jsonContent);

                    return data;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpGet]
        [Route("getStaff")]
        public async Task<List<Object>> GetStaff(int locationId, string date, int groupId = 1)
        {
            try
            {
                locationId = 6156344;
                var technicians = new List<Technician>();


                if (groupId == 1)
                {
                    var services = await GetServiceGroups(locationId);

                    foreach (var service in services)
                    {
                        var response = await GetTechnicianByServiceDate(locationId, service.Id, date);
                        if (response != null)
                        {
                            foreach (var technician in response)
                            {
                                if (!technicians.Any(x => x.TechnicianId == technician.Id))
                                    technicians.Add(technician);
                            }
                        }

                    }
                }

                else
                {
                    var activities = await GetActivityListByGroup(locationId, groupId, date, date);

                }




                //foreach (var technician in technicians)
                //{
                //    if (!technicianList.Any(x => x.Id == technician.Id))
                //        technicianList.Add(new TechnicianList { Id = technician.Id, Name = technician.FullNameDisplayed });
                //}

                //return technicianList;

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("addAppointment")]
        public async Task<JObject> AddAppointment(AppointmentRequest req)
        {
            try
            {
                req.CreditCard = new CreditCardInfo();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.na.book4time.com");
                    client.DefaultRequestHeaders.Add("ApiToken", "D989495CCA2C4C34B927982E15323D8E");
                    client.DefaultRequestHeaders.Add("AccountToken", "CA0FD144F77D4B4DB4044A2715520036");
                    client.DefaultRequestHeaders.Add("UserToken", "BHtgPSTNFSHQmvbrz0X6M11KLJZA3O1ONhzy3quYafMhPndoC1RqzwIo5Yql0hTQ");


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");

                    HttpResponseMessage tokenResponse = await client.PostAsync($"Book4TimeAPI2/api/ExpressAppointment/SaveAppointment?match_result=false", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    //var data = JsonConvert.DeserializeObject<List<SearchAvailabilityResponse>>(jsonContent);

                    return JObject.Parse(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        private ShopifySharp.ShopifyCustomer AddUpdateShopifyCustomer(string email, string firstName, string lastName, long b4tId)
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            List<ShopifySharp.ShopifyCustomer> customers = service.SearchAsync("email:" + email).Result.ToList();
            ShopifySharp.ShopifyCustomer customer;
            if (customers != null && customers.Count > 0)
            {
                // Update
                //customer = ShopifyCustomer(Convert.ToInt64(customers[0].Id));
                customer = customers[0];
                try
                {
                    if (customer.Metafields != null)
                    {
                        var mf = customer.Metafields.Where(x => x.Key == "BOOK4TIMEID").FirstOrDefault();
                        mf.Value = b4tId;
                    }
                    else
                    {
                        ShopifySharp.ShopifyMetaField newMf = new ShopifySharp.ShopifyMetaField();
                        newMf.Key = "BOOK4TIMEID";
                        newMf.ValueType = "integer";
                        newMf.Namespace = "my_fields";
                        newMf.Value = b4tId;
                        List<ShopifySharp.ShopifyMetaField> metafields = new List<ShopifySharp.ShopifyMetaField>();
                        metafields.Add(newMf);
                        customer.Metafields = metafields;

                    }
                    var result = service.UpdateAsync(customer).Result;
                    return result;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                customer = new ShopifySharp.ShopifyCustomer();
                customer.Email = email;
                customer.FirstName = firstName;
                customer.LastName = lastName;
                ShopifySharp.ShopifyMetaField newMf = new ShopifySharp.ShopifyMetaField();
                newMf.Key = "BOOK4TIMEID";
                newMf.ValueType = "integer";
                newMf.Namespace = "my_fields";
                newMf.Value = b4tId;
                List<ShopifySharp.ShopifyMetaField> metafields = new List<ShopifySharp.ShopifyMetaField>();
                metafields.Add(newMf);
                customer.Metafields = metafields;
                var result = service.CreateAsync(customer).Result;
                return result;
            }
        }

    }
}
