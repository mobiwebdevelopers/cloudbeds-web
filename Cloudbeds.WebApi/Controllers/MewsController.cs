﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using Cloudbeds.Data;
using CloudBaseWeb.Api.Entities;
using static Cloudbeds.WebApi.Controllers.Shared.SMSController;
using System.Net.Mail;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/mews")]
    [ApiController]
    [EnableCors("All")]
    [AllowAnonymous]
    public class MewsController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly Uri _apiBaseUrl;
        private readonly MobileKeyDbContext _dbContext;
        private string _code = "CASCADA-P";
        //private string _shopifyToken = "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"; 
        //private string _shopifyUrl = "https://trueomni-dev.myshopify.com";
        private string _shopifyToken = "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";
        private string _shopifyUrl = "https://becascada.myshopify.com";
        static MewsCredentials _credentials;
        public MewsController(IConfiguration configuration, MobileKeyDbContext dbContext)
        {
            _configuration = configuration;
            _apiBaseUrl = new Uri(configuration["MEWS:BaseUrl"]);
            _dbContext = dbContext;
        }

        private MewsCredentials getAccess(string code) {
            if (_credentials == null || _credentials.Code != code)
            {
                var credentials = _dbContext.Set<MewsCredentials>()
                    .Where(c => c.Code == code).FirstOrDefault();
                _code = code;
                _credentials = credentials;
                return credentials;
            }
            else
            {
                return _credentials;
            }
        }
        private List<Mews_DailyRate> getDailyRates(DateTime start, DateTime end)
        {
            try
            {
                var rates = _dbContext.Set<Mews_DailyRate>()
                    .Where(c => c.Date >= start.Date && c.Date <= end.Date).OrderBy(x => x.Date).ToList();

                return rates;

            }
            catch(Exception e)
            {
                return null;
            }
        }


        [HttpPost]
        [Route("getClientConfiguration/{code}")]
        public async Task<JObject> GetClientConfiguration(string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsCategories();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/configuration/get", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getServices/{code}")]
        public async Task<JObject> GetServices(string code)
        {
            try
            {
                var cred = getAccess(code);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsToken();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/services/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        [HttpPost]
        [Route("getResources/{code}")]
        public async Task<ResourceResponse> GetResources(MewsResources req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    req.ResourceFeatures = true;
                    req.ResourceFeatureAssignments = true;

                    var data = new MewsCategories();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Extent = req;
                    data.ResourceIds = req.ResourceIds;
                    data.Limitation = new MewsLimitation { Count = 100 };
                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resources/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResourceResponse>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getAllResources/{code}")]
        public async Task<List<RoomDTO>> GetAllResources(MewsResources req, string code)
        {
            try
            {
                string promocode = req.PromoCode;
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsCategories();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Extent = req;
                    data.Limitation = new MewsLimitation { Count = 100 };
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resources/getAll", content);
                    if (tokenResponse.StatusCode == HttpStatusCode.TooManyRequests)
                    {
                        return null;
                    }
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonContent))
                    {
                        dynamic arreglo = JObject.Parse(jsonContent);
                        var resource = JsonConvert.DeserializeObject<ResourceResponse>(jsonContent);


                        //var resourceCategories = resource.ResourceCategories;
                        //                    var categoryIds = resourceCategories.Select(x => x.Id );
                        //                    var serviceIds = resourceCategories.Select(x => x.ServiceId.ToString() ).ToList();

                        //var resourceIds = resourceCategoryAssignments.Select(x =>  x.ResourceId );


                        //var resources = resource.Resources.Where(x => resourceIds.Contains(x.Id)); 
                        var tempresources = resource.Resources.Where(x => x.State != "Dirty").ToList();
                        var resources = tempresources.Select(x => x.Id).ToList();
                        var resourceCategoryAssignments = resource.ResourceCategoryAssignments.Where(x => resources.Contains(x.ResourceId)).Distinct().ToList();
                        var categoryIds = resourceCategoryAssignments.Select(x => x.CategoryId).Distinct().ToList();
                        var resourceCategoryImages = resource.ResourceCategoryImageAssignments.Where(x => categoryIds.Contains(x.CategoryId)).ToList();
                        var resourceCategories = resource.ResourceCategories.Where(x => categoryIds.Contains(x.Id)).ToList().OrderBy(a=>a.Ordering);
                        var serviceIds = resourceCategories.Select(x => x.ServiceId.ToString()).Distinct().ToList();

                        List<RoomDTO> room = new List<RoomDTO>();
                        foreach (var r in resourceCategories)
                        {
                            var imageIds = resourceCategoryImages.Where(x => x.CategoryId == r.Id).Select(x => x.ImageId.ToString()).ToList();

                            //var imageIds = resourceCategoryImageAssignments.Select(x => x.ImageId.ToString()).ToList();

                            //var temp = resource.ResourceCategoryAssignments.Where(x => x.CategoryId == r.Id).ToList();
                            if ((imageIds != null && imageIds.Count > 0) || (r.Names.EnUs.ToLower().Contains("accessible")))
                            {
                                room.Add(new RoomDTO
                                {
                                    Name = r.Names.EnUs,
                                    Description = r.Descriptions.EnUs != null ? r.Descriptions.EnUs : "",
                                    Images = imageIds,
                                    CategoryId = r.Id.ToString(),
                                    ServiceId = r.ServiceId.ToString(),
                                    //ResourceId = r.Id.ToString(),
                                    Capacity = r.Capacity + r.ExtraCapacity,
                                    RoomData = r.Ordering > 0 ? r.Ordering.ToString() : ""


                                });
                                //var imageId = AssignImageId(r, resourceCategoryAssignments, resourceCategoryImageAssignments);
                                //var categoryId = resourceCategoryAssignments.FirstOrDefault(x => x.ResourceId == r.Id).CategoryId;
                                //var resourceCategory = resourceCategories.FirstOrDefault(x => x.Id == categoryId);
                                //if (categoryId != null)
                                //{
                                //bool exist = false;
                                //for (int i = 0; i < room.Count; i++)
                                //{
                                //    if (room[i].CategoryId == categoryId.ToString())
                                //    {
                                //        exist = true;
                                //        break;
                                //    }
                                //}
                                //if (!exist)
                                //{
                                //    room.Add(new RoomDTO
                                //    {
                                //        Name = resourceCategory.Names.EnUs,
                                //        Description = resourceCategory.Descriptions.EnUs,
                                //        ImageId = imageId,
                                //        CategoryId = categoryId.ToString(),
                                //        ServiceId = resourceCategory.ServiceId.ToString(),
                                //        ResourceId = r.Id.ToString(),
                                //        Capacity = resourceCategory.Capacity + resourceCategory.ExtraCapacity,
                                //        RoomData = resourceCategory.Ordering > 0 ? resourceCategory.Ordering.ToString() : ""


                                //    }
                                //    );
                                //}
                                //}
                                if (imageIds.Count > 0)
                                    await PrepareRoomImagesData(room[room.Count - 1]);
                            }
                        }
                        await PrepareRatesData(room, serviceIds, promocode);
                        await PreparePricingData(room, req.StartUtc, req.EndUtc);
                        return room;

                    }
                }
                return null;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        [HttpPost]
        [Route("getRooms/{code}")]
        public async Task<List<RoomDTO>> GetRooms(MewsResources req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsCategories();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Extent = req;
                    data.Limitation = new MewsLimitation { Count = 50 };

                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resources/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);
                    var resource = JsonConvert.DeserializeObject<ResourceResponse>(jsonContent);


                    //var resourceCategories = resource.ResourceCategories.Where(x => x.Type == "Room" && (x.Names.EnUs != null && x.Names.EnUs.Contains("HN-"))).ToList();
                    var resourceCategories = resource.ResourceCategories;
                    var categoryIds = resourceCategories.Select(x => x.Id);
                    var serviceIds = resourceCategories.Select(x => x.ServiceId.ToString()).ToList();

                    var resourceCategoryAssignments = resource.ResourceCategoryAssignments.Where(x => categoryIds.Contains(x.CategoryId)).ToList();
                    var resourceIds = resourceCategoryAssignments.Select(x => x.ResourceId);

                    var resourceCategoryImageAssignments = resource.ResourceCategoryImageAssignments.Where(x => categoryIds.Contains(x.CategoryId)).ToList();

                    var imageIds = resourceCategoryImageAssignments.Select(x => x.ImageId.ToString()).ToList();

                    //var resources = resource.Resources.Where(x => resourceIds.Contains(x.Id) && (x.Name != null && x.Name.Contains("HN-")));
                    var resources = resource.Resources.Where(x => resourceIds.Contains(x.Id));

                    List<RoomDTO> room = new List<RoomDTO>();

                    foreach (var r in resources)
                    {
                        var imageId = AssignImageId(r, resourceCategoryAssignments, resourceCategoryImageAssignments);
                        var categoryId = resourceCategoryAssignments.FirstOrDefault(x => x.ResourceId == r.Id).CategoryId;
                        var resourceCategory = resourceCategories.FirstOrDefault(x => x.Id == categoryId);
                        if (categoryId != null)
                        {
                            bool exist = false;
                            for (int i = 0; i < room.Count; i++)
                            {
                                if (room[i].CategoryId == categoryId.ToString())
                                {
                                    exist = true;
                                    break;
                                }
                            }
                            if (!exist)
                            {
                                room.Add(new RoomDTO
                                {
                                    Name = resourceCategory.Names.EnUs,
                                    Description = resourceCategory.Descriptions.EnUs,
                                    ImageId = imageId,
                                    CategoryId = categoryId.ToString(),
                                    ServiceId = resourceCategory.ServiceId.ToString(),
                                    ResourceId = r.Id.ToString(),
                                    Capacity = resourceCategory.Capacity + resourceCategory.ExtraCapacity,
                                    RoomData = resourceCategory.Ordering > 0 ? resourceCategory.Ordering.ToString() : ""


                                }
                                );
                            }
                        }
                    }
                    //await PrepareImagesData(room, imageIds);
                    //await PrepareRatesData(room, serviceIds);
                    //await PreparePricingData(room, req.StartUtc, req.EndUtc);

                    return room;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getMonthPrices/{code}")]
        public async Task<List<MewsPriceList>> GetMonthPrices(MewsResources req, string code)
        {
            List<Mews_DailyRate> rates = getDailyRates(req.StartUtc, req.EndUtc);
            var priceListData = new List<MewsPriceList>();
            for (var i = 0; i < req.EndUtc.Day; i++)
            {
                priceListData.Add(new MewsPriceList());
            }
            if (rates != null)
            {
                for (var i = 0; i < rates.Count; i++)
                {
                    priceListData[i].Id = i;
                    priceListData[i].Day = req.StartUtc.AddDays(i).ToString();
                    priceListData[i].Price = Convert.ToInt64(rates[i].Rate);
                }
            }
            return priceListData;
            //try
            //{
            //    var cred = getAccess(code);
            //    using (var client = new HttpClient())
            //    {
            //        client.BaseAddress = _apiBaseUrl;

            //        var data = new MewsCategories();
            //        data.ClientToken = cred.ClientToken;
            //        data.AccessToken = cred.AccessToken;
            //        data.Client = cred.Client;
            //        data.Extent = req;

            //        var objAsJson = JsonConvert.SerializeObject(data);
            //        var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
            //        HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resources/getAll", content);

            //        var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
            //        dynamic arreglo = JObject.Parse(jsonContent);
            //        var resource = JsonConvert.DeserializeObject<ResourceResponse>(jsonContent);


            //        var resourceCategories = resource.ResourceCategories.Where(x => x.Type == "Room" && (x.Names.EnUs != null && x.Names.EnUs.Contains("HN-"))).ToList();
            //        //var resourceCategories = resource.ResourceCategories.Where(x => x.Type == "Room").ToList();
            //        var categoryIds = resourceCategories.Select(x => x.Id);
            //        var serviceIds = resourceCategories.Select(x => x.ServiceId.ToString()).ToList();

            //        var ratesList = await PrepareMonthlyRatesData(serviceIds);
            //        var priceList = await PrepareMonthlyPricingData(ratesList, req.StartUtc, req.EndUtc);

            //        var priceListData = new List<MewsPriceList>();
            //        for (var i = 0; i < req.EndUtc.Day; i++)
            //        {
            //            priceListData.Add(new MewsPriceList());
            //        }


            //        foreach (var e in priceList)
            //        {
            //            if (e != null)
            //            {
            //                if (e.BasePrices != null)
            //                {
            //                    for (var i = 0; i < req.EndUtc.Day; i++)
            //                    {
            //                        var current = priceListData[i];

            //                        if (current.Price == 0 || (current.Price > e.BasePrices[i]) && (e.BasePrices[i] != 0))
            //                        {
            //                            priceListData[i].Id = i;
            //                            priceListData[i].Day = e.DatesUtc[i].ToString();
            //                            priceListData[i].Price = e.BasePrices[i];
            //                        }
            //                    }
            //                }
            //            }

            //        }

            //        return priceListData;
            //    }

            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}

        }

        private async Task PrepareImagesData(List<RoomDTO> room, List<string> imageIds)
        {
            Image[] images = new Image[imageIds.Count];

            for (var i = 0; i < imageIds.Count; i++)
            {
                images[i] = new Image();
                images[i].ImageId = imageIds[i];
                images[i].Width = 346;
                images[i].Height = 196;
                images[i].ResizeMode = "CoverExact";
            }

            var imagesResponse = await GetImages(images, _code);

            //update room
            if (imagesResponse != null)
            {
                for (var i = 0; i < room.Count; i++)
                {
                    var imageId = room[i].ImageId;
                    var img = imagesResponse.ImageUrls.FirstOrDefault(x => x.ImageId == imageId.ToString());
                    room[i].ImageUrl = img != null ? img.Url : "";
                }
            }

        }
        private async Task PrepareRoomImagesData(RoomDTO room)
        {
            Image[] images = new Image[room.Images.Count];

            for (var i = 0; i < room.Images.Count; i++)
            {
                images[i] = new Image();
                images[i].ImageId = room.Images[i];
                images[i].Width = 346;
                images[i].Height = 196;
                images[i].ResizeMode = "CoverExact";
            }

            var imagesResponse = await GetImages(images, _code);

            ////update room
            if (imagesResponse != null && imagesResponse.ImageUrls != null)
            {
                for (int i = 0; i < imagesResponse.ImageUrls.Count; i++)
                {
                    room.Images[i] = imagesResponse.ImageUrls[i].Url;
                }
                room.ImageUrl = imagesResponse.ImageUrls[0].Url;
            }

        }

        private async Task PrepareRatesData(List<RoomDTO> room, List<string> serviceIds, string promocode = "")
        {
            MewsRates rq = new MewsRates();

            string[] data = serviceIds.ToArray();
            rq.ServiceIds = data;
            if (promocode == null)
                promocode = "";

            var ratesResponse = await GetRates(rq, _code, promocode);

            var rateGroups = new List<RateGroup>();
            if (promocode.Length > 0)
            {
                rateGroups = ratesResponse.RateGroups.Where(x => x.IsActive && x.Name.ToString().ToLower() == promocode.ToLower()).ToList();
                //if (rateGroups.Count == 0)
                //{
                //    promocode = "";
                //    rateGroups = ratesResponse.RateGroups.Where(a => a.IsActive && a.Name == "Default" && string.IsNullOrEmpty(a.ExternalIdentifier)).ToList();
                //}
            }
            else
            {
                rateGroups = ratesResponse.RateGroups.Where(a => a.IsActive && a.Name=="Default" && string.IsNullOrEmpty(a.ExternalIdentifier)).ToList();
            }
            if (ratesResponse != null)
            {
                //if (rateGroups.Count == 0) {
                //    rateGroups = ratesResponse.RateGroups.Where(a => a.IsActive && a.Name == "Default").ToList();
                //}
                //var groupId = rateGroups[0].Id;
                for (var i = 0; i < room.Count; i++)
                {
                    try
                    {
                        var serviceId = room[i].ServiceId;
                        if (promocode.Length > 0 && rateGroups != null && rateGroups.Count > 0)
                        {
                            var rate = ratesResponse.Rates.FirstOrDefault(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic && (x.ExternalNames != null && x.ExternalNames.EnUs != null && x.ExternalNames.EnUs == promocode));
                            //if (rate == null)
                            //    rate = ratesResponse.Rates.FirstOrDefault(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic);
                            var rates = ratesResponse.Rates.Where(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic && (x.ExternalNames != null && x.ExternalNames.EnUs != null && x.ExternalNames.EnUs == promocode)).ToList();
                            room[i].RateId = rate != null ? rate.Id.ToString() : "";
                            room[i].RateGroups = rateGroups;
                            room[i].Rates = new List<Rate>();
                            rates.ForEach(r =>
                            {
                                Rate newRate = new Rate();
                                newRate.BaseRateId = r.BaseRateId;
                                newRate.Description = r.Description;
                                newRate.ExternalNames = r.ExternalNames;
                                newRate.GroupId = r.GroupId;
                                newRate.Id = r.Id;
                                newRate.IsActive = r.IsActive;
                                newRate.IsEnabled = r.IsEnabled;
                                newRate.IsPublic = r.IsPublic;
                                newRate.Name = r.Name;
                                newRate.Price = r.Price;
                                newRate.RateId = r.RateId;
                                newRate.ServiceId = r.ServiceId;
                                newRate.ShortName = r.ShortName;

                                room[i].Rates.Add(newRate);
                            });
                        }
                        else
                        {
                            //var rate = ratesResponse.Rates.FirstOrDefault(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic);
                            //var rates = ratesResponse.Rates.Where(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic).ToList();
                            var rate = ratesResponse.Rates.FirstOrDefault(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic && (x.ExternalNames == null || x.ExternalNames.EnUs == null));
                            var rates = ratesResponse.Rates.Where(x => x.ServiceId.ToString() == serviceId && x.IsActive && x.IsEnabled && x.IsPublic && (x.ExternalNames == null || x.ExternalNames.EnUs == null)).ToList();
                            room[i].RateId = rate != null ? rate.Id.ToString() : "";
                            room[i].RateGroups = rateGroups;
                            room[i].Rates = new List<Rate>();
                            rates.ForEach(r =>
                            {
                                Rate newRate = new Rate();
                                newRate.BaseRateId = r.BaseRateId;
                                newRate.Description = r.Description;
                                newRate.ExternalNames = r.ExternalNames;
                                newRate.GroupId = r.GroupId;
                                newRate.Id = r.Id;
                                newRate.IsActive = r.IsActive;
                                newRate.IsEnabled = r.IsEnabled;
                                newRate.IsPublic = r.IsPublic;
                                newRate.Name = r.Name;
                                newRate.Price = r.Price;
                                newRate.RateId = r.RateId;
                                newRate.ServiceId = r.ServiceId;
                                newRate.ShortName = r.ShortName;
                                
                                room[i].Rates.Add(newRate);
                            });
                            
                        }
                    }
                    catch (Exception e)
                    {

                    }
                    //if (i<2)
                    //    room[i].RateId = rates[i]; //rate != null ? rate.Id.ToString() : "";  f66d51b2-02f9-46f3-a196-06fb49a8104e
                    //else
                    //    room[i].RateId = rates[1];

                }
            }
        }
        private async Task<bool> ValidatePromoCode(List<string> serviceIds, string promocode = "")
        {
            MewsRates rq = new MewsRates();
            bool codeOK = false;
            string[] data = serviceIds.ToArray();
            rq.ServiceIds = data;

            var ratesResponse = await GetRates(rq, _code, promocode);

            var rateGroups = new List<RateGroup>();
            if (promocode.Length > 0)
            {
                rateGroups = ratesResponse.RateGroups.Where(x => x.ExternalIdentifier != null && x.ExternalIdentifier.ToString().ToLower() == promocode.ToLower()).ToList();
                if (ratesResponse != null && rateGroups != null && rateGroups.Count > 0)
                {
                    var rate = ratesResponse.Rates.FirstOrDefault(x => x.GroupId.ToString() == rateGroups[0].Id.ToString() && x.IsActive && x.IsEnabled);
                    if (rate != null)
                        codeOK = true;
                }
            }
            return codeOK;
        }

        private async Task<List<string>> PrepareMonthlyRatesData(List<string> serviceIds)
        {
            MewsRates rq = new MewsRates();

            string[] data = serviceIds.ToArray();
            rq.ServiceIds = data;

            var ratesResponse = await GetRates(rq, _code);

            if (ratesResponse != null)
            {
                return ratesResponse.Rates.Select(x => x.Id.ToString()).ToList();                
            }

            return null;
        }

        private async Task PreparePricingData(List<RoomDTO> rooms, DateTime startUtc, DateTime endUtc)
        {
            List<RatePrice> prices = new List<RatePrice>();
            RatePrice price;

            for (var ratei = 0; ratei < rooms[0].Rates.Count; ratei++)
            {
                MewsPricing rq = new MewsPricing();
                rq.RateId = rooms[0].Rates[ratei].Id.ToString();
                rq.StartUtc = startUtc;
                rq.EndUtc = endUtc.AddDays(-1);
                price = await GetPricing(rq, _code);

                if (price != null && price.CategoryPrices != null)
                {
                    prices.Add(price);
                }
            }


            for (var i = 0; i < rooms.Count; i++)//rooms.Count; use a static amount of rooms (query lasts too much)
            {
                try
                {
                    for (var ratei = 0; ratei < rooms[i].Rates.Count; ratei++)
                    {
                        var myPrice = prices[ratei].CategoryPrices.Where(x => x.CategoryId.ToString() == rooms[i].CategoryId).ToList();
                        if (myPrice != null)
                        {
                            //rooms[i].Rates[ratei].Prices = myPrice;
                            rooms[i].Rates[ratei].Price = myPrice[0].Prices[0];
                            rooms[i].Rates[ratei].RateId = rooms[i].Rates[ratei].Id;
                        }
                    }
                    rooms[i].Rates = rooms[i].Rates.OrderBy(x=>x.Price).ToList();
                    rooms[i].Price = rooms[i].Rates[0].Price;
                }
                catch (Exception e)
                {

                }
            }
        }

        private async Task<List<RatePrice>> PrepareMonthlyPricingData(List<string> rateIds, DateTime startUtc, DateTime endUtc)
        {
            var priceList = new List<RatePrice>();
            for (var i = 0; i < rateIds.Count; i++)
            {
                MewsPricing rq = new MewsPricing();
                rq.RateId = rateIds[i];
                rq.StartUtc = startUtc;
                rq.EndUtc = endUtc;
                var price = await GetPricing(rq, _code);
                if (price.BasePrices != null)
                    priceList.Add(price);
                if (priceList.Count > 20)
                    break;
            }

            return priceList;
        }

        private Guid? AssignImageId(Resource r, List<ResourceCategoryAssignment> resourceCategoryAssignments, List<ResourceCategoryImageAssignments> resourceCategoryImageAssignments)
        {

            var categoryId = resourceCategoryAssignments.FirstOrDefault(y => y.ResourceId == r.Id)?.CategoryId;
            var imageId = resourceCategoryImageAssignments.FirstOrDefault(y => y.CategoryId == categoryId)?.ImageId;

            return imageId;
        }

        [HttpPost]
        [Route("getAvailability/{code}")]
        public async Task<MewsAvailabilityResponse> GetAvailability(MewsAvailability req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/services/getAvailability", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MewsAvailabilityResponse>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getRates/{code}/{promocode}")]
        public async Task<RateResponse> GetRates(MewsRates req, string code, string promocode="")
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Extent = new MewsExtentRates();
                    req.Extent.Rates = true;
                    req.Extent.RateGroups = true;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 100 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/rates/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return JsonConvert.DeserializeObject<RateResponse>(jsonContent);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getTaxEnvironments/{code}")]
        public async Task<List<TaxEnvironment>> GetTaxEnvironments(string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var objAsJson = JsonConvert.SerializeObject(cred);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/taxenvironments/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    var data = JsonConvert.DeserializeObject<TaxationResponse>(jsonContent);

                    var taxEnvironments = new List<TaxEnvironment>();
                    taxEnvironments.AddRange(data.TaxEnvironments.Where(X => X.CountryCode == "USA" && X.Code.Contains("OR") && (X.ValidityStartUtc == null || X.ValidityStartUtc <= DateTime.Now) && (X.ValidityEndUtc == null || X.ValidityEndUtc >= DateTime.Now)).ToList());

                    return taxEnvironments;

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getTaxations/{code}")]
        public async Task<List<TaxRate>> GetTaxations(string code)
        {

            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;            

                    var objAsJson = JsonConvert.SerializeObject(cred);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    //HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/taxations/getAll", content);
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/taxenvironments/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    var data = JsonConvert.DeserializeObject<TaxationResponse>(jsonContent);

                    string envCode = _configuration["MEWS:TaxEnvironmentCode"];
                    var taxEnvironments = new List<TaxEnvironment>();
                    //taxEnvironments.AddRange(data.TaxEnvironments.Where(X => X.CountryCode == "USA" && X.Code.Contains("OR") && (X.ValidityStartUtc == null || X.ValidityStartUtc <= DateTime.Now) && (X.ValidityEndUtc == null || X.ValidityEndUtc >= DateTime.Now)).ToList());
                    taxEnvironments.AddRange(data.TaxEnvironments.Where(X => X.Code == envCode).ToList());

                    var taxations = new List<Taxation>();
                    taxations.AddRange(data.Taxations.Where(X => taxEnvironments[0].TaxationCodes.Contains(X.Code)).ToList());
                    var taxCodes = taxations.Select(x => x.Code).Distinct().ToList();
                    var taxRates = new List<TaxRate>();
                    taxRates.AddRange(data.TaxRates.Where(X => taxCodes.Contains(X.TaxationCode) && X.Strategy.Discriminator == Discriminator.Relative && X.Strategy.Value.ValueValue > 0 && (X.ValidityInvervalsUtc == null || (X.ValidityInvervalsUtc[0].EndUtc == null || X.ValidityInvervalsUtc[0].EndUtc > DateTime.Now))).Distinct().ToList());
                    //taxRates.AddRange(data.TaxRates.Where(X => taxCodes.Contains(X.TaxationCode) && X.Strategy.Discriminator == Discriminator.Flat && X.Strategy.Value.ValueValue > 0 && (X.ValidityInvervalsUtc == null || (X.ValidityInvervalsUtc[0].EndUtc == null || X.ValidityInvervalsUtc[0].EndUtc > DateTime.Now))).Distinct().ToList());

                    var distinctList = new List<TaxRate>();
                    taxRates.ForEach(r =>
                    {
                        if (!distinctList.Any(x => x.Code == r.Code))
                        {
                            var taxation = taxations.Where(t => t.Code == r.TaxationCode).FirstOrDefault();
                            r.TaxationCode = taxation.Name;
                            distinctList.Add(r);
                        }
                    });
                    return distinctList;

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getPricing/{code}")]
        public async Task<RatePrice> GetPricing(MewsPricing req, string code)
        {
            try
            {
                var cred = getAccess(code);
                var ratePrice = new RatePrice();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.StartUtc = req.StartUtc.Date;
                    req.EndUtc = req.EndUtc.Date;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/rates/getPricing", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonContent))
                    {
                        dynamic arreglo = JObject.Parse(jsonContent);
                        ratePrice = JsonConvert.DeserializeObject<RatePrice>(jsonContent);
                    }
                    return ratePrice;


                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("addCustomer/{code}")]
        public async Task<MewsAddCustomerResponse> AddCustomer(MewsCustomer req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/add", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var objCustomer = JsonConvert.DeserializeObject<MewsAddCustomerResponse>(jsonContent);
                    AddUpdateShopifyCustomer(req.Email, req.FirstName, req.LastName, objCustomer.Id.ToString());
                    return objCustomer;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("updateCustomer/{code}")]
        public async Task<JObject> UpdateCustomer(MewsCustomerUpdate req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/update", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("updateCustomerInfo/{code}")]
        public async Task<JObject> UpdateCustomerInfo(MewsCustomerUpdate req, string code)
        {
            try
            {
                var getCustomerData = new MewsGetCustomers()
                {
                    Emails = new string[] { req.Email },
                };

                var getCustomerResponse = await GetCustomer(getCustomerData, code);

                if (getCustomerResponse.Customers != null && getCustomerResponse.Customers.Count > 0)
                {
                    //Update
                    var updateCustomerData = new MewsCustomerUpdate()
                    {
                        FirstName = req.FirstName != null && req.FirstName.Length > 0 ? req.FirstName : getCustomerResponse.Customers[0].FirstName,
                        LastName = req.LastName != null && req.LastName.Length > 0 ? req.LastName : getCustomerResponse.Customers[0].LastName,
                        Email = req.Email,
                        Phone = req.Phone,
                        CustomerId = getCustomerResponse.Customers[0].Id.ToString()
                    };
                    var result = await UpdateCustomer(updateCustomerData, code);
                    return result;

                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("updateResource/{code}")]
        public async Task<JObject> UpdateResource(MewsResourcesUpdate req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resources/update", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getCustomer/{code}")]
        public async Task<MewsGetCustomerResponse> GetCustomer(MewsGetCustomers req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return JsonConvert.DeserializeObject<MewsGetCustomerResponse>(jsonContent);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getCustomerInfo/{code}")]
        public async Task<Object> GetCustomerInfo(MewsCustomer req, string code)
        {
            try
            {
                var getCustomerData = new MewsGetCustomers()
                {
                    Emails = new string[] { req.Email },
                };

                var getCustomerResponse = await GetCustomer(getCustomerData, code);
                var users = _dbContext.Set<Cascada_User>().Where(x => x.Email == req.Email && x.IsActive == true).ToList();
                if (getCustomerResponse.Customers != null && getCustomerResponse.Customers.Count > 0)
                {
                    //Update
                    getCustomerResponse.Customers[0].Notes = "Existing";
                    if (users != null && users.Count > 0)
                    {
                        if (string.IsNullOrEmpty(req.FirstName))
                        {
                            if (users[0].FirstName == "" || users[0].LastName == "" || users[0].Phone == "" || users[0].MewsId == "")
                            {
                                users[0].FirstName = getCustomerResponse.Customers[0].FirstName;
                                users[0].LastName = getCustomerResponse.Customers[0].LastName;
                                users[0].Phone = getCustomerResponse.Customers[0].Phone;
                                users[0].MewsId = getCustomerResponse.Customers[0].Id.ToString();

                                _dbContext.Update(users[0]);
                                await _dbContext.SaveChangesAsync();
                            }
                        }
                        else if (users[0].FirstName != req.FirstName || users[0].LastName != req.LastName || users[0].Phone != req.Phone || users[0].MewsId != getCustomerResponse.Customers[0].Id.ToString())
                        {
                            users[0].FirstName = req.FirstName;
                            users[0].LastName = req.LastName;
                            users[0].Phone = req.Phone;
                            users[0].MewsId = getCustomerResponse.Customers[0].Id.ToString();

                            _dbContext.Update(users[0]);
                            await _dbContext.SaveChangesAsync();
                        }

                    }
                    else
                    {
                        var newUser = new Cascada_User();
                        newUser.Email = req.Email;
                        newUser.IsActive = true;
                        newUser.FirstName = string.IsNullOrEmpty(req.FirstName) ? "First" : req.FirstName;
                        newUser.LastName = string.IsNullOrEmpty(req.LastName) ? "Last" : req.LastName;
                        newUser.Phone = req.Phone;
                        newUser.MewsId = getCustomerResponse.Customers[0].Id.ToString();

                        await _dbContext.AddAsync(newUser);
                        await _dbContext.SaveChangesAsync();
                    }

                    var updateCustomerData = new MewsCustomerUpdate()
                    {
                        FirstName = req.FirstName != null && req.FirstName.Length > 0 ? req.FirstName : getCustomerResponse.Customers[0].FirstName,
                        LastName = req.LastName != null && req.LastName.Length > 0 ? req.LastName : getCustomerResponse.Customers[0].LastName,
                        Email = req.Email, Phone = req.Phone,
                        CustomerId = getCustomerResponse.Customers[0].Id.ToString()
                    };
                    var result = await UpdateCustomer(updateCustomerData, code);

                    //AddUpdateShopifyCustomer(req.Email, req.FirstName, req.LastName, getCustomerResponse.Customers[0].Id.ToString());
                    return getCustomerResponse.Customers[0];
                    
                }
                else
                {
                    //add
                    var addCustomerData = new MewsCustomer()
                    {
                        FirstName = string.IsNullOrEmpty(req.FirstName) ? "First" : req.FirstName,
                        LastName = string.IsNullOrEmpty(req.LastName) ? "Last" : req.LastName,
                        Email = req.Email, Phone = req.Phone,
                        OverwriteExisting = false
                    };
                    var result = await AddCustomer(addCustomerData, code);
                    if (users != null && users.Count > 0)
                    {
                        if (users[0].FirstName != req.FirstName || users[0].LastName != req.LastName || users[0].Phone != req.Phone || users[0].MewsId != result.Id.ToString())
                        {
                            users[0].FirstName = req.FirstName;
                            users[0].LastName = req.LastName;
                            users[0].Phone = req.Phone;
                            users[0].MewsId = result.Id.ToString();

                            _dbContext.Update(users[0]);
                            await _dbContext.SaveChangesAsync();
                        }

                    }
                    else
                    {
                        var newUser = new Cascada_User();
                        newUser.Email = req.Email;
                        newUser.IsActive = true;
                        newUser.FirstName = req.FirstName;
                        newUser.LastName = req.LastName;
                        newUser.Phone = req.Phone;
                        newUser.MewsId = result.Id.ToString();

                        await _dbContext.AddAsync(newUser);
                        await _dbContext.SaveChangesAsync();
                    }
                    //AddUpdateShopifyCustomer(req.Email, req.FirstName, req.LastName, result.Id.ToString());
                    return result;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        private ShopifySharp.ShopifyCustomer ShopifyCustomer(long id)
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            var customer = service.GetAsync(id).Result;
            ShopifySharp.ShopifyMetaFieldService mfService = new ShopifySharp.ShopifyMetaFieldService(_shopifyUrl, _shopifyToken);
            var metaFields = mfService.ListAsync(id, "Customer").Result.ToList();
            return customer;
        }
        private List<ShopifySharp.ShopifyCustomer> ShopifyCustomers()
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            var customerList = service.ListAsync().Result;
            return customerList.ToList();
        }
        private ShopifySharp.ShopifyCustomer AddUpdateShopifyCustomer(string email, string firstName, string lastName, string mewsId = "", string spaId = "")
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            List<ShopifySharp.ShopifyCustomer> customers = service.SearchAsync("email:" + email).Result.ToList();
            ShopifySharp.ShopifyCustomer customer;
            if (customers != null && customers.Count > 0)
            {
                // Update
                //customer = ShopifyCustomer(Convert.ToInt64(customers[0].Id));
                customer = customers[0];
                bool update = false;
                if (!string.IsNullOrEmpty(firstName))
                {
                    customer.FirstName = firstName;
                    update = true;
                }
                if (!string.IsNullOrEmpty(lastName))
                {
                    customer.LastName = lastName;
                    update = true;
                }

                try
                {

                    if (customer.Metafields != null)
                    {
                        var mf = customer.Metafields.Where(x => x.Key == "MEWSID").FirstOrDefault();
                        mf.Value = mewsId;
                        mf = customer.Metafields.Where(x => x.Key == "SPAID").FirstOrDefault();
                        if (mf != null)
                            mf.Value = spaId;
                        else
                        {
                            mf = customer.Metafields.Where(x => x.Key == "BOOK4TIMEID").FirstOrDefault();
                            if (mf != null)
                                mf.Value = spaId;
                        }
                    }
                    else
                    {
                        //ShopifySharp.ShopifyMetaField newMf = new ShopifySharp.ShopifyMetaField();
                        //newMf.Key = "MEWSID";
                        //newMf.ValueType = "string";
                        //newMf.Namespace = "my_fields";
                        //newMf.Value = mewsId;
                        //List<ShopifySharp.ShopifyMetaField> metafields = new List<ShopifySharp.ShopifyMetaField>();
                        //metafields.Add(newMf);
                        //newMf = new ShopifySharp.ShopifyMetaField();
                        //newMf.Key = "SPAID";
                        //newMf.ValueType = "string";
                        //newMf.Namespace = "my_fields";
                        //newMf.Value = spaId;
                        //metafields.Add(newMf);
                        //customer.Metafields = metafields;

                    }
                    if (update)
                    {
                        var result = service.UpdateAsync(customer).Result;
                        return result;
                    }
                    else
                    {
                        return customer;
                    }
                }
                catch (Exception e)
                {
                    return customer;
                }
            }
            else
            {
                customer = new ShopifySharp.ShopifyCustomer();
                customer.Email = email;
                customer.FirstName = firstName;
                customer.LastName = lastName;
                ShopifySharp.ShopifyMetaField newMf = new ShopifySharp.ShopifyMetaField();
                newMf.Key = "MEWSID";
                newMf.ValueType = "string";
                newMf.Namespace = "my_fields";
                newMf.Value = mewsId;
                List<ShopifySharp.ShopifyMetaField> metafields = new List<ShopifySharp.ShopifyMetaField>();
                metafields.Add(newMf);
                newMf = new ShopifySharp.ShopifyMetaField();
                newMf.Key = "SPAID";
                newMf.ValueType = "string";
                newMf.Namespace = "my_fields";
                newMf.Value = spaId;
                metafields.Add(newMf);
                customer.Metafields = metafields;
                var result = service.CreateAsync(customer).Result;
                return result;
            }
        }
        [HttpPost]
        [Route("getShopifyCustomer")]
        public async Task<ShopifySharp.ShopifyCustomer> GetShopifyCustomer(MewsCustomer req)
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            List<ShopifySharp.ShopifyCustomer> customers = service.SearchAsync("email:" + req.Email).Result.ToList();
            ShopifySharp.ShopifyCustomer customer;
            if (customers != null && customers.Count > 0)
            {
                customer = customers[0];
            }
            else
            {
                customer = null;
            }
            return customer;
        }
        private void UpdateShopifyCustomer(string email, string mewsId = "", string b4tId = "")
        {
            ShopifySharp.ShopifyCustomerService service = new ShopifySharp.ShopifyCustomerService(_shopifyUrl, _shopifyToken);
            List<ShopifySharp.ShopifyCustomer> customers = service.SearchAsync("email:" + email).Result.ToList();
            ShopifySharp.ShopifyCustomer customer;
            if (customers != null && customers.Count > 0)
            {
                customer = customers[0];
                customer = service.GetAsync(Convert.ToInt64(customers[0].Id)).Result;
                try
                {
                    //if (customer.Metafields != null)
                    //{
                    //    var mf = customer.Metafields.Where(x => x.Key == "MEWSID").FirstOrDefault();
                    //    mf.Value = mewsId;
                    //    mf = customer.Metafields.Where(x => x.Key == "BOOK4TIMEID").FirstOrDefault();
                    //    mf.Value = b4tId;
                    //}
                    //else
                    //{
                    //    ShopifySharp.ShopifyMetaField newMf = new ShopifySharp.ShopifyMetaField();
                    //    newMf.Key = "MEWSID";
                    //    newMf.ValueType = "string";
                    //    newMf.Namespace = "my_fields";
                    //    newMf.Value = mewsId;
                    //    List<ShopifySharp.ShopifyMetaField> metafields = new List<ShopifySharp.ShopifyMetaField>();
                    //    metafields.Add(newMf);
                    //    customer.Metafields = metafields;
                        
                    //}
                    //var result = service.UpdateAsync(customer).Result;

                }
                catch (Exception e)
                {

                }
            }

        }
        [HttpPost]
        [Route("addReservation/{code}")]
        public async Task<JObject> AddReservation(MewsReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                //req.Reservations[0].RateId = "f66d51b2-02f9-46f3-a196-06fb49a8104e"; // a39a59fa-3a08-4822-bdd4-ab0b00e3d21f 0870a3fd-1f53-44e1-9342-ae260066e347
                //req.Reservations[0].AdultCount = 1;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    List<Product> Products = new List<Product>();
                    Products = req.Reservations[0].ProductOrders;
                    req.Reservations[0].ProductOrders = null;
                    req.Reservations[0].StartUtc = req.Reservations[0].StartUtc.AddHours(15).ToUniversalTime();
                    req.Reservations[0].EndUtc = req.Reservations[0].EndUtc.AddHours(12).ToUniversalTime();
                    req.Reservations[0].TotalNights = (req.Reservations[0].EndUtc.Date - req.Reservations[0].StartUtc.Date).Days;
                    if (string.IsNullOrEmpty(req.Reservations[0].VoucherCode))
                    {
                        req.Reservations[0].VoucherCode = null;
                    }
                    if (req.Reservations[0].ProductOrders != null && req.Reservations[0].ProductOrders.Count > 0)
                    {
                        try
                        {
                            for(int i=0;i< req.Reservations[0].ProductOrders.Count;i++)
                            {
                                req.Reservations[0].ProductOrders[i].StartUtc = req.Reservations[0].StartUtc;
                                if (req.Reservations[0].ProductOrders[i].ProductId == "578f76a3-2906-4edf-a487-b15e00e70dfe")
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].StartUtc;
                                }
                                else
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].EndUtc;
                                }
                            }
                        }
                        catch (Exception ee)
                        {

                        }
                    }


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/add", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);
                    ReservationAddResult results = new ReservationAddResult();
                    results = JsonConvert.DeserializeObject<ReservationAddResult>(jsonContent);
                    if (Products != null && Products.Count > 0 && results.Reservations != null && results.Reservations.Count > 0)
                    {
                        try
                        {
                            MewsAddProduct newProduct = new MewsAddProduct();
                            newProduct.ReservationId = results.Reservations[0].Reservation.Id;
                            List<Product2> newProducts = new List<Product2>();
                            Products.ForEach(p =>
                            {
                                Product2 newProduct = new Product2();
                                newProduct.StartUtc = req.Reservations[0].StartUtc;
                                newProduct.EndUtc = req.Reservations[0].EndUtc;
                                UnitAmount amount = new UnitAmount();
                                amount.Currency = "USD";
                                amount.GrossValue= p.UnitAmount;
                                newProduct.UnitAmount = amount;
                                newProduct.Count = p.Count;
                                newProduct.ProductId = p.ProductId;
                                newProducts.Add(newProduct);


                            });
                            //for(int i=0;i<Products.Count;i++)
                            //{
                            //    Products[i].StartUtc = Convert.ToDateTime("8/4/2024 7:00:00 AM");
                            //    Products[i].EndUtc = Convert.ToDateTime("8/6/2024 7:00:00 AM");
                            //}
                            newProduct.Products = newProducts.ToArray();
                            await AddReservationProduct(newProduct, _code);
                        }
                        catch (Exception ee)
                        {

                        }
                    }
                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("startReservation/{code}")]
        public async Task<JObject> StartReservation(MewsStartReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/start", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("priceReservation/{code}")]
        public async Task<JObject> PriceReservation(MewsPriceReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                //req.Reservations[0].RateId = "f66d51b2-02f9-46f3-a196-06fb49a8104e"; // a39a59fa-3a08-4822-bdd4-ab0b00e3d21f 0870a3fd-1f53-44e1-9342-ae260066e347
                //req.Reservations[0].AdultCount = 1;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    req.Reservations[0].StartUtc = req.Reservations[0].StartUtc.AddHours(15).ToUniversalTime();
                    req.Reservations[0].EndUtc = req.Reservations[0].EndUtc.AddHours(12).ToUniversalTime();
                    if (string.IsNullOrEmpty(req.Reservations[0].VoucherCode))
                    {
                        req.Reservations[0].VoucherCode = null;
                    }
                    if (req.Reservations[0].ProductOrders != null && req.Reservations[0].ProductOrders.Count > 0)
                    {
                        try
                        {
                            for (int i = 0; i < req.Reservations[0].ProductOrders.Count; i++)
                            {
                                req.Reservations[0].ProductOrders[i].StartUtc = req.Reservations[0].StartUtc;
                                if (req.Reservations[0].ProductOrders[i].ProductId == "578f76a3-2906-4edf-a487-b15e00e70dfe")
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].StartUtc;
                                }
                                else
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].EndUtc;
                                }
                            }
                        }
                        catch (Exception ee)
                        {

                        }
                    }


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/price", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);
                    ReservationAddResult results = new ReservationAddResult();
                    results = JsonConvert.DeserializeObject<ReservationAddResult>(jsonContent);
                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("cancelReservation/{code}")]
        public async Task<JObject> CancelReservation(MewsCancelReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    req.Notes = "Widget Cancel";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/cancel", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("updateReservation/{code}")]
        public async Task<JObject> UpdateReservation(MewsReservationUpdate data, string code)
        {
            try
            {
                var cred = getAccess(code);
                MewsUpdateReservation req = new MewsUpdateReservation();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    req.ReservationUpdates = new List<MewsReservationUpdate>();
                    req.ReservationUpdates[0].ReservationId = data.ReservationId;
                    req.ReservationUpdates[0].StartUtc = data.StartUtc;
                    req.ReservationUpdates[0].EndUtc = data.EndUtc;
                    req.Reason = "Update Reservation from Widget";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/updateInterval", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("addReservationProduct/{code}")]
        public async Task AddReservationProduct(MewsAddProduct req, string code)
        {
            try
            {
                foreach(var a in req.Products)
                {
                    var obj = new MewsAddProductRequest();
                    obj.UnitAmount = new UnitAmount();
                    obj.Count = a.Count;
                    obj.EndUtc = a.StartUtc;
                    obj.ProductId = a.ProductId;
                    obj.ReservationId = req.ReservationId;
                    obj.StartUtc = a.StartUtc;
                    //obj.UnitAmount.GrossValue = a.UnitAmount;
                    //obj.UnitAmount.Currency = "USD";
                    obj.UnitAmount = a.UnitAmount;
                    await AddProduct(obj, code);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("addProduct/{code}")]
        public async Task<ResourceResponse> AddProduct(MewsAddProductRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/addProduct", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResourceResponse>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("addPayment/{code}")]
        public async Task<JObject> AddPayment(MewsPayment req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/payments/addExternal", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("getPayment/{code}")]
        public async Task<JObject> GetPayment(MewsGetPaymentRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/payments/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("getReservationPricing/{code}")]
        public async Task<JObject> GetReservationPricing(MewsPriceReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/price", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("checkout/{code}")]
        public async Task<JObject> Checkout(MewsProcessReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/process", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("checkin/{code}")]
        public async Task<JObject> Checkin(MewsProcessReservation req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/start", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("resourceBlockDelete/{code}")]
        public async Task<JObject> DeleteResourceBlock(MewsResourceBlock req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/resourceBlocks/delete", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getImages/{code}")]
        public async Task<ImageUrlsData> GetImages(Image[] req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsImage();

                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Images = req;
                    data.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/images/getUrls", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    //return JObject.Parse(jsonContent);
                    return JsonConvert.DeserializeObject<ImageUrlsData>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("getProducts/{code}")]
        public async Task<JObject> GetProducts(MewsProductsRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/products/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        public async Task<MewsProducts> GetMewsProducts(MewsProductsRequest req, string code, bool isUpsell = false)
        {
            try
            {
                MewsProducts result;
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/products/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<MewsProducts>(jsonContent);
                    if (isUpsell)
                    {
                        result.Products = result.Products.Where(p => p.IsActive && p.Price.NetValue != null && p.Price.NetValue > 0 && p.ExternalIdentifier != null && p.ExternalIdentifier == "upsell").ToList();
                        result.CustomerProducts = result.CustomerProducts.Where(p => p.IsActive && p.Price.NetValue != null && p.Price.NetValue > 0 && p.ExternalIdentifier == "upsell").ToList();
                    }
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }

        [HttpPost]
        [Route("getUpsells")]
        public async Task<List<Upsell>> GetUpsells(MewsAvailability req)
        {
            string code = _code;

            try
            {
                List<RoomDTO> room = new List<RoomDTO>();
                List<Upsell> result = new List<Upsell>();
                //LookupReservationRequest rReq = new LookupReservationRequest();
                //rReq.ReservationID = req.ReservationId;
                //var reservation = await GetReservation(rReq.ReservationID);

                //MewsResources reqResources = new MewsResources();
                //reqResources.StartUtc = reservation.StartUtc;
                //reqResources.EndUtc = reservation.EndUtc;
                //reqResources.Resources = true;
                //reqResources.ResourceCategories = true;
                //reqResources.ResourceCategoryAssignments = true;
                //reqResources.Inactive = false;

                //ResourceResponse resources = await GetResources(reqResources, code);
                //var currentCategory = resources.ResourceCategories.Where(c => c.Id.ToString() == reservation.RequestedResourceCategoryId).FirstOrDefault();
                //string accessibleRoom = "";
                //if (currentCategory.Names.EnUs.ToLower().Contains("accessible"))
                //    accessibleRoom = "accessible";
                //var availableCategories = resources.ResourceCategories.Where(c => c.Type.ToLower() == "room" && c.Ordering > currentCategory.Ordering && c.Names.EnUs.ToLower().Contains(accessibleRoom)).ToArray();
                //MewsProductsRequest prodReq = new MewsProductsRequest();
                //string[] serviceIds = new string[1];
                //serviceIds[0] = reservation.ServiceId;
                //prodReq.ServiceIds = serviceIds;
                //var products = await GetMewsProducts(prodReq, code, true);

                //req.StartUtc = reservation.StartUtc;
                //req.EndUtc = reservation.EndUtc;
                //req.ServiceId = reservation.ServiceId;
                //var availability = await GetAvailability(req, _code);
                //if (availability != null)
                //{
                //    var categoryIds = availability.CategoryAvailabilities.Select(x => x.CategoryId).ToList();

                //    var resourceCategories = resources.ResourceCategories.Where(x => categoryIds.Contains(x.Id));
                //    var resourceCategoryAssignments = resources.ResourceCategoryAssignments.Where(x => categoryIds.Contains(x.CategoryId));
                //    var assignments = resourceCategories.Select(x => x.Id.ToString()).ToList();
                //    var categoryAssignments = resourceCategories.Select(x => x.ResourceId.ToString()).ToList();
                //    var filteredResources = resources.Resources.Where(x => assignments.Contains(x.Id.ToString()));

                //    var resourceCategoryImageAssignments = resources.ResourceCategoryImageAssignments.Where(x => categoryIds.Contains(x.CategoryId)).ToList();
                //    var imageIds = resourceCategoryImageAssignments.Select(x => x.ImageId.ToString()).ToList();

                //    foreach (var r in filteredResources)
                //    {
                //        var imageId = AssignImageId(r, resourceCategoryAssignments, resourceCategoryImageAssignments);
                //        var categoryId = resourceCategoryAssignments.FirstOrDefault(x => x.ResourceId == r.Id).CategoryId;
                //        var resourceCategory = resources.ResourceCategories.FirstOrDefault(x => x.Id == categoryId);


                //        room.Add(new RoomDTO
                //        {
                //            Name = r.Name,
                //            Description = resourceCategory.Descriptions.EnUs,
                //            ImageId = imageId,
                //            CategoryId = categoryId.ToString(),
                //            ServiceId = resourceCategory.ServiceId.ToString(),
                //            ResourceId = r.Id.ToString(),
                //            Capacity = resourceCategory.Capacity
                //        });
                //    }

                //    await PrepareImagesData(room, imageIds);
                //    await PrepareRatesData(room, new List<string>() { req.ServiceId }, "");
                //    await PreparePricingData(room, req.StartUtc.AddDays(1), req.EndUtc);

                //    result.Add(new Upsell
                //    {
                //        ReservationId = req.ReservationId,
                //        Name = r.Name,
                //        Description = resourceCategory.Descriptions.EnUs,
                //        CategoryId = categoryId.ToString(),
                //        ServiceId = resourceCategory.ServiceId.ToString(),
                //        ResourceId = r.Id.ToString(),
                //    });

                //}

                Upsell upsell = new Upsell { Name = "Early Check-In", Description = "Check-in and access your room at 1:00p (normal Check-In time is 4:00p)", Price = 35, Daily = false, ImageUrl = "/images/static/EarlyCheckin.png" };
                result.Add(upsell);

                upsell = new Upsell { Name = "Skyloft Suite w/Queen Room", Description = " Suite with extra (Queen Bed Room), ~500sf - Juliet balcony,All natural mattress and bedding, Full kitchenette with speed oven & induction cooking, Floor - to - ceiling glass and tile bathrooms, Washer and dryer, Automated sheer and blackout curtains, 55 inches 4k TV ", Price = 24, Daily = true, ImageUrl = "https://cdn.mews.com/Media/Image/4ae37098-fb6f-4f9c-af3b-b0e80101dc02" };
                result.Add(upsell);

                upsell = new Upsell { Name = "Two Bedroom Suite Classic", Description = " 2 Bedroom (1 King Bed, 1 Queen & 1 Pullout)", Price = 84, Daily = true, ImageUrl = "https://cdn.mews.com/Media/Image/81863ca4-7520-4299-bbc9-b0e80103035f" };
                result.Add(upsell);
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        // POST api/<OhipController>
        [HttpPost]
        public void Post([FromBody] string value)
        {

        }

        // PUT api/<OhipController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OhipController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("getTokenizedCard/{code}")]
        public async Task<string> GetTokenizedCard(MewsCreditCard req, string code)
         {
            try
            {
                //JToken maskedCard;
                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri("https://api.sandbox.datatrans.com");
                //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                //    ASCIIEncoding.ASCII.GetBytes("1100024916:ECZwVkQxDS9PnKWe")));

                //    HttpResponseMessage tokenResponse = await client.GetAsync("upp/services/v1/inline/token?transactionId=" + req.TransactionId);

                //    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                //    maskedCard = JObject.Parse(jsonContent)["maskedCard"];
                //}

                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var cred = getAccess(code);

                    var data = new MewsCreditCardRequest();
                    var cardData = new CreditCardData();

                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.CustomerId = req.CustomerId;
                    data.Limitation = new MewsLimitation { Count = 10 };

                    cardData.Expiration = req.Expiration;
                    cardData.StorageData = req.TransactionId;
                    //cardData.ObfuscatedNumber = maskedCard.ToString();
                    data.CreditCardData = cardData;


                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage cardTokenResponse = await client.PostAsync("api/connector/v1/creditCards/addTokenized", content);

                    var cardContent = await cardTokenResponse.Content.ReadAsStringAsync();
                    if (cardContent != null)
                    {

                    }
                }

                return "";

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [HttpPost]
        [Route("getCreditCards/{code}")]
        public async Task<MewsCreditCardResponse> GetCreditCards(MewsGetCreditCardRequest req, string code)
        {
            try
            {
                MewsCreditCardResponse result;
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/creditCards/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<MewsCreditCardResponse>(jsonContent);
                    return result;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        [HttpPost]
        [Route("updatePrice/{code}")]
        public async Task<JObject> UpdatePrice(PriceUpdateRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/rates/updatePrice", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JObject.Parse(jsonContent);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("addRestrictions/{code}")]
        public async Task<JObject> AddRestrictions(MewsRestrictionsRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/restrictions/add", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JObject.Parse(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        [Route("confirmReservation/{code}")]
        public async Task<JObject> ConfirmReservation(ConfirmReservationRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/confirm", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JObject.Parse(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("updateAvailability/{code}")]
        public async Task<JObject> UpdateAvailability(UpdateAvailabilityRequest req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/services/updateAvailability", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JObject.Parse(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("getCustomerReservations/{code}")]
        public async Task<MewsReservationResponse> GetCustomerReservations(MewsCustomer req, string code)
        {
            var getCustomerData = new MewsGetCustomers()
            {
                Emails = new string[] { req.Email },
            };

            var getCustomerResponse = await GetCustomer(getCustomerData, code);
            try
            {
                string[] customerIds = new string[1];
                customerIds[0] = getCustomerResponse.Customers[0].Id.ToString(); // "dc0ec154-338d-4566-b01b-ae78003cff32" "7c89757e-4d3f-433f-9450-ae280051fae6"
                string[] states = new string[3];
                states[0] = "Confirmed";
                states[1] = "Started";
                states[2] = "Processed";
                MewsReservationRequest request = new MewsReservationRequest()
                {
                    CustomerIds = customerIds, 
                    States = states
                };
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    request.ClientToken = cred.ClientToken;
                    request.AccessToken = cred.AccessToken;
                    request.Client = cred.Client;
                    request.Limitation = new MewsLimitation { Count = 10 };
                    
                    var objAsJson = JsonConvert.SerializeObject(request);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

                    MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                    {
                        AccessToken = cred.AccessToken,
                        Client = cred.Client,
                        ClientToken = cred.ClientToken,
                        Limitation = new MewsLimitation { Count = 10 }
                    };
                    if (result.Reservations != null && result.Reservations.Count > 0)
                    {
                        var pReq = new MewsProductsRequest()
                        {
                            ServiceIds = new string[] { result.Reservations[0].ServiceId }
                        };
                        var products = await GetMewsProducts(pReq, code);
                        
                        priceRequest.ServiceOrderIds = new List<string>();
                        //priceRequest.OrderItemIds = new List<string>();
                        for (int i = 0; i < result.Reservations.Count; i++)
                        {
                            result.Reservations[i].TotalNights = (Convert.ToDateTime(result.Reservations[i].EndUtc.Date) - Convert.ToDateTime(result.Reservations[i].StartUtc.Date)).Days;
                            //switch (result.Reservations[i].State)
                            //{
                            //    case "Started":
                            //        result.Reservations[i].State = "Checked In";
                            //        break;
                            //}
                            priceRequest.ServiceOrderIds.Clear();
                            priceRequest.ServiceOrderIds.Add(result.Reservations[i].Id);
                            var resPrices = await GetReservationItemsPrice(priceRequest, code);
                            if (resPrices != null && resPrices.OrderItems != null && resPrices.OrderItems.Count > 0)
                            {
                                result.Reservations[i].GrossAmount = resPrices.OrderItems.Where(a=>a.AccountingState == "Open").Sum(c => c.Amount.GrossValue);
                                result.Reservations[i].NetAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.NetValue);
                                result.Reservations[i].Tax = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.Tax); ;
                                //result.Reservations[i].Items = resPrices.Reservations[0].Items.OrderBy(x => x.ConsumptionUTC).ToList();
                                result.Reservations[i].OrderItems = resPrices.OrderItems.OrderBy(x => x.ConsumptionUTC).ToList();
                                switch(result.Reservations[i].State.ToLower())
                                {
                                    case "confirmed":
                                        result.Reservations[i].Ready = result.Reservations[i].StartUtc <= DateTime.Now.AddDays(1);
                                        break;
                                    case "started":
                                        result.Reservations[i].Ready = result.Reservations[i].EndUtc <= DateTime.Now.Date;
                                        break;
                                    default:
                                        result.Reservations[i].Ready = false;
                                        break;
                                }
                                result.Reservations[i].OrderItems.ForEach(p =>
                                {
                                    var prod = products.CustomerProducts.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod == null)
                                        prod = products.Products.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod != null)
                                        p.RevenueType = prod.Name;
                                    else if (p.RevenueType == "Service")
                                        p.RevenueType = "Room Rate";
                                });

                                
                            }

                            MewsPricing rq = new MewsPricing();
                            rq.RateId = result.Reservations[i].RateId.ToString();
                            rq.StartUtc = result.Reservations[i].StartUtc;
                            rq.EndUtc = result.Reservations[i].EndUtc;
                            RatePrice price = await GetPricing(rq, _code);
                            for(int night=0; night < result.Reservations[i].TotalNights; night++)
                            {
                                //result.Reservations[i].NetAmount += price.BaseAmountPrices[night].NetValue;
                                //result.Reservations[i].GrossAmount += price.BaseAmountPrices[night].GrossValue;
                            }
                        }
                    }
                    result.Reservations = result.Reservations.OrderBy(x => x.StartUtc).ThenBy(y => y.EndUtc).ToList();
                    return result;

                }

            }
            catch (Exception e)
            {
                return null;
            }

            //var getCustomerData = new MewsGetCustomers()
            //{
            //    Emails = new string[] { req.Email },
            //};

            //var getCustomerResponse = await GetCustomer(getCustomerData, _code);
            //if (getCustomerResponse.Customers.Count > 0)
            //{
            //    string[] customerIds = new string[1];
            //    customerIds[0] = getCustomerResponse.Customers[0].Id.ToString();
            //    MewsReservationRequest request = new MewsReservationRequest()
            //    {
            //        CustomerIds = customerIds 
            //    };
            //    try
            //    {
            //        var cred = getAccess(code);
            //        using (var client = new HttpClient())
            //        {
            //            client.BaseAddress = _apiBaseUrl;

            //            req.ClientToken = cred.ClientToken;
            //            req.AccessToken = cred.AccessToken;
            //            req.Client = cred.Client;

            //            var objAsJson = JsonConvert.SerializeObject(request);
            //            var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
            //            HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll", content);

            //            var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
            //            dynamic arreglo = JObject.Parse(jsonContent);

            //            return JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

            //        }

            //    }
            //    catch (Exception e)
            //    {
            //        return null;
            //    }
            //}
            //else
            //{
            //    return null;

            //}
        }
        [HttpPost]
        [Route("getReservationItemsPrice/{code}")]
        public async Task<MewsOrderItemsPriceResponse> GetReservationItemsPrice(MewsReservationPriceRequest res, string code)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var objAsJson = JsonConvert.SerializeObject(res);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    //HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAllItems", content);
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/orderitems/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    var result = JsonConvert.DeserializeObject<MewsOrderItemsPriceResponse>(jsonContent);
                    return result;

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("sendSmsCode")]
        public bool SendSmsCode(SMSCodeRequest req)
        {
            try
            {
                var shareSms = new ShareSMS();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var request = new RestSharp.RestRequest("api/v1/sms/send", RestSharp.Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("ApiKey", shareSms._apiKey);

                request.AddJsonBody(new SMSRequest
                {
                    Body = "this is Your secure code: " + req.Code,
                    Number = req.Phone,
                    DeviceKey = ""

                });
                var response = shareSms._client.Execute<SMSResponse>(request);

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("sendEmailCode")]
        public bool SendEmailCode(EmailCodeRequest req)
        {
            try
            {
                var email = "monitor@trueomni.com";
                var password = "viyyukgjgvrhoikc";

                var mailMessage = new MailMessage(email, req.GuestEmail, "Cascada Confirmation Code", req.Code);
                var smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Port = 587;
                smtpClient.Credentials = new NetworkCredential(email, password);
                smtpClient.Send(mailMessage);
                smtpClient.Dispose();

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("getRestrictions/{code}")]
        public async Task<List<Restriction>> GetRestrictions(string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    var data = new MewsRestrictionsRequest();
                    data.ClientToken = cred.ClientToken;
                    data.AccessToken = cred.AccessToken;
                    data.Client = cred.Client;
                    data.Limitation = new MewsLimitation { Count = 10 };
                    data.ServiceIds = new List<string>();
                    data.ServiceIds.Add(_configuration["MEWS:StayId"]);
                    var objAsJson = JsonConvert.SerializeObject(data);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/restrictions/getAll", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<Restriction>>(jsonContent);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        [HttpPost]
        [Route("lookup-old")]
        public async Task<Reservation> LookupOld(LookupReservationRequest req, string code = "CASCADA-P")
        {
            Reservation reservation = new Reservation();
            List<Reservation> reservations = new List<Reservation>();
            List<GuestRoom> rooms = new List<GuestRoom>();
            string[] reservationIds = new string[1];
            List<string> customerIds = new List<string>();
            string searchValue = "";
            var searchType = 0; // ReservationId

            if (!string.IsNullOrEmpty(req.Phone))
            {
                // Search by Phone 
                searchType = 2;
                searchValue = req.Phone;
            }
            else if (!string.IsNullOrEmpty(req.ReservationID))
            {
                if (req.ReservationID.IndexOf("-") == -1)
                {
                    // By Confirmation # 
                    searchType = 1;
                    searchValue = req.ReservationID;
                }
                else
                {
                    // Reservation Id
                    reservationIds[0] = req.ReservationID;
                    searchValue = req.ReservationID;
                }
            }
            else
            {
                // Other
                searchType = 9;
            }

            string[] states = new string[3];
            states[0] = "Confirmed";
            states[1] = "Started";
            states[2] = "Processed";
            MewsTimeInterval dDateRange = new MewsTimeInterval();
            //dDateRange.StartUtc = DateTime.Now.AddDays(-1);
            //dDateRange.EndUtc = DateTime.Now.AddDays(10);
            dDateRange.StartUtc = DateTime.Now.AddDays(90);
            dDateRange.EndUtc = DateTime.Now.AddDays(100);
            MewsReservationRequest request = new MewsReservationRequest()
            {
                States=states, CollidingUtc = dDateRange, ReservationIds=string.IsNullOrEmpty(reservationIds[0]) ? null : reservationIds

            };
            var cred = getAccess(code);
            using (var client = new HttpClient())
            {
                client.BaseAddress = _apiBaseUrl;

                request.ClientToken = cred.ClientToken;
                request.AccessToken = cred.AccessToken;
                request.Client = cred.Client;
                request.Limitation = new MewsLimitation { Count = 10 };
                var objAsJson = JsonConvert.SerializeObject(request);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                dynamic arreglo = JObject.Parse(jsonContent);

                var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

                MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                {
                    AccessToken = cred.AccessToken,
                    Client = cred.Client,
                    ClientToken = cred.ClientToken,
                    Limitation = new MewsLimitation { Count = 10 }
                };
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    if (searchType > 0)
                    {
                        // not using reservationId
                        switch(searchType)
                        {
                            case 1: // ConfirmationNumber
                                result.Reservations = result.Reservations.Where(r => r.ReservationNumber == searchValue).ToList();
                                break;
                            case 2: // Phone
                                var customerIds_temp = result.Reservations.Select(r => r.AccountId.ToString()).ToList();
                                var getCustomerDataSearch = new MewsGetCustomers()
                                {
                                    CustomerIds = customerIds_temp.ToArray()
                                };

                                var getCustomerSearchResponse = await GetCustomer(getCustomerDataSearch, code);
                                var customers = getCustomerSearchResponse.Customers.Where(a => a.Phone != null && a.Phone == searchValue).ToList();
                                customerIds = customers.Select(c => c.Id.ToString()).ToList();
                                result.Reservations = result.Reservations.Where(r => r.AccountType == "Customer" && customerIds.Contains(r.AccountId.ToString())).ToList();
                                break;
                        }
                    }

                    if (result.Reservations != null && result.Reservations.Count > 0)
                    {
                        var pReq = new MewsProductsRequest()
                        {
                            ServiceIds = new string[] { result.Reservations[0].ServiceId }
                        };
                        var products = await GetMewsProducts(pReq, code);

                        priceRequest.ServiceOrderIds = new List<string>();
                        //priceRequest.OrderItemIds = new List<string>();
                        for (int i = 0; i < result.Reservations.Count; i++)
                        {
                            result.Reservations[i].TotalNights = (Convert.ToDateTime(result.Reservations[i].EndUtc.Date) - Convert.ToDateTime(result.Reservations[i].StartUtc.Date)).Days;
                            priceRequest.ServiceOrderIds.Clear();
                            priceRequest.ServiceOrderIds.Add(result.Reservations[i].Id);
                            var resPrices = await GetReservationItemsPrice(priceRequest, code);
                            if (resPrices != null && resPrices.OrderItems != null && resPrices.OrderItems.Count > 0)
                            {
                                //result.Reservations[i].GrossAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.GrossValue);
                                //result.Reservations[i].NetAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.NetValue);
                                //result.Reservations[i].Tax = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.Tax);
                                result.Reservations[i].GrossAmount = resPrices.OrderItems.Sum(c => c.Amount.GrossValue);
                                result.Reservations[i].NetAmount = resPrices.OrderItems.Sum(c => c.Amount.NetValue);
                                result.Reservations[i].Tax = resPrices.OrderItems.Sum(c => c.Amount.Tax);
                                if (result.Reservations[i].Tax == 0 && result.Reservations[i].GrossAmount > result.Reservations[i].NetAmount)
                                {
                                    result.Reservations[i].Tax = result.Reservations[i].GrossAmount - result.Reservations[i].NetAmount;
                                }
                                //result.Reservations[i].Items = resPrices.Reservations[0].Items.OrderBy(x => x.ConsumptionUTC).ToList();
                                result.Reservations[i].OrderItems = resPrices.OrderItems.OrderBy(x => x.ConsumptionUTC).ToList();
                                switch (result.Reservations[i].State.ToLower())
                                {
                                    case "confirmed":
                                        reservation.Status = "confirmed";
                                        result.Reservations[i].Ready = result.Reservations[i].StartUtc <= DateTime.Now.AddDays(1);
                                        break;
                                    case "started":
                                        reservation.Status = "checked_in";
                                        result.Reservations[i].Ready = result.Reservations[i].EndUtc <= DateTime.Now.Date;
                                        break;
                                    default:
                                        reservation.Status = result.Reservations[i].State.ToLower();
                                        result.Reservations[i].Ready = false;
                                        break;
                                }
                                result.Reservations[i].OrderItems.ForEach(p =>
                                {
                                    var prod = products.CustomerProducts.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod == null)
                                        prod = products.Products.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod != null)
                                        p.RevenueType = prod.Name;
                                    else if (p.RevenueType == "Service")
                                    {
                                        // Space
                                        p.RevenueType = "Room Rate";
                                    }
                                });


                            }
                            GuestRoom room = new GuestRoom();
                            string[] resourceIds = new string[2];
                            resourceIds[0] = result.Reservations[i].RequestedResourceCategoryId;
                            resourceIds[1] = result.Reservations[i].AssignedResourceId;
                            MewsResources resourceReq = new MewsResources();
                            resourceReq.Resources = true;
                            resourceReq.ResourceCategories = true;
                            resourceReq.Inactive = false;
                            resourceReq.ResourceIds = resourceIds;
                            var resourceResponse = GetResources(resourceReq, code).Result;
                            if (resourceResponse.ResourceCategories != null && resourceResponse.ResourceCategories.Length > 0)
                            {
                                room.RoomName = resourceResponse.ResourceCategories[0].Names.EnUs;
                            }
                            if (resourceResponse.Resources != null && resourceResponse.Resources.Length > 0)
                            {
                                room.RoomNumber = resourceResponse.Resources[0].Name;
                                room.RoomId = resourceResponse.Resources[0].Id.ToString();
                            }
                            room.StartDate = result.Reservations[i].StartUtc;
                            room.EndDate = result.Reservations[i].EndUtc;
                            room.Adults = result.Reservations[i].PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                            room.Children = result.Reservations[i].PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                            rooms.Add(room);
                            //MewsPricing rq = new MewsPricing();
                            //rq.RateId = result.Reservations[i].RateId.ToString();
                            //rq.StartUtc = result.Reservations[i].StartUtc;
                            //rq.EndUtc = result.Reservations[i].EndUtc;
                            //RatePrice price = await GetPricing(rq, _code);
                            //for (int night = 0; night < result.Reservations[i].TotalNights; night++)
                            //{
                            //    //result.Reservations[i].NetAmount += price.BaseAmountPrices[night].NetValue;
                            //    //result.Reservations[i].GrossAmount += price.BaseAmountPrices[night].GrossValue;
                            //}
                        }
                    }

                }
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    result.Reservations = result.Reservations.OrderBy(x => x.StartUtc).ThenBy(y => y.EndUtc).ToList();
                    for(int i=0;i<result.Reservations.Count;i++)
                    {
                        var tempResult = result.Reservations[i];
                        reservation.ConfirmationNumber = result.Reservations[i].ReservationNumber;
                        reservation.ResvNameId = tempResult.Id;
                        reservation.CheckInDate = tempResult.StartUtc;
                        reservation.CheckOutDate = tempResult.EndUtc;
                        reservation.AdultCount = tempResult.PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        reservation.ChildrenCount = tempResult.PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        reservation.Guests = new List<Guest>();
                        reservation.ServiceId = tempResult.ServiceId;
                        reservation.Total = tempResult.NetAmount;
                        reservation.TaxesAndFees = tempResult.Tax;
                        var getCustomerData = new MewsGetCustomers()
                        {
                            CustomerIds = new string[] { tempResult.AccountId },
                        };

                        var getCustomerResponse = await GetCustomer(getCustomerData, code);
                        if (getCustomerResponse.Customers != null && getCustomerResponse.Customers.Count > 0)
                        {
                            var customer = getCustomerResponse.Customers[0];
                            var guest = new Guest();
                            guest.Email = customer.Email;
                            guest.FirstName = customer.FirstName;
                            guest.LastName = customer.LastName;
                            guest.ProfileId = customer.Id.ToString();
                            reservation.Guests.Add(guest);
                            reservation.GuestName = customer.FirstName + " " + customer.LastName;
                        }
                        reservation.Rooms = rooms;
                        reservation.DayCount = (reservation.CheckOutDate.Date - reservation.CheckInDate.Date).Days;
                        reservations.Add(reservation);
                    }
                }
                if (reservations.Count > 0)
                    return reservations[0];
                else
                    return null;

            }

        }
        [HttpPost]
        [Route("lookup")]
        public async Task<List<Reservation>> Lookup(LookupReservationRequest req, string code = "CASCADA-P")
        {
            Reservation reservation = null;
            List<Reservation> reservations = new List<Reservation>();
            List<GuestRoom> rooms = null;
            string[] reservationIds = new string[1];
            List<string> customerIds = new List<string>();
            string searchValue = "";
            var searchType = 0; // ReservationId

            if (!string.IsNullOrEmpty(req.Phone))
            {
                // Search by Phone 
                searchType = 2;
                searchValue = req.Phone;
            }
            else if (!string.IsNullOrEmpty(req.ReservationID))
            {
                if (req.ReservationID.IndexOf("-") == -1)
                {
                    // By Confirmation # 
                    searchType = 1;
                    searchValue = req.ReservationID;
                }
                else
                {
                    // Reservation Id
                    reservationIds[0] = req.ReservationID;
                    searchValue = req.ReservationID;
                }
            }
            else
            {
                // Other
                searchType = 9;
            }

            string[] states = new string[3];
            states[0] = "Confirmed";
            states[1] = "Started";
            states[2] = "Processed";
            MewsTimeInterval dDateRange = new MewsTimeInterval();
            dDateRange.StartUtc = DateTime.Now.AddDays(-1);
            dDateRange.EndUtc = DateTime.Now.AddDays(1);
            //dDateRange.StartUtc = DateTime.Now.AddDays(80);
            //dDateRange.EndUtc = DateTime.Now.AddDays(110);
            MewsReservationRequest request = new MewsReservationRequest()
            {
                States = states,
                CollidingUtc = dDateRange,
                ReservationIds = string.IsNullOrEmpty(reservationIds[0]) ? null : reservationIds

            };
            var cred = getAccess(code);
            using (var client = new HttpClient())
            {
                client.BaseAddress = _apiBaseUrl;

                request.ClientToken = cred.ClientToken;
                request.AccessToken = cred.AccessToken;
                request.Client = cred.Client;
                request.Limitation = new MewsLimitation { Count = 100 };
                var objAsJson = JsonConvert.SerializeObject(request);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                dynamic arreglo = JObject.Parse(jsonContent);

                var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

                MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                {
                    AccessToken = cred.AccessToken,
                    Client = cred.Client,
                    ClientToken = cred.ClientToken,
                    Limitation = new MewsLimitation { Count = 10 }
                };
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    if (searchType > 0)
                    {
                        // not using reservationId
                        switch (searchType)
                        {
                            case 1: // ConfirmationNumber
                                result.Reservations = result.Reservations.Where(r => r.ReservationNumber == searchValue).ToList();
                                break;
                            case 2: // Phone
                                var customerIds_temp = result.Reservations.Select(r => r.AccountId.ToString()).ToList();
                                var getCustomerDataSearch = new MewsGetCustomers()
                                {
                                    CustomerIds = customerIds_temp.ToArray()
                                };

                                var getCustomerSearchResponse = await GetCustomer(getCustomerDataSearch, code);
                                var customers = getCustomerSearchResponse.Customers.Where(a => a.Phone != null && a.Phone.Contains(searchValue)).ToList();
                                customerIds = customers.Select(c => c.Id.ToString()).ToList();
                                result.Reservations = result.Reservations.Where(r => r.AccountType == "Customer" && customerIds.Contains(r.AccountId.ToString())).ToList();
                                break;
                        }
                    }

                    if (result.Reservations != null && result.Reservations.Count > 0)
                    {
                        var pReq = new MewsProductsRequest()
                        {
                            ServiceIds = new string[] { result.Reservations[0].ServiceId }
                        };
                        var products = await GetMewsProducts(pReq, code);

                        priceRequest.ServiceOrderIds = new List<string>();
                        //priceRequest.OrderItemIds = new List<string>();
                        for (int i = 0; i < result.Reservations.Count; i++)
                        {
                            result.Reservations[i].TotalNights = (Convert.ToDateTime(result.Reservations[i].EndUtc.Date) - Convert.ToDateTime(result.Reservations[i].StartUtc.Date)).Days;
                            priceRequest.ServiceOrderIds.Clear();
                            priceRequest.ServiceOrderIds.Add(result.Reservations[i].Id);
                            var resPrices = await GetReservationItemsPrice(priceRequest, code);
                            if (resPrices != null && resPrices.OrderItems != null && resPrices.OrderItems.Count > 0)
                            {
                                //result.Reservations[i].GrossAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.GrossValue);
                                //result.Reservations[i].NetAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.NetValue);
                                //result.Reservations[i].Tax = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.Tax);
                                result.Reservations[i].GrossAmount = resPrices.OrderItems.Sum(c => c.Amount.GrossValue);
                                result.Reservations[i].NetAmount = resPrices.OrderItems.Sum(c => c.Amount.NetValue);
                                result.Reservations[i].Tax = resPrices.OrderItems.Sum(c => c.Amount.Tax);
                                if (result.Reservations[i].Tax == 0 && result.Reservations[i].GrossAmount > result.Reservations[i].NetAmount)
                                {
                                    result.Reservations[i].Tax = result.Reservations[i].GrossAmount - result.Reservations[i].NetAmount;
                                }
                                //result.Reservations[i].Items = resPrices.Reservations[0].Items.OrderBy(x => x.ConsumptionUTC).ToList();
                                result.Reservations[i].OrderItems = resPrices.OrderItems.OrderBy(x => x.ConsumptionUTC).ToList();
                                result.Reservations[i].OrderItems.ForEach(p =>
                                {
                                    var prod = products.CustomerProducts.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod == null)
                                        prod = products.Products.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                                    if (prod != null)
                                        p.RevenueType = prod.Name;
                                    else if (p.RevenueType == "Service")
                                    {
                                        // Space
                                        p.RevenueType = "Room Rate";
                                    }
                                });


                            }
                            //MewsPricing rq = new MewsPricing();
                            //rq.RateId = result.Reservations[i].RateId.ToString();
                            //rq.StartUtc = result.Reservations[i].StartUtc;
                            //rq.EndUtc = result.Reservations[i].EndUtc;
                            //RatePrice price = await GetPricing(rq, _code);
                            //for (int night = 0; night < result.Reservations[i].TotalNights; night++)
                            //{
                            //    //result.Reservations[i].NetAmount += price.BaseAmountPrices[night].NetValue;
                            //    //result.Reservations[i].GrossAmount += price.BaseAmountPrices[night].GrossValue;
                            //}
                        }
                    }

                }
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    result.Reservations = result.Reservations.OrderBy(x => x.StartUtc).ThenBy(y => y.EndUtc).ToList();
                    for (int i = 0; i < result.Reservations.Count; i++)
                    {
                        rooms = new List<GuestRoom>();
                        GuestRoom room = new GuestRoom();
                        string[] resourceIds = new string[2];
                        resourceIds[0] = result.Reservations[i].RequestedResourceCategoryId;
                        resourceIds[1] = result.Reservations[i].AssignedResourceId;
                        MewsResources resourceReq = new MewsResources();
                        resourceReq.Resources = true;
                        resourceReq.ResourceCategories = true;
                        resourceReq.Inactive = false;
                        resourceReq.ResourceIds = resourceIds;
                        var resourceResponse = GetResources(resourceReq, code).Result;
                        if (resourceResponse.ResourceCategories != null && resourceResponse.ResourceCategories.Length > 0)
                        {
                            room.RoomName = resourceResponse.ResourceCategories[0].Names.EnUs;
                        }
                        if (resourceResponse.Resources != null && resourceResponse.Resources.Length > 0)
                        {
                            room.RoomNumber = resourceResponse.Resources[0].Name;
                            room.RoomId = resourceResponse.Resources[0].Id.ToString();
                        }
                        room.StartDate = result.Reservations[i].StartUtc;
                        room.EndDate = result.Reservations[i].EndUtc;
                        room.Adults = result.Reservations[i].PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        room.Children = result.Reservations[i].PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        rooms.Add(room);

                        reservation = new Reservation();
                        var tempResult = result.Reservations[i];
                        reservation.ConfirmationNumber = result.Reservations[i].ReservationNumber;
                        reservation.ResvNameId = tempResult.Id;
                        reservation.CheckInDate = tempResult.StartUtc;
                        reservation.CheckOutDate = tempResult.EndUtc;
                        reservation.AdultCount = tempResult.PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        reservation.ChildrenCount = tempResult.PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                        reservation.Guests = new List<Guest>();
                        reservation.ServiceId = tempResult.ServiceId;
                        reservation.Total = tempResult.NetAmount;
                        reservation.TaxesAndFees = tempResult.Tax;
                        switch (result.Reservations[i].State.ToLower())
                        {
                            case "confirmed":
                                reservation.Status = "confirmed";
                                result.Reservations[i].Ready = result.Reservations[i].StartUtc <= DateTime.Now.AddDays(1);
                                break;
                            case "started":
                                reservation.Status = "checked_in";
                                result.Reservations[i].Ready = result.Reservations[i].EndUtc <= DateTime.Now.Date;
                                break;
                            default:
                                reservation.Status = result.Reservations[i].State.ToLower();
                                result.Reservations[i].Ready = false;
                                break;
                        }
                        var getCustomerData = new MewsGetCustomers()
                        {
                            CustomerIds = new string[] { tempResult.AccountId },
                        };

                        var getCustomerResponse = await GetCustomer(getCustomerData, code);
                        if (getCustomerResponse.Customers != null && getCustomerResponse.Customers.Count > 0)
                        {
                            var customer = getCustomerResponse.Customers[0];
                            var guest = new Guest();
                            guest.Email = customer.Email;
                            guest.FirstName = customer.FirstName;
                            guest.LastName = customer.LastName;
                            guest.ProfileId = customer.Id.ToString();
                            reservation.Guests.Add(guest);
                            reservation.GuestName = customer.FirstName + " " + customer.LastName;
                        }
                        reservation.Rooms = rooms;
                        reservation.DayCount = (reservation.CheckOutDate.Date - reservation.CheckInDate.Date).Days;
                        reservations.Add(reservation);
                    }
                }
                return reservations;

            }

        }
        [HttpPost]
        [Route("getReservation/{reservationId}")]
        public async Task<ResDetails> GetReservation(String reservationId)
        {
            string code = _code;
            string[] reservationIds = new string[1];
            reservationIds[0] = reservationId;


            string[] states = new string[3];
            states[0] = "Confirmed";
            states[1] = "Started";
            states[2] = "Processed";
            MewsTimeInterval dDateRange = new MewsTimeInterval();
            //dDateRange.StartUtc = DateTime.Now.AddDays(-1);
            //dDateRange.EndUtc = DateTime.Now.AddDays(10);
            dDateRange.StartUtc = DateTime.Now.AddDays(80);
            dDateRange.EndUtc = DateTime.Now.AddDays(110);
            MewsReservationRequest request = new MewsReservationRequest()
            {
                States = states,
                CollidingUtc = dDateRange,
                ReservationIds = string.IsNullOrEmpty(reservationIds[0]) ? null : reservationIds

            };
            var cred = getAccess(code);
            using (var client = new HttpClient())
            {
                client.BaseAddress = _apiBaseUrl;

                request.ClientToken = cred.ClientToken;
                request.AccessToken = cred.AccessToken;
                request.Client = cred.Client;
                request.Limitation = new MewsLimitation { Count = 10 };
                var objAsJson = JsonConvert.SerializeObject(request);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                dynamic arreglo = JObject.Parse(jsonContent);

                var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);
                



                MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                {
                    AccessToken = cred.AccessToken,
                    Client = cred.Client,
                    ClientToken = cred.ClientToken,
                    Limitation = new MewsLimitation { Count = 10 }
                };
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    var pReq = new MewsProductsRequest()
                    {
                        ServiceIds = new string[] { result.Reservations[0].ServiceId }
                    };
                    var products = await GetMewsProducts(pReq, code);

                    priceRequest.ServiceOrderIds = new List<string>();
                    
                    int i = 0;
                    result.Reservations[i].TotalNights = (Convert.ToDateTime(result.Reservations[i].EndUtc.Date) - Convert.ToDateTime(result.Reservations[i].StartUtc.Date)).Days;
                    priceRequest.ServiceOrderIds.Clear();
                    priceRequest.ServiceOrderIds.Add(result.Reservations[i].Id);
                    var resPrices = await GetReservationItemsPrice(priceRequest, code);
                    if (resPrices != null && resPrices.OrderItems != null && resPrices.OrderItems.Count > 0)
                    {
                        result.Reservations[i].GrossAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.GrossValue);
                        result.Reservations[i].NetAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.NetValue);
                        result.Reservations[i].Tax = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.Tax); ;
                        //result.Reservations[i].Items = resPrices.Reservations[0].Items.OrderBy(x => x.ConsumptionUTC).ToList();
                        result.Reservations[i].OrderItems = resPrices.OrderItems.OrderBy(x => x.ConsumptionUTC).ToList();
                        switch (result.Reservations[i].State.ToLower())
                        {
                            case "confirmed":
                                result.Reservations[i].Ready = result.Reservations[i].StartUtc <= DateTime.Now.AddDays(1);
                                break;
                            case "started":
                                result.Reservations[i].Ready = result.Reservations[i].EndUtc <= DateTime.Now.Date;
                                break;
                            default:
                                result.Reservations[i].Ready = false;
                                break;
                        }
                        result.Reservations[i].OrderItems.ForEach(p =>
                        {
                            var prod = products.CustomerProducts.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                            if (prod == null)
                                prod = products.Products.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                            if (prod != null)
                                p.RevenueType = prod.Name;
                            else if (p.RevenueType == "Service")
                                p.RevenueType = "Room Rate";
                        });


                    }
                    string[] resourceIds = new string[1];
                    resourceIds[0] = result.Reservations[0].AssignedResourceId;
                    MewsResources resourceReq = new MewsResources();
                    resourceReq.Resources = true;
                    resourceReq.ResourceCategories = true;
                    resourceReq.Inactive = false;
                    resourceReq.ResourceIds = resourceIds;
                    var resourceResponse = GetResources(resourceReq, code).Result;

                    List<RoomDTO> room = new List<RoomDTO>();
                    room.Add(new RoomDTO
                    {
                        Name = resourceResponse.ResourceCategories[0].Names.EnUs,
                        Description = resourceResponse.ResourceCategories[0].Descriptions.EnUs,
                        CategoryId = resourceResponse.ResourceCategories[0].Id.ToString(),
                        ServiceId = resourceResponse.ResourceCategories[0].ServiceId.ToString()
                    });

                    List<string> seriveIds = new List<string>();
                    seriveIds.Add(result.Reservations[0].ServiceId);

                    await PrepareRatesData(room, seriveIds, "");
                    await PreparePricingData(room, result.Reservations[0].StartUtc, result.Reservations[0].EndUtc);
                    result.Reservations[0].AdultCount = result.Reservations[0].PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                    result.Reservations[0].ChildCount = result.Reservations[0].PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();

                    result.Reservations[0].Rooms = room;
                }


                return result.Reservations[0];

            }

        }
        [HttpPost]
        [Route("addGuest/{code}")]
        public async Task<MewsAddCompanionResponse> AddGuestToReservation(MewsCompanion req, string code)
        {
            try
            {
                var cred = getAccess(code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };

                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/addCompanion", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var objCustomer = JsonConvert.DeserializeObject<MewsAddCompanionResponse>(jsonContent);
                    return objCustomer;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("getGuestPayment/{code}")]
        public async Task<List<CreditCard>> getGuestPayment(MewsGetCreditCardRequest request, string code)
        {
            try
            {
                var getCustomerResponse = await GetCreditCards(request, code);
                return getCustomerResponse.CreditCards;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("uploadImage")]
        public async Task<string> UploadImage([FromBody] GuestPhotoRequest request)
        {
            return "";
            try
            {
                var cred = getAccess(_code);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    MewsFileRequest req = new MewsFileRequest();
                    req.ClientToken = cred.ClientToken;
                    req.AccessToken = cred.AccessToken;
                    req.Client = cred.Client;
                    req.Limitation = new MewsLimitation { Count = 10 };
                    req.CustomerId = new Guid(request.GuestID);
                    req.Data = request.Image.Substring(request.Image.IndexOf(",") + 1);
                    req.Name = !string.IsNullOrEmpty(request.Name) ? request.Name : "IDScan";
                    req.Type = "image/png";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/addFile", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return jsonContent;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
            //try
            //{
            //    string guestID = request.GuestID;
            //    string image = request.Image;
            //    using (var httpClient = new HttpClient())
            //    {

            //        //var httpRequest2 = HttpContext.Request;
            //        using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "/customers/addFile"))
            //        {
            //            string b64Image = image.Substring(image.IndexOf(",") + 1);
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}
        }


        private string phoneString(string input)
        {
            string result = "";
            if (!string.IsNullOrEmpty(input))
            {
                result = input.Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
            }
            return result;
        }

    }
}
