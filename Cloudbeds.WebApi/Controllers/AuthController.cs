﻿using System;
using System.Threading.Tasks;
using Cloudbeds.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Cloudbeds.WebApi.Controllers
{
    [Route("api/auth")]
    [EnableCors("All")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ICloudbedsClient _cloudbedsClient;

        public AuthController(ICloudbedsClient cloudbedsClient)
        {
            _cloudbedsClient = cloudbedsClient;
        }

        /// <summary>
        /// If you are setting manually the initial access and refresh tokens you need to execute the next URL
        /// https://hotels.cloudbeds.com/api/v1.1/oauth?client_id=live1_164567_vhdbZoY6JGBT5LQWraUSq1if&redirect_uri=https://localhost:44315/api/auth/callback&response_type=code
        /// https://hotels.cloudbeds.com/api/v1.1/oauth?client_id=live1_164567_TePEuoUlqGgwMNZxBiCmHcrn&redirect_uri=https://cloudbeds-api.azurewebsites.net/api/auth/callback&response_type=code
        /// https://hotels.cloudbeds.com/api/v1.1/oauth?client_id=live1_164567_lCVXcAUQs5O1m3udDTeYzBvr&redirect_uri=https://cloudbedsapi.mobimanage.com/api/auth/callback&response_type=code
        /// https://hotels.cloudbeds.com/api/v1.1/oauth?client_id=live1_164567_69O5uGkNUi4IbvTfDJCrqHph&redirect_uri=https://hos-api.trueomni.com/api/auth/callback&response_type=code
        /// https://hotels.cloudbeds.com/api/v1.1/oauth?client_id=live1_164567_VD27C615pxQLuzZyJFKmcrOP&redirect_uri=https://trueomni-hos-api.azurewebsites.net/api/auth/callback&response_type=code
        /// You will land in this method with an AuthenticationCode
        /// With this code app will be equest an access token.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [Route("callback")]
        public async Task<string> CloudbedsAuthCallBack([FromQuery] string code)
        {
            //return "test:" + code;
            return await _cloudbedsClient.GetAccessToken(code);
        }
        [Route("trybe")]
        public async Task<string> TrybeAuthCallBack([FromQuery] string code)
        {
            return code;
        }
        [Route("tintup")]
        public async Task<string> TintUpAuthCallBack([FromQuery] string code)
        {
            return code;
        }

        public async Task<string> GetAccessToken()
        {
            try
            {
                return await _cloudbedsClient.GetAccessToken();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Route("refresh")]
        public async Task<string> RefreshAccessToken()
        {
            try
            {
                return await _cloudbedsClient.RefreshAccessToken();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [Route("check")]
        public async Task<string> CheckAccessToken()
        {
            try
            {
                return await _cloudbedsClient.CheckAccessToken();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}