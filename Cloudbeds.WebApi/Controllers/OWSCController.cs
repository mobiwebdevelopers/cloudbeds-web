﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OWSCloudReservationService;
using OWSCloudResvAdvancedService;
using OWSCloudNameService;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [Route("api/owsc")]
    [AllowAnonymous]
    public class OWSCController : ControllerBase
    {
        private readonly UserContext _userContext;

        private readonly IConfiguration _configuration;
        private readonly ICloudbedsClient _cloudbedsClient;
        private readonly IMobileKeyService _mobileKeyService;
        private readonly Uri _apiBaseUrl;
        private const string Percentage = "percentage";
        private const string Rate = "rate";
        private OWSCloudReservationService.ReservationPortTypeClient owsClient;
        private OWSCloudResvAdvancedService.ResvAdvancedPortTypeClient owsAdvClient;
        private OWSCloudNameService.NamePortTypeClient owsNameClient;
        
        private OWSCloudReservationService.HotelReference hotel = new OWSCloudReservationService.HotelReference();
        private OWSCloudReservationService.FetchBookingRequest bookingRequest = new OWSCloudReservationService.FetchBookingRequest();
        private OWSCloudReservationService.GetReservationStatusRequest reservationRequest = new OWSCloudReservationService.GetReservationStatusRequest();
        private CheckInRequest checkinRequest = new CheckInRequest();
        private CheckOutRequest checkoutRequest = new CheckOutRequest();
        private OWSCloudReservationService.OGHeader header = new OWSCloudReservationService.OGHeader();
        private OWSCloudResvAdvancedService.OGHeader headerAdv = new OWSCloudResvAdvancedService.OGHeader();
        private OWSCloudResvAdvancedService.HotelReference hotelAdv = new OWSCloudResvAdvancedService.HotelReference();
        private OWSCloudNameService.OGHeader headerName = new OWSCloudNameService.OGHeader();
        private bool _isCloud = true;
        public OWSCController(IConfiguration configuration, ICloudbedsClient cloudbedsClient, IMobileKeyService mobileKeyService, UserContext userContext)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            _configuration = configuration;
            _cloudbedsClient = cloudbedsClient;
            _mobileKeyService = mobileKeyService;
            _apiBaseUrl = new Uri(configuration["CloudbedsApi:BaseUrl"]);
            _userContext = userContext;

            OWSCloudReservationService.OGHeaderAuthentication auth = new OWSCloudReservationService.OGHeaderAuthentication();
            OWSCloudResvAdvancedService.OGHeaderAuthentication authAdv = new OWSCloudResvAdvancedService.OGHeaderAuthentication();
            OWSCloudNameService.OGHeaderAuthentication authName = new OWSCloudNameService.OGHeaderAuthentication();

            hotel.hotelCode = "GERO";
            hotel.chainCode = "CHA";
            //hotel.Value = "GERO";

            bookingRequest.HotelReference = hotel;
            reservationRequest.HotelReference = hotel;
            hotelAdv.chainCode = hotel.chainCode;
            hotelAdv.hotelCode = hotel.hotelCode;
            //hotelAdv.Value = hotel.Value;

            OWSCloudReservationService.OGHeaderAuthenticationUserCredentials cred = new OWSCloudReservationService.OGHeaderAuthenticationUserCredentials();
            cred.UserName = "OMNI79JEQP8ECSTRN89Q";
            cred.UserPassword = "znVrzpY93SLtnpq6DyfN6UkptxXTfe";
            cred.Domain = "OWS";
            auth.UserCredentials = cred;

            OWSCloudResvAdvancedService.OGHeaderAuthenticationUserCredentials credAdv = new OWSCloudResvAdvancedService.OGHeaderAuthenticationUserCredentials();
            credAdv.UserName = "OMNI79JEQP8ECSTRN89Q";
            credAdv.UserPassword = "znVrzpY93SLtnpq6DyfN6UkptxXTfe";
            credAdv.Domain = "OWS";
            authAdv.UserCredentials = credAdv;

            OWSCloudNameService.OGHeaderAuthenticationUserCredentials credName = new OWSCloudNameService.OGHeaderAuthenticationUserCredentials();
            credName.UserName = "OMNI79JEQP8ECSTRN89Q";
            credName.UserPassword = "znVrzpY93SLtnpq6DyfN6UkptxXTfe";
            credName.Domain = "OWS";
            authName.UserCredentials = credName;

            header.Authentication = auth;
            header.timeStamp = DateTime.Now;
            header.transactionID = "000000";

            headerAdv.Authentication = authAdv;
            headerAdv.timeStamp = DateTime.Now;
            headerAdv.transactionID = "000000";

            headerName.Authentication = authName;
            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = "000000";

            OWSCloudReservationService.EndPoint origin = new OWSCloudReservationService.EndPoint();
            origin.entityID = "KIOSK";
            origin.systemType = "KIOSK";
            OWSCloudReservationService.EndPoint destination = new OWSCloudReservationService.EndPoint();
            destination.entityID = "TI";
            destination.systemType = "PMS";

            header.Origin = origin;
            header.Destination = destination;

            OWSCloudResvAdvancedService.EndPoint originAdv = new OWSCloudResvAdvancedService.EndPoint();
            originAdv.entityID = "KIOSK";
            originAdv.systemType = "KIOSK";

            OWSCloudResvAdvancedService.EndPoint destinationAdv = new OWSCloudResvAdvancedService.EndPoint();
            destinationAdv.entityID = "TI";
            destinationAdv.systemType = "PMS";

            headerAdv.Origin = originAdv;
            headerAdv.Destination = destinationAdv;

            OWSCloudNameService.EndPoint originName = new OWSCloudNameService.EndPoint();
            originName.entityID = "KIOSK";
            originName.systemType = "KIOSK";
            OWSCloudNameService.EndPoint destinationName = new OWSCloudNameService.EndPoint();
            destinationName.entityID = "TI";
            destinationName.systemType = "PMS";

            headerName.Origin = originName;
            headerName.Destination = destinationName;

            System.ServiceModel.BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
            //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            //CustomBinding binding = new CustomBinding();
            

            EndpointAddress address = new EndpointAddress(new Uri("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/Reservation?wsdl"));
            EndpointAddress addressAdv = new EndpointAddress(new Uri("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/ResvAdvanced"));
            EndpointAddress addressName = new EndpointAddress(new Uri("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/Name"));

            owsClient = new OWSCloudReservationService.ReservationPortTypeClient(binding, address);
            owsAdvClient = new OWSCloudResvAdvancedService.ResvAdvancedPortTypeClient(binding, addressAdv);
            owsNameClient = new OWSCloudNameService.NamePortTypeClient(binding, addressName);



        }
        private String GetNextUniqueId(String CodePrefix)
        {
            String id = CodePrefix;
            Guid newGuid = Guid.NewGuid();
            return id + ":" + newGuid;
        }

        [HttpPost]
        [Route("lookup")]
        public async Task<Reservation> GetReservation(LookupReservationRequest request)
        {
            try
            {
                var reservationId = new OWSCloudReservationService.UniqueID();
                reservationId.type = OWSCloudReservationService.UniqueIDType.INTERNAL;
                reservationId.Value = request.ReservationID; // "23900";
                var balance = false;
                if (request.Balance)
                    balance = request.Balance;
                var result = FindReservation(reservationId, balance, request.FirstName, request.LastName);
                if (result == null)
                {
                    reservationId.type = OWSCloudReservationService.UniqueIDType.EXTERNAL;
                    reservationId.Value = request.ReservationID; // "23900";
                    result = FindReservation(reservationId, request.Balance);
                }
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        private Reservation FindReservation(OWSCloudReservationService.UniqueID key, Boolean balance = true, String firstName="", String lastName="")
        {
            

            Reservation reservationResult = new Reservation();

            header.timeStamp = DateTime.Now;
            header.transactionID = GetNextUniqueId("SANI");
            try
            {
                OWSCloudReservationService.FutureBookingSummaryRequest bookingRequest = new OWSCloudReservationService.FutureBookingSummaryRequest();
                OWSCloudReservationService.FetchBookingFilters filter = new OWSCloudReservationService.FetchBookingFilters();
                OWSCloudReservationService.FutureBookingSummaryResponse bookingResponse = new OWSCloudReservationService.FutureBookingSummaryResponse();
                filter.ConfirmationNumber = key;
                bookingRequest.AdditionalFilters = filter;

                var resultBooking = new OWSCloudReservationService.FutureBookingSummaryResponse1();
                resultBooking = (OWSCloudReservationService.FutureBookingSummaryResponse1)ExecuteReservationRequest(bookingRequest, "FutureBookingSummary");
                if (resultBooking != null && resultBooking.FutureBookingSummaryResponse != null && resultBooking.FutureBookingSummaryResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                {
                    reservationResult.Rooms = new List<GuestRoom>();
                    if (resultBooking.FutureBookingSummaryResponse.HotelReservations.Length > 0)
                    {
                        int index = 0;
                        Guest guest = new Guest();
                        if (lastName.Length > 0 || firstName.Length > 0)
                        {
                            for (int i = 0; i < resultBooking.FutureBookingSummaryResponse.HotelReservations.Length; i++)
                            {
                                for (int j = 0; j < resultBooking.FutureBookingSummaryResponse.HotelReservations[i].ResGuests.Length; j++)
                                {
                                    OWSCloudReservationService.Customer customer = (OWSCloudReservationService.Customer)resultBooking.FutureBookingSummaryResponse.HotelReservations[i].ResGuests[j].Profiles[0].Item;
                                    if (customer.PersonName.firstName.ToUpper() == firstName.ToUpper() && customer.PersonName.lastName.ToUpper() == lastName.ToUpper())
                                    {
                                        index = i;
                                        guest.Gender = customer.gender.ToString();
                                        guest.FirstName = customer.PersonName.firstName;
                                        guest.LastName = customer.PersonName.lastName;
                                        if (customer.PersonName.nameTitle != null)
                                            guest.Title = customer.PersonName.nameTitle[0];
                                        break;
                                    }

                                }
                            }
                        }
                        else
                        {
                            OWSCloudReservationService.Customer customer = (OWSCloudReservationService.Customer)resultBooking.FutureBookingSummaryResponse.HotelReservations[index].ResGuests[0].Profiles[0].Item;
                            guest.Gender = customer.gender.ToString();
                            guest.FirstName = customer.PersonName.firstName;
                            guest.LastName = customer.PersonName.lastName;

                        }
                        for (int i = 0; i < resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList.Length; i++)
                        {
                            if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].source == null)
                                reservationResult.ConfirmationNumber = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].Value;
                            else if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].source == "RESVID")
                                reservationResult.ResvNameId = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].UniqueIDList[i].Value;
                        }
                        reservationResult.Status = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].computedReservationStatus.ToString();
                        reservationResult.AdultCount = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].GuestCounts.GuestCount[0].count;
                        reservationResult.ChildrenCount = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].GuestCounts.GuestCount[1].count;

                        reservationResult.Guests = new List<Guest>();
                        reservationResult.Guests.Add(guest);

                        reservationResult.CheckInDate = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].TimeSpan.StartDate;
                        reservationResult.CheckOutDate = Convert.ToDateTime(resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].TimeSpan.Item);
                        //if (resultBooking.FutureBookingSummaryResponse.HotelReservations[0].RoomStays[0].ExpectedCharges != null)
                        //    reservationResult.Balance = resultBooking.FutureBookingSummaryResponse.HotelReservations[0].RoomStays[0].ExpectedCharges.;
                        reservationResult.Total = Convert.ToDecimal(resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].Total.Value);
                        reservationResult.CurrencyCode = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].Total.currencyCode;

                        GuestRoom room = new Models.GuestRoom();
                        OWSCloudReservationService.RoomType roomType = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0];
                        OWSCloudReservationService.Paragraph t = roomType.RoomTypeDescription;
                        room.RoomName = ((OWSCloudReservationService.Text)t.Items[0]).Value;
                        room.Description = ((OWSCloudReservationService.Text)t.Items[0]).Value;
                        room.RoomTypeName = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].roomTypeCode;
                        if (resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].RoomNumber != null)
                            room.RoomNumber = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomTypes[0].RoomNumber[0].ToString();

                        room.Rate = resultBooking.FutureBookingSummaryResponse.HotelReservations[index].RoomStays[0].RoomRates[0].Rates[0].Base.Value;
                        room.Adults = reservationResult.AdultCount;
                        room.Children = reservationResult.ChildrenCount;
                        room.StartDate = reservationResult.CheckInDate;
                        room.EndDate = reservationResult.CheckOutDate;
                        room.CurrencyCode = reservationResult.CurrencyCode;
                        reservationResult.Rooms.Add(room);
                    }

                }
                else
                {
                    return null;
                }
                if (balance)
                {
                    OWSCloudResvAdvancedService.InvoiceRequest invoiceRequest = new InvoiceRequest();
                    var resv = new OWSCloudResvAdvancedService.UniqueID();
                    resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
                    resv.source = "RESV_NAME_ID";
                    resv.Value = reservationResult.ResvNameId.ToString();
                    ReservationRequestBase resvReq = new ReservationRequestBase();
                    resvReq.HotelReference = hotelAdv;
                    resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
                    resvReq.ReservationID[0] = resv;

                    invoiceRequest.ReservationRequest = new ReservationRequestBase();
                    invoiceRequest.ReservationRequest.ReservationID = resvReq.ReservationID;
                    invoiceRequest.ReservationRequest.HotelReference = hotelAdv;
                    try
                    {
                        var result = (OWSCloudResvAdvancedService.InvoiceResponse1)ExecuteResvAdvancedRequest(invoiceRequest, "Invoice");
                        if (result.InvoiceResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        {
                            if (result.InvoiceResponse.Invoice != null)
                            {
                                reservationResult.BillingWindows = new List<Guest>();
                                reservationResult.Balance = 0;
                                reservationResult.BillingCount = result.InvoiceResponse.Invoice.Length;
                                for (int i = 0; i < result.InvoiceResponse.Invoice.Length; i++)
                                {
                                    reservationResult.Balance = reservationResult.Balance + Convert.ToDecimal(result.InvoiceResponse.Invoice[i].CurrentBalance.Value);
                                    if (result.InvoiceResponse.Invoice[i].ProfileIDs != null && result.InvoiceResponse.Invoice[i].ProfileIDs.Length > 0)
                                    {
                                        Guest guest = new Guest();
                                        guest.ProfileId = result.InvoiceResponse.Invoice[i].ProfileIDs[0].Value;
                                        reservationResult.BillingWindows.Add(guest);
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }



                return reservationResult;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        //private async Task<Reservation> GetInvoice(string resvNameId)
        //{
        //    Reservation reservationResult = new Reservation();
        //    headerAdv.timeStamp = DateTime.Now;
        //    headerAdv.transactionID = GetNextUniqueId("SANI");
        //    try
        //    {
        //        var resv = new OWSCloudResvAdvancedService.UniqueID();
        //        resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
        //        resv.source = "RESV_NAME_ID";
        //        resv.Value = resvNameId;
        //        ReservationRequestBase resvReq = new ReservationRequestBase();
        //        resvReq.HotelReference = hotelAdv;
        //        resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
        //        resvReq.ReservationID[0] = resv;
        //        OWSCloudResvAdvancedService.InvoiceRequest invoiceRequest = new InvoiceRequest();
                
        //        invoiceRequest.ReservationRequest = new ReservationRequestBase();
        //        invoiceRequest.ReservationRequest.ReservationID = resvReq.ReservationID;
        //        invoiceRequest.ReservationRequest.HotelReference = hotelAdv;
        //        try
        //        {

        //            var result = await owsAdvClient.InvoiceAsync(headerAdv, invoiceRequest);
        //            if (result.InvoiceResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //            {
        //                reservationResult.Balance = result.InvoiceResponse.ExpectedCharges.CurrentBalance.Value;
        //                reservationResult.Total = result.InvoiceResponse.ExpectedCharges.TotalCharges.Value;
        //                reservationResult.CurrencyCode = result.InvoiceResponse.ExpectedCharges.CurrentBalance.currencyCode;
        //                return reservationResult;
        //            }
        //            else
        //                return null;

        //        }
        //        catch (Exception ex)
        //        {
        //            return null;

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }

        //}
        [HttpGet]
        [Route("reservation")]
        public async Task<Reservation> GetReservationData(LookupReservationRequest request)
        {
            try
            {
                var reservationId = new OWSCloudReservationService.UniqueID();
                reservationId.type = OWSCloudReservationService.UniqueIDType.INTERNAL;
                reservationId.Value = request.ReservationID; // "23900";
                var result = FindReservation(reservationId);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        //[HttpPost]
        //[Route("checkin")]
        //public async Task<IActionResult> CheckIn(LookupReservationRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ReservationID))
        //    {
        //        return BadRequest("ReservationId parameter should not be null or empty.");
        //    }

        //    var reservation = await GetReservationData(request);

        //    if (reservation == null)
        //    {
        //        return StatusCode(StatusCodes.Status404NotFound, "error retrrieving reservation");
        //    }
        //    switch (reservation.Status.ToUpper())
        //    {
        //        case "NOSHOW":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Reservation is pending confirmation.");
        //        case "INHOUSE":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked in.");
        //        case "CANCELED":
        //            return StatusCode(StatusCodes.Status403Forbidden, "This reservation was canceled.");
        //        case "CHECKEDOUT":
        //            return StatusCode(StatusCodes.Status403Forbidden, "Guest already checked out");
        //        case "RESERVED":
        //            break;
        //        default:
        //            break;
        //    }

        //    if (reservation.CheckOutDate < DateTime.Now.Date)
        //    {
        //        //return StatusCode(StatusCodes.Status403Forbidden, "Reservation End date is expired.");
        //    }
        //    if (reservation.CheckInDate > DateTime.Now.Date)
        //    {
        //        return StatusCode(StatusCodes.Status403Forbidden, "Reservation Checkin date is not here yet.");
        //    }
        //    if (reservation.Rooms == null || !reservation.Rooms.Any())
        //    {
        //        return StatusCode(StatusCodes.Status403Forbidden, "ReservationId does not contains rooms.");
        //    }

        //    try
        //    {
        //        if (String.IsNullOrEmpty(reservation.Rooms[0].RoomNumber))
        //        {
        //            OWSCloudReservationService.AssignRoomRequest myRequest = new OWSCloudReservationService.AssignRoomRequest();
        //            myRequest.HotelReference = hotel;
        //            var resv1 = new OWSCloudReservationService.UniqueID();
        //            resv1.type = OWSCloudReservationService.UniqueIDType.EXTERNAL;
        //            resv1.source = "RESV_NAME_ID";
        //            resv1.Value = reservation.ResvNameId.ToString();
        //            myRequest.ResvNameId = resv1;
        //            myRequest.StationID = "KIOSK";
        //            try
        //            {
        //                header.timeStamp = DateTime.Now;
        //                header.transactionID = GetNextUniqueId("SANI");
        //                var result = await owsClient.AssignRoomAsync(header, myRequest);
        //                if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                    reservation.Rooms[0].RoomNumber = result.AssignRoomResponse.RoomNoAssigned;
        //            }
        //            catch (Exception ex)
        //            {
        //            }



        //        }

        //        var resv = new OWSCloudResvAdvancedService.UniqueID();
        //        resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
        //        resv.source = "RESV_NAME_ID";
        //        resv.Value = reservation.ResvNameId.ToString();
        //        ReservationRequestBase resvReq = new ReservationRequestBase();
        //        resvReq.HotelReference = hotelAdv;
        //        resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
        //        resvReq.ReservationID[0] = resv;
        //        checkinRequest.ReservationRequest = resvReq;
        //        try
        //        {
        //            headerAdv.timeStamp = DateTime.Now;
        //            headerAdv.transactionID = GetNextUniqueId("SANI");

        //            var result = await owsAdvClient.CheckInAsync(headerAdv, checkinRequest);
        //            if (result.CheckInResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok(result);
        //            else
        //                return BadRequest(result.CheckInResponse.Result.Text[0].Value);

        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);

        //        }
        //        //var result = await _mobileKeyService.SaveUserEndpoint(_userContext.Profile.UserId, request.ReservationID);

        //        //return Ok(result);
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}
        [HttpPost]
        [Route("checkinOnly")]
        public async Task<IActionResult> CheckInOnly(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }


            var resv = new OWSCloudResvAdvancedService.UniqueID();
            resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
            resv.source = "RESV_NAME_ID";
            resv.Value = request.ReservationID.ToString();
            ReservationRequestBase resvReq = new ReservationRequestBase();
            resvReq.HotelReference = hotelAdv;
            resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
            resvReq.ReservationID[0] = resv;
            checkinRequest.ReservationRequest = resvReq;

            if (String.IsNullOrEmpty(request.RoomID) || (request.RoomID == "Auto-assign"))
            {
                OWSCloudReservationService.AssignRoomRequest myRequest = new OWSCloudReservationService.AssignRoomRequest();
                myRequest.HotelReference = hotel;
                var resv1 = new OWSCloudReservationService.UniqueID();
                resv1.type = OWSCloudReservationService.UniqueIDType.EXTERNAL;
                resv1.source = "RESV_NAME_ID";
                resv1.Value = request.ReservationID.ToString();
                myRequest.ResvNameId = resv1;
                myRequest.StationID = "KIOSK";
                try
                {
                    var result = (OWSCloudReservationService.AssignRoomResponse1)ExecuteReservationRequest(myRequest, "AssignRoom");
                    if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        request.RoomID = result.AssignRoomResponse.RoomNoAssigned;
                }
                catch (Exception ex)
                {
                }



            }

            try
            {
                var result = (OWSCloudResvAdvancedService.CheckInResponse1)ExecuteResvAdvancedRequest(checkinRequest, "CheckIn");
                if (result.CheckInResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                    return Ok(result);
                else
                {
                    if (result.CheckInResponse.Result.Text != null)
                        return BadRequest(result.CheckInResponse.Result.Text[0].Value);
                    else
                        return BadRequest("Error Check In");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpPost]
        [Route("checkout")]
        public async Task<IActionResult> CheckOut(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            try
            {


                var resv = new OWSCloudResvAdvancedService.UniqueID();
                resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = request.ReservationID.ToString();
                ReservationRequestBase resvReq = new ReservationRequestBase();
                resvReq.HotelReference = hotelAdv;
                resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
                resvReq.ReservationID[0] = resv;
                Guest guest1 = null;
                Guest guest2 = null;
                if (request.Profile1 != null && request.Profile2 != null)
                {
                    if (request.Profile1 != request.Profile2)
                    {
                        guest1 = await GetGuestProfile(request.Profile1);
                        guest2 = await GetGuestProfile(request.Profile2);
                        if (guest1.Email != null && guest2.Email != null && guest1.Email != guest2.Email)
                        {
                            OWSCloudNameService.OGHeader headerName = new OWSCloudNameService.OGHeader();
                            if (guest2.ProfileId != request.Profile2)
                            {
                                OWSCloudNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
                                reqUpdateEmail.NameEmail = new OWSCloudNameService.NameEmail();
                                reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(guest2.ProfileId);
                                reqUpdateEmail.NameEmail.operaIdSpecified = true;
                                reqUpdateEmail.NameEmail.Value = guest1.Email;
                                reqUpdateEmail.NameEmail.primary = true;

                                try
                                {
                                    var result = (OWSCloudNameService.UpdateEmailResponse1)ExecuteNameRequest(reqUpdateEmail, "UpdateEmail");
                                    if (result.UpdateEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result.UpdateEmailResponse.Result.Text[0].Value);
                                }
                                catch (Exception e)
                                {

                                }

                            }
                            else
                            {
                                OWSCloudNameService.InsertEmailRequest reqInsertEmail = new InsertEmailRequest();
                                var resvInsert = new OWSCloudNameService.UniqueID();
                                resvInsert.type = OWSCloudNameService.UniqueIDType.INTERNAL;
                                resvInsert.Value = guest2.ProfileId;
                                reqInsertEmail.NameID = resvInsert;

                                reqInsertEmail.NameEmail = new OWSCloudNameService.NameEmail();
                                reqInsertEmail.NameEmail.operaIdSpecified = false;
                                reqInsertEmail.NameEmail.Value = guest1.Email;
                                reqInsertEmail.NameEmail.primary = true;

                                try
                                {
                                    var result2 = (OWSCloudNameService.InsertEmailResponse1)ExecuteNameRequest(reqInsertEmail, "InsertEmail");
                                    if (result2.InsertEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result2.InsertEmailResponse.Result.Text[0].Value);
                                    else
                                        guest2.ProfileId = result2.InsertEmailResponse.Result.IDs[0].operaId.ToString();
                                }
                                catch (Exception e)
                                {

                                }

                            }
                        }
                        else
                        {

                        }
                    }
                }

                checkoutRequest.ReservationRequest = resvReq;
                checkoutRequest.overrideEmailPrivacy = true;
                checkoutRequest.EmailFolio = true;
                checkoutRequest.EmailFolioSpecified = true;
                checkoutRequest.overrideEmailPrivacySpecified = true;

                try
                {
                    var result = (OWSCloudResvAdvancedService.CheckOutResponse1)ExecuteResvAdvancedRequest(checkoutRequest, "CheckOut");
                    if (result.CheckOutResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                    {
                        if (guest1 != null && guest2 != null && guest1.Email != guest2.Email)
                        {
                            OWSCloudNameService.OGHeader headerName = new OWSCloudNameService.OGHeader();
                            if (guest2.ProfileId != request.Profile2)
                            {
                                OWSCloudNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
                                reqUpdateEmail.NameEmail = new OWSCloudNameService.NameEmail();
                                reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(guest2.ProfileId);
                                if (guest2.Email.Length > 0)
                                    reqUpdateEmail.NameEmail.Value = guest2.Email;
                                else
                                    reqUpdateEmail.NameEmail.Value = " ";

                                reqUpdateEmail.NameEmail.operaIdSpecified = true;
                                reqUpdateEmail.NameEmail.primary = true;

                                try
                                {
                                    var result2 = (OWSCloudNameService.UpdateEmailResponse1)ExecuteNameRequest(reqUpdateEmail, "UpdateEmail");
                                    if (result2.UpdateEmailResponse.Result.resultStatusFlag.ToString() != "SUCCESS")
                                        return BadRequest(result2.UpdateEmailResponse.Result.Text[0].Value);
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }

                        return Ok("Success");
                    }
                    else
                        return BadRequest(result.CheckOutResponse.Result.Text[0].Value);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        //[HttpPost]
        //[Route("update-email")]
        //public async Task<IActionResult> UpdateEmail(UpdateProfileEmailRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ProfileID))
        //    {
        //        return BadRequest("ProfileId parameter should not be null or empty.");
        //    }

        //    try
        //    {
        //        OWSCloudNameService.UpdateEmailRequest reqUpdateEmail = new UpdateEmailRequest();
        //        reqUpdateEmail.NameEmail.operaId = Convert.ToInt64(request.ProfileID);
        //        reqUpdateEmail.NameEmail.Value = request.Email;
        //        OWSCloudNameService.OGHeader headerName = new OWSCloudNameService.OGHeader();
                
        //        headerName.timeStamp = DateTime.Now;
        //        headerName.transactionID = GetNextUniqueId("SANI");

        //        try
        //        {
        //            var result = await owsNameClient.UpdateEmailAsync(headerName, reqUpdateEmail);
        //            if (result.UpdateEmailResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok("Success");
        //            else
        //                return BadRequest(result.UpdateEmailResponse.Result.Text[0].Value);
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}
        private async Task<Guest> GetGuestProfile(string profileId)
        {
            OWSCloudNameService.FetchEmailListRequest reqEmail = new FetchEmailListRequest();
            OWSCloudNameService.UniqueID id = new OWSCloudNameService.UniqueID();
            id.type = OWSCloudNameService.UniqueIDType.INTERNAL;
            id.Value = profileId;
            reqEmail.NameID = id;

            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = GetNextUniqueId("SANI");
            Guest guest = new Guest();
            try
            {
                var result = (OWSCloudNameService.FetchEmailListResponse1)ExecuteNameRequest(reqEmail, "FetchEmail");
                if (result.FetchEmailListResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                {
                    if (result.FetchEmailListResponse.NameEmailList != null)
                    {
                        guest.ProfileId = result.FetchEmailListResponse.NameEmailList[0].operaId.ToString();
                        guest.Email = result.FetchEmailListResponse.NameEmailList[0].Value;
                    }
                    else
                    {
                        guest.ProfileId = profileId;
                        guest.Email = "";
                    }
                }
                return guest;
            }
            catch (Exception ex)
            {
                return guest;
            }
        }
        //[HttpPost]
        //[Route("profile")]
        //public async Task<IActionResult> GetProfile(UpdateProfileEmailRequest request)
        //{
        //    if (request == null)
        //    {
        //        return BadRequest("Request parameter should not be null.");
        //    }

        //    if (string.IsNullOrWhiteSpace(request.ProfileID))
        //    {
        //        return BadRequest("ProfileId parameter should not be null or empty.");
        //    }

        //    try
        //    {
        //        OWSCloudNameService.FetchEmailListRequest reqEmail = new FetchEmailListRequest();
        //        OWSCloudNameService.UniqueID id = new OWSCloudNameService.UniqueID();
        //        id.type = OWSCloudNameService.UniqueIDType.INTERNAL;
        //        id.Value = request.ProfileID;
        //        reqEmail.NameID = id;

        //        headerName.timeStamp = DateTime.Now;
        //        headerName.transactionID = GetNextUniqueId("SANI");

        //        try
        //        {
        //            var result = await owsNameClient.FetchEmailListAsync(headerName, reqEmail);
        //            if (result.FetchEmailListResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //                return Ok(result);
        //            else
        //                return BadRequest(result.FetchEmailListResponse.Result.Text[0].Value);
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //    catch (ArgumentException ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, e);
        //    }
        //}

        [HttpPost]
        [Route("guest-messages")]
        public async Task<IActionResult> GuestMessages(LookupReservationRequest request)
        {
            if (request == null)
            {
                return BadRequest("Request parameter should not be null.");
            }

            if (string.IsNullOrWhiteSpace(request.ReservationID))
            {
                return BadRequest("ReservationId parameter should not be null or empty.");
            }

            try
            {
                var resv = new OWSCloudResvAdvancedService.UniqueID();
                resv.type = OWSCloudResvAdvancedService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = request.ReservationID.ToString();
                ReservationRequestBase resvReq = new ReservationRequestBase();
                resvReq.HotelReference = hotelAdv;
                resvReq.ReservationID = new OWSCloudResvAdvancedService.UniqueID[1];
                resvReq.ReservationID[0] = resv;
                checkinRequest.ReservationRequest = resvReq;
                try
                {
                    OWSCloudResvAdvancedService.GuestMessagesRequest guestRequest = new GuestMessagesRequest();

                    guestRequest.ReservationRequest = new ReservationRequestBase();
                    guestRequest.ReservationRequest.HotelReference = hotelAdv;
                    //var Ids = new OWSResvAdvancedService.UniqueID[1];
                    //Ids[0] = resv;
                    guestRequest.ReservationRequest = resvReq;
                    //guestRequest.ReservationRequest.ReservationID = Ids;


                    var result = (OWSCloudResvAdvancedService.GuestMessagesResponse1)ExecuteResvAdvancedRequest(guestRequest, "GuestMessages");
                    if (result.GuestMessagesResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        return Ok(result);
                    else
                        return BadRequest(result.GuestMessagesResponse.Result.Text[0].Value);

                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);

                }
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e);
            }
        }

        //[HttpGet]
        //[Route("ReservationList")]
        //public async Task<JObject> GetReservationList()
        //{
        //    try
        //    {
        //        headerAdv.timeStamp = DateTime.Now;
        //        try
        //        {
        //            OWSCloudResvAdvancedService.FetchQueueReservationsRequest request = new FetchQueueReservationsRequest();
        //            request.HotelReference = hotelAdv;
        //            headerAdv.transactionID = "1b6db1d6-2d57-4856-b7c0-b7e87411209a";
        //            var resultStatus = await owsAdvClient.FetchQueueReservationsAsync(headerAdv, request);
        //            if (resultStatus.FetchQueueReservationsResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
        //            {

        //                return JObject.FromObject(resultStatus);
        //            }
        //            else
        //                return null;

        //        }
        //        catch (Exception e)
        //        {
        //            return null;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        //[HttpPost]
        //[Route("status")]
        //public async Task<JObject> SaveReservationStatus(ReservationStatusRequest request)
        //{
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            httpClient.DefaultRequestHeaders.Authorization =
        //                new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
        //            httpClient.BaseAddress = _apiBaseUrl;

        //            using (var httpRequest = new HttpRequestMessage(HttpMethod.Put, "api/v1.1/putReservation"))
        //            {
        //                httpRequest.Content = GetFormUrlContentFromRequest(request);
        //                using (var response = await httpClient
        //                    .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
        //                    .ConfigureAwait(false))
        //                {
        //                    response.EnsureSuccessStatusCode();
        //                    return JObject.Parse(await response.Content.ReadAsStringAsync());
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        //[HttpPost]
        //[Route("payment")]
        //public async Task<JObject> SavePayment(ReservationPaymentRequest request)
        //{
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            httpClient.DefaultRequestHeaders.Authorization =
        //                new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
        //            httpClient.BaseAddress = _apiBaseUrl;

        //            using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postPayment"))
        //            {
        //                httpRequest.Content = GetFormUrlContentFromRequest(request);
        //                using (var response = await httpClient
        //                    .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
        //                    .ConfigureAwait(false))
        //                {
        //                    response.EnsureSuccessStatusCode();
        //                    return JObject.Parse(await response.Content.ReadAsStringAsync());
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        //[HttpPost]
        //[Route("voidPayment")]
        //public async Task<JObject> SavePayment(VoidReservationPaymentRequest request)
        //{
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            httpClient.DefaultRequestHeaders.Authorization =
        //                new AuthenticationHeaderValue("Bearer", await _cloudbedsClient.GetAccessToken());
        //            httpClient.BaseAddress = _apiBaseUrl;

        //            using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "api/v1.1/postVoidPayment"))
        //            {
        //                httpRequest.Content = GetFormUrlContentFromRequest(request);
        //                using (var response = await httpClient
        //                    .SendAsync(httpRequest, HttpCompletionOption.ResponseHeadersRead)
        //                    .ConfigureAwait(false))
        //                {
        //                    response.EnsureSuccessStatusCode();
        //                    return JObject.Parse(await response.Content.ReadAsStringAsync());
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        //[HttpPost]
        //[Route("reservations")]
        //public async Task<JObject> GetReservations(GetReservationsRequest request)
        //{
        //    try
        //    {
        //        LookupReservationRequest luRequest = new LookupReservationRequest();
        //        luRequest.ReservationID = request.
        //        var reservation = await GetReservationData(request);
        //        return JObject.FromObject(reservation);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}
        [HttpPost]
        [Route("assign-room")]
        public async Task<JObject> AssignRoom(LookupReservationRequest request)
        {
            try
            {
                var reservation = await GetReservationData(request);
                OWSCloudReservationService.AssignRoomRequest myRequest = new OWSCloudReservationService.AssignRoomRequest();
                myRequest.HotelReference = hotel;
                var resv = new OWSCloudReservationService.UniqueID();
                resv.type = OWSCloudReservationService.UniqueIDType.EXTERNAL;
                resv.source = "RESV_NAME_ID";
                resv.Value = reservation.ResvNameId.ToString();
                myRequest.ResvNameId = resv;
                myRequest.StationID = "KIOSK";
                try
                {
                    var result = (OWSCloudReservationService.AssignRoomResponse1)ExecuteReservationRequest(myRequest, "AssignRoom");
                    if (result.AssignRoomResponse.Result.resultStatusFlag.ToString() == "SUCCESS")
                        return JObject.FromObject(result);
                    else
                        return JObject.Parse("Error Checking out...");
                }
                catch (Exception ex)
                {
                    return JObject.Parse(ex.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private FormUrlEncodedContent GetFormUrlContentFromRequest<T>(T request)
        {
            var keyValuePairArrays = new List<KeyValuePair<string, string>>();
            var properties = typeof(T).GetProperties().Where(p => p.Name != "AccessToken");
            foreach (var property in properties)
            {
                var value = property.GetValue(request);
                switch (property.PropertyType.Name)
                {
                    case "DateTime":
                        value = ((DateTime)property.GetValue(request)).ToString("yyyy-MM-dd");
                        break;

                    case "ReservationRoom[]":
                        var rooms = (ReservationRoom[])property.GetValue(request);
                        for (var i = 0; i < rooms.Length; i++)
                        {
                            var roomProperties = typeof(ReservationRoom).GetProperties();
                            foreach (var roomProperty in roomProperties)
                            {
                                var roomPropertyValue = roomProperty.GetValue(rooms[i]);
                                keyValuePairArrays.Add(new KeyValuePair<string, string>(
                                    $"{CamelCase(property.Name)}[{i}][{CamelCase(roomProperty.Name)}]", roomPropertyValue.ToString()));
                            }
                        }

                        continue;
                }

                keyValuePairArrays.Add(new KeyValuePair<string, string>($"{CamelCase(property.Name)}", value.ToString()));
            }

            return new FormUrlEncodedContent(keyValuePairArrays.ToArray());
        }

        private string CamelCase(string name)
        {
            return $"{name[0].ToString().ToLower()}{name.Substring(1)}";
        }

        private object ExecuteReservationRequest(object request, string action)
        {
            header.timeStamp = DateTime.Now;
            header.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {
                try
                {
                    switch (action.ToLower())
                    {
                        case "futurebookingsummary":
                            return owsClient.FutureBookingSummaryAsync(header, (OWSCloudReservationService.FutureBookingSummaryRequest)request).GetAwaiter().GetResult();
                        case "assignroom":
                            return owsClient.AssignRoomAsync(header, (OWSCloudReservationService.AssignRoomRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                //System.ServiceModel.BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
                System.ServiceModel.BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
                //System.ServiceModel.WSHttpBinding binding = new WSHttpBinding(SecurityMode.Transport);
                //CustomBinding binding = new CustomBinding();
                //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                EndpointAddress address = new EndpointAddress("https://ove-osb.microsdc.us:9015/OPERA9OSB/opera/OWS_WS_51/Reservation?wsdl");
                owsClient = new OWSCloudReservationService.ReservationPortTypeClient(binding, address);
                using (var scope = new OperationContextScope(owsClient.InnerChannel))
                {

                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(header.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    var soapInspector = new SoapInspectorBehavior();
                    try
                    {
                        header.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        header.Authentication.UserCredentials.UserPassword = null;
                        header.Authentication.UserCredentials.Domain = "OWS";
                        
                        owsClient.Endpoint.EndpointBehaviors.Add(soapInspector);
                        switch (action.ToLower())
                        {
                            case "futurebookingsummary":
                                return owsClient.FutureBookingSummaryAsync(header, (OWSCloudReservationService.FutureBookingSummaryRequest)request).GetAwaiter().GetResult();
                            case "assignroom":
                                return owsClient.AssignRoomAsync(header, (OWSCloudReservationService.AssignRoomRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }
        private object ExecuteResvAdvancedRequest(object request, string action)
        {
            headerAdv.timeStamp = DateTime.Now;
            headerAdv.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {
                try
                {
                    switch (action.ToLower())
                    {
                        case "invoice": // token error
                            return owsAdvClient.InvoiceAsync(headerAdv, (OWSCloudResvAdvancedService.InvoiceRequest)request).GetAwaiter().GetResult();
                        case "checkin": // ok
                            return owsAdvClient.CheckInAsync(headerAdv, (OWSCloudResvAdvancedService.CheckInRequest)request).GetAwaiter().GetResult();
                        case "checkout":
                            return owsAdvClient.CheckOutAsync(headerAdv, (OWSCloudResvAdvancedService.CheckOutRequest)request).GetAwaiter().GetResult();
                        case "guestmessages":
                            return owsAdvClient.GuestMessagesAsync(headerAdv, (OWSCloudResvAdvancedService.GuestMessagesRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                using (var scope = new OperationContextScope(owsAdvClient.InnerChannel))
                {
                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(headerAdv.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    try
                    {
                        headerAdv.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        headerAdv.Authentication.UserCredentials.UserPassword = null;
                        headerAdv.Authentication.UserCredentials.Domain = "OWS";
                        switch (action.ToLower())
                        {
                            case "invoice": // token error
                                return owsAdvClient.InvoiceAsync(headerAdv, (OWSCloudResvAdvancedService.InvoiceRequest)request).GetAwaiter().GetResult();
                            case "checkin": // ok
                                return owsAdvClient.CheckInAsync(headerAdv, (OWSCloudResvAdvancedService.CheckInRequest)request).GetAwaiter().GetResult();
                            case "checkout":
                                return owsAdvClient.CheckOutAsync(headerAdv, (OWSCloudResvAdvancedService.CheckOutRequest)request).GetAwaiter().GetResult();
                            case "guestmessages":
                                return owsAdvClient.GuestMessagesAsync(headerAdv, (OWSCloudResvAdvancedService.GuestMessagesRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }
        private object ExecuteNameRequest(object request, string action)
        {
            headerName.timeStamp = DateTime.Now;
            headerName.transactionID = GetNextUniqueId("SANI");
            if (!_isCloud)
            {

                try
                {
                    switch (action.ToLower())
                    {
                        case "fetchaddress":
                            return owsNameClient.FetchAddressListAsync(headerName, (OWSCloudNameService.FetchAddressListRequest)request).GetAwaiter().GetResult();
                        case "updateemail":
                            return owsNameClient.UpdateEmailAsync(headerName, (OWSCloudNameService.UpdateEmailRequest)request).GetAwaiter().GetResult();
                        case "insertemail":
                            return owsNameClient.InsertEmailAsync(headerName, (OWSCloudNameService.InsertEmailRequest)request).GetAwaiter().GetResult();
                        case "fetchemail":
                            return owsNameClient.FetchEmailListAsync(headerName, (OWSCloudNameService.FetchEmailListRequest)request).GetAwaiter().GetResult();
                        default:
                            return null;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                using (var scope = new OperationContextScope(owsNameClient.InnerChannel))
                {
                    MessageHeaders messageHeadersElement = OperationContext.Current.OutgoingMessageHeaders;
                    messageHeadersElement.Add(new SoapSecurityHeader(headerName.transactionID, "OMNI79JEQP8ECSTRN89Q", "znVrzpY93SLtnpq6DyfN6UkptxXTfe", DateTime.UtcNow.AddMinutes(2).ToString("s") + "Z", DateTime.UtcNow.ToString("s") + "Z"));

                    try
                    {
                        headerName.Authentication.UserCredentials.UserName = "OMNI79JEQP8ECSTRN89Q@OVE.OPERA9";
                        headerName.Authentication.UserCredentials.UserPassword = null;
                        headerName.Authentication.UserCredentials.Domain = "OWS";
                        switch (action.ToLower())
                        {
                            case "fetchaddress":
                                return owsNameClient.FetchAddressListAsync(headerName, (OWSCloudNameService.FetchAddressListRequest)request).GetAwaiter().GetResult();
                            case "updateemail":
                                return owsNameClient.UpdateEmailAsync(headerName, (OWSCloudNameService.UpdateEmailRequest)request).GetAwaiter().GetResult();
                            case "insertemail":
                                return owsNameClient.InsertEmailAsync(headerName, (OWSCloudNameService.InsertEmailRequest)request).GetAwaiter().GetResult();
                            case "fetchemail":
                                return owsNameClient.FetchEmailListAsync(headerName, (OWSCloudNameService.FetchEmailListRequest)request).GetAwaiter().GetResult();
                            default:
                                return null;
                        }

                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
        }

    }
}