﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.Xml;

namespace Cloudbeds.WebApi.Controllers
{
    public class SoapSecurityHeader : MessageHeader
    {
        private readonly string _password, _username, _expiredDate, _createdDate;

        public SoapSecurityHeader(string id, string username, string password, string expired, string created)
        {
            _password = password;
            _username = username;
            _expiredDate = expired;
            _createdDate = created;
            this.Id = id;
        }

        public string Id { get; set; }

        public override string Name
        {
            get { return "Security"; }
        }

        public override string Namespace
        {
            get { return "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"; }
        }
        public override bool MustUnderstand
        {
            get
            {
                return true;
            }
        }
        protected override void OnWriteStartHeader(System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            writer.WriteStartElement("wsse", Name, Namespace);
            writer.WriteAttributeString("s", "mustUnderstand", "http://schemas.xmlsoap.org/soap/envelope/", "0");
            //writer.WriteAttributeString("s:mustUnderstand", "0");
            //writer.WriteAttributeString("mustUnderstand", "0");
            writer.WriteXmlnsAttribute("wsse", Namespace);
            writer.WriteXmlnsAttribute("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            //writer.WriteXmlnsAttribute("mustUnderstand", "0");
        }


        protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("wsu:Id", "UsernameToken-" + this.Id);
            //writer.WriteAttributeString("wsu:Id", "UsernameToken-FC74A697E2DA91DA43161349474033562");

            writer.WriteStartElement("wsse", "Username", Namespace);
            writer.WriteValue(_username);
            writer.WriteEndElement();

            writer.WriteStartElement("wsse", "Password", Namespace);
            writer.WriteAttributeString("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteValue(_password);
            writer.WriteEndElement();

            // <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"                  >f8nUe3YupTU5ISdCy3X9Gg==</wsse:Nonce>
            writer.WriteStartElement("wsse", "Nonce", null);
            writer.WriteAttributeString("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
            writer.WriteValue("f8nUe3YupTU5ISdCy3X9Gg==");
            writer.WriteEndElement();

            writer.WriteEndElement();

            writer.WriteStartElement("wsu", "Timestamp", null);
            writer.WriteAttributeString("wsu:Id", "TS-" + this.Id);
            //writer.WriteAttributeString("wsu:Id", "TS-FC74A697E2DA91DA43161349474033561");
            writer.WriteStartElement("wsu:Created");
            writer.WriteValue(_createdDate);
            writer.WriteEndElement();

            writer.WriteStartElement("wsse", "Expires", Namespace);
            writer.WriteValue(_expiredDate);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        #region IDispatchMessageInspector Members
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            Console.WriteLine("IDispatchMessageInspector.AfterReceiveRequest called.");
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            Console.WriteLine("IDispatchMessageInspector.BeforeSendReply called.");
        }
        #endregion


    }

    public class SoapMessageInspector : IClientMessageInspector
    {
        public string LastRequestXml { get; private set; }
        public string LastResponseXml { get; private set; }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            LastRequestXml = request.ToString();
            return request;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            LastResponseXml = reply.ToString();
        }
    }

    public class SoapInspectorBehavior : IEndpointBehavior
    {
        private readonly SoapMessageInspector inspector_ = new SoapMessageInspector();

        public string LastRequestXml => inspector_.LastRequestXml;
        public string LastResponseXml => inspector_.LastResponseXml;

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(inspector_);
        }
    }

    // Client message inspector  
    public class SimpleMessageInspector : IClientMessageInspector
    {
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            // Implement this method to inspect/modify messages after a message  
            // is received but prior to passing it back to the client
            Console.WriteLine("AfterReceiveReply called");
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            // Implement this method to inspect/modify messages before they
            // are sent to the service  
            Console.WriteLine("BeforeSendRequest called");
            return null;
        }
    }
    // Endpoint behavior  
    public class SimpleEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            // No implementation necessary  
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(new SimpleMessageInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // No implementation necessary  
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            // No implementation necessary  
        }
    }
    // Configuration element
    //public class SimpleBehaviorExtensionElement : BehaviorExtensionElement
    //{
    //    public override Type BehaviorType
    //    {
    //        get { return typeof(SimpleEndpointBehavior); }
    //    }

    //    protected override object CreateBehavior()
    //    {
    //        // Create the  endpoint behavior that will insert the message  
    //        // inspector into the client runtime  
    //        return new SimpleEndpointBehavior();
    //    }
    //}
}
