﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cloudbeds.Data;
using Cloudbeds.WebApi.Infrastructure;
using Cloudbeds.WebApi.Infrastructure.Extensions;
using Cloudbeds.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;
using System.IO;
using Stripe;
using CloudBaseWeb.Api.Entities;
using static Cloudbeds.WebApi.Controllers.Shared.SMSController;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace Cloudbeds.WebApi.Controllers
{
    [ApiController]
    [Route("api/arpon")]
    [EnableCors("All")]
    [AllowAnonymous]
    public class ArponController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly Uri _apiBaseUrl;
        private readonly MobileKeyDbContext _dbContext;
        private string _username = "trueomni";
        private string _password = "trueomni";
        public ArponController(IConfiguration configuration, MobileKeyDbContext dbContext)
        {
            _configuration = configuration;
            _apiBaseUrl = new Uri(configuration["ArponApi:BaseUrl"]);
            _dbContext = dbContext;
        }

        //[HttpPost]
        //[Route("getCustomer/{code}")]
        //public async Task<MewsGetCustomerResponse> GetCustomer(MewsGetCustomers req, string code)
        //{
        //    try
        //    {
        //        var cred = getAccess(code);
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = _apiBaseUrl;

        //            req.ClientToken = cred.ClientToken;
        //            req.AccessToken = cred.AccessToken;
        //            req.Client = cred.Client;
        //            req.Limitation = new MewsLimitation { Count = 10 };

        //            var objAsJson = JsonConvert.SerializeObject(req);
        //            var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
        //            HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/getAll", content);

        //            var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
        //            dynamic arreglo = JObject.Parse(jsonContent);

        //            return JsonConvert.DeserializeObject<MewsGetCustomerResponse>(jsonContent);

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }

        //}

        [HttpPost]
        [Route("addReservation/{code}")]
        public async Task<JObject> AddReservation(MewsReservation req, string code)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;

                    req.Reservations[0].ProductOrders = null;
                    req.Reservations[0].StartUtc = req.Reservations[0].StartUtc.AddHours(15).ToUniversalTime();
                    req.Reservations[0].EndUtc = req.Reservations[0].EndUtc.AddHours(12).ToUniversalTime();
                    req.Reservations[0].TotalNights = (req.Reservations[0].EndUtc.Date - req.Reservations[0].StartUtc.Date).Days;
                    if (string.IsNullOrEmpty(req.Reservations[0].VoucherCode))
                    {
                        req.Reservations[0].VoucherCode = null;
                    }
                    if (req.Reservations[0].ProductOrders != null && req.Reservations[0].ProductOrders.Count > 0)
                    {
                        try
                        {
                            for (int i = 0; i < req.Reservations[0].ProductOrders.Count; i++)
                            {
                                req.Reservations[0].ProductOrders[i].StartUtc = req.Reservations[0].StartUtc;
                                if (req.Reservations[0].ProductOrders[i].ProductId == "578f76a3-2906-4edf-a487-b15e00e70dfe")
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].StartUtc;
                                }
                                else
                                {
                                    req.Reservations[0].ProductOrders[i].EndUtc = req.Reservations[0].EndUtc;
                                }
                            }
                        }
                        catch (Exception ee)
                        {

                        }
                    }


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/add", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);
                    ReservationAddResult results = new ReservationAddResult();
                    results = JsonConvert.DeserializeObject<ReservationAddResult>(jsonContent);
                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("startReservation/{code}")]
        public async Task<JObject> StartReservation(MewsStartReservation req, string code)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/start", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("cancelReservation/{code}")]
        public async Task<JObject> CancelReservation(MewsCancelReservation req, string code)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    req.Notes = "Widget Cancel";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/cancel", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("updateReservation/{code}")]
        public async Task<JObject> UpdateReservation(MewsReservationUpdate data, string code)
        {
            try
            {
                MewsUpdateReservation req = new MewsUpdateReservation();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    req.ReservationUpdates = new List<MewsReservationUpdate>();
                    req.ReservationUpdates[0].ReservationId = data.ReservationId;
                    req.ReservationUpdates[0].StartUtc = data.StartUtc;
                    req.ReservationUpdates[0].EndUtc = data.EndUtc;
                    req.Reason = "Update Reservation from Widget";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/updateInterval", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [HttpPost]
        [Route("checkout")]
        public async Task<JObject> Checkout(ArponReservationRequest req)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/FrontOffice/ChangeStatus", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
        [HttpPost]
        [Route("checkin")]
        public async Task<JObject> Checkin(ArponReservationRequest req)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/FrontOffice/ChangeStatus", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    return arreglo;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        // POST api/<OhipController>
        [HttpPost]
        public void Post([FromBody] string value)
        {

        }

        // PUT api/<OhipController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OhipController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("getCustomerReservations/{code}")]
        public async Task<MewsReservationResponse> GetCustomerReservations(MewsCustomer req, string code)
        {
            var getCustomerData = new MewsGetCustomers()
            {
                Emails = new string[] { req.Email },
            };

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;


                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    dynamic arreglo = JObject.Parse(jsonContent);

                    var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

                    return result;

                }

            }
            catch (Exception e)
            {
                return null;
            }

        }
        [HttpPost]
        [Route("lookup")]
        public async Task<List<Reservation>> Lookup(LookupReservationRequest req, string code = "CASCADA-P")
        {
            Reservation reservation = null;
            List<Reservation> reservations = new List<Reservation>();
            List<GuestRoom> rooms = null;
            string[] reservationIds = new string[1];
            List<string> customerIds = new List<string>();
            string searchValue = "";
            var searchType = 0; // ReservationId

            if (!string.IsNullOrEmpty(req.Phone))
            {
                // Search by Phone 
                searchType = 2;
                searchValue = req.Phone;
            }
            else if (!string.IsNullOrEmpty(req.ReservationID))
            {
                if (req.ReservationID.IndexOf("-") == -1)
                {
                    // By Confirmation # 
                    searchType = 1;
                    searchValue = req.ReservationID;
                }
                else
                {
                    // Reservation Id
                    reservationIds[0] = req.ReservationID;
                    searchValue = req.ReservationID;
                }
            }
            else
            {
                // Other
                searchType = 9;
            }

            string[] states = new string[3];
            states[0] = "Confirmed";
            states[1] = "Started";
            states[2] = "Processed";
            MewsTimeInterval dDateRange = new MewsTimeInterval();
            //dDateRange.StartUtc = DateTime.Now.AddDays(-1);
            //dDateRange.EndUtc = DateTime.Now.AddDays(10);
            dDateRange.StartUtc = DateTime.Now.AddDays(80);
            dDateRange.EndUtc = DateTime.Now.AddDays(110);
            MewsReservationRequest request = new MewsReservationRequest()
            {
                States = states,
                CollidingUtc = dDateRange,
                ReservationIds = string.IsNullOrEmpty(reservationIds[0]) ? null : reservationIds

            };
            using (var client = new HttpClient())
            {
                client.BaseAddress = _apiBaseUrl;

                request.Limitation = new MewsLimitation { Count = 10 };
                var objAsJson = JsonConvert.SerializeObject(request);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                dynamic arreglo = JObject.Parse(jsonContent);

                var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);

                MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                {
                    Limitation = new MewsLimitation { Count = 10 }
                };
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    if (searchType > 0)
                    {
                        // not using reservationId
                        switch (searchType)
                        {
                            case 1: // ConfirmationNumber
                                result.Reservations = result.Reservations.Where(r => r.ReservationNumber == searchValue).ToList();
                                break;
                            case 2: // Phone
                                var customerIds_temp = result.Reservations.Select(r => r.AccountId.ToString()).ToList();
                                var getCustomerDataSearch = new MewsGetCustomers()
                                {
                                    CustomerIds = customerIds_temp.ToArray()
                                };

                                result.Reservations = result.Reservations.Where(r => r.AccountType == "Customer" && customerIds.Contains(r.AccountId.ToString())).ToList();
                                break;
                        }
                    }

                    if (result.Reservations != null && result.Reservations.Count > 0)
                    {
                    }

                }
                if (result.Reservations != null && result.Reservations.Count > 0)
                {
                    result.Reservations = result.Reservations.OrderBy(x => x.StartUtc).ThenBy(y => y.EndUtc).ToList();
                    for (int i = 0; i < result.Reservations.Count; i++)
                    {
                    }
                }
                return reservations;

            }

        }
        [HttpPost]
        [Route("getReservation/{reservationId}")]
        public async Task<ResDetails> GetReservation(String reservationId)
        {
            string[] reservationIds = new string[1];
            reservationIds[0] = reservationId;


            string[] states = new string[3];
            states[0] = "Confirmed";
            states[1] = "Started";
            states[2] = "Processed";
            MewsTimeInterval dDateRange = new MewsTimeInterval();
            //dDateRange.StartUtc = DateTime.Now.AddDays(-1);
            //dDateRange.EndUtc = DateTime.Now.AddDays(10);
            dDateRange.StartUtc = DateTime.Now.AddDays(80);
            dDateRange.EndUtc = DateTime.Now.AddDays(110);
            MewsReservationRequest request = new MewsReservationRequest()
            {
                States = states,
                CollidingUtc = dDateRange,
                ReservationIds = string.IsNullOrEmpty(reservationIds[0]) ? null : reservationIds

            };
            using (var client = new HttpClient())
            {
                client.BaseAddress = _apiBaseUrl;

                var objAsJson = JsonConvert.SerializeObject(request);
                var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/reservations/getAll/2023-06-06", content);

                var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                dynamic arreglo = JObject.Parse(jsonContent);

                var result = JsonConvert.DeserializeObject<MewsReservationResponse>(jsonContent);




                MewsReservationPriceRequest priceRequest = new MewsReservationPriceRequest()
                {
                    Limitation = new MewsLimitation { Count = 10 }
                };
                //if (result.Reservations != null && result.Reservations.Count > 0)
                //{
                //    var pReq = new MewsProductsRequest()
                //    {
                //        ServiceIds = new string[] { result.Reservations[0].ServiceId }
                //    };
                //    var products = await GetMewsProducts(pReq, code);

                //    priceRequest.ServiceOrderIds = new List<string>();

                //    int i = 0;
                //    result.Reservations[i].TotalNights = (Convert.ToDateTime(result.Reservations[i].EndUtc.Date) - Convert.ToDateTime(result.Reservations[i].StartUtc.Date)).Days;
                //    priceRequest.ServiceOrderIds.Clear();
                //    priceRequest.ServiceOrderIds.Add(result.Reservations[i].Id);
                //    var resPrices = await GetReservationItemsPrice(priceRequest, code);
                //    if (resPrices != null && resPrices.OrderItems != null && resPrices.OrderItems.Count > 0)
                //    {
                //        result.Reservations[i].GrossAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.GrossValue);
                //        result.Reservations[i].NetAmount = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.NetValue);
                //        result.Reservations[i].Tax = resPrices.OrderItems.Where(a => a.AccountingState == "Open").Sum(c => c.Amount.Tax); ;
                //        //result.Reservations[i].Items = resPrices.Reservations[0].Items.OrderBy(x => x.ConsumptionUTC).ToList();
                //        result.Reservations[i].OrderItems = resPrices.OrderItems.OrderBy(x => x.ConsumptionUTC).ToList();
                //        switch (result.Reservations[i].State.ToLower())
                //        {
                //            case "confirmed":
                //                result.Reservations[i].Ready = result.Reservations[i].StartUtc <= DateTime.Now.AddDays(1);
                //                break;
                //            case "started":
                //                result.Reservations[i].Ready = result.Reservations[i].EndUtc <= DateTime.Now.Date;
                //                break;
                //            default:
                //                result.Reservations[i].Ready = false;
                //                break;
                //        }
                //        result.Reservations[i].OrderItems.ForEach(p =>
                //        {
                //            var prod = products.CustomerProducts.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                //            if (prod == null)
                //                prod = products.Products.Where(a => a.Id == p.Data.Product.ProductId).FirstOrDefault();
                //            if (prod != null)
                //                p.RevenueType = prod.Name;
                //            else if (p.RevenueType == "Service")
                //                p.RevenueType = "Room Rate";
                //        });


                //    }
                //    string[] resourceIds = new string[1];
                //    resourceIds[0] = result.Reservations[0].AssignedResourceId;
                //    MewsResources resourceReq = new MewsResources();
                //    resourceReq.Resources = true;
                //    resourceReq.ResourceCategories = true;
                //    resourceReq.Inactive = false;
                //    resourceReq.ResourceIds = resourceIds;
                //    var resourceResponse = GetResources(resourceReq, code).Result;

                //    List<RoomDTO> room = new List<RoomDTO>();
                //    room.Add(new RoomDTO
                //    {
                //        Name = resourceResponse.ResourceCategories[0].Names.EnUs,
                //        Description = resourceResponse.ResourceCategories[0].Descriptions.EnUs,
                //        CategoryId = resourceResponse.ResourceCategories[0].Id.ToString(),
                //        ServiceId = resourceResponse.ResourceCategories[0].ServiceId.ToString()
                //    });

                //    List<string> seriveIds = new List<string>();
                //    seriveIds.Add(result.Reservations[0].ServiceId);

                //    await PrepareRatesData(room, seriveIds, "");
                //    await PreparePricingData(room, result.Reservations[0].StartUtc, result.Reservations[0].EndUtc);
                //    result.Reservations[0].AdultCount = result.Reservations[0].PersonCounts.Where(x => x.AgeCategoryId == "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();
                //    result.Reservations[0].ChildCount = result.Reservations[0].PersonCounts.Where(x => x.AgeCategoryId != "049668e2-4b4e-4caa-b5f8-b0c000720b84").Count();

                //    result.Reservations[0].Rooms = room;
                //}


                return result.Reservations[0];

            }

        }
        [HttpPost]
        [Route("uploadImage")]
        public async Task<string> UploadImage([FromBody] GuestPhotoRequest request)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = _apiBaseUrl;
                    MewsFileRequest req = new MewsFileRequest();
                    req.Limitation = new MewsLimitation { Count = 10 };
                    req.CustomerId = new Guid(request.GuestID);
                    req.Data = request.Image.Substring(request.Image.IndexOf(",") + 1);
                    req.Name = !string.IsNullOrEmpty(request.Name) ? request.Name : "IDScan";
                    req.Type = "image/png";
                    var objAsJson = JsonConvert.SerializeObject(req);
                    var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage tokenResponse = await client.PostAsync("api/connector/v1/customers/addFile", content);

                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    return jsonContent;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }


        private string phoneString(string input)
        {
            string result = "";
            if (!string.IsNullOrEmpty(input))
            {
                result = input.Replace("+", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");
            }
            return result;
        }

    }
}
