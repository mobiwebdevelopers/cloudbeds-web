using System;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider;
using AssaAbloy.ServiceProvider.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Cloudbeds.Data.Tests
{
    [TestClass]
    public class MobileKeyServiceTests
    {
        private readonly MobileKeyService _mobileKeyService;

        public MobileKeyServiceTests()
        {
            var assaAbloyServiceMoq = new Mock<IAssaAbloyService>();
            var visionlineServiceMoq = new Mock<IVisionlineService>();
            assaAbloyServiceMoq.Setup(m => m.GetEndpointStatus(It.IsAny<string>())).ReturnsAsync(new GetEndpointStatusResponse
            {
                EndpointStatus = "Any"
            });
            assaAbloyServiceMoq.Setup(m => m.GetInvitationCode(It.IsAny<string>())).ReturnsAsync(new GetInvitationCodeResponse
            {
                EndpointId = Guid.NewGuid().ToString(),
                InvitationCode = Guid.NewGuid().ToString()
            });

            _mobileKeyService = new MobileKeyService(
                new MobileKeyDbContext("data source=.\\SQLEXPRESS;initial catalog=MobileKey;Trusted_Connection=True;MultipleActiveResultSets=True"),
                null, visionlineServiceMoq.Object);
        }

        [TestMethod]
        public async Task GetUserInvitationCodesShouldNotReturnNull()
        {
            var result = await _mobileKeyService.GetUserEndpoints(1);

            Assert.IsNotNull(result);
        }
    }
}
