﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider.Models;
using Newtonsoft.Json;

using System.Collections.Generic;
using System.IO;
using System.Net;

namespace AssaAbloy.ServiceProvider
{
    public class VisionlineService : IVisionlineService
    {
        // The following key is used for PMS verification of card image at all True Omni kiosk installation: 1E6DA95073ED11EABC550242AC1300035D0E7AD8F60C489E825CD6BE73D9F162
        private readonly string _baseUrl;

        private readonly string _userName;

        private readonly string _password;

        private const string NewLine = "\n";

        private const string ContentMD5 = "Content-MD5";

        private const string ApiSessionUrl = "/api/v1/sessions";

        private const string ApiCardUrl = "/api/v1/cards?action=mobileAccess";
        private const string ApiCardsUrl = "/api/v1/cards";
        private const string ApiCreateCardUrl = "/api/v1/cards?action=encode&autoJoin=true&encoder=ENCODER&overwriteValidCard=true";
        private const string ApiCreateCardUrl2 = "/api/v1/cards?authenticationKey=true&autoJoin=true&cardType=Plus&pendingImage=true&sendOnline=false";
        private const string ApiGetCardUrl = "/api/v1/cards?action=readCard&encoder=ENCODER";
        private const string ApiGetCardsUrl = "/api/v1/cards";

        public VisionlineService(string baseUrl, string userName, string password)
        {
            _baseUrl = baseUrl;
            _userName = userName;
            _password = password;
        }

        public async Task<VisionlineSession> OpenSession(string url="",string username="",string password="")
        {

            using (var client = GetHttpClient())
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var content = GetHttpContent(new GetSessionRequest
                {
                    Username = _userName,
                    Password = _password
                });
                try
                {
                    var response = await client.PostAsync("/api/v1/sessions", content);
                    var responseString = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<VisionlineSession>(responseString);
                    return result;

                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public async Task<bool> CloseSession(VisionlineSession session)
        {
            using (var client = GetHttpClient())
            {
                var apiUrl = $"{ApiSessionUrl}/{session.Id}";
                AddMD5Header(client, session, apiUrl, "DELETE");

                var response = await client.DeleteAsync(apiUrl);
                var responseString = await response.Content.ReadAsStringAsync();

                return true;
            }
        }

        public async Task<CreateMobileKeyResponse> CreateMobileKey(VisionlineSession session, CreateMobileKeyRequest request)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            using (var client = GetHttpClient())
            {
                var content = GetHttpContent(request);

                var urlParams = request.AutoJoin ? "&autoJoin=true" : "&override=true";
                var url = $"{ApiCardUrl}{urlParams}";
                AddMD5Header(client, content, session, url, "POST");

                var response = await client.PostAsync(url, content);
                var responseString = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<CreateMobileKeyResponse>(responseString);
                return result;
            }
        }
        public async Task<GetCardInfoResponse> CancelPhysicalKey(VisionlineSession session, GetPhysicalKeysRequest keyRequest)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            //using (var client = GetHttpClient())
            //{
            //    var content = GetHttpContent(keyRequest);

            //    var urlParams = "?guestDoor=" + keyRequest.GuestDoor + "&startTime=" + keyRequest.StartTime + "&expireTime=" + keyRequest.ExpireTime;
            //    var url = $"{ApiGetCardsUrl}{urlParams}";
            //    //var url = $"{ApiGetCardsUrl}";
            //    AddMD5Header(client, content, session, url, "POST");

            //    try
            //    {
            //        var response = await client.PostAsync(url, content);
            //        var responseString = await response.Content.ReadAsStringAsync();

            //        var result = JsonConvert.DeserializeObject<GetCardInfoResponse>(responseString);
            //        return result;
            //    }
            //    catch(Exception e)
            //    {
            //        return null;
            //    }
            //}

            string strJsonBody = JsonConvert.SerializeObject(keyRequest, Formatting.Indented);
            string contentMd5 = MD5Hash(strJsonBody);
            UTF8Encoding encoding = new UTF8Encoding();

            string apiCard = ApiCardsUrl;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl + apiCard);
            //Disable certificate validity check
            request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };

            // REQUEST
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Date = DateTime.Now;
            request.Headers["Content-MD5"] = contentMd5;

            //Sign the request
            string newLine = "\n";
            string line1 = string.Format("GET{0}", newLine);
            string line2 = string.Format("{0}{1}", contentMd5, newLine);
            string line3 = string.Format("{0}{1}", "application/json; charset=utf-8", newLine);
            string line4 = string.Format("{0}{1}", request.Headers.Get("Date"), newLine);
            string line5 = string.Format("{0}", apiCard);
            string toSign = line1 + line2 + line3 + line4 + line5;

            //String to be signed with access key received from the /sessions call
            //Console.WriteLine(toSign);

            string hashed = HmacSha1Hash(toSign, session.AccessKey);
            string authorizationHeader = "AWS " + session.Id + ":" + hashed;

            Console.WriteLine(authorizationHeader);
            request.Headers["Authorization"] = authorizationHeader;

            try
            {
                byte[] bodyData = encoding.GetBytes(strJsonBody);
                request.ContentLength = bodyData.Length;
                using (System.IO.Stream s = request.GetRequestStream())
                {
                    s.Write(bodyData, 0, bodyData.Length);
                }
            }
            catch (Exception e)
            {
                return null;
            }

            // RESPONSE
            HttpWebResponse response = null;
            string jsonResponse = "";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (System.IO.Stream s = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                        Console.WriteLine("==================================================");
                        Console.WriteLine("CANCEL KEY REQUEST");
                        Console.WriteLine("==================================================");
                        var result = JsonConvert.DeserializeObject<GetCardInfoResponse>(jsonResponse);
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<GetCardInfoResponse> CreatePhysicalKey(VisionlineSession session, CreatePhysicalKeyRequest keyRequest)
        {
            string strJsonBody = JsonConvert.SerializeObject(keyRequest, Formatting.Indented);
            string contentMd5 = MD5Hash(strJsonBody);
            UTF8Encoding encoding = new UTF8Encoding();

            string apiCard = ApiCreateCardUrl;
            if (keyRequest.Override)
            {
                apiCard = apiCard.Replace("&overwriteValidCard=true", "&override=true&overwriteValidCard=true");
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl + apiCard);
            //Disable certificate validity check
            request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };

            // REQUEST
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Date = DateTime.Now;
            request.Headers["Content-MD5"] = contentMd5;

            //Sign the request
            string newLine = "\n";
            string line1 = string.Format("POST{0}", newLine);
            string line2 = string.Format("{0}{1}", contentMd5, newLine);
            string line3 = string.Format("{0}{1}", "application/json; charset=utf-8", newLine);
            string line4 = string.Format("{0}{1}", request.Headers.Get("Date"), newLine);
            string line5 = string.Format("{0}", apiCard);
            string toSign = line1 + line2 + line3 + line4 + line5;

            //String to be signed with access key received from the /sessions call
            //Console.WriteLine(toSign);

            string hashed = HmacSha1Hash(toSign, session.AccessKey);
            string authorizationHeader = "AWS " + session.Id + ":" + hashed;

            Console.WriteLine(authorizationHeader);
            request.Headers["Authorization"] = authorizationHeader;

            try
            {
                byte[] bodyData = encoding.GetBytes(strJsonBody);
                request.ContentLength = bodyData.Length;
                using (System.IO.Stream s = request.GetRequestStream())
                {
                    s.Write(bodyData, 0, bodyData.Length);
                }
            }
            catch (WebException e)
            {
                return null;
            }

            // RESPONSE
            HttpWebResponse response = null;
            string jsonResponse = "";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (System.IO.Stream s = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                        Console.WriteLine("==================================================");
                        Console.WriteLine("CREATE KEY REQUEST");
                        Console.WriteLine("==================================================");
                        var result = JsonConvert.DeserializeObject<GetCardInfoResponse>(jsonResponse);
                        return result;
                    }
                }
            }
            catch (WebException e)
            {
                return null;
            }

        }
        public async Task<GetCardInfoResponse> CreateKeyImage(VisionlineSession session, CreateKeyImageRequest keyRequest, bool overwrite = true)
        {
            string strJsonBody = JsonConvert.SerializeObject(keyRequest, Formatting.Indented);
            string contentMd5 = MD5Hash(strJsonBody);
            UTF8Encoding encoding = new UTF8Encoding();

            string apiCard = ApiCreateCardUrl2;
            // copies
            //if (!overwrite)
            //{
            //    apiCard = apiCard.Replace("override=true", "override=false");
            //}
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl + apiCard);
            //Disable certificate validity check
            request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };

            // REQUEST
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Date = DateTime.Now;
            request.Headers["Content-MD5"] = contentMd5;

            //Sign the request
            string newLine = "\n";
            string line1 = string.Format("POST{0}", newLine);
            string line2 = string.Format("{0}{1}", contentMd5, newLine);
            string line3 = string.Format("{0}{1}", "application/json; charset=utf-8", newLine);
            string line4 = string.Format("{0}{1}", request.Headers.Get("Date"), newLine);
            string line5 = string.Format("{0}", apiCard);
            string toSign = line1 + line2 + line3 + line4 + line5;

            //String to be signed with access key received from the /sessions call
            //Console.WriteLine(toSign);

            string hashed = HmacSha1Hash(toSign, session.AccessKey);
            string authorizationHeader = "AWS " + session.Id + ":" + hashed;

            Console.WriteLine(authorizationHeader);
            request.Headers["Authorization"] = authorizationHeader;

            try
            {
                byte[] bodyData = encoding.GetBytes(strJsonBody);
                request.ContentLength = bodyData.Length;
                using (System.IO.Stream s = request.GetRequestStream())
                {
                    s.Write(bodyData, 0, bodyData.Length);
                }
            }
            catch (WebException e)
            {
                return null;
            }

            // RESPONSE
            HttpWebResponse response = null;
            string jsonResponse = "";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (System.IO.Stream s = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                        Console.WriteLine("==================================================");
                        Console.WriteLine("CREATE KEY REQUEST");
                        Console.WriteLine("==================================================");
                        var result = JsonConvert.DeserializeObject<GetCardInfoResponse>(jsonResponse);
                        return result;
                    }
                }
            }
            catch (System.Exception e)
            {
                return null;
            }

        }
        public async Task<CreateMobileKeyResponse> GetCards(VisionlineSession session, string request)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            using (var client = GetHttpClient())
            {
                var content = GetHttpContent(request);

                var urlParams = request;
                var url = $"{ApiCardUrl}{urlParams}";
                AddMD5Header(client, content, session, url, "POST");

                var response = await client.PostAsync(url, content);
                var responseString = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<CreateMobileKeyResponse>(responseString);
                return result;
            }
        }
        public async Task<GetCardInfoResponse> GetCardInfo(VisionlineSession session)
        {

            string strJsonBody = "";
            string contentMd5 = MD5Hash(strJsonBody);
            UTF8Encoding encoding = new UTF8Encoding();

            string apiCard = ApiGetCardUrl ;// ApiCardsUrl + "action=readCard&encoder=ENCODER";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl + apiCard);
            //Disable certificate validity check
            request.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };

            // REQUEST
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Date = DateTime.Now;
            request.Headers["Content-MD5"] = contentMd5;

            //Sign the request
            string newLine = "\n";
            string line1 = string.Format("POST{0}", newLine);
            string line2 = string.Format("{0}{1}", contentMd5, newLine);
            string line3 = string.Format("{0}{1}", "application/json; charset=utf-8", newLine);
            string line4 = string.Format("{0}{1}", request.Headers.Get("Date"), newLine);
            string line5 = string.Format("{0}", apiCard);
            string toSign = line1 + line2 + line3 + line4 + line5;

            //String to be signed with access key received from the /sessions call
            //Console.WriteLine(toSign);

            string hashed = HmacSha1Hash(toSign, session.AccessKey);
            string authorizationHeader = "AWS " + session.Id + ":" + hashed;

            Console.WriteLine(authorizationHeader);
            request.Headers["Authorization"] = authorizationHeader;

            try
            {
                byte[] bodyData = encoding.GetBytes(strJsonBody);
                request.ContentLength = bodyData.Length;
                using (System.IO.Stream s = request.GetRequestStream())
                {
                    s.Write(bodyData, 0, bodyData.Length);
                }
            }
            catch (WebException e)
            {
                return null;
            }

            // RESPONSE
            HttpWebResponse response = null;
            string jsonResponse = "";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (System.IO.Stream s = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                        Console.WriteLine("==================================================");
                        Console.WriteLine("CREATE KEY REQUEST");
                        Console.WriteLine("==================================================");

                        return JsonConvert.DeserializeObject<GetCardInfoResponse>(jsonResponse);
                    }
                }
            }
            catch (WebException e)
            {
                return null;
            }

        }

        private HttpClient GetHttpClient()
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

            var client = new HttpClient(httpClientHandler);
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Date = DateTime.Now;

            return client;
        }

        private HttpContent GetHttpContent(object request)
        {
            if (request != null)
            {
                var requestStr = JsonConvert.SerializeObject(request, Formatting.Indented);
                var content = new StringContent(requestStr, Encoding.UTF8, "application/json");
                content.Headers.Add(ContentMD5, MD5Hash(requestStr));
                return content;
            }
            else
            {
                var requestStr = "";
                var content = new StringContent(requestStr, Encoding.UTF8, "application/json");
                content.Headers.Add(ContentMD5, MD5Hash(requestStr));
                return content;

            }

            //if (request != null && request.ToString().Length > 0)
            //{
            //    var requestStr = JsonConvert.SerializeObject(request, Formatting.Indented);
            //    var content = new StringContent(requestStr, Encoding.UTF8, "application/json"); 
            //    content.Headers.Add(ContentMD5, MD5Hash(requestStr));
            //    return content;
            //}
            //else
            //{
            //    return null;
            //}

        }

        private void AddMD5Header(HttpClient client, HttpContent content, VisionlineSession session, string apiUrl, string method)
        {
            string contentMd5 = MD5Hash("");
            if (content != null)
                contentMd5 = content.Headers.GetValues(ContentMD5).First();
            UTF8Encoding encoding = new UTF8Encoding();

            var hashedStr = new StringBuilder();
            hashedStr.Append($"{method}{NewLine}");
            hashedStr.Append($"{contentMd5}{NewLine}");
            hashedStr.Append($"application/json; charset=utf-8{NewLine}");
            var dateStr = client.DefaultRequestHeaders.GetValues("Date").First();
            hashedStr.Append($"{dateStr}{NewLine}");
            hashedStr.Append(apiUrl);

            //Sign the request
            string newLine = "\n";
            string line1 = string.Format("POST{0}", newLine);
            string line2 = string.Format("{0}{1}", contentMd5, newLine);
            string line3 = string.Format("{0}{1}", "application/json; charset=utf-8", newLine);
            string line4 = string.Format("{0}{1}", dateStr, newLine);
            string line5 = string.Format("{0}", apiUrl);
            string toSign = line1 + line2 + line3 + line4 + line5;


            var hashed = HmacSha1Hash(toSign, session.AccessKey);
            //var hashed = HmacSha1Hash(hashedStr.ToString(), session.AccessKey);
            var authorizationHeader = $"AWS {session.Id}:{hashed}";
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", authorizationHeader);
        }

        private void AddMD5Header(HttpClient client, VisionlineSession session, string apiUrl, string method)
        {
            var hashedStr = new StringBuilder();
            hashedStr.Append($"{method}{NewLine}");
            hashedStr.Append(NewLine);
            hashedStr.Append(NewLine);
            hashedStr.Append($"{client.DefaultRequestHeaders.GetValues("Date").First()}{NewLine}");
            hashedStr.Append(apiUrl);
            var hashed = HmacSha1Hash(hashedStr.ToString(), session.AccessKey);
            var authorizationHeader = $"AWS {session.Id}:{hashed}";
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", authorizationHeader);
        }

        private static string MD5Hash(string input)
        {
            System.Security.Cryptography.MD5 hs = System.Security.Cryptography.MD5.Create();
            byte[] bytes = Encoding.Default.GetBytes(input);
            string result = Convert.ToBase64String(hs.ComputeHash(bytes));
            return result;
        }

        private static string HmacSha1Hash(string stringToSign, string accessKey)
        {
            var keyBytes = Encoding.UTF8.GetBytes(accessKey);
            var inputBytes = Encoding.UTF8.GetBytes(stringToSign);
            var hmac = new HMACSHA1(keyBytes);
            var bits = hmac.ComputeHash(inputBytes);
            var result = Convert.ToBase64String(hmac.ComputeHash(inputBytes));
            return result;
        }
    }
}
