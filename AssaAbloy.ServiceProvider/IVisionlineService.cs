﻿using System.Threading.Tasks;
using AssaAbloy.ServiceProvider.Models;

namespace AssaAbloy.ServiceProvider
{
    public interface IVisionlineService
    {
        Task<VisionlineSession> OpenSession(string url = "", string username = "", string password = "");

        Task<bool> CloseSession(VisionlineSession session);

        Task<CreateMobileKeyResponse> CreateMobileKey(VisionlineSession session, CreateMobileKeyRequest request);
        Task<CreateMobileKeyResponse> GetCards(VisionlineSession session, string cardId);
        Task<GetCardInfoResponse> CreatePhysicalKey(VisionlineSession session, CreatePhysicalKeyRequest request);
        Task<GetCardInfoResponse> CreateKeyImage(VisionlineSession session, CreateKeyImageRequest request, bool overwrite = true);
        Task<GetCardInfoResponse> CancelPhysicalKey(VisionlineSession session, GetPhysicalKeysRequest request);
        Task<GetCardInfoResponse> GetCardInfo(VisionlineSession session);
    }
}
