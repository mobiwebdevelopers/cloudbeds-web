﻿using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class VisiOnlineInstance
    {
        [JsonProperty("SystemId")]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
