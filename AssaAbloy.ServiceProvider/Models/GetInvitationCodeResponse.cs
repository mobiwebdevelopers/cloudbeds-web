﻿namespace AssaAbloy.ServiceProvider.Models
{
    public class GetInvitationCodeResponse
    {
        public string EndpointId { get; set; }

        public string InvitationCode { get; set; }
    }
}
