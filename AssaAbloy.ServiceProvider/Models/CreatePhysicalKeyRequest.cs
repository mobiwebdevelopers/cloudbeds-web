﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class CreatePhysicalKeyRequest
    {

        [JsonProperty("doorOperations")]
        public IList<DoorOperation> DoorOperations { get; set; }

        [JsonProperty("expireTime")]
        public string ExpireTime { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        //[JsonProperty("startTime")]
        //public string StartTime { get; set; }

        //[JsonProperty("guestDoor")]
        //public string guestDoor { get; set; }
        //[JsonProperty("label")]
        //public string Label { get; set; }

        //[JsonProperty("description")]
        //public string Description { get; set; }


        //[JsonProperty("serialNumbers")]
        //public IList<string> SerialNumbers { get; set; }

        [JsonIgnore]
        public bool Override { get; set; }
    }
    public class CreateKeyImageRequest
    {

        [JsonProperty("doorOperations")]
        public IList<DoorOperation> DoorOperations { get; set; }

        [JsonProperty("expireTime")]
        public string ExpireTime { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("serialNumbers")]
        public IList<string> SerialNumbers { get; set; }

    }
}
