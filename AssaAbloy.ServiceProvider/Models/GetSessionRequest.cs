﻿using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class GetSessionRequest
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
