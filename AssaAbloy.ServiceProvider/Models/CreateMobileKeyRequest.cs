﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class CreateMobileKeyRequest
    {
        [JsonProperty("endPointID")]
        public string EndpointId { get; set; }

        [JsonProperty("expireTime")]
        public string ExpireTime { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("doorOperations")]
        public IList<DoorOperation> DoorOperations { get; set; }

        [JsonIgnore]
        public bool AutoJoin { get; set; }
    }
}
