﻿using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class GetInvitationCodeRequest
    {
        [JsonProperty("endpointId")]
        public string EndpointId { get; set; }
    }
}
