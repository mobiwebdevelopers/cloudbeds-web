﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace AssaAbloy.ServiceProvider.Models
{
    public class GetCardInfoResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("cardHolder")]
        public string CardHolder { get; set; }

        [JsonProperty("created")]
        public string Created { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("expireTime")]
        public string ExpireTime { get; set; }

        [JsonProperty("cancelled")]
        public bool Cancelled { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("encryptedImage")]
        public string EncryptedImage { get; set; }
        [JsonProperty("authenticationKey")]
        public string AuthenticationKey { get; set; }

        [JsonProperty("serialNumbers")]
        public List<string> SerialNumbers { get; set; }
        [JsonProperty("doorOperations")]
        public IList<DoorOperation> DoorOperations { get; set; }
    }
}
