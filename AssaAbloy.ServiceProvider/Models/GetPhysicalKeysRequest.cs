﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class GetPhysicalKeysRequest
    {

        [JsonProperty("expireTime")]
        public string ExpireTime { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("guestDoor")]
        public string GuestDoor { get; set; }
    }
}
