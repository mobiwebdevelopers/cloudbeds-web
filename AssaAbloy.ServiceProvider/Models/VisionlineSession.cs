﻿namespace AssaAbloy.ServiceProvider.Models
{
    public class VisionlineSession
    {
        public string Id { get; set; }

        public string AccessKey { get; set; }
    }
}
