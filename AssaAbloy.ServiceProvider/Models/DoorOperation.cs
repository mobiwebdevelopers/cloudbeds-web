﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class DoorOperation
    {
        [JsonProperty("doors")]
        public IList<string> Doors { get; set; }

        [JsonProperty("operation")]
        public string Operation { get; set; }
    }
}
