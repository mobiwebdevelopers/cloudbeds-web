﻿using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider.Models
{
    public class CreateMobileKeyResponse
    {
        [JsonProperty("credentialID")]
        public int CredentialId { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("developerMessage")]
        public string DeveloperMessage { get; set; }
    }
}
