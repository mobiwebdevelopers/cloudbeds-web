﻿namespace AssaAbloy.ServiceProvider.Models
{
    public class GetEndpointStatusResponse
    {
        public string EndpointStatus { get; set; }
        public string bleCapability { get; set; }
        public string hceCapability { get; set; }
        public string nfcCapability { get; set; }
        public string deviceManufacturer { get; set; }
        public string applicationVersion { get; set; }
        public string deviceModel { get; set; }
        public string seosAppletVersion { get; set; }
        public string osVersion { get; set; }
        public string seosTsmEndpointId { get; set; }

        /*
            "bleCapability": null,
            "hceCapability": null,
            "nfcCapability": null,
            "deviceManufacturer": null,
            "applicationVersion": null,
            "deviceModel": null,
            "seosAppletVersion": null,
            "osVersion": null,
            "seosTsmEndpointId": null
            */
    }
}
