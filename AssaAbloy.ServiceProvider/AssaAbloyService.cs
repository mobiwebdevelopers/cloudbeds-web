﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider.Models;
using Newtonsoft.Json;

namespace AssaAbloy.ServiceProvider
{
    public class AssaAbloyService : IAssaAbloyService
    {
        private readonly string _baseUrl;

        private readonly AuthenticationHeaderValue _authHeader;

        public AssaAbloyService(string baseUrl, string tenantUserName, string tenantPassword)
        {
            _baseUrl = baseUrl;
            var authBytes = Encoding.ASCII.GetBytes($"{tenantUserName}:{tenantPassword}");
            _authHeader = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authBytes));
        }

        public async Task<IEnumerable<VisiOnlineInstance>> GetVisiOnlineInstances()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync($"{_baseUrl}/system");
                var responseString = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<IList<VisiOnlineInstance>>(responseString);
                return result;
            }
        }

        public async Task<GetInvitationCodeResponse> GetInvitationCode(string endpointId)
        {
            using (var client = GetHttpClient())
            {
                var content = GetHttpContent(new GetInvitationCodeRequest
                {
                    EndpointId = endpointId
                });

                var response = await client.PostAsync($"{_baseUrl}/endpoint/invitation", content);
                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<GetInvitationCodeResponse>(responseString);
                return result;
            }
        }

        public async Task<GetEndpointStatusResponse> GetEndpointStatus(string endpointId)
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync($"{_baseUrl}/endpoint/{endpointId}");
                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<GetEndpointStatusResponse>(responseString);

                return result;
            }
        }

        private HttpClient GetHttpClient()
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

            var client = new HttpClient(httpClientHandler);
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/vnd.assaabloy-com.credential-2.4+json"));
            client.DefaultRequestHeaders.Authorization = _authHeader;

            return client;
        }

        private HttpContent GetHttpContent(object request)
        {
            return new StringContent(JsonConvert.SerializeObject(request, Formatting.Indented), Encoding.UTF8, "application/vnd.assaabloy-com.credential-2.4+json"); ;
        }
    }
}
