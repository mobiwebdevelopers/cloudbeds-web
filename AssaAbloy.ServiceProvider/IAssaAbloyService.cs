﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider.Models;

namespace AssaAbloy.ServiceProvider
{
    public interface IAssaAbloyService
    {
        Task<IEnumerable<VisiOnlineInstance>> GetVisiOnlineInstances();

        Task<GetInvitationCodeResponse> GetInvitationCode(string endpointId);

        Task<GetEndpointStatusResponse> GetEndpointStatus(string endpointId);
    }
}
