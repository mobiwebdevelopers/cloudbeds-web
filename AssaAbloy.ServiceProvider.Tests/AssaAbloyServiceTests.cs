using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssaAbloy.ServiceProvider.Tests
{
    [TestClass]
    public class AssaAbloyServiceTests
    {
        private readonly IAssaAbloyService _assaAbloyService;

        public AssaAbloyServiceTests()
        {
            _assaAbloyService = new AssaAbloyService("https://onboarding.credential-services.api.assaabloy.com",
                "trueomni-demo-tenant", "7XVdMBwKYH9dMAdzQtDrOYGiw1vckmYZ");
        }

        [TestMethod]
        public async Task GetVisiOnlineInstancesShouldReturnNotEmpty()
        {
            var result = await _assaAbloyService.GetVisiOnlineInstances();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());
        }

        [TestMethod]
        public async Task GetInvitationCodeShouldReturnNotEmpty()
        {
            var endpoint = "test12345";
            var result = await _assaAbloyService.GetInvitationCode(endpoint);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.EndpointId, endpoint);
            Assert.IsFalse(string.IsNullOrWhiteSpace(result.InvitationCode));
        }
    }
}
