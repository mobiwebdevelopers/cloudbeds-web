using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssaAbloy.ServiceProvider.Tests
{
    [TestClass]
    public class VisionlineServiceTests
    {
        private readonly IVisionlineService _visionlineService;

        public VisionlineServiceTests()
        {
            _visionlineService = new VisionlineService("https://70.162.30.144", "sym", "sym");
        }

        [TestMethod]
        public async Task OpenSessionShouldReturnNotEmpty()
        {
            var result = await _visionlineService.OpenSession();

            Assert.IsNotNull(result);
            Assert.IsFalse(string.IsNullOrWhiteSpace(result.AccessKey));
        }

        [TestMethod]
        public async Task CreateMobileKeyReturnNotEmpty()
        {
            var session = await _visionlineService.OpenSession();
            Assert.IsNotNull(session);

            var result = await _visionlineService.CreateMobileKey(session, new CreateMobileKeyRequest
            {
                Format = "rfid48",
                Label = "%ROOMRANGE%:%UUID%:%CARDNUM%",
                Description = "Hotel California",
                ExpireTime = DateTime.Now.AddDays(1).ToString("yyyyMMddTHHmm"),
                EndpointId = "3235be38-ab3a-44a0-80f0-4ee538e6b6d5",
                DoorOperations = new List<DoorOperation>
                {
                    new DoorOperation
                    {
                        Operation = "guest",
                        Doors = new[] { "102" }
                    }
                }
            });

            Assert.IsNotNull(result);
            Assert.AreNotEqual(result.CredentialId, 0);
        }

        [TestMethod]
        public async Task CloseSessionShouldReturnTrue()
        {
            var session = await _visionlineService.OpenSession();
            Assert.IsNotNull(session);

            var result = await _visionlineService.CloseSession(session);
            Assert.IsTrue(result);
        }
    }
}
