﻿using CloudBaseWeb.Api.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CloudBaseWeb.Api.Infrastructure.Configurations
{
    internal class UserEndpointConfiguration : IEntityTypeConfiguration<UserEndpoint>
    {
        public void Configure(EntityTypeBuilder<UserEndpoint> builder)
        {
        }
    }
}