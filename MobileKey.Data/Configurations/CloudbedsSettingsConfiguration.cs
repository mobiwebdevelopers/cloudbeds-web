﻿using CloudBaseWeb.Api.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CloudBaseWeb.Api.Infrastructure.Configurations
{
    internal class CloudbedsSettingsConfiguration : IEntityTypeConfiguration<CloudbedsSettings>
    {
        public void Configure(EntityTypeBuilder<CloudbedsSettings> builder)
        {
        }
    }
}