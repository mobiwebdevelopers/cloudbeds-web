﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CloudBaseWeb.Api.Entities;
using AssaAbloy.ServiceProvider.Models;
namespace Cloudbeds.Data
{
    public interface IMobileKeyService
    {
        Task<IList<UserEndpoint>> GetUserEndpoints(int userApiId);

        Task<UserEndpoint> SaveUserEndpoint(int userApiId, string reservationId);

        Task<UserEndpoint> IssueMobileKey(int userApiId, string reservationId, string endpoint, DateTime expireTime, string[] doors);
        Task<GetCardInfoResponse> IssuePhysicalKey(string reservationId, string serialNumber, DateTime expireTime, string[] doors);
        Task<GetCardInfoResponse> IssueKeyImage(string reservationId, string[] serialNumber, DateTime expireTime, string[] doors, bool overwrite = true);
        Task<GetCardInfoResponse> CancelPhysicalKey(string reservationId, string serialNumber, DateTime startTime, DateTime expireTime, string[] doors);
        //Task<UserEndpoint> GetCards(string cardId);
        Task<GetCardInfoResponse> GetCardInfo();
    }
}
