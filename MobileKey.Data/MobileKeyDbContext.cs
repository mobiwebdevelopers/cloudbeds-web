﻿using System;
using System.Data;
using System.Threading.Tasks;
using CloudBaseWeb.Api.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Cloudbeds.Data
{
    public class MobileKeyDbContext : DbContext
    {
        private IDbContextTransaction _currentTransaction;
        private readonly string _connectionString;

        public MobileKeyDbContext(DbContextOptions<MobileKeyDbContext> options) :
            base(options)
        {
        }

        public MobileKeyDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!string.IsNullOrEmpty(_connectionString))
                optionsBuilder.UseSqlServer(_connectionString);
        }

        public async Task BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (_currentTransaction != null)
                return;

            _currentTransaction = await Database.BeginTransactionAsync(isolationLevel);
        }

        public async Task CloseTransaction()
            => await CloseTransaction(exception: null);

        public async Task CloseTransaction(Exception exception)
        {
            try
            {
                if (_currentTransaction != null && exception != null)
                {
                    _currentTransaction.Rollback();
                    return;
                }

                await SaveChangesAsync();

                _currentTransaction?.Commit();
            }
            catch
            {
                _currentTransaction?.Rollback();
                throw;
            }
            finally
            {
                _currentTransaction?.Dispose();
                _currentTransaction = null;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SharedKey>().ToTable("SharedKey");
            modelBuilder.Entity<MewsCredentials>().ToTable("MewsCredentials");
            modelBuilder.Entity<OhipCredentials>().ToTable("OhipCredentials");
            modelBuilder.Entity<Mews_DailyRate>().ToTable("MEWS_DailyRate");
            modelBuilder.Entity<Zenoti_LoginUser>().ToTable("Zenoti_LoginUser");
            modelBuilder.Entity<Zenoti_Token>().ToTable("Zenoti_Token");
            modelBuilder.Entity<Zenoti_Guest>().ToTable("Zenoti_Guest");
            modelBuilder.Entity<Zenoti_Class>().ToTable("Zenoti_Classes");
            modelBuilder.Entity<Zenoti_Category>().ToTable("Zenoti_Categories");
            modelBuilder.Entity<Cascada_User>().ToTable("Cascada_User");

            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}
