﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssaAbloy.ServiceProvider;
using AssaAbloy.ServiceProvider.Models;
using CloudBaseWeb.Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cloudbeds.Data
{
    public class MobileKeyService : IMobileKeyService
    {
        private readonly MobileKeyDbContext _dbContext;
        private readonly IAssaAbloyService _assaAbloyService;
        private readonly IVisionlineService _visionlineService;

        public MobileKeyService(MobileKeyDbContext dbContext, IAssaAbloyService assaAbloyService, IVisionlineService visionlineService)
        {
            _dbContext = dbContext;
            _assaAbloyService = assaAbloyService;
            _visionlineService = visionlineService;
        }

        public async Task<IList<UserEndpoint>> GetUserEndpoints(int userApiId)
        {
            if (userApiId < 0)
            {
                throw new ArgumentException("UserApiId parameter should be greater than zero.", nameof(userApiId));
            }

            var userEndpoints = await _dbContext.Set<UserEndpoint>()
                .Where(m => m.UserApiId == userApiId)
                .Where(m => m.IsActive)
                .ToListAsync();

            if (userEndpoints != null)
            {
                foreach (var userEndpoint in userEndpoints)
                {
                    var assaAbloyEndpointStatus = await _assaAbloyService.GetEndpointStatus(userEndpoint.EndpointId);
                    if (assaAbloyEndpointStatus.EndpointStatus != null)
                    {
                        userEndpoint.Status = assaAbloyEndpointStatus.EndpointStatus;

                    }
                    else
                    {
                        userEndpoint.IsActive = false;
                    }
                    _dbContext.Update(userEndpoint);
                }
            }

            await _dbContext.SaveChangesAsync();
            return userEndpoints;
        }

        // TODO: Implement transaction.
        public async Task<UserEndpoint> SaveUserEndpoint(int userApiId, string reservationId)
        {
            if (userApiId < 0)
            {
                throw new ArgumentException("UserApiId parameter should be greater than zero.", nameof(userApiId));
            }

            if (string.IsNullOrWhiteSpace(reservationId))
            {
                throw new ArgumentException("ReservationId parameter should not be null or empty.", nameof(reservationId));
            }

            var endpointId = Guid.NewGuid().ToString();
            var assaAbloyInvitationCode = await _assaAbloyService.GetInvitationCode(endpointId);
            if (assaAbloyInvitationCode == null)
            {
                throw new ArgumentException($"Invitation code could not be retrieved for endpoint: '{endpointId}'");
            }

            var assaAbloyEndpointStatus = await _assaAbloyService.GetEndpointStatus(endpointId);
            var userEndpoint = new UserEndpoint
            {
                UserApiId = userApiId,
                EndpointId = endpointId,
                ReservationId = reservationId,
                InvitationCode = assaAbloyInvitationCode.InvitationCode,
                Status = assaAbloyEndpointStatus.EndpointStatus,

                IsActive = true,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            _dbContext.Add(userEndpoint);
            await _dbContext.SaveChangesAsync();

            return userEndpoint;
        }

        // TODO: Implement transaction.
        public async Task<UserEndpoint> IssueMobileKey(int userApiId, string reservationId, string endpoint, DateTime expireTime, string[] doors)
        {
            if (userApiId < 0)
            {
                throw new ArgumentException("UserApiId parameter should be greater than zero.", nameof(userApiId));
            }

            if (string.IsNullOrWhiteSpace(reservationId))
            {
                throw new ArgumentException("ReservationId parameter should not be null or empty.", nameof(reservationId));
            }

            if (string.IsNullOrWhiteSpace(endpoint))
            {
                throw new ArgumentException("Endpoint parameter should not be null or empty.", nameof(endpoint));
            }

            UserEndpoint userEndpoint = null;
            try
            {
                userEndpoint = await _dbContext.Set<UserEndpoint>()
                    .Where(m => m.UserApiId == userApiId)
                    .Where(m => m.ReservationId == reservationId)
                    .Where(m => m.EndpointId == endpoint)
                    .Where(m => m.CredentialId == 0)
                    .Where(m => m.IsActive)
                    .FirstOrDefaultAsync();

            }
            catch (Exception e)
            {
            }
            if (userEndpoint == null)
            {
                throw new ArgumentException($"There is no UserEndpoint with the credential for the given parameters userId: '{userApiId}' reservationId: '{reservationId}' endpoint: '{endpoint}'", nameof(userApiId));
            }

            var hasIssued = await _dbContext.Set<UserEndpoint>()
                                         .Where(m => m.ReservationId == reservationId)
                                         .Where(m => m.CredentialId > 0)
                                         .Where(m => m.IsActive)
                                         .AnyAsync();

            var visionlineSession = await _visionlineService.OpenSession();
            if (visionlineSession == null
                || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
                || string.IsNullOrWhiteSpace(visionlineSession.Id))
            {
                throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
            }

            var issueKeyResponse = await _visionlineService.CreateMobileKey(visionlineSession, new CreateMobileKeyRequest
            {
                EndpointId = userEndpoint.EndpointId,
                Format = "rfid48",
                ExpireTime = expireTime.ToString("yyyyMMddTHHmm"),
                DoorOperations = new[]
                {
                    new DoorOperation
                    {
                        Doors = doors,
                        Operation = "guest"
                    }
                },
                Description = $"%UUID%-{reservationId}",
                Label = "Room#%ROOMRANGE%",
                AutoJoin = hasIssued
            });

            //await _visionlineService.CloseSession(visionlineSession);
            if (issueKeyResponse == null || issueKeyResponse.CredentialId <= 0)
            {
                throw new ArgumentException($"Key could not be issued, please check Assa Abloy service. Message: '{issueKeyResponse.Message}'. DeveloperMessage: '{issueKeyResponse.DeveloperMessage}'");
            }

            userEndpoint.CredentialId = issueKeyResponse.CredentialId;
            var assaAbloyEndpointStatus = await _assaAbloyService.GetEndpointStatus(userEndpoint.EndpointId);
            if (assaAbloyEndpointStatus == null)
            {
                throw new ArgumentException($"Endpoint status could not be retrieved, endpoint: '{userEndpoint.EndpointId}'", nameof(assaAbloyEndpointStatus));
            }

            userEndpoint.Status = assaAbloyEndpointStatus.EndpointStatus;
            userEndpoint.UpdatedAt = DateTime.UtcNow;

            _dbContext.Update(userEndpoint);
            await _dbContext.SaveChangesAsync();

            return userEndpoint;
        }

        public async Task<GetCardInfoResponse> IssuePhysicalKey(string reservationId, string serialNumber, DateTime expireTime, string[] doors)
        {

            if (string.IsNullOrWhiteSpace(reservationId))
            {
                throw new ArgumentException("ReservationId parameter should not be null or empty.", nameof(reservationId));
            }

            //if (string.IsNullOrWhiteSpace(serialNumber))
            //{
            //    throw new ArgumentException("Serial Number parameter should not be null or empty.", nameof(serialNumber));
            //}


            var visionlineSession = await _visionlineService.OpenSession();
            if (visionlineSession == null
                || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
                || string.IsNullOrWhiteSpace(visionlineSession.Id))
            {
                throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
            }
            var issueKeyResponse = await _visionlineService.CreatePhysicalKey(visionlineSession, new CreatePhysicalKeyRequest
            {
                //SerialNumbers = new[] { serialNumber },
                Format = "rfid48",
                ExpireTime = expireTime.ToString("yyyyMMddTHHmm"),
                DoorOperations = new[]
                {
                    new DoorOperation
                    {
                        Doors = doors,
                        Operation = "guest"
                    }
                }
            });


            return issueKeyResponse;
        }
        public async Task<GetCardInfoResponse> IssueKeyImage(string reservationId, string[] serialNumber, DateTime expireTime, string[] doors, bool overwrite = true)
        {

            if (string.IsNullOrWhiteSpace(reservationId))
            {
                throw new ArgumentException("ReservationId parameter should not be null or empty.", nameof(reservationId));
            }

            var visionlineSession = await _visionlineService.OpenSession();
            if (visionlineSession == null
                || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
                || string.IsNullOrWhiteSpace(visionlineSession.Id))
            {
                throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
            }
            
            var issueKeyResponse = await _visionlineService.CreateKeyImage(visionlineSession, new CreateKeyImageRequest
            {
                SerialNumbers = serialNumber,
                Format = "rfid48",
                ExpireTime = expireTime.ToString("yyyyMMddTHHmm"),
                DoorOperations = new[]
                {
                    new DoorOperation
                    {
                        Doors = doors,
                        Operation = "guest"
                    }
                }
            }, overwrite);


            return issueKeyResponse;
        }

        public async Task<GetCardInfoResponse> CancelPhysicalKey(string reservationId, string serialNumber, DateTime startTime, DateTime expireTime, string[] doors)
        {

            if (string.IsNullOrWhiteSpace(reservationId))
            {
                throw new ArgumentException("ReservationId parameter should not be null or empty.", nameof(reservationId));
            }

            //if (string.IsNullOrWhiteSpace(serialNumber))
            //{
            //    throw new ArgumentException("Serial Number parameter should not be null or empty.", nameof(serialNumber));
            //}


            var visionlineSession = await _visionlineService.OpenSession();
            if (visionlineSession == null
                || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
                || string.IsNullOrWhiteSpace(visionlineSession.Id))
            {
                throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
            }
            var issueKeyResponse = await _visionlineService.CancelPhysicalKey(visionlineSession, new GetPhysicalKeysRequest
            {
                StartTime = startTime.ToString("yyyyMMddTHHmm"),
                ExpireTime = expireTime.ToString("yyyyMMddTHHmm"),
                GuestDoor = doors[0]
            });


            return issueKeyResponse;
        }

        //public async Task<UserEndpoint> GetCards(string encoder="")
        //{
        //    var visionlineSession = await _visionlineService.OpenSession();
        //    if (visionlineSession == null
        //        || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
        //        || string.IsNullOrWhiteSpace(visionlineSession.Id))
        //    {
        //        throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
        //    }

        //    var getCardsResponse = await _visionlineService.CreateMobileKey(visionlineSession, new CreateMobileKeyRequest
        //    {
        //        EndpointId = userEndpoint.EndpointId,
        //        Format = "rfid48",
        //        ExpireTime = expireTime.ToString("yyyyMMddTHHmm"),
        //        DoorOperations = new[]
        //        {
        //            new DoorOperation
        //            {
        //                Doors = doors,
        //                Operation = "guest"
        //            }
        //        },
        //        Description = $"%UUID%-{reservationId}",
        //        Label = "Room#%ROOMRANGE%",
        //        AutoJoin = hasIssued
        //    });

        //    await _visionlineService.CloseSession(visionlineSession);
        //    if (issueKeyResponse == null || issueKeyResponse.CredentialId <= 0)
        //    {
        //        throw new ArgumentException($"Key could not be issued, please check Assa Abloy service. Message: '{issueKeyResponse.Message}'. DeveloperMessage: '{issueKeyResponse.DeveloperMessage}'");
        //    }

        //    userEndpoint.CredentialId = issueKeyResponse.CredentialId;
        //    var assaAbloyEndpointStatus = await _assaAbloyService.GetEndpointStatus(userEndpoint.EndpointId);
        //    if (assaAbloyEndpointStatus == null)
        //    {
        //        throw new ArgumentException($"Endpoint status could not be retrieved, endpoint: '{userEndpoint.EndpointId}'", nameof(assaAbloyEndpointStatus));
        //    }

        //    userEndpoint.Status = assaAbloyEndpointStatus.EndpointStatus;
        //    userEndpoint.UpdatedAt = DateTime.UtcNow;

        //    _dbContext.Update(userEndpoint);
        //    await _dbContext.SaveChangesAsync();

        //    return userEndpoint;
        //}
        public async Task<GetCardInfoResponse> GetCardInfo()
        {

            var visionlineSession = await _visionlineService.OpenSession();
            if (visionlineSession == null
                || string.IsNullOrWhiteSpace(visionlineSession.AccessKey)
                || string.IsNullOrWhiteSpace(visionlineSession.Id))
            {
                throw new ArgumentException("Visionline session could not be initialized.", nameof(visionlineSession));
            }

            return await _visionlineService.GetCardInfo(visionlineSession);
        }

    }
}
