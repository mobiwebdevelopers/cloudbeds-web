﻿using System.Collections.Generic;

namespace CloudBaseWeb.Api.Entities
{
    public class UserEndpoint : Entity
    {
        public int UserApiId { get; set; }

        public string EndpointId { get; set; }

        public string ReservationId { get; set; }

        public int CredentialId { get; set; }

        public string InvitationCode { get; set; }

        public string Status { get; set; }
    }
}
