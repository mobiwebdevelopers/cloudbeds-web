﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseWeb.Api.Entities
{
    public class Mews_DailyRate : Entity
    {
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }

    }
}
