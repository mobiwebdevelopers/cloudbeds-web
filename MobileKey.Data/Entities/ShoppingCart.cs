﻿using System;

namespace CloudBaseWeb.Api.Entities
{
    public class ShoppingCart: Entity
    {
        public Guid GUID { get; set; }
        public int ReservationId { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
