﻿
using System;

namespace CloudBaseWeb.Api.Entities
{
    public class Trybe_Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }
}
