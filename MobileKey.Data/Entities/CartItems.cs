﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseWeb.Api.Entities
{
    public class CartItems : Entity
    {
        public int ShoppingCartId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal SubTotal { get; set; }
    }
}
