﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudBaseWeb.Api.Entities
{
    public class Zenoti_Token
    {
        public int Id { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("access_token_expiry")]
        public DateTime AccessTokenExpiry { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("refresh_token_expiry")]
        public DateTime RefreshTokenExpiry { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("token_id")]
        public string TokenId { get; set; }

        [JsonProperty("app_id")]
        public string AppId { get; set; }

        [JsonProperty("user_type")]
        public string UserType { get; set; }
        public int UserId { get; set; }
        public bool Active { get; set; }

    }
}
