﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseWeb.Api.Entities
{
    public class SharedKey : Entity
    {
        //public int Id { get; set; }
        public string Email { get; set; }
        public string ReservationId { get; set; }
        public bool Claimed { get; set; }
        //public DateTime CreatedAt { get; set; }
    }
}
