﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudBaseWeb.Api.Entities
{
    public class Trybe_LoginUser
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string GrantType { get; set; }
        public string AppSecret { get; set; }
        public string AppId { get; set; }
        public string DeviceId { get; set; }

    }
}
