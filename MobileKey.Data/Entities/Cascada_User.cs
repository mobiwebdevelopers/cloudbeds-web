﻿
using System;

namespace CloudBaseWeb.Api.Entities
{
    public class Cascada_User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public string MewsId { get; set; }
        public string ZenotiId { get; set; }
    }

}
