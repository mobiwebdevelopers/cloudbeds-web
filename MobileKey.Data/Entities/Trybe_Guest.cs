﻿
using System;

namespace CloudBaseWeb.Api.Entities
{
    public class Trybe_Guest_Entity
    {
        public int Id { get; set; }
        public string GuestId { get; set; }
        public GuestStatus Status { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
    }

}
