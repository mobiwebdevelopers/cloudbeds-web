﻿
using System;

namespace CloudBaseWeb.Api.Entities
{
    public class Zenoti_Class
    {
        public int Id { get; set; }
        public string Class_Name { get; set; }
        public string Code { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
    }
}
