﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseWeb.Api.Entities
{
    public class OhipCredentials : Entity
    {
        public string Code { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AuthUser { get; set; }
        public string AuthPwd { get; set; }
        public string AppKey { get; set; }

    }
}
