﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudBaseWeb.Api.Entities
{
    public class MewsCredentials : Entity
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public string ClientToken { get; set; }
        public string AccessToken { get; set; }
        public string Client { get; set; }

    }
}
