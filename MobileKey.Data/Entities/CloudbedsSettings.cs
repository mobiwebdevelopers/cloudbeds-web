﻿namespace CloudBaseWeb.Api.Entities
{
    public class CloudbedsSettings : Entity
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
