﻿
using System;

namespace CloudBaseWeb.Api.Entities
{
    public class Zenoti_Guest
    {
        public int Id { get; set; }
        public string GuestId { get; set; }
        public GuestStatus Status { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
    }

    public enum GuestStatus
    {
        Available = 1,
        NotAvailable = 2
    }
}
