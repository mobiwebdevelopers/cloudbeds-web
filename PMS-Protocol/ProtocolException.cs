﻿using System;

namespace PMS
{
	public class ProtocolException : Exception
	{
		#region Constructor

		internal ProtocolException(string message) : base(message)
		{
		}

		#endregion Constructor
	}
}