﻿using PMS.Properties;

namespace PMS
{
	public sealed class ChecksumException : ProtocolException
	{
		#region Constructor

		internal ChecksumException() : base(Messages.ChecksumError)
		{
		}

		#endregion Constructor
	}
}