﻿using PMS.Properties;

namespace PMS
{
	public sealed class BusyDeviceException : ProtocolException
	{
		#region Constructor

		internal BusyDeviceException() : base(Messages.BusyDevice)
		{
		}

		#endregion Constructor
	}
}